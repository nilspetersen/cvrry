%include "cvrry_head.i"

/* NUMPY */
%include "numpy.i"
%init %{
import_array();
%}

%apply (int DIM1, int*IN_ARRAY1) {(int np_arr_in_size, int*np_arr_in)};
%apply (int DIM1, float*IN_ARRAY1) {(int np_arr_in_size, float*np_arr_in)};
%apply (int DIM1, float*ARGOUT_ARRAY1) {(int np_arr_out_size, float*np_arr_out)};
%apply (int DIM1, int*ARGOUT_ARRAY1) {(int np_arr_out_size, int*np_arr_out)};

%apply (int DIM1, float*IN_ARRAY1) {(int arr_1_size, const float*float_in_1)};
%apply (int DIM1, float*IN_ARRAY1) {(int arr_2_size, const float*float_in_2)};

%apply (int DIM1, int*IN_ARRAY1) {(int nfrags, const int*frags_indices)};
%apply (int DIM1, int*IN_ARRAY1) {(int nfrags_a, const int*frags_indices_a)};
%apply (int DIM1, int*IN_ARRAY1) {(int nfrags_b, const int*frags_indices_b)};

%apply (int DIM1, float*ARGOUT_ARRAY1) {(int nfrags_sq, float*rmsd_matrix)};
%apply (int DIM1, float*ARGOUT_ARRAY1) {(int nfrags_a_times_b, float*rmsd_matrix)};

%apply (int DIM1, int*IN_ARRAY1) {(int nbasepairs, int*basepair_info)};

%apply (int DIM1, float*ARGOUT_ARRAY1) {(int out_n_rmsds, float*out_rmsds)};
%apply (int DIM1, int*ARGOUT_ARRAY1) {(int out_n_matchlen, int*out_matchlen)};
%apply (int DIM1, int*ARGOUT_ARRAY1) {(int out_n_match_i, int*out_match_i)};
%apply (int DIM1, int*ARGOUT_ARRAY1) {(int out_n_match_j, int*out_match_j)};
%apply (int DIM1, int*ARGOUT_ARRAY1) {(int n_char_pairs, int*pair_counts)};

%apply (int DIM1, int*IN_ARRAY1) {(int in_npairs1, const int*in_pairs_i)};
%apply (int DIM1, int*IN_ARRAY1) {(int in_npairs2, const int*in_pairs_j)};

/* C WRAPPERS */

%inline %{
void
set_random_seed(unsigned int seed)
{
    srand(seed);
}

/* indexed_chainset_rmsds
 *
 * function to get rmsds fast as numpy array in python
 *
 * out: rmsd_matrix - (i * n + j) indexing
 * must be allocated before!
 */
void
indexed_chainset_rmsd_matrix_2sets(int nfrags_a_times_b, float*rmsd_matrix,
				   int nfrags_a, const int*frags_indices_a,
				   int nfrags_b, const int*frags_indices_b,
				   const struct indexed_chainset*chainset)
{
    int i, j;
    size_t ii, jj;
    for(i = 0; i < nfrags_a; ++i) {
	ii = (size_t) frags_indices_a[i];
	for(j = 0; j < nfrags_b; ++j) {
	    jj = (size_t) frags_indices_b[j];
	    rmsd_matrix[i * nfrags_b + j] = (ii == jj ? 0 : indexed_chainset_pair_rmsd(chainset,
										       ii, jj));
	}
    }
}

void
indexed_chainset_rmsd_matrix_sym(int nfrags_sq, float*rmsd_matrix,
				 int nfrags, const int*frags_indices,
				 const struct indexed_chainset*chainset)
{
    int i, j;
    for(i = 0; i < nfrags; ++i) {
	rmsd_matrix[i * nfrags + i] = 0;
	for(j = i+1; j < nfrags; ++j) {
	    rmsd_matrix[i * nfrags + j] =
		indexed_chainset_pair_rmsd(chainset,
					   (size_t) frags_indices[i],
					   (size_t) frags_indices[j]);
	    rmsd_matrix[j * nfrags + i] = rmsd_matrix[i * nfrags + j];
	}
    }
}

int
rna_coord_set_basepair_info(struct rna_coord*rna,
			    int nbasepairs, int*basepair_info)
{
    const char*this_sub="rna_coord_set_basepair_info";
    size_t memsize;
    if((size_t) nbasepairs != rna->size) {
	err_printf(this_sub,
		   "size of rna_coord does not fit size of baspair_info array\n");
	return 1;
    }
    free_if_not_null(rna->basepair_info);
    memsize = rna->size * sizeof(*rna->basepair_info);
    rna->basepair_info = E_MALLOC(memsize);
    memcpy(rna->basepair_info, basepair_info, memsize);
    return 0;
}

float
rna_alignment_simple_rmsd(const struct pair_set*pairset,
			  const struct rna_coord*source,
			  const struct rna_coord*target)
{
    int err=0;
    float rmsd;
    err = rna_coord_rmsd(NULL, &rmsd, pairset, source, target);
    if(err) {
	return -1.0;
    }
    return rmsd;
}

float
rna_alignment_simple_psi(const struct pair_set*pairset,
			 const struct rna_coord*source,
			 const struct rna_coord*target,
			 enum superpos_methods method)
{
    int err=0;
    float psi;
    err = percentage_of_structure_identity(&psi,
					   NULL,
					   NULL,
					   pairset,
					   source, 
					   target,
					   4,
					   method);
    if(err) {
	return -1.0;
    }
    return psi;
}

struct pair_set*
rna_optimized_superpos(const struct pair_set*pairset_in,
		       const struct rna_coord*source,
		       const struct rna_coord*target,
		       enum superpos_methods method)
{
    float dummy_psi;
    struct pair_set*pset_out=NULL;
    int err=0;
    err = percentage_of_structure_identity(&dummy_psi,
					   &pset_out,
					   NULL,
					   pairset_in,
					   source,
					   target,
					   4,
					   method);
    if(err) {
	return NULL;
    }
    return pset_out;
}

float
continuous_match_simple_rmsd(const struct continuous_match*match,
			     const struct rna_coord*source,
			     const struct rna_coord*target,
			     size_t fraglen)
{
    int err;
    float rmsd;
    err = rna_coord_rmsd_continuous_match(NULL, &rmsd, *match,
					  source, target, fraglen);
    if(err) {
	return -1.0;
    }
    return rmsd;
}

void
__float_matrix_get_numpy_array(int np_arr_out_size, float*np_arr_out,
			       const struct float_matrix*mat)
{
    memcpy(np_arr_out,
	   *mat->vals,
	   mat->n_rows * mat->n_cols * sizeof(*np_arr_out));
}

void
__float_array_to_numpy_array(int np_arr_out_size, float*np_arr_out,
			     const struct float_array*arr)
{
    memcpy(np_arr_out, arr->vals, arr->size * sizeof(*arr->vals));    
}

void
__int_array_to_numpy_array(int np_arr_out_size, int*np_arr_out,
			   const struct int_array*arr)
{
    memcpy(np_arr_out, arr->vals, arr->size * sizeof(*arr->vals));    
}

void
__size_t_array_to_numpy_array(int np_arr_out_size, int*np_arr_out,
			      const struct size_t_array*arr)
{
    int i;
    for(i = 0; i < np_arr_out_size; ++i) {
	np_arr_out[i] = (int) arr->vals[i];
    }
}

struct float_matrix*
__numpy_array_to_float_matrix(int np_arr_in_size,
			      float*np_arr_in,
			      int n_rows)
{
    int n_cols = np_arr_in_size / n_rows;
    return float_matrix_from_array(n_rows, n_cols, np_arr_in);
}

struct size_t_array*
numpy_array_to_size_t_array(int np_arr_in_size, int*np_arr_in)
{
    struct size_t_array*arr=NULL;
    int i;
    arr = new_size_t_array((size_t) np_arr_in_size);
    for(i = 0; i < np_arr_in_size; ++i) {
	arr->vals[i] = (size_t) np_arr_in[i];
    }
    return arr;
}

struct float_array*
numpy_array_to_float_array(int np_arr_in_size, float*np_arr_in)
{
    struct float_array*arr_out=NULL;
    arr_out = new_float_array(np_arr_in_size);
    if(!arr_out) {
	return NULL;
    }
    memcpy(arr_out->vals, np_arr_in, np_arr_in_size * sizeof(*np_arr_in));    
    return arr_out;
}

int
__testfunc_getmatches(int out_n_rmsds, float*out_rmsds,
		      int out_n_matchlen, int*out_matchlen,
		      int out_n_match_i, int*out_match_i,
		      int out_n_match_j, int*out_match_j,
		      int in_npairs1, const int*in_pairs_i,
		      int in_npairs2, const int*in_pairs_j,
		      const struct rna_coord_stringset*stringset,
		      const struct rna_coord_set*chainset,
		      size_t fraglen)
{
    int err=0;
    int i;
    struct continuous_match*matches=NULL;
    struct double_index*pairs=NULL;
    matches = E_MALLOC(in_npairs1 * sizeof(*matches));
    if(!err) {
	pairs = new_double_index(in_npairs1);
	err = !pairs;
    }
    if(!err) {
	for(i = 0; i < in_npairs1; ++i) {
	    size_t_array_set(pairs->index_a, i, (size_t) in_pairs_i[i]);
	    size_t_array_set(pairs->index_b, i, (size_t) in_pairs_j[i]);
	}
    }
    if(!err) {
	err = compute_matches(matches, pairs, stringset);
    }
    if(!err) {
	err = compute_rmsds_matches(out_rmsds, pairs, matches, chainset, fraglen);
    }
    if(!err) {
	for(i = 0; i < in_npairs1; ++i) {
	    out_matchlen[i] = (int) matches[i].len;
	    out_match_i[i] = (int) matches[i].matchpos_a;
	    out_match_j[i] = (int) matches[i].matchpos_b;
	}	
    }
    free_if_not_null(matches);
    double_index_destroy(pairs);
    return err;
}

struct match_energy_func*
make_z_score_energy_function(int arr_1_size,
			     const float*float_in_1, /* means */
			     int arr_2_size,
			     const float*float_in_2, /* sigmas */
			     float shift,
			     float min)
{
    return make_z_score_energy_func(arr_1_size,
				    float_in_1,
				    float_in_2,
				    shift,
				    min);
}

struct match_energy_func*
make_z_logistic_energy_function(int arr_1_size,
				const float*float_in_1, /* means */
				int arr_2_size,
				const float*float_in_2, /* sigmas */
				float shift,
				float slope)
{
    return make_z_logistic_energy_func(arr_1_size,
				       float_in_1,
				       float_in_2,
				       shift,
				       slope);
}

struct match_energy_func*
make_mean_logistic_energy_function(int arr_1_size,
				   const float*float_in_1, /* means */
				   float shift,
				   float slope)
{
    return make_mean_logistic_energy_func(arr_1_size,
					  float_in_1,
					  shift,
					  slope);
}

void
__population_get_states_np(int np_arr_out_size, int*np_arr_out,
			   const struct population*pop)
{
    size_t i, j, k=0;
    np_arr_out[k++] = (int) pop->n_parents;
    np_arr_out[k++] = (int) pop->individuals[0]->alpha_index->index->size;
    for(i = 0; i < pop->n_parents; ++i) {
	const struct alphabet_index *alpha_index = pop->individuals[i]->alpha_index;
	np_arr_out[k++] = (int) alpha_index->border_i;
	for(j = 0; j < alpha_index->index->size; ++j) {
	    np_arr_out[k++] = (int) alpha_index->index->index_a->vals[j];
	}
	for(j = 0; j < alpha_index->index->size; ++j) {
	    np_arr_out[k++] = (int) alpha_index->index->index_b->vals[j];    
	}
    }
}

int
__population_set_states_np(struct population*pop,
			   int np_arr_in_size, int*np_arr_in)
{
    const char*this_sub="__population_set_states_np";
    size_t i, j, k=0;
    size_t n_parents, alpha_size;

    n_parents = (size_t) np_arr_in[k++];
    alpha_size = (size_t) np_arr_in[k++];

    // assert n_parents and alpha_size
    if(n_parents != pop->n_parents) {
	err_printf(this_sub,
		   ("Number of parent individuals from numpy array (%zu) "
		    "and in population (%zu) don't match"),
		   n_parents, pop->n_parents);
	return 1;
    }
    if(alpha_size != pop->individuals[0]->alpha_index->index->size) {
	err_printf(this_sub,
		   ("Alphabet size in numpy array (%zu) "
		    "and in population (%zu) don't match"),
		   alpha_size, pop->individuals[0]->alpha_index->index->size);
	return 1;
    }

    pop->energy_up_to_date = NO;
    for(i = 0; i < pop->n_parents; ++i) {
	struct alphabet_index *alpha_index = pop->individuals[i]->alpha_index;
	alpha_index->border_i = (size_t) np_arr_in[k++];
	for(j = 0; j < alpha_size; ++j) {
	    alpha_index->index->index_a->vals[j] = (size_t) np_arr_in[k++];
	}
	for(j = 0; j < alpha_size; ++j) {
	    alpha_index->index->index_b->vals[j] = (size_t) np_arr_in[k++];    
	}
    }
    return 0;
}

void
__continuous_matches_lens_as_np_array(int np_arr_out_size,
				      int*np_arr_out,
				      const struct continuous_match*matches)
{
    int i;
    for(i = 0; i < np_arr_out_size; ++i) {
	np_arr_out[i] = (int) matches[i].len;
    }
}

void
__rna_coord_seq_collect_stats(int n_char_pairs,
			      int*pair_counts,
			      const struct rna_coord_seq*seq,
			      int with_break_chars)
{
    size_t i;
    for(i = 0; i < n_char_pairs; ++i) {
	pair_counts[i] = 0;
    }
    if(with_break_chars) {
	rna_coord_seq_pairs_count_with_break_chars(
	    pair_counts, seq);
    } else {
	rna_coord_seq_pairs_count_no_break_chars(
	    pair_counts, seq);
    }
}

float
get_match_energy(const struct match_energy_func*efunc,
		 size_t matchlen,
		 float rmsd)
{
    float energy;
    struct continuous_match match;
    match.len = matchlen;
    match.matchpos_a = 0;
    match.matchpos_b = 0;
    match_energy_get_energy(&energy, efunc, &match, rmsd);
    return energy;
}

struct continuous_match*
rna_coord_seq_lcs(const struct rna_coord_seq*seq_1,
		  const struct rna_coord_seq*seq_2)
{
    struct continuous_match*match=new_continuous_match();
    int err = rna_coord_seq_lcs_match(match, seq_1, seq_2);
    if(err) {
	continuous_match_destroy(match);
	return NULL;
    }
    return match;
}

int
count_char_pairs(const struct rna_coord_seq*seq,
		 int with_break_chars)
{
    return rna_coord_seq_nchar_pairs(seq, with_break_chars);
}

%}

/* PYTHON CODE */
%pythoncode %{
def res_id_2_str(res_id):
    return "{}{}".format(res_id.resnum, res_id.icode.replace('?', ''))

def rna_coord_get_res_ids(rna_coord):
    return [res_id_2_str(rna_coord_get_res_id(rna_coord, i))
            for i in range(rna_coord.size)]

def rna_coord_get_full_sequence(rna_coord):
    return ["{}".format(char_matrix_access_line(rna_coord.sequence, i))
	    for i in range(rna_coord.size)]

def fn_rna_frag_coord(*args):
    return "_".join(str(arg) for arg in args) + ".cvrry"

def float_matrix_get_numpy_matrix(mat):
    m = mat.n_rows
    n = mat.n_cols
    return __float_matrix_get_numpy_array(m*n, mat).reshape((m,n))

def numpy_matrix_to_float_matrix(mat):
    m, n = mat.shape
    return __numpy_array_to_float_matrix(mat.flatten(), m)

def float_array_2_numpy_array(arr):
    return __float_array_to_numpy_array(arr.size, arr)

def int_array_2_numpy_array(arr):
    return __int_array_to_numpy_array(arr.size, arr)

def size_t_array_2_numpy_array(arr):
    return __size_t_array_to_numpy_array(arr.size, arr)

def testfunc_getmatches(pairs_tuple, stringset, chainset, fraglen):
    npairs = len(pairs_tuple)
    pairs_i = [i for i, _ in pairs_tuple]
    pairs_j = [j for _, j in pairs_tuple]
    err, rmsds, match_len, match_i, match_j = __testfunc_getmatches(
	npairs, npairs, npairs, npairs, pairs_i, pairs_j, stringset, chainset, fraglen
    )
    if err:
        raise RuntimeError("failed to get matches")
    return rmsds, list(zip(match_len, match_i, match_j))

def cpy_pairs_list_2_double_index(double_index, pair_list):
    if len(pair_list) != double_index.size:
        raise RuntimeError(f"length of pair list ({len(pair_list)}) and "
			   "double_index ({double_index.size}) don't match")
    for ii, (i, j) in enumerate(pair_list):
        size_t_array_set(double_index.index_a, ii, int(i))
        size_t_array_set(double_index.index_b, ii, int(j))

def pairs_list_2_double_index(pair_list):
    double_index = cv_new_double_index(len(pair_list))
    cpy_pairs_list_2_double_index(double_index, pair_list)
    return double_index

def population_get_states_np(pop):
    arr_size = 2 + pop.n_parents * (1 + 2 * population_get_alphabet_size(pop));
    return __population_get_states_np(arr_size, pop)

def population_set_states_np(pop, np_arr_in):
    if __population_set_states_np(pop, np_arr_in):
        raise RuntimeError("Failed to set population state from numpy array")

def indivual_get_matchlengths(ind):
    return __continuous_matches_lens_as_np_array(
	ind.size, ind.matches)

def rna_coord_seq_stats(coord_seq, with_breaks):
    with_breaks_int = 1 if with_breaks else 0
    size = count_char_pairs(coord_seq, with_breaks_int)
    return __rna_coord_seq_collect_stats(
	size, coord_seq, with_breaks_int)

def population_get_scores_best_individual(pop):
    population_assert_up2date_probabilities(pop)
    best_i = population_best_individual_index(pop)
    best_individual = population_get_individual(pop, best_i)
    matchlengths = indivual_get_matchlengths(best_individual)
    rmsds = float_array_2_numpy_array(best_individual.match_rmsds)
    energies = float_array_2_numpy_array(best_individual.energies)
    return matchlengths, rmsds, energies

def pylist_to_rna_atom_types_array(atom_types_list):
    atom_types_dict = {
        "P": P,
        "O5": O5,
        "C5": C5,
        "C4": C4,
        "O4": O4,
        "C3": C3,
        "O3": O3,
        "C2": C2,
        "C1": C1
    }
    atom_types_arr = cv_new_rna_atom_types_array(len(atom_types_list))
    for i, atom_type in enumerate(atom_types_list):
        rna_atom_types_array_set(
            atom_types_arr, i, atom_types_dict[atom_type]
        )
    return atom_types_arr

def load_indexed_chainset(fn_chainlist,
                          chaindir,
                          fragment_pattern,
                          atom_types):
    pattern_size_t = numpy_array_to_size_t_array(fragment_pattern)
    atom_types_array = pylist_to_rna_atom_types_array(atom_types)
    chainset = indexed_chainset_load_with_pattern(
        fn_chainlist, chaindir, pattern_size_t, atom_types_array
    )
    rna_atom_types_array_destroy(atom_types_array)
    size_t_array_destroy(pattern_size_t)
    return chainset

%}


%include "cvrry_body.i"
