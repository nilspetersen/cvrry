#!/usr/bin/perl

use lib "perl/swig";
use cvrry;

my $fn_rna = ("/work/petersen/no_backup/projects/cvrry/".
	      "structural_alphabet/rmsd_histograms/nrlist3.74/".
	      "chains/4Z4D_B_1_C2_F0.rnachain");
my $rna = cvrry::rna_coord_read_binary_file($fn_rna);

cvrry::rna_coord_dump($rna);

cvrry::rna_coord_destroy($rna);
