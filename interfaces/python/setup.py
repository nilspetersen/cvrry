import os
import numpy as np
from distutils.core import setup, Extension

src_dir = "c_src"

src_files = [os.path.join(src_dir, fn)
             for fn in os.listdir(src_dir)
             if fn[-2:] == ".c"]

extension_mod = Extension("_cvrry",
                          ["swig/cvrry_wrap_python.c"] + src_files,
                          extra_compile_args=['-fopenmp'],
                          extra_link_args=['-lgomp'])
# extra_compile_args=['-g']

setup(name="cvrry",
      version='0.1',
      author='Nils Petersen',
      author_email='petersen@zbh.uni-hamburg.de',
      include_dirs=[src_dir, np.get_include()],
      ext_modules=[extension_mod],
      py_modules=['cvrry'],
      package_dir={'': 'swig'})
