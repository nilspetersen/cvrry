#!/usr/bin/env bash

# command line arguments: $1 is the chainlistfile,
# $2 the directory with continuous chains 

../bin/chainpiece_lengths.x $1 $2 | awk 'BEGIN{sum=0} {sum += ($6-6)<0?0:$6-6 } END {print sum}'
