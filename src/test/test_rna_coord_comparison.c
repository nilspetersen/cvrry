#include "rna_frag_coord.h"
#include "rna_coord.h"
#include "mprintf.h"

int
main(int argc, char *argv[])
{
    int err=0;

    const char*fn_tmp_bin_coord_1="../testfiles/test_temp_out/rna_1.rnachain";
    const char*fn_cif_testfile_1="../testfiles/cif/1TRA.cif";
    const char*chainid_1="A";
    int model_i_1=1;
    struct rna_frag_coord*frag_coord_1=NULL;
    struct rna_coord*rna_1_reloaded=NULL;

    if(!err) {
	frag_coord_1 = read_n_compute_rna_frag_coord(fn_cif_testfile_1,
						     chainid_1, model_i_1);
	if(!frag_coord_1) {
	    err = 1;
	}
    }
    
    if(!err) {
	if(!rna_coord_same(frag_coord_1->rna_backbone,
			   frag_coord_1->rna_backbone)) {
	    err = 1;
	}
    }

    if(!err) {
	err = rna_coord_write_binary_file(fn_tmp_bin_coord_1,
					  frag_coord_1->rna_backbone);
    }

    if(!err) {
	rna_1_reloaded = rna_coord_read_binary_file(fn_tmp_bin_coord_1);
	if(!rna_1_reloaded) {
	    err = 1;
	}
    }

    if(!err) {
	if(!rna_coord_same(rna_1_reloaded, rna_1_reloaded)) {
	    err = 1;
	}
    }

    if(!err) {
	if(!rna_coord_same(frag_coord_1->rna_backbone, rna_1_reloaded)) {
	    err = 1;
	}
    }

    rna_frag_coord_destroy(frag_coord_1);
    rna_coord_destroy(rna_1_reloaded);

    if(err) {
	mprintf("Test failed\n");
    } else {
	mprintf("Test successful\n");
    }
    return err;
}
