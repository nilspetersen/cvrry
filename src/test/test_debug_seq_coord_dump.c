#include "rna_alphabet.h"
#include "rna_seq_coord.h"

int
main(int argc, char*argv[])
{
    const char*this_sub="main";
    struct rna_seq_coord*seq_coord=NULL;
    if(argc < 2) {
	mprintf("usage: %s infile\n", argv[0]);
	return EXIT_FAILURE;
    }
    seq_coord = rna_seq_coord_read_binary(argv[1]);
    if(!seq_coord) {
	err_printf(this_sub, "no seq coord here sir!\n");
	return EXIT_FAILURE;
    }
    rna_seq_coord_dump(seq_coord);
    rna_seq_coord_destroy(seq_coord);
}
