#include "rna_alphabet.h"

int
main()
{
    /* const char*fn_seq = ( */
    /* 	"/work/petersen/no_backup/projects/cvrry/structural_alphabet/nrlist_clustering/" */
    /* 	"seqs/seqs_with_basepairs/4V6W_A5_1.coordseq"); */

    const char*fn_coord = (
	"/work/petersen/no_backup/projects/cvrry/structural_alphabet/"
	"nrlist_clustering/coords/4V6W_A5_1.rnacoord");

    const char*fn_alphabet = (
    	"/work/petersen/no_backup/projects/cvrry/structural_alphabet/"
    	"optimizations/nr_set_min50_max250/alpha6_z2_seed2/bestfinal.alphabet");

    const char*fn_seq_tmp = "tmp.seq";

    struct rna_coord*rna=NULL;
    struct rna_coord_seq*seq_1=NULL,*seq_2=NULL;
    struct rna_coord_alphabet*alphabet=NULL;
    int err=1;

    rna = rna_coord_read_binary_file(fn_coord);

    if(rna) {
	/* rna_coord_dump(rna); */
	alphabet = rna_coord_alphabet_read_binary_file(fn_alphabet);
    }
    if(alphabet) {
	seq_1 = rna_coord_seq_w_break_chars(rna, alphabet);
    }
    if(seq_1) {
	/* rna_coord_seq_dump(seq_1); */
	err = rna_coord_seq_write_binary_file(fn_seq_tmp, seq_1);
    }
    if(!err) {
	seq_2 = rna_coord_seq_read_binary_file(fn_seq_tmp);
    }
    if(seq_2) {
    	rna_coord_seq_dump(seq_1);
    }
    rna_coord_alphabet_destroy(alphabet);
    rna_coord_destroy(rna);
    rna_coord_seq_destroy(seq_1);
    rna_coord_seq_destroy(seq_2);
}
