#include "suffix_arr.h"


int
main(int argc, char *argv[])
{
    int seq_a[] = {0, 1, 2, 3, 4, 2, 3, 2, 3};
    int seq_b[] = {1, 4, 2, 3, 2, 5, 4};
    struct int_array seq_arr_a = {9, seq_a};
    struct int_array seq_arr_b = {7, seq_b};
    int min_letter=0;
    struct continuous_match match;
    int err=0;
    if(!err) {
	err = longest_common_substring(&match,
				       min_letter,
				       &seq_arr_a,
				       &seq_arr_b);
    }
    if(!err) {
	dump_match(&match, &seq_arr_a, &seq_arr_b);
    }
    return 0;
}
