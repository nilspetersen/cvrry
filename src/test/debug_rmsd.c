
#include "rna_coord.h"
#include "cvrry_parse_mmcif.h"
#include "fio.h"
#include "lsqf.h"
#include "mprintf.h"

int
main(int argc, char *argv[])
{
    int err=0;
    struct cvrry_cif *cif1=NULL, *cif2=NULL;
    struct rna_coord *rna1=NULL, *rna2=NULL;
    struct pair_set *pairset=NULL;

    const char fname_1[] = "../testfiles/6D9J_2.cif";
    const char fname_2[] = "../testfiles/6D9J_1.cif";

    struct transformation transform;
    float rmsd=-1.0;

    /* make dummy pairset */
    if(!err) {
	pairset = pair_set_new(1);
	err = !pairset;
    }
    if(!err) {
	pairset->indices[0][0] = 0;
	pairset->indices[0][1] = 0;
	pairset->n = 1;
    }
    /* read the mmcif files */
    if(!err) {
	err = read_mmcif(&cif1,
			 &rna1,
			 fname_1,
			 "5",
			 1);
    }
    if(!err) {
	err = read_mmcif(&cif2,
			 &rna2,
			 fname_2,
			 "5",
			 1);
    }
    /* write the mmcif files */
    if(!err) {
	err = write_mmcif(cif1, "6D9J", "before_rot_1.cif");
    }
    if(!err) {
	err = write_mmcif(cif2, "6D9J", "before_rot_2.cif");
    }
    /* get the transform and rmsd */
    if(!err) {
	int err2 = rna_coord_rmsd(&transform,
				  &rmsd,
				  pairset,
				  rna1, 
				  rna2);
	if(err2) {
	    mprintf("There was an error for the superposition\n");
	}
    }
    if(!err) {
	mprintf("RMSD=%f\n", rmsd);
    }
	
    /* do the transform -> shift and rotate rna2 !! */
    /* write the new .pdb files */


    rna_coord_destroy(rna1);
    rna_coord_destroy(rna2);
    cvrry_cif_destroy(cif1);
    cvrry_cif_destroy(cif2);
    pair_set_destroy(pairset);

    return err;
}
