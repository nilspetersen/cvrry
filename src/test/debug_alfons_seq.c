#include "rna_coord.h"
#include "rna_alphabet.h"

int
main()
{
    const char*fn_rna_a = (
    	"/work/petersen/no_backup/projects/cvrry/structural_alphabet/cifcoords/1AM0_A_1.rnacoord");
    
    /* const char*fn_rna_a = ( */
    /* 	"/work/petersen/no_backup/projects/cvrry/structural_alphabet/" */
    /* 	"train_test_sets_align_opt/minlen50_maxlen350/chains_cut/5Y7M_D_1_C0_F1.rnachain"); */
    /* const char*fn_rna_a = ( */
    /* 	"/work/petersen/no_backup/projects/cvrry/structural_alphabet/" */
    /* 	"train_test_sets_align_opt/minlen50_maxlen350/chains_cut/6QKL_N_1_C0_F2.rnachain"); */
    const char*fn_alphabet = (
	"/work/petersen/no_backup/projects/cvrry/structural_alphabet/"
	"optimizations/nr_set_min50_max250/alpha7_z2_seed1/bestfinal.alphabet");
    struct rna_coord*rna_a=NULL;
    struct rna_coord_alphabet*alphabet=NULL;
    struct rna_coord_seq*coord_seq=NULL;

    alphabet = rna_coord_alphabet_read_binary_file(fn_alphabet);
    
    rna_a = rna_coord_read_binary_file(fn_rna_a);

    /* rna_coord_dump(rna_a); */

    coord_seq = rna_coord_seq_fullseq(rna_a, alphabet);

    rna_coord_seq_destroy(coord_seq);
    rna_coord_alphabet_destroy(alphabet);
    rna_coord_destroy(rna_a);
}
