#include "fio.h"
#include "mprintf.h"
#include "rna_frag_coord.h"

int main(int argc, char *argv[])
{
    const char *this_sub = "main";
    const char *fname_bin="frag_coord.bin";
    struct rna_frag_coord *frag_coord_a=NULL;
    struct rna_frag_coord *frag_coord_b=NULL;
    int err;

    frag_coord_a = read_n_compute_rna_frag_coord("../testfiles/cif/3JCS.cif", "5", 1);
    if(!frag_coord_a) {
	goto err_exit;
    }
    rna_frag_coord_dump(frag_coord_a);

    err = rna_frag_coord_write_binary(frag_coord_a, fname_bin);
    if(err) {
	goto err_exit;
    }
    
    frag_coord_b = rna_frag_coord_read_binary(fname_bin, YES);
    if(!frag_coord_b) {
	goto err_exit;
    }

    mprintf("\n\n\n### AGAIN\n\n\n");
    
    rna_frag_coord_dump(frag_coord_b);

    rna_frag_coord_destroy(frag_coord_a);
    rna_frag_coord_destroy(frag_coord_b);
    return EXIT_SUCCESS;

err_exit:
    err_printf(this_sub, "MIST\n");
    rna_frag_coord_destroy(frag_coord_a);
    rna_frag_coord_destroy(frag_coord_b);
    return EXIT_FAILURE;
}
