#include "rna_alphabet.h"


int
main(int argc, char*argv[])
{
    const char*fn_seq_1="/work/petersen/no_backup/projects/cvrry/structural_alphabet/rfam_chains/seqs/seqs_no_basepairs/1asy_R_1.coordseq";
    const char*fn_seq_2="/work/petersen/no_backup/projects/cvrry/structural_alphabet/rfam_chains/seqs/seqs_no_basepairs/1asz_R_1.coordseq";

    struct rna_coord_seq*seq_1,*seq_2;

    struct float_pair scores;
    struct size_t_pair sums;

    seq_1 = rna_coord_seq_read_binary_file(fn_seq_1);
    seq_2 = rna_coord_seq_read_binary_file(fn_seq_2);

    sums = rna_coord_seq_lcs_sums(seq_1, seq_2);
    mprintf("first=%zu\n", sums.first);
    mprintf("second=%zu\n", sums.second);

    scores = rna_coord_seq_weighted_lcs_sums(seq_1, seq_2);
    mprintf("first=%f\n", scores.first);
    mprintf("second=%f\n", scores.second);

    rna_coord_seq_destroy(seq_1);
    rna_coord_seq_destroy(seq_2);
    return 0;
}
    
