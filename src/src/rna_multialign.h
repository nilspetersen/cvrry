#ifndef RNA_MULTIALIGN
#define RNA_MULTIALIGN


#include "rna_alphabet.h"
#include "pair_set.h"


struct rna_multialign {
    struct pair_set*pset_multi;
    struct rna_coord_seq**seqs;
    struct rna_coord**coords;
    size_t n_seqs;
    size_t n_coords;
};

struct rna_multialign*
new_rna_multialign(size_t max_n_chains);

void
rna_multialign_destroy(struct rna_multialign*multialign);

void
rna_multialign_set_pset(struct rna_multialign*multialign,
			const struct pair_set*pset_multi);

void
rna_multialign_add_seq_and_coord(struct rna_multialign*multialign,
				 const struct rna_coord_seq*seq,
				 const struct rna_coord*coord);

size_t
rna_multialign_length(const struct rna_multialign*multialign);

float
rna_multialign_gap_share(const struct rna_multialign*multialign);

#endif
