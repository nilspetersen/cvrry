#include "cvrry_optparse.h"
#include "string.h"
#include "e_malloc.h"
#include "mprintf.h"

void
getarg(char **target, const char *src)
{
    free_if_not_null(*target);
    *target = E_MALLOC((strlen(src) + 1) * sizeof(**target));
    strcpy(*target, src);
}

void
print_optslist(const struct option *long_options)
{
    const struct option *opt_pointer = long_options;
    for( ; opt_pointer->name != 0; opt_pointer++) {
	mprintf("  --%s -%c ", opt_pointer->name, (char) opt_pointer->val);
	if(opt_pointer->has_arg == required_argument) {
	    mprintf("[arg]");
	}
	mprintf("\n");
    }
}
