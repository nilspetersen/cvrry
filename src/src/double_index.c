#include "bin_utils.h"
#include "double_index.h"

struct double_index*
new_double_index(size_t len)
{
    struct double_index*double_index=E_MALLOC(sizeof(*double_index));
    double_index->index_a = new_size_t_array(len);
    double_index->index_b = new_size_t_array(len);
    double_index->size = len;
    return double_index;
}

void
double_index_destroy(struct double_index*double_index)
{
    if(double_index) {
	size_t_array_destroy(double_index->index_a);
	size_t_array_destroy(double_index->index_b);
	free(double_index);
    }
}

void
double_index_cpy(struct double_index*dest, const struct double_index*src)
{
    size_t_array_cpy(dest->index_a, src->index_a);
    size_t_array_cpy(dest->index_b, src->index_b);
}

struct double_index*
double_index_copy(const struct double_index*index_in)
{
    struct double_index*index_out=NULL;
    index_out = new_double_index(index_in->size);
    double_index_cpy(index_out, index_in);
    return index_out;
}

void
double_index_dump(const struct double_index*double_index)
{
    mprintf("double-index size=%zu\n", double_index->size);
    size_t_array_dump("%zu ", double_index->index_a);
    size_t_array_dump("%zu ", double_index->index_b);
}

int
double_index_write_binary(FILE*f_out,
			  const struct double_index*double_index)
{
    const char*this_sub="double_index_write_binary";
    size_t size;
    int err=0;
    err = m_bin_write(NULL, &size, sizeof(double_index->size),
		      1, f_out, this_sub);
    if(!err) {
	err = size_t_array_write_binary(f_out, double_index->index_a);
    }
    if(!err) {
	err = size_t_array_write_binary(f_out, double_index->index_b);
    }
    return err;
}

struct double_index*
double_index_read_binary(FILE*f_in)
{
    struct double_index*double_index=NULL;

    double_index = E_MALLOC(sizeof(*double_index));
    double_index->size = 0;
    double_index->index_a = double_index->index_b = NULL;
    
    if(fread(&double_index->size,
		 sizeof(double_index->size),
		 1, f_in) == 1) {
	double_index->index_a = size_t_array_read_binary(f_in);
    }
    if(double_index->index_a) {
	double_index->index_b = size_t_array_read_binary(f_in);
    }
    if(!double_index->index_b) {
	double_index_destroy(double_index);
	return NULL;
    }
    return double_index;
}
