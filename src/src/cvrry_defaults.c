#include "cvrry_defaults.h"
#include <string.h>

#define N_PAR_FRAG 6  /* values per fragment */

struct norm_params*
default_normalization()
{
    struct norm_params*frag_norm=NULL;
    float mu[N_PAR_FRAG] = {6.092, 15.326, 23.056, 11.142, 21.176, -219.265};
    float sigma[N_PAR_FRAG] = {0.445, 2.271, 4.708, 1.368, 3.938, 439.047};
    frag_norm = norm_params_new(N_PAR_FRAG);
    memcpy(frag_norm->mu, mu, N_PAR_FRAG * sizeof(*mu));
    memcpy(frag_norm->sigma, sigma, N_PAR_FRAG * sizeof(*sigma));
    return frag_norm;
}

struct feat_weights*
default_feat_weights()
{
    float default_w[N_PAR_FRAG] = {0.157297,
				   0.140898,
				   0.466623,
				   0.0778772,
				   0.401336,
				   0.130956};
    struct feat_weights *feat_weights = feat_weights_new(N_PAR_FRAG);
    memcpy(feat_weights->w, default_w, N_PAR_FRAG * sizeof(*default_w));
    return feat_weights;
}
