#ifndef DOUBLE_INDEX_H
#define DOUBLE_INDEX_H

#include "array.h"

struct double_index {
    size_t size;
    struct size_t_array *index_a; 
    struct size_t_array *index_b;
};

struct double_index*
new_double_index(size_t len);

void
double_index_destroy(struct double_index*double_index);

void
double_index_cpy(struct double_index*dest, const struct double_index*src);

struct double_index*
double_index_copy(const struct double_index*index_in);

void
double_index_dump(const struct double_index*double_index);

int
double_index_write_binary(FILE*f_out,
			  const struct double_index*double_index);

struct double_index*
double_index_read_binary(FILE*f_in);

#endif
