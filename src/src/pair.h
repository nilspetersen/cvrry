#ifndef PAIR_H
#define PAIR_H

#include <stdlib.h>


#define PAIR_CLASS_HEAD(PREFIX, DTYPE)					\
									\
    struct DTYPE ## _pair {PREFIX DTYPE first; PREFIX DTYPE second;};	\
									\
    struct DTYPE ## _pair*						\
    new_ ## DTYPE ## _pair();						\
									\
    void								\
    DTYPE ## _pair_destroy(struct DTYPE ## _pair *pair);


PAIR_CLASS_HEAD(, float)
PAIR_CLASS_HEAD(, size_t)


#endif
