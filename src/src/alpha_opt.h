#ifndef ALPHAOPT_H
#define ALPHAOPT_H

#include "cvrry_indexed_chainset.h"
#include "array.h"
#include "alphabet_index.h"

struct individual {

    /* public and interacting */
    size_t size;		       /* the number of sequence pairs */
    struct alphabet_index*alpha_index; /* it's the state of the system */

    /* local - all have the same size as pairs */
    struct continuous_match*matches;
    struct float_array*match_rmsds;
    struct float_array*energies;
    struct rna_coord_stringset*strings;

    /* fixed (and shared with other individuals) */
    const struct population *pop;
};

struct population {
    size_t n_parents;
    size_t offspring_rate;	/* e.g. 3 if there are 2 offspring indivivuals
				   created for every parent */
    size_t n_total;
    struct individual**individuals;
    
    /* shared stuff */
    struct double_index*pairs;
    struct rna_coord_set*coord_set;
    struct float_matrix**alphabet_rmsds;
    size_t n_chains;
    size_t fraglen;
    float temperature;
    struct match_energy_func*energy_func;
    struct move_strategy*moveset;

    /* private stuff */
    struct float_array*energies;
    struct float_array*boltzman_weights;
    struct float_array*probabilities;
    struct float_array*cumulative_probabilities;
    struct int_array*err_energies;

    struct individual**survivors;
    enum yes_no energy_up_to_date;
};

struct population*
population_from_indexed_chainset(const struct indexed_chainset*indexed_chainset,
				 const struct match_energy_func*energy_func,
				 const struct move_strategy*moveset,
				 const struct rna_coord_set*alphabet_candidates,
				 const struct double_index*pairs,
				 size_t n_parents,
				 size_t offspring_rate);

void
population_destroy(struct population*pop);

void
population_set_temperature(struct population*pop,
			   float temperature);

int
population_step(struct population*pop);

float
population_get_entropy(struct population*pop);

float
population_get_avg_energy(struct population*pop);

float
population_get_avg_matchlen(struct population*pop);

float
population_get_avg_alphabet_size(struct population*pop);

float
population_get_avg_rmsds(struct population*pop);

void
population_dump_probs(struct population*pop);

size_t
population_get_alphabet_size(const struct population*pop);

const struct individual*
population_get_individual(const struct population*pop, size_t i);

void
population_transfer_state(struct population*dest, const struct population*src);

size_t
population_best_individual_index(const struct population*pop);

int
population_assert_up2date_probabilities(struct population*pop);

int
population_best_individual_write_match_pdbs(const struct population*pop,
					    size_t match_i,
					    const char*outdir);

struct float_matrix*
population_rmsds_to_alphabet(const struct population*pop);

struct int_array*
population_best_individual_strings_merged(const struct population*pop);

struct size_t_array*
populations_best_individual_alphabet_choice(const struct population*pop);

const struct alphabet_index*
populations_best_individual_alphabet_index(const struct population*pop);

struct float_array*
population_best_individual_letter_frequencies(const struct population*pop);

#endif
