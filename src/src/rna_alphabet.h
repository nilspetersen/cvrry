#ifndef RNA_ALPHABET_H
#define RNA_ALPHABET_H

#include "rna_coord.h"
#include "suffix_arr.h"
#include "pair.h"

void
set_rna_coord_string(struct int_array*string,
		     const struct float_matrix*rmsds,
		     const struct alphabet_index*alpha_index);

struct rna_coord_alphabet {
    struct rna_coord_set*fragments;
    struct size_t_array*fragment_pattern;
    struct rna_atom_types_array*atom_types;
    struct alphabet_index*alpha_index;
    struct float_array*log_letter_freq;
};

size_t
rna_coord_alphabet_size(const struct rna_coord_alphabet*alphabet);

size_t
rna_coord_alphabet_get_fraglen(const struct rna_coord_alphabet*alphabet);

struct rna_coord_alphabet*
rna_coord_alphabet_assemble(const struct rna_coord_set*letter_chains,
			    const struct size_t_array*fragment_pattern,
			    const struct rna_atom_types_array*atom_types,
			    const struct alphabet_index*alpha_index,
			    const struct float_array*letter_frequencies);

void
rna_coord_alphabet_destroy(struct rna_coord_alphabet*alphabet);

int
rna_coord_alphabet_write_binary_file(const char*fn_out,
				     const struct rna_coord_alphabet*alphabet);

struct rna_coord_alphabet*
rna_coord_alphabet_read_binary_file(const char*fn_in);

enum rna_coord_seq_type {
    RNA_COORD_SEQ_EMPTY = 0,
    RNA_COORD_SEQ_SIMPLE = 1,
    RNA_COORD_SEQ_W_BREAK_CHARS = 2,
    RNA_COORD_SEQ_FULLSEQ = 3,
    RNA_COORD_SEQ_SIMULATED = 4
};

struct rna_coord_seq {
    enum rna_coord_seq_type type;
    size_t fraglen;
    size_t alphabet_size;
    int chain_break_char;
    struct int_array*seq;
    struct size_t_array*positions;
    /* For significance computation: */
    struct size_t_array*pieces_ends; /* contains the number of pieces */
    struct size_t_array*pieces;
    struct float_array*log_p_sum;
    struct float_array*log_n_pieces; /* log(number of pieces of length l) */
    struct float_array*log_p;	     /* significance log for each character */
    /* for full-sequences with empty characters */
    struct int_array*coord_positions;
};

size_t
rna_coord_seq_size(const struct rna_coord_seq*seq);

enum yes_no
rna_coord_seq_is_break_char(const struct rna_coord_seq*seq, size_t i);

size_t
rna_coord_seq_size_nobreaks(const struct rna_coord_seq*seq);

struct rna_coord_seq*
rna_coord_seq_copy(const struct rna_coord_seq*seq);

struct rna_coord_seq*
rna_coord_seq_simple(const struct rna_coord*rna,
		     const struct rna_coord_alphabet*alphabet);

struct rna_coord_seq*
rna_coord_seq_w_break_chars(const struct rna_coord*rna,
			    const struct rna_coord_alphabet*alphabet);

struct rna_coord_seq*
rna_coord_seq_fullseq_fill_up(const struct rna_coord_seq*seq_simple,
			      const struct rna_coord*rna,
			      enum yes_no add_endtail);

struct rna_coord_seq*
rna_coord_seq_fullseq_withoption(const struct rna_coord*rna,
				 const struct rna_coord_alphabet*alphabet,
				 enum yes_no add_endtail);

struct rna_coord_seq*
rna_coord_seq_fullseq_notail(const struct rna_coord*rna,
			     const struct rna_coord_alphabet*alphabet);

struct rna_coord_seq*
rna_coord_seq_fullseq(const struct rna_coord*rna,
		      const struct rna_coord_alphabet*alphabet);

void
rna_coord_seq_destroy(struct rna_coord_seq*seq);

void
rna_coord_seq_dump(const struct rna_coord_seq*seq);

size_t
rna_coord_seq_get_lastpos(const struct rna_coord_seq*seq);

int
rna_coord_seq_write_binary(FILE*f_out,
			   const struct rna_coord_seq*seq);

struct rna_coord_seq*
rna_coord_seq_read_binary(FILE*f_in);

int
rna_coord_seq_write_binary_file(const char*fname,
				const struct rna_coord_seq*seq);

struct rna_coord_seq*
rna_coord_seq_read_binary_file(const char*fname);

int
rna_coord_seq_lcs_match(struct continuous_match *match,
			const struct rna_coord_seq*seq_1,
			const struct rna_coord_seq*seq_2);

int
rna_coord_seq_add_basepair_info(struct rna_coord_seq*seq,
				const struct rna_coord*rna);

struct size_t_pair
rna_coord_seq_lcs_sums(const struct rna_coord_seq*seq_1,
		       const struct rna_coord_seq*seq_2);

struct float_pair
rna_coord_seq_weighted_lcs_sums(const struct rna_coord_seq*seq_1,
				const struct rna_coord_seq*seq_2);

struct float_pair
rna_coord_seq_logistic_weighted_lcs_sums(const struct rna_coord_seq*seq_1,
					 const struct rna_coord_seq*seq_2,
					 float x_0, float k);

struct float_pair
rna_coord_seq_weighted_lcs_max(const struct rna_coord_seq*seq_1,
			       const struct rna_coord_seq*seq_2);

int
rna_coord_seq_append_to_fasta_file(const char*fname,
				   const char*header,
				   const struct rna_coord_seq*seq);

int
rna_coord_seq_write_to_fasta_file(const char*fname,
				  const char*header,
				  const struct rna_coord_seq*seq);

struct pair_set*
rna_coord_seq_full_backmap_pair_set(const struct rna_coord_seq*seq1,
				    const struct rna_coord_seq*seq2,
				    const struct pair_set*pset_align);

size_t
rna_coord_seq_nobreakchar_len(const struct rna_coord_seq*seq);

#endif
