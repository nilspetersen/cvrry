#ifndef CVRRY_PARSE_MMCIF
#define CVRRY_PARSE_MMCIF

#include <stdlib.h>
#include <stdio.h>
#include "coord.h"
#include "lsqf.h"
#include "binary_file.h"

struct cvrry_cif_line {
    char *prefix;
    char *suffix;
    struct RPoint r;
};

struct cvrry_cif {
    char **sequence;
    int *chain_numbers; /* label_entity_id */
    int *res_numbers; /* label_seq_id */
    size_t seqlen;
    size_t n_lines;
    char *mem;
    struct cvrry_cif_line *lines;
};

int
write_mmcif(const struct cvrry_cif *cif, const char *pdbid, const char *filename);

size_t 				/* error if it returns 0 */
cvrry_cif_write_bin(FILE *outfile, const struct cvrry_cif*cif);

int
cvrry_cif_write_binary(FILE *outfile, const struct cvrry_cif*cif);

int
cvrry_cif_write_binary_file(const char*fname, const struct cvrry_cif*cif);

struct cvrry_cif*
cvrry_cif_read_binary(FILE *infile);

struct cvrry_cif*
cvrry_cif_read_binary_file(const char*fname);

void
cvrry_cif_dump(const struct cvrry_cif *cif);

void
cvrry_cif_destroy(struct cvrry_cif*cif);

int
read_mmcif(struct cvrry_cif**cif,
	   struct rna_coord**rna_backbone,
	   const char*filename,
	   const char*chain_id,
	   size_t model_number);

void
transform_mmcif(struct cvrry_cif*cif,
		const struct transformation *transform);

#endif	/* CVRRY_PARSE_MMCIF */
