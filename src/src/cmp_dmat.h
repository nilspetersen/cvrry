/*
 * $Id$
 */

#ifndef CMP_DMAT_I_H
#define CMP_DMAT_I_H

#include "pair_set.h"
#include "rna_coord.h"
#include "rna_alphabet.h"

float
coverage_fracdme_rna(const struct pair_set *pset,
		     const struct rna_coord *rna1,
		     const struct rna_coord *rna2,
		     float thresh);

float
coverage_fracdme_rna_seq(const struct pair_set *mapped_pset,
			 const struct rna_coord_seq*seq1,
			 const struct rna_coord_seq*seq2,
			 const struct rna_coord *rna1,
			 const struct rna_coord *rna2,
			 float thresh);

/* this one is frac_dme for rna structures ! */
float
dme_thresh_rna(const struct pair_set *pset,
	       const struct rna_coord *rna1,
	       const struct rna_coord *rna2,
	       float thresh);

float
good_bad_rna(const struct pair_set *pset,
	     const struct rna_coord *rna1,
	     const struct rna_coord *rna2,
	     float thresh);

float
percentage_of_similar_distances_rna(const struct pair_set *pset,
				    const struct rna_coord *rna1,
				    const struct rna_coord *rna2,
				    float thresh);

float
percentage_of_similar_distances_fixed_frags(const struct RPoint *bb1,
					    const struct RPoint *bb2,
					    size_t n_atoms,
					    float thresh);

float
distmat_gdtts_fixed_frags(const struct RPoint *bb1,
			  const struct RPoint *bb2,
			  size_t n_atoms);

#endif /* CMP_DMAT_H */
