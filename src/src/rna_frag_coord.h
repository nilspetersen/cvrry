#ifndef RNA_FRAG_COORD_H
#define RNA_FRAG_COORD_H

#include "rna_coord.h"
#include "cvrry_parse_mmcif.h"
#include "rna_frags.h"

struct rna_frag_coord {
    struct cvrry_cif *cif;
    struct rna_coord *rna_backbone;
    struct rna_frags *frags;
};

struct rna_frag_coord*
read_n_compute_rna_frag_coord(const char *mmcif_filename,
			      const char *chain_id,
			      size_t model_i);

void
rna_frag_coord_destroy(struct rna_frag_coord *frag_coord);

void
rna_frag_coord_dump(const struct rna_frag_coord *frag_coord);

size_t
rna_frag_coord_get_seq_len(const struct rna_frag_coord *frag_coord);

int
rna_frag_coord_write_binary(const struct rna_frag_coord *frag_coord,
			    const char *fname_out);

struct rna_frag_coord*
rna_frag_coord_read_binary(const char *fname_in, enum yes_no verbose_err);

void
print_cvrry_chain_information(struct rna_frag_coord *rna);

struct rna_frag_coord*
rna_frag_coord_from_rna_coord(const struct rna_coord*rna);

#endif
