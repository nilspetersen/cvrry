#include "psi.h"
#include "prosup.h"

int
percentage_of_structure_identity(float *psi,
				 struct pair_set **pairset_superimposed,
				 struct transformation *transform,
				 const struct pair_set *pairset,
				 const struct rna_coord *source,
				 const struct rna_coord *target,
				 float rmsd_thresh,
				 enum superpos_methods sup_method)
{
    int err=0;
    float dummy_rmsd;
    struct pair_set *reduced_alignment=NULL;
    size_t minlen = source->size < target->size ? source->size : target->size;

    switch(sup_method) {
    case ITERATIVE_REMOVAL:
	reduced_alignment = rna_coord_superimpose(pairset, source, target, rmsd_thresh);
	break;
    case MAXSUB:
	reduced_alignment = maxsub(pairset, source, target, rmsd_thresh, 4);
	break;
    case PROSUP_REFINE:
	reduced_alignment = rna_promax(source, target, pairset, 4, 4.0);
	break;
    }

    if(!reduced_alignment) {
	*psi = 0;
    } else {
	*psi = (float) pair_set_get_netto_alignmentlength(reduced_alignment) / (float) minlen;
    }
    
    /* TODO: set 'if transform ?' */
    if(reduced_alignment) {
	err = rna_coord_rmsd(transform, &dummy_rmsd, reduced_alignment, source, target);
    } else {
	err = rna_coord_rmsd(transform, &dummy_rmsd, pairset, source, target);
    }

    if(pairset_superimposed){
	*pairset_superimposed = reduced_alignment;
    } else {
	pair_set_destroy(reduced_alignment);
    }
    
    if(err) {
	return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
