#include "rna_alphabet_stats.h"

static void
count_and_add_letters(int*count_arr,
		      int shift, 
		      const struct int_array*seq)
{
    size_t i;
    for(i = 0; i < seq->size; ++i) {
	int p = seq->vals[i] + shift;
	if(p >= 0) {
	    count_arr[p]++;
	}
    }
}

void
rna_coord_seq_count_and_add_letters(int*count_arr,
				    int shift,
				    const struct rna_coord_seq*seq)
{
    count_and_add_letters(count_arr, shift, seq->seq);
}

static void
count_and_add_pairs(int*count_arr,
		    int shift,
		    const struct int_array*seq,
		    int alphabet_size)
{
    size_t i;
    int p1, p2, p;
    for(i = 0; i + 1 < seq->size; ++i) {
	p1 = seq->vals[i] + shift;
	p2 = seq->vals[i + 1] + shift;
	if(p1 < 0 || p2 < 0) {
	    continue;
	}
	p = p1 * alphabet_size + p2;
	count_arr[p]++;
    }
}

void
rna_coord_seq_count_and_add_pairs(int*count_arr,
				  int shift,
				  const struct rna_coord_seq*seq)
{
    count_and_add_pairs(count_arr, shift, seq->seq, shift + (int) seq->alphabet_size);
}

void
rna_coord_seq_pairs_count_no_break_chars(int*count_arr,
					 const struct rna_coord_seq*seq)
{
    rna_coord_seq_count_and_add_pairs(count_arr, 0, seq);
}

void
rna_coord_seq_pairs_count_with_break_chars(int*count_arr,
					   const struct rna_coord_seq*seq)
{
    rna_coord_seq_count_and_add_pairs(count_arr, -seq->chain_break_char, seq);
}

int
rna_coord_seq_nchar_pairs(const struct rna_coord_seq*seq,
			  int with_break_chars)
{
    int shift = (int) seq->alphabet_size;
    if(with_break_chars) {
	shift -= seq->chain_break_char;
    }
    return shift*shift;
}

/* /\* count and add with and without gap letters *\/ */
/* void */
/* rna_coord_seq_count_singles_and_pairs(int*count_arr_singles, */
/* 				      int*count_arr_pairs, */
/* 				      int shift, */
/* 				      const struct rna_coord_seq*seq) */
/* { */
/*     rna_coord_seq_count_and_add_letters(count_arr_singles, shift, seq); */
/*     rna_coord_seq_count_and_add_pairs(count_arr_pairs, shift, seq); */
/* } */

/* void */
/* rna_coord_seq_count_and_add_no_break_chars(int*count_arr_singles, */
/* 					   int*count_arr_pairs, */
/* 					   const struct rna_coord_seq*seq) */
/* { */
/*     rna_coord_seq_count_singles_and_pairs(count_arr_singles, */
/* 					  count_arr_pairs, */
/* 					  0, seq); */
/* } */

/* void */
/* rna_coord_seq_count_and_add_with_break_chars(int*count_arr_singles, */
/* 					     int*count_arr_pairs, */
/* 					     const struct rna_coord_seq*seq) */
/* { */
/*     rna_coord_seq_count_singles_and_pairs(count_arr_singles, */
/* 					  count_arr_pairs, */
/* 					  -seq->chain_break_char, */
/* 					  seq); */
/* } */

