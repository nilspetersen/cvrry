/*
 * 22 March 2001
 * rcsid = $Id$
 */
#ifndef FIO_H
#define FIO_H

#include <stdio.h>

FILE *mfopen (const char *fname, const char *mode, const char *s);
int file_no_cache (FILE *fp);
int file_clear_cache (FILE *fp);
int close_if_open_file(FILE *stream);

#endif /* FIO_H */
