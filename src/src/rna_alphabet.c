#include <math.h>
#include "binary_file.h"
#include "rna_alphabet.h"
#include "fio.h"
#include "rna_frags.h"
#include "pair_set.h"
#include "draw_random.h"

const char*RNA_ALPHABET_CHARACTERS = "ACDEFGHIKLMNPQRSTVWYBZ";
const int N_RNA_ALPHABET_CHARACTERS = 22;

#define RNA_COORD_SEQ_SRC_VERSION 100001

static int
get_mindist_letter(const float*rmsd_line,
		   const struct alphabet_index*alpha_index)
{
    size_t i;
    int letter=0;
    float rmsd_min;
    rmsd_min = rmsd_line[alphabet_index_get_select(alpha_index, 0)];
    for(i = 1; i < alpha_index->border_i; ++i) {
	float rmsd = rmsd_line[alphabet_index_get_select(alpha_index, i)];
	if(rmsd < rmsd_min) {
	    rmsd_min = rmsd;
	    letter = i;
	}
    }
    return letter;
}

void
set_rna_coord_string(struct int_array*string,
		     const struct float_matrix*rmsds,
		     const struct alphabet_index*alpha_index)
{
    size_t i;
    for(i = 0; i < string->size; ++i) {
	string->vals[i] = get_mindist_letter(
	    float_matrix_access_line(rmsds, i), alpha_index);
    }
}

void
rna_coord_alphabet_destroy(struct rna_coord_alphabet*alphabet)
{
    if(alphabet) {
	rna_coord_set_destroy(alphabet->fragments);
	size_t_array_destroy(alphabet->fragment_pattern);
	rna_atom_types_array_destroy(alphabet->atom_types);
	alphabet_index_destroy(alphabet->alpha_index);
	float_array_destroy(alphabet->log_letter_freq);
	free(alphabet);
    }
}

enum yes_no
rna_coord_seq_is_break_char(const struct rna_coord_seq*seq, size_t i)
{
    return seq->seq->vals[i] == seq->chain_break_char;
}

size_t
rna_coord_alphabet_size(const struct rna_coord_alphabet*alphabet)
{
    return alphabet->fragments->n_chains;
}

size_t
rna_coord_alphabet_get_fraglen(const struct rna_coord_alphabet*alphabet)
{
    return size_t_array_last(alphabet->fragment_pattern) + 1;
}

static struct rna_coord_alphabet*
empty_rna_coord_alphabet()
{
    struct rna_coord_alphabet*alphabet = E_MALLOC(sizeof(*alphabet));
    alphabet->fragments = NULL;
    alphabet->alpha_index = NULL;
    alphabet->fragment_pattern = NULL;
    alphabet->atom_types = NULL;
    alphabet->log_letter_freq = NULL;
    return alphabet;
}

struct rna_coord_alphabet*
rna_coord_alphabet_assemble(const struct rna_coord_set*letter_chains,
			    const struct size_t_array*fragment_pattern,
			    const struct rna_atom_types_array*atom_types,
			    const struct alphabet_index*alpha_index,
			    const struct float_array*letter_frequencies)
{
    struct rna_coord_alphabet*alphabet=empty_rna_coord_alphabet();
    if(alphabet) {
	alphabet->fragments = rna_coord_set_alphabet_selection(letter_chains,
							       alpha_index);
    }
    if(alphabet->fragments) {
	alphabet->alpha_index = new_alphabet_index(rna_coord_alphabet_size(alphabet));
	alphabet_index_choose_all(alphabet->alpha_index);
    }
    if(alphabet->alpha_index) {
	alphabet->atom_types = rna_atom_types_array_copy(atom_types);
    }
    if(alphabet->atom_types) {
	alphabet->fragment_pattern = size_t_array_copy(fragment_pattern);
    }
    if(alphabet->fragment_pattern) {
	alphabet->log_letter_freq = float_array_apply(letter_frequencies,
						      &logf);
    }
    if(!alphabet->log_letter_freq) {
	rna_coord_alphabet_destroy(alphabet);
	return NULL;
    }
    return alphabet;
}

int
rna_coord_alphabet_write_binary(FILE*f_out,
				const struct rna_coord_alphabet*alphabet)
{
    const char*this_sub="rna_coord_alphabet_write_binary";
    int err=0;
    if(!rna_coord_alphabet_size(alphabet)) {
	err_printf(this_sub, "Can not print empty alphabet\n");
	return 1;
    }    
    err = rna_coord_set_write_binary(f_out, alphabet->fragments);
    if(!err) {
	err = size_t_array_write_binary(f_out, alphabet->fragment_pattern);
    }
    if(!err) {
	err = rna_atom_types_array_write_binary(f_out, alphabet->atom_types);
    }
    if(!err) {
	err = alphabet_index_write_binary(f_out, alphabet->alpha_index);
    }
    if(!err) {
	err = float_array_write_binary(f_out, alphabet->log_letter_freq);
    }
    if(err) {
	err_printf(this_sub, "Failed to write RNA-alphabet in binary format\n");
    }
    return err;
}

int
rna_coord_alphabet_write_binary_file(const char*fn_out,
				     const struct rna_coord_alphabet*alphabet)
{
    const char*this_sub="rna_coord_alphabet_write_binary_file";
    FILE *f_out = mfopen(fn_out, "wb", this_sub);
    int err;
    if(!f_out) {
	return 1;
    }
    err = rna_coord_alphabet_write_binary(f_out, alphabet);
    fclose(f_out);
    return err;
}

struct rna_coord_alphabet*
rna_coord_alphabet_read_binary(FILE*f_in)
{
    const char*this_sub="rna_coord_alphabet_read_binary";
    struct rna_coord_alphabet*alphabet=empty_rna_coord_alphabet();
    if(alphabet) {
	alphabet->fragments = rna_coord_set_read_binary(f_in);
    }
    if(alphabet->fragments) {
	alphabet->fragment_pattern = size_t_array_read_binary(f_in);
    }
    if(alphabet->fragment_pattern) {
	alphabet->atom_types = rna_atom_types_array_read_binary(f_in);
    }
    if(alphabet->atom_types) {
	alphabet->alpha_index = alphabet_index_read_binary(f_in);
    }
    if(alphabet->alpha_index) {
	alphabet->log_letter_freq = float_array_read_binary(f_in);;
    }
    if(!alphabet->log_letter_freq) {
	err_printf(this_sub, "failed");
	rna_coord_alphabet_destroy(alphabet);
	return NULL;
    }
    return alphabet;
}

struct rna_coord_alphabet*
rna_coord_alphabet_read_binary_file(const char*fn_in)
{
    const char*this_sub="rna_coord_alphabet_read_binary_file";
    struct rna_coord_alphabet*alphabet=NULL;
    FILE *f_in=NULL;
    f_in = mfopen(fn_in, "rb", this_sub);
    if(!f_in) {
	return NULL;
    }
    alphabet = rna_coord_alphabet_read_binary(f_in);
    fclose(f_in);
    return alphabet;
}

static struct rna_coord_seq*
empty_rna_coord_seq()
{
    struct rna_coord_seq*seq = E_MALLOC(sizeof(*seq));
    seq->type = RNA_COORD_SEQ_EMPTY;
    seq->fraglen = 0;
    seq->alphabet_size = 0;
    seq->chain_break_char = -1;
    seq->seq = NULL;
    seq->positions = NULL;
    seq->pieces_ends = NULL;
    seq->pieces = NULL;
    seq->log_p_sum = NULL;
    seq->log_n_pieces = NULL;
    seq->log_p = NULL;
    seq->coord_positions = NULL;
    return seq;
}

size_t
rna_coord_seq_size(const struct rna_coord_seq*seq)
{
    return seq->seq->size;
}

size_t
rna_coord_seq_size_nobreaks(const struct rna_coord_seq*seq)
{
    size_t size = rna_coord_seq_size(seq);
    if(seq->type == RNA_COORD_SEQ_W_BREAK_CHARS) {
	size -= (seq->pieces_ends->size - 1);
    }
    return size;
}

struct rna_coord_seq*
rna_coord_seq_copy(const struct rna_coord_seq*old)
{
    struct rna_coord_seq*new = empty_rna_coord_seq();
    new->type = old->type;
    new->fraglen = old->fraglen;
    new->alphabet_size = old->alphabet_size;
    new->chain_break_char = old->chain_break_char;
    new->seq = int_array_copy(old->seq);
    new->positions = size_t_array_copy(old->positions);
    new->pieces_ends = size_t_array_copy(old->pieces_ends);
    new->pieces = size_t_array_copy(old->pieces);
    new->log_p_sum = float_array_copy(old->log_p_sum);
    new->log_n_pieces = float_array_copy(old->log_n_pieces);
    return new;
}

void
rna_coord_seq_destroy(struct rna_coord_seq*seq)
{
    if(seq) {
	int_array_destroy(seq->seq);
	size_t_array_destroy(seq->positions);
	size_t_array_destroy(seq->pieces_ends);
	size_t_array_destroy(seq->pieces);
	float_array_destroy(seq->log_p_sum);
	float_array_destroy(seq->log_n_pieces);
	float_array_destroy(seq->log_p);
	int_array_destroy(seq->coord_positions);
	free(seq);
    }
}

void
rna_coord_seq_dump(const struct rna_coord_seq*seq)
{
    const char*this_sub="rna_coord_seq_dump";
    if(seq) {
	mprintf("Fraglen=%zu\n", seq->fraglen);
	mprintf("Alphabet-size=%zu\n", seq->alphabet_size);
	mprintf("Chainbreak character=%i\n", seq->chain_break_char);
	mprintf("Seq:\n");
	int_array_dump("%i ", seq->seq);
	mprintf("Positions:\n");
	size_t_array_dump("%zu ", seq->positions);
	mprintf("Piece-ends:\n");
	size_t_array_dump("%zu ", seq->pieces_ends);
	mprintf("Pieces:\n");
	size_t_array_dump("%zu ", seq->pieces);
	mprintf("Log-P cumulative:\n");
	float_array_dump("%.4f ", seq->log_p_sum);
	mprintf("Log(N-pieces):\n");
	float_array_dump("%.4f ", seq->log_n_pieces);
	mprintf("Log-P alphabet:\n");
	float_array_dump("%.4f ", seq->log_p);
	mprintf("Coord positions:\n");
	int_array_dump("%i ", seq->coord_positions);
    } else {
	err_printf(this_sub, "*seq pointer is NULL\n");
    }
}

size_t
rna_coord_seq_get_lastpos(const struct rna_coord_seq*seq)
{
    return size_t_array_last(seq->positions);
}

int
rna_coord_seq_write_binary(FILE*f_out,
			   const struct rna_coord_seq*seq)
{
    const char*this_sub="rna_coord_seq_write_binary";
    int err=0;
    if(!err) {
	err = write_src_version(RNA_COORD_SEQ_SRC_VERSION, f_out);
    }
    if(!err) {
	err = m_bin_write(NULL, &seq->type,
			  sizeof(seq->type),
			  1, f_out, this_sub);
    }
    if(!err) {
	err = m_bin_write(NULL, &seq->fraglen,
			  sizeof(seq->fraglen),
			  1, f_out, this_sub);
    }
    if(!err) {
	err = m_bin_write(NULL, &seq->alphabet_size,
			  sizeof(seq->alphabet_size),
			  1, f_out, this_sub);
    }
    if(!err) {
	err = m_bin_write(NULL, &seq->chain_break_char,
			  sizeof(seq->chain_break_char),
			  1, f_out, this_sub);
    }
    if(!err) {
	err = int_array_write_binary(f_out, seq->seq);
    }
    if(!err) {
	err = size_t_array_write_binary(f_out, seq->positions);
    }
    if(!err) {
	err = size_t_array_write_binary(f_out, seq->pieces_ends);
    }
    if(!err) {
	err = size_t_array_write_binary(f_out, seq->pieces);
    }
    if(!err) {
	err = float_array_write_binary(f_out, seq->log_p_sum);
    }
    if(!err) {
	err = float_array_write_binary(f_out, seq->log_n_pieces);
    }
    if(!err) {
	err = float_array_write_binary(f_out, seq->log_p);
    }
    if(!err) {
	err = int_array_write_binary(f_out, seq->coord_positions);
    }
    return err;
}

struct rna_coord_seq*
rna_coord_seq_read_binary(FILE*f_in)
{
    const char*this_sub="rna_coord_seq_read_binary";
    int err=1;
    struct rna_coord_seq*seq=empty_rna_coord_seq();
    if(seq) {
	err = assert_src_version(RNA_COORD_SEQ_SRC_VERSION, f_in);
    }
    if(!err) {	
	err = (fread(&seq->type,
		     sizeof(seq->type),
		     1, f_in) != 1);
    }
    if(!err) {
	err = (fread(&seq->fraglen,
		     sizeof(seq->fraglen),
		     1, f_in) != 1);
    }
    if(!err) {
	err = (fread(&seq->alphabet_size,
		     sizeof(seq->alphabet_size),
		     1, f_in) != 1);
    }
    if(!err) {
	err = (fread(&seq->chain_break_char,
		     sizeof(seq->chain_break_char),
		     1, f_in) != 1);
    }
    if(!err) {
	seq->seq = int_array_read_binary(f_in);
	seq->positions = size_t_array_read_binary(f_in);
	seq->pieces_ends = size_t_array_read_binary(f_in);
	seq->pieces = size_t_array_read_binary(f_in);
    	seq->log_p_sum = float_array_read_binary(f_in);
	seq->log_n_pieces = float_array_read_binary(f_in);
	seq->log_p = float_array_read_binary(f_in);
	seq->coord_positions = int_array_read_binary(f_in);
	/* check errors */
	switch(seq->type) {
	case RNA_COORD_SEQ_FULLSEQ:    
	    err = ((!seq->positions) || (!seq->seq) || (!seq->coord_positions) ||
		   (!seq->log_p));
	    break;
	case RNA_COORD_SEQ_W_BREAK_CHARS:
	    err = ((!seq->pieces_ends) || (!seq->pieces));
	    /* no break here is intended! */
	case RNA_COORD_SEQ_SIMPLE:
	    err = ((!seq->positions) || (!seq->seq) || (!seq->log_p) || err);
	    break;
	case RNA_COORD_SEQ_SIMULATED:
	    err = ((!seq->seq) || err);
	    break;
	case RNA_COORD_SEQ_EMPTY:
	    break;
	}
    }
    if(err) {
	err_printf(this_sub, "failed\n");
	rna_coord_seq_destroy(seq);
	return NULL;
    }
    return seq;
}

BINARY_IO_BODY(rna_coord_seq)

struct rna_coord_seq*
rna_coord_seq_simple(const struct rna_coord*rna,
		     const struct rna_coord_alphabet*alphabet)
{
    struct float_matrix*rmsds=NULL;
    struct rna_coord_seq*seq=empty_rna_coord_seq();
    if(seq) {
	seq->type = RNA_COORD_SEQ_SIMPLE;
	seq->positions = get_fragpositions(rna_coord_alphabet_get_fraglen(alphabet), rna);
    }
    if(seq->positions) {
	seq->seq = new_int_array(seq->positions->size);
    }
    if(seq->seq) {
	rmsds = calc_rna_coord_vs_alphabet_rmsds_chainbreaks(rna,
							     seq->positions,
							     alphabet->fragments,
							     alphabet->fragment_pattern,
							     alphabet->atom_types);
    }
    if(rmsds) {
	set_rna_coord_string(seq->seq, rmsds, alphabet->alpha_index);
	float_matrix_destroy(rmsds);
	seq->fraglen = rna_coord_alphabet_get_fraglen(alphabet);
	seq->alphabet_size = rna_coord_alphabet_size(alphabet);
	seq->log_p = float_array_copy(alphabet->log_letter_freq);
    }
    if(seq->log_p) {
	return seq;
    }
    rna_coord_seq_destroy(seq);
    return NULL;
}

static void
rna_coord_seq_transfer_i(struct rna_coord_seq*target,
			 const struct rna_coord_seq*src,
			 size_t i)
{
    target->seq->vals[target->seq->size] = src->seq->vals[i];
    target->positions->vals[target->positions->size] = src->positions->vals[i];
    target->seq->size++;
    target->positions->size++;
}

static void
rna_coord_seq_add_chain_break(struct rna_coord_seq*target)
{
    size_t i=target->positions->size;
    size_t_array_push(target->positions, target->positions->vals[i - 1]);
    int_array_push(target->seq, target->chain_break_char);
}

static int
rna_coord_seq_transfer_w_chainbreaks(struct rna_coord_seq*target,
				     const struct rna_coord_seq*src)
{
    int err=0;
    size_t i;
    target->seq->size = target->positions->size = 0;
    target->pieces_ends->size = target->pieces->size = 0;
    if(rna_coord_seq_size(src)) {
	rna_coord_seq_transfer_i(target, src, 0);
	size_t_array_push(target->pieces, 0);	
    }
    for(i = 1; i < rna_coord_seq_size(src); ++i) {
	if(src->positions->vals[i] != src->positions->vals[i - 1] + 1) {
	    size_t_array_push(target->pieces_ends, target->seq->size - 1);
	    rna_coord_seq_add_chain_break(target);
	}
	rna_coord_seq_transfer_i(target, src, i);
	size_t_array_push(target->pieces, target->pieces_ends->size);
    }
    size_t_array_push(target->pieces_ends, target->seq->size - 1);
    target->fraglen = src->fraglen;
    target->alphabet_size = src->alphabet_size;
    return err;
}

static struct rna_coord_seq*
rna_coord_seq_with_chainbreaks(const struct rna_coord_seq*seq_in)
{
    struct rna_coord_seq*seq_out=NULL;
    size_t size=2*rna_coord_seq_size(seq_in);
    int err=1;
    if(size) {
	seq_out = empty_rna_coord_seq();
    }
    /* TODO: check type and if seq_in is already with chainbreaks just copy ? */
    if(seq_out) {
	seq_out->log_p = float_array_copy(seq_in->log_p);
    }
    if(seq_out->log_p) {
	seq_out->type = RNA_COORD_SEQ_W_BREAK_CHARS;
	seq_out->positions = new_size_t_array(size);
    }
    if(seq_out->positions) {
	seq_out->seq = new_int_array(size);
    }
    if(seq_out->seq) {
	seq_out->pieces_ends = new_size_t_array(size);
    }
    if(seq_out->pieces_ends) {
	 seq_out->pieces = new_size_t_array(size);
    }
    if(seq_out->pieces) {
	err = rna_coord_seq_transfer_w_chainbreaks(seq_out, seq_in);
    }
    if(err) {
	rna_coord_seq_destroy(seq_out);
	return NULL;
    }
    return seq_out;
}

static void
cumulative_log_p_sum(float*log_p_sum, /* it's 1 larger than seq_len! */
		     size_t seq_len,
		     const int*seq,
		     const float*letter_freq_logs,
		     const size_t*positions)
{
    size_t i;
    log_p_sum[0] = 0;
    if(!seq_len) {
	return;
    }
    log_p_sum[1] = -letter_freq_logs[seq[0]];
    for(i = 1; i < seq_len; ++i) {
	if(positions[i] == positions[i-1]) {
	    /* it's a chain-break character/sentinel */
	    log_p_sum[i+1] = 0;
	} else {
	    log_p_sum[i+1] = log_p_sum[i] - letter_freq_logs[seq[i]];
	}
    }
}

static int
rna_coord_seq_add_log_p_sum(struct rna_coord_seq*seq,
			    const struct float_array*log_letter_freq)
{
    const char*this_sub="rna_coord_seq_add_log_p_sum";
    if(log_letter_freq->size != seq->alphabet_size) {
	err_printf(this_sub,
		   ("size of log-letter-frequencies (%zu) "
		    "and sequence alphabet size (%zu) do not match!\n"),
		   log_letter_freq->size, seq->alphabet_size);
	return 1;
    }
    float_array_destroy(seq->log_p_sum);
    seq->log_p_sum = new_float_array(seq->seq->size + 1);
    cumulative_log_p_sum(seq->log_p_sum->vals,
			 seq->seq->size,
			 seq->seq->vals,
			 log_letter_freq->vals,
			 seq->positions->vals);
    return 0;
}

static void
rna_coord_seq_add_log_n_pieces(struct rna_coord_seq*seq)
{
    size_t max_piece_len;
    struct size_t_array *n_pieces_arr=NULL, *piece_count_by_len=NULL;
    size_t i, piece_start, piece_count;

    max_piece_len = rna_coord_seq_size(seq);

    piece_count_by_len = new_size_t_array_initialized(max_piece_len + 1, 0);
    
    /* part one: record the lengths of the fragments */
    for(i = 0, piece_start = 0; i < seq->pieces_ends->size; ++i) {
	size_t piece_end = seq->pieces_ends->vals[i];
	size_t piece_len = piece_end + 1 - piece_start;
	piece_count_by_len->vals[piece_len] += 1;
	piece_start = piece_end + 1;
    }
    
    /* n_pieces_arr is one larger than the other arrays
       to add in the first loop iteration iteration */
    n_pieces_arr = new_size_t_array(max_piece_len + 2); 
    n_pieces_arr->vals[n_pieces_arr->size-1] = 0;
    /* part two (dynamic programming): sum the number of fragments of each length */
    for(i = n_pieces_arr->size - 2, piece_count = 0; i > 0; --i) {
	piece_count += piece_count_by_len->vals[i];
	n_pieces_arr->vals[i] = piece_count + n_pieces_arr->vals[i + 1];
    }
    n_pieces_arr->vals[i] = 0;
    /* part three: compute the logarithms */
    float_array_destroy(seq->log_n_pieces);
    seq->log_n_pieces = new_float_array(n_pieces_arr->size);
    for(i = 0; i < seq->log_n_pieces->size; ++i) {
	if(n_pieces_arr->vals[i] > 0.0) {
	    seq->log_n_pieces->vals[i] = logf((float) n_pieces_arr->vals[i]);
	} else {
	    seq->log_n_pieces->vals[i] = 0;
	}
    }
    /* free temporary memory */
    size_t_array_destroy(piece_count_by_len);
    size_t_array_destroy(n_pieces_arr);
}

struct rna_coord_seq*
rna_coord_seq_w_break_chars(const struct rna_coord*rna,
			    const struct rna_coord_alphabet*alphabet)
{
    int err=1;
    struct rna_coord_seq*seq_simple=NULL;
    struct rna_coord_seq*seq_w_breaks=NULL;
    seq_simple = rna_coord_seq_simple(rna, alphabet);
    if(seq_simple) {
	seq_w_breaks = rna_coord_seq_with_chainbreaks(seq_simple);
    }
    if(seq_w_breaks) {
	err = rna_coord_seq_add_log_p_sum(seq_w_breaks, alphabet->log_letter_freq);
    }
    if(!err){
	rna_coord_seq_add_log_n_pieces(seq_w_breaks);
    }
    rna_coord_seq_destroy(seq_simple);
    if(err) {
	rna_coord_seq_destroy(seq_w_breaks);
	return NULL;
    }
    return(seq_w_breaks);
}

/* static void */
/* merge_strings_of_2_alphabets(int*seq_out, */
/* 			     size_t size, */
/* 			     const int*seq1, */
/* 			     const int*seq2, */
/* 			     int min1, */
/* 			     int min2, */
/* 			     int max2) */
/* { */
/*     size_t i; */
/*     for(i = 0; i < size; ++i) { */
/* 	seq_out[i] = (seq1[i] - min1) * (max2 - min2 + 1) + (seq2[i] - min2); */
/*     } */
/* } */

static void
rna_coord_seq_simple_to_full_add_letter(struct rna_coord_seq*seq_full,
					int letter,
					int coord_pos)
{
    int_array_push(seq_full->seq, letter);
    int_array_push(seq_full->coord_positions, coord_pos);
}

/* rna_coord_seq_simple_to_full_add_n_gap_letters
 *
 * coord_pos: should be the position of the previous alphabet letter + 1
 * n_fragpositions: should be fraglen -1
 */
static void
rna_coord_seq_simple_to_full_add_n_gap_letters(struct rna_coord_seq*seq_full,
					       size_t n,
					       size_t first_coord_pos,
					       size_t n_fragpos)
{
    size_t i;
    for(i = 0; i < n; ++i) {
	rna_coord_seq_simple_to_full_add_letter(seq_full,
						seq_full->chain_break_char,
						i < n_fragpos ? ((int) (first_coord_pos + i)) : -1);
    }
}

static int
rna_coord_seq_transfer_simple_to_full(struct rna_coord_seq*seq_full,
				      const struct rna_coord_seq*seq_simple,
				      const struct rna_coord*rna,
				      enum yes_no add_endtail)
{
    const char*this_sub="rna_coord_seq_transfer_simple_to_full";
    size_t i, p1, p2;
    int cifseq_i, cifseq_j;
    size_t n_fragpos;
    size_t pos_fragshift;
    if(add_endtail) {
	n_fragpos = seq_simple->fraglen - 1;
	pos_fragshift = 0;
    } else {
	n_fragpos = 0;
	pos_fragshift = seq_simple->fraglen / 2;
    }
    seq_full->positions->size = 0;
    seq_full->seq->size = 0;
    seq_full->coord_positions->size = 0;
    if(!seq_simple->seq->size) {
	return 0;
    }
    if(seq_simple->type != RNA_COORD_SEQ_SIMPLE) {
	err_printf(this_sub, "Sequence is not of type RNA_COORD_SEQ_SIMPLE");
	return 1;
    }
    for(i = 0; i < seq_simple->seq->size - 1; ++i) {
	rna_coord_seq_simple_to_full_add_letter(seq_full,
						seq_simple->seq->vals[i],
						(int) seq_simple->positions->vals[i] + pos_fragshift);
	p1 = seq_simple->positions->vals[i];
	p2 = seq_simple->positions->vals[i + 1];
	cifseq_i = rna->cif_seq_num[p1];
	cifseq_j = rna->cif_seq_num[p2];
	if(cifseq_j > cifseq_i) {
	    rna_coord_seq_simple_to_full_add_n_gap_letters(seq_full,
							   cifseq_j - cifseq_i - 1,
							   seq_simple->positions->vals[i] + 1,
							   n_fragpos);
	} else {
	    return 1;
	}
    }
    rna_coord_seq_simple_to_full_add_letter(seq_full,
					    seq_simple->seq->vals[i],
					    (int) seq_simple->positions->vals[i] + pos_fragshift);
    rna_coord_seq_simple_to_full_add_n_gap_letters(seq_full,
						   n_fragpos,
						   seq_simple->positions->vals[i] + 1,
						   n_fragpos);
    seq_full->positions->size = seq_full->seq->size;
    return 0;
}

struct rna_coord_seq*
rna_coord_seq_fullseq_fill_up(const struct rna_coord_seq*seq_simple,
			      const struct rna_coord*rna,
			      enum yes_no add_endtail)
{
    const char*this_sub="rna_coord_seq_fullseq_fill_up";
    struct rna_coord_seq*seq_full=NULL;
    size_t full_seq_len;
    int err=1;
    full_seq_len = rna_coord_seqlen_by_mmcif_indices(rna);
    if(full_seq_len) {
	seq_full = empty_rna_coord_seq();
    }
    if(seq_full) {
	seq_full->type = RNA_COORD_SEQ_FULLSEQ;
	seq_full->fraglen = seq_simple->fraglen;
	seq_full->alphabet_size = seq_simple->alphabet_size;
	seq_full->log_p = float_array_copy(seq_simple->log_p);
	seq_full->positions = size_t_range(0, full_seq_len, 1);
	seq_full->coord_positions = new_int_array(full_seq_len);
    }
    if(seq_full->positions) {
	seq_full->seq = new_int_array(full_seq_len);
    }
    if(seq_full->seq) {
	err = rna_coord_seq_transfer_simple_to_full(seq_full, seq_simple, rna, add_endtail);
    }
    if(!err) {
	return seq_full;
    }
    err_printf(this_sub, "failed");
    rna_coord_seq_destroy(seq_full);
    return NULL;
}

struct rna_coord_seq*
rna_coord_seq_fullseq_withoption(const struct rna_coord*rna,
				 const struct rna_coord_alphabet*alphabet,
				 enum yes_no add_endtail)
{
    const char*this_sub="rna_coord_seq_fullseq";
    struct rna_coord_seq *seq_simple=NULL, *seq_full=NULL;
    seq_simple = rna_coord_seq_simple(rna, alphabet);
    if(seq_simple) {
	seq_full = rna_coord_seq_fullseq_fill_up(seq_simple, rna, add_endtail);
    }
    rna_coord_seq_destroy(seq_simple);
    if(!seq_full) {
	err_printf(this_sub, "failed\n");
    }
    return seq_full;
}

struct rna_coord_seq*
rna_coord_seq_fullseq(const struct rna_coord*rna,
		      const struct rna_coord_alphabet*alphabet)
{
    return rna_coord_seq_fullseq_withoption(rna, alphabet, YES);
}

struct rna_coord_seq*
rna_coord_seq_fullseq_notail(const struct rna_coord*rna,
			     const struct rna_coord_alphabet*alphabet)
{
    return rna_coord_seq_fullseq_withoption(rna, alphabet, NO);
}


/* CAREFUL HERE: THIS IS QUITE SPECIFIC, THE LOWEST LETTER HAS TO BE 0 
   FOR THE ALPHABET AND THE BASEPAIRS!
 */
static void
merge_string_and_basepairs(int*seq_out,
			   size_t size,
			   const int*seq1,
			   const int*seq2,
			   int n_letters_2)
{
    size_t i;
    for(i = 0; i < size; ++i) {
	seq_out[i] = seq1[i] * n_letters_2 + seq2[i];
    }
}

static void
reset_chainbreak_positions(struct rna_coord_seq*seq)
{
    size_t i;
    if(seq->type == RNA_COORD_SEQ_W_BREAK_CHARS) {
	for(i = 0; i + 1 < seq->pieces_ends->size; ++i) {
	    size_t j = seq->pieces_ends->vals[i] + 1;
	    seq->seq->vals[j] = seq->chain_break_char;
	}
    }
}

static int
rna_coord_seq_add_basepair_info_with_chainbreaks(struct rna_coord_seq*seq,
						 const struct rna_coord*rna)
{
    const char*this_sub="rna_coord_seq_add_basepair_info_with_chainbreaks";
    size_t seqlen=rna_coord_seq_size(seq);
    size_t fraglen=seq->fraglen;
    int *basepairs_str=NULL;
    size_t i;
    if(seqlen > rna->size + fraglen - 1) {
	err_printf(this_sub,
		   ("Sequence (%zu) is too long for rna "
		    "chain (%zu) and fragment length (%zu)\n"),
		   seqlen, rna->size, fraglen);
	return 1;
    }
    basepairs_str = E_MALLOC(seqlen * sizeof(*basepairs_str));
    for(i = 0; i < seqlen; ++i) {
	size_t j = seq->positions->vals[i] + (fraglen / 2);
	basepairs_str[i] = rna->basepair_info[j];
    }
    merge_string_and_basepairs(seq->seq->vals,
			       seqlen,
			       seq->seq->vals,
			       basepairs_str,
			       3);
    seq->alphabet_size *= 3;
    reset_chainbreak_positions(seq);
    free_if_not_null(basepairs_str);
    return 0;
}

static int
rna_coord_seq_add_basepair_info_fullseq(struct rna_coord_seq*seq,
					const struct rna_coord*rna)
{
    size_t i;
    int p, c, j;
    size_t fraglen=seq->fraglen;
    for(i = 0; i < rna_coord_seq_size(seq); ++i) {
	p = seq->coord_positions->vals[i];
	c = seq->seq->vals[i];
	if(c == seq->chain_break_char || p < 0) {
	    continue;
	}
	j = p + (fraglen / 2);
	seq->seq->vals[i] = seq->seq->vals[i] * 3 + rna->basepair_info[j];
    }
    seq->alphabet_size *= 3;
    return 0;
}

int
rna_coord_add_basepair_info_log_p(struct rna_coord_seq*seq)
{
    size_t i, j;
    size_t alphasize_div3=seq->alphabet_size / 3;
    struct float_array*new_log_p=NULL;
    new_log_p = new_float_array(3 * alphasize_div3);
    if(!new_log_p) {
	return 1;
    }
    for(i = 0; i < alphasize_div3; ++i) {
	for(j = 0; j < 3; ++j) {
	    new_log_p->vals[i * 3 + j] = seq->log_p->vals[i];
	}
    }
    float_array_destroy(seq->log_p);
    seq->log_p = new_log_p;
    return 0;
}

int
rna_coord_seq_add_basepair_info(struct rna_coord_seq*seq,
				const struct rna_coord*rna)
{
    const char*this_sub="rna_coord_seq_add_basepair_info";
    int err=0;
    if(!rna->basepair_info) {
	err_printf(this_sub, "Basepair info required\n");
	return 1;
    }
    switch(seq->type) {
    case RNA_COORD_SEQ_EMPTY:
	return 0;
    case RNA_COORD_SEQ_SIMPLE:
    case RNA_COORD_SEQ_W_BREAK_CHARS:
	err = rna_coord_seq_add_basepair_info_with_chainbreaks(seq, rna);
	break;
    case RNA_COORD_SEQ_FULLSEQ:
	err= rna_coord_seq_add_basepair_info_fullseq(seq, rna);
	break;
    default:
	err_printf(this_sub, "not a valid pattern type!\n");
	return 1;
    }
    if(!err) {
	err = rna_coord_add_basepair_info_log_p(seq);
    }
    if(err) {
	err_printf(this_sub, "failed!\n");
    }
    return err;
}

static struct int_array*
rna_coord_seq_get_sequence_with_sentinels(int*sentinel,
					  const struct rna_coord_seq*seq)
{
    size_t i;
    struct int_array*new_string=int_array_copy(seq->seq);
    if(!new_string) {
	return NULL;
    }
    if(seq->type == RNA_COORD_SEQ_W_BREAK_CHARS) {
	for(i = 0; i + 1 < seq->pieces_ends->size; ++i) {
	    size_t j = seq->pieces_ends->vals[i] + 1;
	    new_string->vals[j] = *sentinel;
	    (*sentinel)++;
	}
    }
    return new_string;
}

static int
max_int(int a, int b)
{
    return a > b ? a : b;
}

int
rna_coord_seq_lcs_match(struct continuous_match *match,
			const struct rna_coord_seq*seq_1,
			const struct rna_coord_seq*seq_2)
{
    struct int_array*str_1=NULL;
    struct int_array*str_2=NULL;
    int sentinel = max_int(int_array_max(seq_1->seq),
			   int_array_max(seq_2->seq)) + 1;
    int err=1;
    str_1 = rna_coord_seq_get_sequence_with_sentinels(&sentinel, seq_1);
    if(str_1) {
	str_2 = rna_coord_seq_get_sequence_with_sentinels(&sentinel, seq_2);
    }	
    if(str_2) {
	err = longest_common_substring(match, 0, str_1, str_2);
    }
    if(!err) {
	if(match->len) {
	    match->matchpos_a = seq_1->positions->vals[match->matchpos_a];
	    match->matchpos_b = seq_2->positions->vals[match->matchpos_b];
	    match->len += seq_1->fraglen - 1;
	}
    }
    int_array_destroy(str_1);
    int_array_destroy(str_2);
    return err;
}

/* 
 * alignment free scores - NOT TESTED YET !
 */

struct size_t_pair
rna_coord_seq_lcs_sums(const struct rna_coord_seq*seq_1,
		       const struct rna_coord_seq*seq_2)
{
    struct size_t_pair sums={0, 0};
    struct int_array*str_1=NULL, *str_2=NULL;
    const int min_letter=0;
    int sentinel = max_int(int_array_max(seq_1->seq),
 			   int_array_max(seq_2->seq)) + 1;
    if(!(rna_coord_seq_size(seq_1) && rna_coord_seq_size(seq_2))) {
	return sums;
    }
    str_1 = rna_coord_seq_get_sequence_with_sentinels(&sentinel, seq_1);
    str_2 = rna_coord_seq_get_sequence_with_sentinels(&sentinel, seq_2);
    lcs_sums(&sums, min_letter, str_1, str_2);
    int_array_destroy(str_1);
    int_array_destroy(str_2);
    return sums;
}

static int
rna_coord_seq_weighted_lcs_scores_inner(struct float_pair*sums,
					const struct rna_coord_seq*seq_1,
					const struct rna_coord_seq*seq_2,
					enum lcs_logp_score_type score_type,
					const struct float_pair*score_function_params)
{
    const char*this_sub="rna_coord_seq_weighted_lcs_scores_inner";
    int err=1;
    struct int_array*str_1=NULL, *str_2=NULL;
    const int min_letter=0;
    int sentinel = max_int(int_array_max(seq_1->seq),
 			   int_array_max(seq_2->seq)) + 1;
    sums->first = sums->second = 0.0;
    if(!(rna_coord_seq_size(seq_1) && rna_coord_seq_size(seq_2))) {
	return 0;
    }
    str_1 = rna_coord_seq_get_sequence_with_sentinels(&sentinel, seq_1);
    str_2 = rna_coord_seq_get_sequence_with_sentinels(&sentinel, seq_2);
    err = lcs_log_p_scores(sums,
			   min_letter,
			   str_1,
			   str_2,
			   seq_1->log_p_sum,
			   seq_2->log_p_sum,
			   seq_1->log_n_pieces,
			   seq_2->log_n_pieces,
			   score_type,
			   score_function_params);
    int_array_destroy(str_1);
    int_array_destroy(str_2);
    if(err) {
	err_printf(this_sub, "failed\n");
    }
    return err;    
}

struct float_pair
rna_coord_seq_weighted_lcs_sums(const struct rna_coord_seq*seq_1,
				const struct rna_coord_seq*seq_2)
{
    struct float_pair sums={0.0, 0.0};
    rna_coord_seq_weighted_lcs_scores_inner(&sums, seq_1, seq_2, LOGP_SUM, NULL);
    return sums;
}

struct float_pair
rna_coord_seq_logistic_weighted_lcs_sums(const struct rna_coord_seq*seq_1,
					 const struct rna_coord_seq*seq_2,
					 float x_0, float k)
{
    struct float_pair params_logistic_function={x_0, k};
    struct float_pair sums={0.0, 0.0};
    rna_coord_seq_weighted_lcs_scores_inner(&sums, seq_1, seq_2, LOGP_LOGISTIC_SUM,
					    &params_logistic_function);
    return sums;    
}

struct float_pair
rna_coord_seq_weighted_lcs_max(const struct rna_coord_seq*seq_1,
			       const struct rna_coord_seq*seq_2)
{
    struct float_pair sums={0.0, 0.0};
    rna_coord_seq_weighted_lcs_scores_inner(&sums, seq_1, seq_2, LOGP_MAX, NULL);
    return sums;
}

static int
rna_coord_seq_write_fasta(FILE*f_out,
			  const char*header,
			  const struct rna_coord_seq*seq)
{
    const char*this_sub = "rna_coord_seq_write_fasta";
    size_t i, seqlen;
    const int*str; 
    const size_t n_lines_2_break=50;
    if(seq->chain_break_char != -1) {
	err_printf(this_sub,
		   "Chain break character has to be -1 but is %i\n",
		   seq->chain_break_char);
	return 1;
    }
    str = seq->seq->vals;
    seqlen = rna_coord_seq_size(seq);
    mfprintf(f_out, ">%s\n", header);
    for(i = 0; i < seqlen; ++i) {
	mfprintf(f_out, "%c", RNA_ALPHABET_CHARACTERS[str[i] + 1]);
	/* TODO: is there a maximum length required for muscle */
	if((i + 1) % n_lines_2_break == 0) {
	    mfprintf(f_out, "\n");
	}
    }
    if(i % n_lines_2_break) {
	mfprintf(f_out, "\n");
    }
    return 0;
}

static int
rna_coord_seq_write_2_fasta_file(const char*fname,
				 const char*write_mode,
				 const char*header,
				 const struct rna_coord_seq*seq)
{
    const char*this_sub="rna_coord_seq_write_2_fasta_file";
    int err;
    FILE *f_out=NULL;
    f_out = mfopen(fname, write_mode, this_sub);
    if(!f_out) {
	return 1;
    }
    err = rna_coord_seq_write_fasta(f_out, header, seq);
    close_if_open_file(f_out);
    return err;
}

int
rna_coord_seq_write_to_fasta_file(const char*fname,
				  const char*header,
				  const struct rna_coord_seq*seq)
{
    return rna_coord_seq_write_2_fasta_file(fname, "w", header, seq);
}

int
rna_coord_seq_append_to_fasta_file(const char*fname,
				   const char*header,
				   const struct rna_coord_seq*seq)
{
    return rna_coord_seq_write_2_fasta_file(fname, "a", header, seq);
}

struct pair_set*
rna_coord_seq_full_backmap_pair_set(const struct rna_coord_seq*seq1,
				    const struct rna_coord_seq*seq2,
				    const struct pair_set*pset_align)
{
    struct pair_set*pset_mapped=NULL;
    size_t k;
    int i, j, pi, pj;
    pset_mapped = pair_set_new(pset_align->n);
    if(!pset_mapped) {
	return NULL;
    }
    for(k = 0; k < pset_align->n; ++k) {
	i = pset_align->indices[k][0];
	j = pset_align->indices[k][1];
	if((i == GAP_INDEX) || (j == GAP_INDEX)) {
	    continue;
	}
	pi = seq1->coord_positions->vals[i];
	pj = seq2->coord_positions->vals[j];
	if((pi < 0) || (pj < 0)) {
	    continue;
	}
	pair_set_add_pair(pset_mapped, pi, pj);
    }
    return pset_mapped;
}

size_t
rna_coord_seq_nobreakchar_len(const struct rna_coord_seq*seq)
{
    size_t i, len=0;
    for(i = 0; i < seq->seq->size; ++i) {
	if(!rna_coord_seq_is_break_char(seq, i)) {
	    len++;
	}
    }
    return len;
}

static struct rna_coord_seq*
rna_coord_seq_allocate_sim_seq(size_t size)
{
    struct rna_coord_seq*seq=NULL;
    seq = empty_rna_coord_seq();
    seq->type = RNA_COORD_SEQ_SIMULATED;
    seq->seq = new_int_array_initialized(size,
					 seq->chain_break_char);
    return seq;
}

static size_t
choice(size_t n_options, const float*cumu_probs)
{
    size_t choice;
    float p = draw_random_float();
    choice = bin_search_cumulative_probability_c_arr(p,
						     n_options,
						     cumu_probs);
    return choice;
}

static void
inplace_cumulative(size_t size, float*arr)
{
    size_t i;
    for(i = 1; i < size; ++i) {
	arr[i] = arr[i] + arr[i-1];
    }
}

static void
inplace_cumulative_array(struct float_array*arr)
{
    inplace_cumulative(arr->size, arr->vals);
}

static void
inplace_cumulative_matrix(struct float_matrix*mat)
{
    size_t i;
    for(i = 0; i < mat->n_rows; ++i) {
	inplace_cumulative(mat->n_cols, mat->vals[i]);
    }
}

static void
seq_sim(struct rna_coord_seq*sim_seq,
	const struct float_array*cumu_priors,
	const struct float_matrix*cumu_conditionals)
{
    size_t i;
    sim_seq->seq->vals[0] = (int) choice(cumu_priors->size,
					 cumu_priors->vals);
    for(i = 1; i < sim_seq->seq->size; ++i) {
	sim_seq->seq->vals[i] = (int) choice(
	    cumu_conditionals->n_cols,
	    cumu_conditionals->vals[sim_seq->seq->vals[i - 1]]);
    }
}

static void		  /* Chain break char has to be -1 */
shift_back_chain_break(struct rna_coord_seq*sim_seq)
{
    size_t i;
    for(i = 0; i < sim_seq->seq->size; ++i) {
	sim_seq->seq->vals[i] -= 1;
    }
}

struct rna_coord_seq*
rna_coord_seq_simulate(size_t seqlen,
		       const struct float_array*priors,
		       const struct float_matrix*conditionals,
		       enum yes_no with_chain_breaks) /* optionally add logp values? */
{
    struct float_array*cumu_priors=NULL;
    struct float_matrix*cumu_conditionals=NULL;
    struct rna_coord_seq*sim_seq=NULL;

    sim_seq = rna_coord_seq_allocate_sim_seq(seqlen);
    if(sim_seq){
	cumu_priors = float_array_copy(priors);
    }
    if(cumu_priors) {
	inplace_cumulative_array(cumu_priors);
	cumu_conditionals = float_matrix_copy(conditionals);
    }
    if(cumu_conditionals) {
	inplace_cumulative_matrix(cumu_conditionals);
	seq_sim(sim_seq, cumu_priors, cumu_conditionals);
	if(with_chain_breaks) {
	    shift_back_chain_break(sim_seq);
	}
    }
    float_array_destroy(cumu_priors);
    float_matrix_destroy(cumu_conditionals);
    return sim_seq;
}
