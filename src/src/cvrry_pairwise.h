#ifndef CVRRY_PAIRWISE_H
#define CVRRY_PAIRWISE_H

#include <stdio.h>
#include "rna_comparison.h"
#include "chainlist.h"

struct cvrry_pairwise_parameters {
    struct norm_params *frag_norm;
    struct feat_weights *weights;
    struct rna_align_params params;
    struct rna_cmp_opts cmp_opts;
};

struct cvrry_pairwise_paths {
    char *fname_dataset_file;
    char *path_cvrry_db;
};

struct cvrry_pairwise_streams {
    FILE *metrics;
    FILE *alignments;
};

enum comparison_type {
    ALL_VS_ALL,
    ONE_VS_ALL
};

struct cvrry_pairwise_options {
    struct cvrry_pairwise_parameters align_cmp_pars;
    struct cvrry_pairwise_paths paths;
    struct cvrry_pairwise_streams streams;
    enum comparison_type cmp_type;
};

struct cvrry_pairwise_options*
cvrry_pairwise_options_new();

int
get_cvrry_pairwise_options(struct cvrry_pairwise_options *opt,
			   int argc,
			   char *argv[]);

void
cvrry_pairwise_options_destroy(struct cvrry_pairwise_options *opt);

int
one_vs_all_align(struct cvrry_pairwise_options*opt,
		 const struct chain_description *ch,
		 const struct chain_list *chainlist,
		 size_t start_i);

int
all_pairs_align(struct cvrry_pairwise_options*opt,
		const struct chain_list *chainlist);

int
alignlist_align(struct cvrry_pairwise_options*opt,
		const struct align_list *alignlist);

#endif
