#include "rna_matchfiles.h"
#include "fio.h"
#include "lsqf.h"

static void
rna_res_id_stream(FILE*stream, const struct rna_res_id*res)
{
    mfprintf(stream, "%i%c", res->resnum, res->icode);
}

int
write_match_chimera_script(const char*fn_chim_script,
			   const char*fn_pdb_1,
			   const char*fn_pdb_2,
			   const struct rna_coord*rna_1,
			   const struct rna_coord*rna_2,
			   const struct continuous_match *match)
{
    const char*this_sub="write_match_chimera_script";
    FILE*outfile=NULL;
    int err=0;
    size_t lastpos_shift = match->len ? match->len - 1 : 0;
    outfile = mfopen(fn_chim_script, "w", this_sub);
    err = !outfile;
    if(!err) {
	mfprintf(outfile, "open %s\nopen %s\n", fn_pdb_1, fn_pdb_2);
	mfprintf(outfile, "background solid white\n");
	mfprintf(outfile, "~show\nribbon\n");
	mfprintf(outfile, "color darkgrey #0\ncolor yellow #1\n");
	mfprintf(outfile, "color purple #0:");
	rna_res_id_stream(outfile, rna_1->res + match->matchpos_a);
    	mfprintf(outfile, "-");
	rna_res_id_stream(outfile, rna_1->res + match->matchpos_a + lastpos_shift);
    	mfprintf(outfile, "\n");
	mfprintf(outfile, "color red #1:");
	rna_res_id_stream(outfile, rna_2->res + match->matchpos_b);
	mfprintf(outfile, "-");
	rna_res_id_stream(outfile, rna_2->res + match->matchpos_b + lastpos_shift);
	mfprintf(outfile, "\n");
    }
    if(outfile) {
	fclose(outfile);
    }
    return err;
}

static void
write_sequence(FILE*outfile,
	       const struct int_array*str,
	       size_t matchborder_lo,
	       size_t matchborder_hi)
{
    const char*border_marker="|| ";
    const size_t newline_at=25;
    size_t i;
    mfprintf(outfile, ">\n");
    for(i = 0; i < str->size; ++i) {
	if(i == matchborder_lo) {
	    mfprintf(outfile, "%s", border_marker);
	}
	mfprintf(outfile, "%i ", str->vals[i]);
	if(i + 1 == matchborder_hi) {
	    mfprintf(outfile, "%s", border_marker);
	}
	if(!((i+1) % newline_at)) {
	    mfprintf(outfile, "\n");
	}
    }
    if(i % newline_at) {
	mfprintf(outfile, "\n");
    }
}

static void
write_match_sequences(FILE *f_out,
		      const struct continuous_match*match,
		      const struct int_array*str_1,
		      const struct int_array*str_2)
{
    write_sequence(f_out, str_1, match->matchpos_a, match->matchpos_a + match->len);
    write_sequence(f_out, str_2, match->matchpos_b, match->matchpos_b + match->len);
}

int
write_match_sequences_file(const char*fname,
			   const struct continuous_match*match,
			   const struct int_array*str_1,
			   const struct int_array*str_2)
{
    const char*this_sub="write_match_sequences_file";
    FILE *f_out=mfopen(fname, "w", this_sub);
    if(f_out) {
	write_match_sequences(f_out, match, str_1, str_2);
	close_if_open_file(f_out);
	return 0;
    }
    return 1;
}

int
write_rna_coord_seq_match_sequences_file(const char*fname,
					 const struct continuous_match*match,
					 const struct rna_coord_seq*seq_1,
					 const struct rna_coord_seq*seq_2)
{
    const char*this_sub="write_rna_coord_match_sequences_file";
    int match_i, match_j, err=0;
    struct continuous_match match_raw;    

    if(!match->len) {
	match_raw.len = 0;
    } else if(match->len < seq_1->fraglen) {
	err_printf(this_sub,
		   "match (%zu) shorter than fragment length (%zu)\n",
		   match->len, seq_1->fraglen);
	return 1;
    } else {
	match_raw.len = match->len - seq_1->fraglen + 1;
    }
    
    match_i = size_t_array_get_i(seq_1->positions, match->matchpos_a);
    if(match_i < 0) {
	err_printf(this_sub,
		   "Did not find match_pos (%zu) in the positions array\n",
		   match->matchpos_a);
	return 1;
    }
    match_raw.matchpos_a = (size_t) match_i;

    match_j = size_t_array_get_i(seq_2->positions, match->matchpos_b);
    if(match_j < 0) {
	err_printf(this_sub,
		   "Did not find match_pos (%zu) in the positions array\n",
		   match->matchpos_b);
	return 1;
    }
    match_raw.matchpos_b = (size_t) match_j;

    err = write_match_sequences_file(fname, &match_raw, seq_1->seq, seq_2->seq);

    return err;
}

int
write_match_pdb_files(const char*fname_1,
		      const char*fname_2,
		      const struct continuous_match*match,
		      const struct rna_coord*rna_1,
		      const struct rna_coord*rna_2,
		      size_t fraglen)
{
    const char*this_sub="write_match_pdb_files";
    struct transformation transform;
    struct rna_coord*rna_1_transformed=NULL;
    float rmsd;
    int err=rna_coord_rmsd_continuous_match(&transform,
					    &rmsd,
					    *match,
					    rna_1,
					    rna_2,
					    fraglen);
    if(!err) {
	rna_1_transformed = rna_coord_transform_superposed(rna_1, &transform);
	err = !rna_1_transformed;
    }
    if(!err) {
	rna_coord_2_pdb(rna_1_transformed, fname_1);
	rna_coord_2_pdb(rna_2, fname_2);
    }
    if(err) {
	err_printf(this_sub, "failed");
    }
    rna_coord_destroy(rna_1_transformed);
    return err;
}
