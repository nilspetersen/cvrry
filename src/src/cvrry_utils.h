#ifndef CVRRY_UTILS
#define CVRRY_UTILS

#include <stdlib.h>
#include "chainlist.h"

size_t
posdiff(size_t a, size_t b);

/* string functions */

char*
copy_string(char *str);

void
remove_final_slash(char*dirname);

char*
no_final_slash(char *dirname);

char*
merge_paths(const char*prefix,
	    const char*suffix);

size_t
strlen_int(int integer);

char*
cif_make_outfilename(const char*fn_in, const char*dir_out);

#endif
