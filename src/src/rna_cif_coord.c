#include "rna_cif_coord.h"
#include "fio.h"

static struct rna_cif_coord*
empty_rna_cif_coord()
{
    struct rna_cif_coord*rna=NULL;
    rna = E_MALLOC(sizeof(*rna));
    rna->cif = NULL;
    rna->rna_backbone = NULL;
    return rna;
}

void
rna_cif_coord_destroy(struct rna_cif_coord*cif_coord)
{
    if(cif_coord) {
	rna_coord_destroy(cif_coord->rna_backbone);
	cvrry_cif_destroy(cif_coord->cif);
	free(cif_coord);
    }
}

struct rna_cif_coord*
rna_cif_coord_read_cif(const char*filename,
		       const char*chain_id,
		       size_t model_number)
{
    int err=0;
    struct rna_cif_coord*cif_coord=NULL;
    cif_coord = empty_rna_cif_coord();
    err = read_mmcif(&cif_coord->cif,
		     &cif_coord->rna_backbone,
		     filename,
		     chain_id,
		     model_number);
    if(!err) {
	compute_chain_breaks(cif_coord->rna_backbone);
    }
    if(err) {
	rna_cif_coord_destroy(cif_coord);
	return NULL;
    }
    return cif_coord;
}

static struct rna_cif_coord*
rna_cif_coord_read_binary(FILE *infile)
{
    struct rna_cif_coord*cif_coord=NULL;
    cif_coord = empty_rna_cif_coord();
    cif_coord->cif = cvrry_cif_read_binary(infile);
    if(cif_coord->cif) {
	cif_coord->rna_backbone = rna_coord_read_binary(infile);
    }
    if(!cif_coord->rna_backbone) {
	rna_cif_coord_destroy(cif_coord);
    }
    return cif_coord;
}

struct rna_cif_coord*
rna_cif_coord_read_binary_file(const char*fname)
{
    const char*this_sub="rna_cif_coord_read_binary_file";
    struct rna_cif_coord*cif_coord=NULL;
    FILE*infile=NULL;
    infile = mfopen(fname, "rb", this_sub);
    if(!infile) {
	return NULL;
    }
    cif_coord = rna_cif_coord_read_binary(infile);
    fclose(infile);
    return cif_coord;
}

static int
rna_cif_coord_write_binary(FILE*outfile,
			   const struct rna_cif_coord*cif_coord)
{
    int err=0;
    err = cvrry_cif_write_binary(outfile, cif_coord->cif);
    if(!err) {
	err = rna_coord_write_binary(outfile, cif_coord->rna_backbone);
    }
    return err;
}

int
rna_cif_coord_write_binary_file(const char*fname,
				const struct rna_cif_coord*cif_coord)
{
    const char*this_sub="rna_cif_coord_write_binary_file";
    FILE*outfile;
    int err = 0;
    outfile = mfopen(fname, "wb", this_sub);
    if(!outfile) {
	return 1;
    }
    err = rna_cif_coord_write_binary(outfile, cif_coord);
    fclose(outfile);
    return err;
}

const struct rna_coord*
rna_cif_coord_get_rna_backbone(const struct rna_cif_coord*cif_coord)
{
    return cif_coord->rna_backbone;
}
