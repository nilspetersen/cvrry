/*
 * 12 Sep 2001
 * Define a score matrix. 
 */

#ifndef SCORE_MAT_H
#define SCORE_MAT_H

struct score_mat {
    float **mat;
    size_t n_rows, n_cols;
};

struct score_mat*
score_mat_new(const size_t n_rows, const size_t n_cols);

struct score_mat*
score_mat_copy(const struct score_mat*old_mat);

void
score_mat_info(const struct score_mat *score_mat,
	       float *min,
	       float *max,
	       float *av,
	       float *std_dev);

void
score_mat_destroy(struct score_mat *smat);

struct score_mat*
score_mat_shift(struct score_mat *mat1, const float shift);

void
dump_score_mat(const struct score_mat*smat);

#endif /* SCORE_MAT_H */
