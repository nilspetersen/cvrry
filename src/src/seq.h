#ifndef SEQ_H
#define SEQ_H

#include <stdlib.h>
#include "yesno.h"

struct seq {
    char *seq;      /* Real sequence, with newlines removed. */
    char *comment;  /* FASTA comment, with newlines and leading ">" removed */
    size_t length;  /* Length of sequence, not including null terminator */
};

void
seq_destroy(struct seq *s);

enum yes_no
seq_same(const struct seq *s1, const struct seq *s2);

#endif
