#ifndef CONTINUOUS_MATCH_H
#define CONTINUOUS_MATCH_H

#include "array.h"

struct continuous_match {
    size_t len;
    size_t matchpos_a;
    size_t matchpos_b;	 
};

void
dump_match(struct continuous_match *match,
	   const struct int_array *seq_a,
	   const struct int_array *seq_b);

void
continuous_match_destroy(struct continuous_match*match);

struct continuous_match*
new_continuous_match();

struct continuous_match*
new_continuous_match_initialized(size_t len, size_t matchpos_a, size_t matchpos_b);

void
continuous_match_cpy(struct continuous_match *dest,
		     const struct continuous_match *src);

#endif
