#include "rna_multalign_scores.h"
#include "cmp_dmat.h"
#include "rna_alphabet.h"
#include "rna_multialign.h"
#include "lsqf.h"
#include "cvrry_utils.h"

static struct pair_set*
rna_multialign_get_matches_pairset(const struct pair_set*pset_multi,
				   const struct rna_coord_seq*seq1,
				   const struct rna_coord_seq*seq2,
				   size_t msa_index_seq1,
				   size_t msa_index_seq2)
{
    size_t i, iii, ii, jj;
    size_t fragment_center_shift = seq1->fraglen / 2;
    struct pair_set*pset_nogaps = NULL;
    pset_nogaps = pairset_without_gaps_from_multi(pset_multi,
						  msa_index_seq1,
						  msa_index_seq2);
    for(i = 0, iii = 0; i < pset_nogaps->n; ++i) {
	/* exclude chain break characters */
	ii = pset_nogaps->indices[i][0];
	jj = pset_nogaps->indices[i][1];
	if((seq1->seq->vals[ii] == seq1->chain_break_char ||
	    seq2->seq->vals[jj] == seq2->chain_break_char)) {
	    continue;
	}
	/* replace the index using the positions from the rna_coord_seq structures */
	pset_nogaps->indices[iii][0] = seq1->positions->vals[ii] + fragment_center_shift;
	pset_nogaps->indices[iii][1] = seq2->positions->vals[jj] + fragment_center_shift;
	iii++;
    }
    return pset_nogaps;
}

float
rna_multalign_frac_dme_one_pair(const struct pair_set *pset_multi,
				const struct rna_coord_seq*seq1,
				const struct rna_coord_seq*seq2,
				const struct rna_coord*rna1,
				const struct rna_coord*rna2,
				size_t msa_index_seq1,
				size_t msa_index_seq2,
				float thresh,
				enum dme_norm_type normtype)
{
    float frac_dme = 0.0;
    struct pair_set*positions_matches=NULL;
    positions_matches = rna_multialign_get_matches_pairset(pset_multi,
							   seq1,
							   seq2,
							   msa_index_seq1,
							   msa_index_seq2);
    if(positions_matches->n) {
	frac_dme = dme_thresh_rna(positions_matches,
				  rna1,
				  rna2,
				  thresh);
    }
    switch(normtype) {
	size_t len1, len2, minlen;
	float coverage;
    case DME_NORM_COVERAGE:
	len1 = rna_coord_seq_size_nobreaks(seq1);
	len2 = rna_coord_seq_size_nobreaks(seq2);
	minlen = len1 < len2 ? len1 : len2;
	if(minlen == 0) {
	    frac_dme = 0.0;
	    break;
	}
	coverage = ((float) positions_matches->n) / ((float) minlen);
	frac_dme *= coverage;
	break;
    case DME_NORM_NONORM:
    default:
	break;
    }
    pair_set_destroy(positions_matches);
    return frac_dme;
}

float
rna_multalign_all_pairs_avg_frac_dme(struct rna_multialign*multialign,
				     float thresh,
				     enum dme_norm_type normtype)
{
    size_t i, j;
    size_t nchains=multialign->n_seqs;
    float avg_frac_dme=0.0;
    for(i = 0; i < nchains; ++i) {
	for(j = i + 1; j < nchains; ++j) {
	    avg_frac_dme += rna_multalign_frac_dme_one_pair(multialign->pset_multi,
							    multialign->seqs[i],
							    multialign->seqs[j],
							    multialign->coords[i],
							    multialign->coords[j],
							    i,
							    j,
							    thresh,
							    normtype);
	}
    }
    avg_frac_dme /= (float) (nchains * (nchains - 1) / 2);
    return avg_frac_dme;
}

/* static size_t */
/* rna_multalign_count_matches(const struct pair_set*pset_multi, */
/* 			    const struct rna_coord_seq*seq1, */
/* 			    const struct rna_coord_seq*seq2, */
/* 			    size_t msa_index_seq1, */
/* 			    size_t msa_index_seq2) */
/* { */
/*     size_t i, match_counter=0; */
/*     for(i = 0; i < pset_multi->n; ++i) { */
/* 	int ii = pset_multi->indices[i][msa_index_seq1]; */
/* 	int jj = pset_multi->indices[i][msa_index_seq2]; */
/* 	if((ii == GAP_INDEX) || (jj == GAP_INDEX)) { */
/* 	    continue; */
/* 	} */
/* 	if((seq1->seq->vals[ii] == seq1->chain_break_char || */
/* 	    seq2->seq->vals[jj] == seq2->chain_break_char)) { */
/* 	    continue; */
/* 	} */
/* 	if(seq1->seq->vals[ii] == seq2->seq->vals[jj]) { */
/* 	    match_counter++; */
/* 	} */
/*     } */
/*     return match_counter; */
/* } */

/* size_t 				/\* TODO: make static, but use find_center 2 ! *\/ */
/* rna_multalign_find_center_old(const struct rna_multialign*multialign) */
/* { */
/*     struct size_t_array *match_counts=NULL; */
/*     size_t i, j, nchains, argmax_matches; */
/*     nchains = multialign->n_seqs; */
/*     match_counts = new_size_t_array_initialized(nchains, 0); */
/*     for(i = 0; i < nchains; ++i) { */
/* 	for(j = i+1; j < nchains; ++j) { */
/* 	    size_t count = rna_multalign_count_matches(multialign->pset_multi, */
/* 						       multialign->seqs[i], */
/* 						       multialign->seqs[j], */
/* 						       i, */
/* 						       j); */
/* 	    match_counts->vals[i] += count; */
/* 	    match_counts->vals[j] += count; */
/* 	} */
/*     } */
/*     argmax_matches = size_t_array_argmax(match_counts); */
/*     size_t_array_destroy(match_counts); */
/*     return argmax_matches; */
/* } */

static void
rna_multialign_count_letters_in_column(struct size_t_array*letter_counters,
				       const struct rna_multialign*multialign,
				       size_t col_i)
{
    size_t i;
    int j, letter;
    size_t_array_set_all(letter_counters, 0);
    for(i = 0; i < multialign->n_seqs; ++i) {
	j = multialign->pset_multi->indices[col_i][i];
	if(j == GAP_INDEX) {
	    continue;
	}
	letter = multialign->seqs[i]->seq->vals[j];
	if(letter == multialign->seqs[i]->chain_break_char) {
	    continue;
	}
	letter_counters->vals[letter]++;
    }
}

static float
rna_multialign_matches_frac_within_column(const struct size_t_array*letter_counters,
					  float normalizer)
{
    size_t i, n_matches=0;
    for(i = 0; i < letter_counters->size; ++i) {
	size_t n = letter_counters->vals[i];
	if(n > 1) {
	    n_matches += n * (n - 1) / 2;
	}
    }
    return (float) n_matches / normalizer;
}

float
rna_multialign_frac_matches(const struct rna_multialign*multialign)
{
    float frac_matches = 0.0;
    size_t n_chains, i, align_len = multialign->pset_multi->n;
    struct size_t_array*letter_counters = NULL;
    float normalizer;
    n_chains = multialign->pset_multi->m;
    if(n_chains <= 1) {
	return 1.0;
    }
    normalizer = (float) (n_chains * (n_chains - 1) / 2);
    letter_counters = new_size_t_array(multialign->seqs[0]->alphabet_size);
    for(i = 0; i < align_len; ++i) {
	rna_multialign_count_letters_in_column(letter_counters, multialign, i);
	frac_matches += rna_multialign_matches_frac_within_column(letter_counters, normalizer);
    }
    size_t_array_destroy(letter_counters);
    return frac_matches / (float) align_len;
}

static void
rna_multialign_add_column_matches(struct size_t_array*match_counters,
				  const struct size_t_array*letter_counters,
				  const struct rna_multialign*multialign,
				  size_t col_i)
{
    size_t i;
    int j, letter;
    for(i = 0; i < match_counters->size; ++i) {
	j = multialign->pset_multi->indices[col_i][i];
	if(j == GAP_INDEX) {
	    continue;
	}
	letter = multialign->seqs[i]->seq->vals[j];
	if(letter == multialign->seqs[i]->chain_break_char) {
	    continue;
	}
	match_counters->vals[i] += letter_counters->vals[letter];
    }
}

static size_t
rna_multalign_find_center(const struct rna_multialign*multialign)
{
    size_t i, center;
    struct size_t_array*letter_counters=NULL;
    struct size_t_array*match_counters=NULL;
    letter_counters = new_size_t_array(multialign->seqs[0]->alphabet_size);
    match_counters = new_size_t_array_initialized(multialign->n_seqs, 0);
    for(i = 0; i < multialign->pset_multi->n; ++i) {
	rna_multialign_count_letters_in_column(letter_counters,
					       multialign, i);
	rna_multialign_add_column_matches(match_counters, letter_counters, multialign, i);
    }
    center = size_t_array_argmax(match_counters);
    size_t_array_destroy(letter_counters);
    size_t_array_destroy(match_counters);
    return center;
}

static int
rna_multalign_sup_to_file(const char*fn_out,
			  const struct rna_multialign*multialign,
			  size_t src_i,
			  size_t target_i)
{
    int err=1;
    struct pair_set*nogaps_pset=NULL;
    struct rna_coord*src=NULL;
    float dummy_rmsd;
    struct transformation transform;
    if(src_i == target_i) {
	err = rna_coord_2_pdb(multialign->coords[src_i], fn_out);
	return err;
    }
    nogaps_pset = rna_multialign_get_matches_pairset(multialign->pset_multi,
						     multialign->seqs[src_i],
						     multialign->seqs[target_i],
						     src_i,
						     target_i);
    if(nogaps_pset) {
	err = rna_coord_rmsd(&transform,
			     &dummy_rmsd,
			     nogaps_pset,
			     multialign->coords[src_i],
			     multialign->coords[target_i]);
    }
    if(!err) {
	src = rna_coord_transform_superposed(multialign->coords[src_i], &transform);
	err = !src;
    }
    if(!err) {
	err = rna_coord_2_pdb(src, fn_out);
    }
    rna_coord_destroy(src);
    pair_set_destroy(nogaps_pset);
    return err;
}

int
rna_multialign_write_to_files(const struct rna_multialign*multialign,
			      const char*path_out)
{
    int err=0;
    size_t i;
    size_t center_i;
    char *fn_out_template=NULL, *fn_out=NULL;
    fn_out_template = merge_paths(path_out, "chain_%zu.pdb");
    fn_out = E_MALLOC((strlen(fn_out_template) + 1) * sizeof(*fn_out));
    center_i = rna_multalign_find_center(multialign);
    for(i = 0; !err && i < multialign->n_coords; ++i) {
	sprintf(fn_out, fn_out_template, i);
	err = rna_multalign_sup_to_file(fn_out,
					multialign,
					i,
					center_i);
    }
    free_if_not_null(fn_out);
    free_if_not_null(fn_out_template);
    return err;
}

static float
rna_multalign_pairwise_rmsd(const struct rna_multialign*multialign,
			    size_t src_i,
			    size_t target_i)
{
    float rmsd=0.0;
    struct pair_set*nogaps_pset=NULL;
    if(src_i == target_i) {
	return 0.0;
    }
    nogaps_pset = rna_multialign_get_matches_pairset(multialign->pset_multi,
						     multialign->seqs[src_i],
						     multialign->seqs[target_i],
						     src_i,
						     target_i);
    if(nogaps_pset) {
	if(nogaps_pset->n > 0) {
	    rna_coord_rmsd(NULL,
			   &rmsd,
			   nogaps_pset,
			   multialign->coords[src_i],
			   multialign->coords[target_i]);
	}
    }
    pair_set_destroy(nogaps_pset);
    return rmsd;
}

float
rna_multalign_all_pairs_avg_rmsd(struct rna_multialign*multialign)
{
    size_t i, j;
    float avg_rmsd=0.0;
    for(i = 0; i < multialign->n_coords; ++i) {
	for(j = i + 1; j < multialign->n_coords; ++j) {
	    avg_rmsd += rna_multalign_pairwise_rmsd(multialign, i, j);
	}
    }
    if(multialign->n_coords > 1) {
	avg_rmsd /= (float) (i*(i-1) / 2);
    }
    return avg_rmsd;
}

