#ifndef RNA_SEQ_COORD_H
#define RNA_SEQ_COORD_H

#include "cvrry_parse_mmcif.h"
#include "rna_alphabet.h"

struct rna_seq_coord {
    struct cvrry_cif *cif;
    struct rna_coord *rna_backbone;
    struct rna_coord_seq *seq;
};

void
rna_seq_coord_destroy(struct rna_seq_coord*seq_coord);

struct rna_seq_coord*
read_n_compute_rna_seq_coord(const char*mmcif_filename,
			     const char*chain_id,
			     size_t model_i,
			     const struct rna_coord_alphabet*alphabet);

int
rna_seq_coord_write_binary(const struct rna_seq_coord*seq_coord,
			   const char*fname_out);

struct rna_seq_coord*
rna_seq_coord_read_binary(const char*fname);

void
rna_seq_coord_dump(const struct rna_seq_coord*seq_coord);

int
rna_seq_coord_fill_missing_characters(struct rna_seq_coord*seq_coord,
				      enum yes_no add_tail);

size_t
rna_seq_coord_scoremat_dim(const struct rna_seq_coord*seq_coord);

size_t
rna_seq_coord_seqlen(const struct rna_seq_coord*seq_coord);

int
rna_seq_coord_add_basepair_info(struct rna_seq_coord*seq_coord);

#endif
