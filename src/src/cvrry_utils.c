#include "cvrry_utils.h"
#include <string.h>
#include "e_malloc.h"

size_t
posdiff(size_t a, size_t b)
{
    if(a < b) {
	return 0;
    }
    return a - b;
}

/* string functions */

char*
copy_string(char *str)
{
    char *copy = E_MALLOC(strlen(str) + 1);
    strcpy(copy, str);
    return copy;
}

void
remove_final_slash(char*dirname)
{
    size_t lastpos = strlen(dirname) - 1; 
    if(dirname[lastpos] == '/') {
	dirname[lastpos] = '\0';
    }
}

char*
no_final_slash(char *dirname)
{
    char *new_dirname = copy_string(dirname);
    remove_final_slash(new_dirname);
    return new_dirname;
}

char*
merge_paths(const char*prefix,
	    const char*suffix)
{
    size_t lprefix;
    char*new_path=NULL;
    lprefix = strlen(prefix);
    if(prefix[lprefix-1] == '/') {
	lprefix--;
    }
    new_path = E_MALLOC((lprefix + strlen(suffix) + 2) * sizeof(*new_path));
    strncpy(new_path, prefix, lprefix);
    new_path[lprefix] = '/';
    strcpy(new_path + lprefix + 1, suffix);
    return new_path;
}

size_t
strlen_int(int integer)
{
    size_t size = 1;
    int i;
    if(integer < 0) {
	size += 1;
	integer *= -1;
    }
    for(i = 10; i <= integer; i *= 10) {
	size += 1;
    }
    return size;
}

static int
str_find_last_occurrence(const char *str, size_t strlen, char c)
{
    int i;
    for(i = strlen; i > -1 && str[i] != c; --i);
    return i;
}

char*
cif_make_outfilename(const char*fn_in, const char*dir_out)
{
    int from, len_prefix;
    size_t len_fn_in = strlen(fn_in);
    char*fn_out = E_MALLOC((strlen(dir_out) + len_fn_in + 16) * sizeof(*fn_out));
    strcpy(fn_out, dir_out);
    if(fn_out[strlen(fn_out) - 1] != '/') {
	strcat(fn_out, "/");
    }
    from = str_find_last_occurrence(fn_in, len_fn_in, '/') + 1;
    len_prefix = str_find_last_occurrence(fn_in + from, len_fn_in - from, '.');
    strncat(fn_out, fn_in + from, (size_t) len_prefix);
    strcat(fn_out, ".cif");
    return fn_out;
}
