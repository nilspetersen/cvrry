#ifndef RNA_ALPHABET_STATS_H
#define RNA_ALPHABET_STATS_H

#include "rna_alphabet.h"


void
rna_coord_seq_pairs_count_no_break_chars(int*count_arr,
					 const struct rna_coord_seq*seq);

void
rna_coord_seq_pairs_count_with_break_chars(int*count_arr,
					   const struct rna_coord_seq*seq);

int
rna_coord_seq_nchar_pairs(const struct rna_coord_seq*seq,
			  int with_break_chars);


#endif
