#include "rna_coord_stringset.h"
#include "rna_alphabet.h"

struct rna_coord_stringset*
make_rna_coord_stringset(const struct rna_coord_set*chains,
			 size_t fraglen)
{
    size_t i, chain_counter=0;
    struct rna_coord_stringset*stringset = NULL;
    stringset = E_MALLOC(sizeof(*stringset));
    stringset->size = chains->n_chains;
    stringset->strings = E_MALLOC(stringset->size * sizeof(*stringset->strings));
    for(i = 0; i < stringset->size; ++i) {
	if(chains->chains[i]->size >= fraglen) {
	    stringset->strings[chain_counter] = new_int_array(chains->chains[i]->size - fraglen + 1);
	    chain_counter++;
	}
    }
    stringset->size = chain_counter;
    return stringset;
}

void
rna_coord_stringset_destroy(struct rna_coord_stringset*stringset)
{
    size_t i;
    if(stringset) {
	for(i = 0; i < stringset->size; ++i) {
	    int_array_destroy(stringset->strings[i]);
	}
	free_if_not_null(stringset->strings);
	free(stringset);
    }
}

const struct int_array*
rna_coord_stringset_get_string(const struct rna_coord_stringset*stringset,
			       size_t i)
{
    return stringset->strings[i];
}

void
rna_coord_stringset_set(struct rna_coord_stringset*stringset,
			const struct float_matrix*const*rmsds_list,
			const struct alphabet_index*alpha_index)
{
    size_t i;
    for(i = 0; i < stringset->size; ++i) {
	set_rna_coord_string(stringset->strings[i], rmsds_list[i], alpha_index);
    }
}

int
compute_matches(struct continuous_match*matches, /* has to be the same length as pairs */
		const struct double_index*pairs,
		const struct rna_coord_stringset*stringset)
{
    const char*this_sub="compute_matches";
    size_t i;
    int err=0;
    for(i = 0; !err && i < pairs->size; ++i) {
	err = longest_common_substring(matches + i,
				       0,
				       stringset->strings[pairs->index_a->vals[i]],
				       stringset->strings[pairs->index_b->vals[i]]);
    }
    if(err) {
	err_printf(this_sub, "failed\n");
    }
    return err;
}

struct int_array*
rna_coord_stringset_strings_merged(const struct rna_coord_stringset*stringset)
{
    size_t total_size=0, i, cpy_count=0;
    struct int_array*strings_merged=NULL;
    for(i = 0; i < stringset->size; ++i) {
	total_size += stringset->strings[i]->size;
    }
    strings_merged = new_int_array(total_size);
    for(i = 0; i < stringset->size; ++i) {
	int_array_cpy_from_to(strings_merged, stringset->strings[i], cpy_count, 0);
	cpy_count += stringset->strings[i]->size;
    }
    return strings_merged;
}
