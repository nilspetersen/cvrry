/*
 * 8 March 2002.
 * Print pair_set in a prettier manner.
 * We do all output via scr_printf() which fills a buffer and
 * hands it to the interpreter.
 *
 * $Id$
 */

#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "coord.h"
#include "e_malloc.h"
#include "mprintf.h"
#include "pair_set.h"
#include "scratch.h"
#include "seq.h"
#include "yesno.h"

/* ---------------- Constants      -----------------------------
 */
/* static const char *NL = "\n"; */
enum { CHAR_PER_LINE = 60 }; /* Change this if you want more per
                                line of alignment output. Nothing
                                else will break. */

/* ---------------- getmax         -----------------------------
 * Walk backwards down an array of numbers and find the largest
 * which is an interesting number (not -1);
 */
static int
getmax (int *first, int *last) {
    int *i;
    for (i = last - 1; i >= first; i--)
        if (*i != GAP_INDEX)
            return *i;
    return GAP_INDEX;
}

/* ---------------- xtrct    -----------------------------------
 * Given a number, return the ord'th digit, counting from
 * right. Given 123456, and asked for the two'th digit, return
 * 5.
 */
static int
xtrct(int num, const int ord)
{
    int tmp, i;
    tmp = num;
    for (i = 0; i < ord; i++)
        tmp /= 10;
    for (i = 0; i < ord; i++)
        tmp *= 10;
    tmp = num - tmp;
    for (i = 1; i < ord; i++)
        tmp /=10;
    return tmp;
}

/* ---------------- do_nums        -----------------------------
 * This reads our array of numbers and actually prints out the
 * digits.
 */
static void
do_nums(int *first, int *last)
{
    unsigned ord, orders;
    int maxtop = getmax (first, last);
    if (maxtop <= 0)
        return;
    orders = (unsigned) log10 ((double) maxtop) + 1;
    for (ord = orders; ord > 0; ord--) {
        int *p;
        for ( p = first; p < last; p++) {
            int n = *p + 1;
            if ( n ) { /* if it is a gap, *p = -1, so we skip this */
                char c = ' ';
                if ( ! (n % 5))
                    c = ':';
                if ( ! (n % 10))
                    c = xtrct (n, ord) + '0';
                scr_printf ("%c", c);
            } else {
                scr_printf ("%c", ' ');
            }
        }
        scr_printf ("\n");
    }
}

/* ---------------- do_strings     -----------------------------
 */
static void
do_strings ( char *sfirst, char *slast)
{
    for ( ; sfirst < slast; sfirst++)
        scr_printf ("%c", *sfirst);
    scr_printf ("\n");
}

/* ---------------- do_printing    -----------------------------
 * We are given arrays of numbers to go at top and bottom,
 * along with a null terminated array of strings.
 * Print them with c_per_line on each line.
 */
static char*
do_printing(int *numtop,
	    int *numbottom,
	    char **strings,
	    size_t length,
	    size_t c_per_line)
{
    size_t done = 0;
    char **s;
    char *t;

    while (done < length) {
        unsigned this = c_per_line;
        if (this > length - done)
            this = length - done - 1;

        do_nums (numtop + done, numtop + done + this);
        s = strings;
        while ((t = *s++))
            do_strings (t + done, t + done + this);
        do_nums (numbottom + done, numbottom + done + this);
        scr_printf ("\n");
        done += c_per_line;
    }
    return (scr_printf ("%s", ""));
}

/* ---------------- do_printing_fasta    -----------------------------
 * We are given a null terminated array of comments to go at top,
 * along with a null terminated array of strings.
 * Print them with c_per_line on each line.
 */
static char *
do_printing_fasta (char **comments,
		   char **strings,
		   size_t length,
                   size_t c_per_line)
{
    size_t done = 0;
    char **c;
    char *d;
    char **s;
    char *t;
    
    c = comments;
    s = strings;
    while ((d = *c++) && (t = *s++)) {
        scr_printf ("%s\n", d);
        done = 0;
        while (done < length) {
            unsigned this = c_per_line;
            if (this > length - done)
                this = length - done - 1;
            
            do_strings (t + done, t + done + this);
            done += c_per_line;
        }
    }

    return (scr_printf ("%s", ""));
}

/* ---------------- get_seq_id    ------------------------------
 * For our alignment, get out the % sequence identity.
 * Do not count gaps.
 */
struct triplet
get_seq_id(const struct pair_set *pair_set,
	   const struct seq *s1,
	   const struct seq *s2)
{
    struct triplet result;
    unsigned length, aligned, ident;
    int **p;
    size_t i;
    p = pair_set->indices;

    length = aligned = ident = 0;
    for ( i = 0; i < pair_set->n; i++ ) {
        int a = p[i][0], b = p[i][1];
        length++;
        if ((a == GAP_INDEX) || (b == GAP_INDEX))
            continue;
        aligned++;
        if (tolower(s1->seq[a]) == tolower(s2->seq[b]))
            ident++;
    }
    result.length = length;
    result.ident = ident;
    result.aligned = aligned;
    if (length != pair_set->n)
        err_printf ("get_seq_id", "Silly bloody bug %s\n", __FILE__);
    return (result);
}

float
get_seq_identity(const struct pair_set *pair_set,
		 const struct seq *s1,
		 const struct seq *s2)
{
    struct triplet result = get_seq_id(pair_set, s1, s2);
    return ((float) result.ident / (float) result.aligned);
}

unsigned
get_seq_id_simple(const struct pair_set *pair_set,
		  const struct seq *s1,
		  const struct seq *s2)
{
    struct triplet result = get_seq_id(pair_set, s1, s2);
    return (result.ident);
}

/* ---------------- pair_set_pretty_string ---------------------
 * The philosophy is that we begin by assuming the worst case -
 * two essentially unaligned sequences. We then fill out long
 * strings for each sequence with either residues or gaps.
 */
char *
pair_set_pretty_string (const struct pair_set *pair_set,
                        const struct seq *s1,
			const struct seq *s2)
{
    char **strings;
    char *res;
    char *seq1, *seq2;
    int *numtop, *numbottom;
    size_t i, length, idx;
    const char *empty = " - Empty Alignment -";

    scr_reset();
    if (pair_set->n == 0)
        return (scr_printf ("%s", empty));
    length = (pair_set->n + 1);

    seq1 = E_MALLOC ( length * sizeof (seq1[0]));
    seq2 = E_MALLOC ( length * sizeof (seq1[0]));
    numtop = E_MALLOC ( length * sizeof (numtop[0]));
    numbottom = E_MALLOC (length * sizeof (numbottom[0]));
    memset (seq1, '-', length);
    memset (seq2, '-', length);
    for (i = 0; i < length; i++) {
        numtop[i] = GAP_INDEX;
        numbottom[i] = GAP_INDEX;
    }
    seq1[length -1] = '\0';
    seq2[length -1] = '\0';

    { /* This fills out the basic sequence strings and lists of seq numbers */
        int **p;
        char *t1 = seq1,
             *t2 = seq2;
        int *nt  = numtop,
            *nb  = numbottom;
        p = pair_set->indices;
        for ( idx = 0; idx < pair_set->n; idx++, t1++, t2++, nt++, nb++) {
            int a = p[idx][0], b = p[idx][1];
            if (a != GAP_INDEX) {
                *t1 = s1->seq[a];
                *nt = a;
            }
            if (b != GAP_INDEX) {
                *t2 = s2->seq[b];
                *nb = b;
            }
        }
    }
    strings = E_MALLOC (5 * sizeof (strings[0]));
    strings [0] = seq1;
    strings [1] = seq2;
    strings [2] = NULL;
    
    scr_reset();
    {
        float f;
        struct triplet r = get_seq_id (pair_set, s1, s2);
        f = ((float) r.ident / (float) r.aligned) * 100;
        scr_printf ("Seq ID %.3g %% (%u / %u) in %u total including gaps\n",
                    f, r.ident, r.aligned, r.length);
    }
    res = do_printing (numtop, numbottom, strings, length, CHAR_PER_LINE);
    free (strings);
    free (numtop);
    free (numbottom);
    free (seq1);
    free (seq2);
    return res;
}

/* ---------------- pair_set_fasta_string ---------------------
 * The philosophy is that we begin by assuming the worst case -
 * two essentially unaligned sequences. We then fill out long
 * strings for each sequence with either residues or gaps.
 */
char *
pair_set_fasta_string (const struct pair_set *pair_set,
                       const struct seq *s1,
		       const struct seq *s2)
{
    char **comments;
    char **strings;
    char *res;
    char *seq1, *seq2;
    char *com1, *com2;
    size_t length, idx, com1len, com2len;
    const char *empty = " - Empty Alignment -";

    scr_reset();
    if (pair_set->n == 0)
        return (scr_printf ("%s", empty));
    length = (pair_set->n + 1);

    seq1 = E_MALLOC ( length * sizeof (seq1[0]));
    seq2 = E_MALLOC ( length * sizeof (seq2[0]));
    memset (seq1, '-', length);
    memset (seq2, '-', length);
    seq1[length - 1] = '\0';
    seq2[length - 1] = '\0';

    { /* This fills out the basic sequence strings and comments */
        int **p;
        char *t1 = seq1,
             *t2 = seq2;
        p = pair_set->indices;
        for ( idx = 0; idx < pair_set->n; idx++, t1++, t2++) {
            int a = p[idx][0], b = p[idx][1];
            if (a != GAP_INDEX) {
                *t1 = s1->seq[a];
            }
            if (b != GAP_INDEX) {
                *t2 = s2->seq[b];
            }
        }
    }

    com1len = (s1->comment ? strlen(s1->comment) : 0) + 2;
    com2len = (s2->comment ? strlen(s2->comment) : 0) + 2;
    com1 = E_MALLOC ( com1len * sizeof (com1[0]));
    com2 = E_MALLOC ( com2len * sizeof (com2[0]));
    com1[0] = '>';
    com2[0] = '>';
    com1[com1len - 1] = '\0';
    com2[com2len - 1] = '\0';
    if (s1->comment)
        strcpy(com1 + 1, s1->comment);
    if (s2->comment)
        strcpy(com2 + 1, s2->comment);
    
    comments = E_MALLOC (3 * sizeof (comments[0]));
    comments [0] = com1;
    comments [1] = com2;
    comments [2] = NULL;
    strings = E_MALLOC (3 * sizeof (strings[0]));
    strings [0] = seq1;
    strings [1] = seq2;
    strings [2] = NULL;

    scr_reset();
    res = do_printing_fasta (comments, strings, length, CHAR_PER_LINE);

    free (comments);
    free (com1);
    free (com2);
    free (strings);
    free (seq1);
    free (seq2);

    return res;
}
