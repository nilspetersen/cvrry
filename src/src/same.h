#ifndef SAME_H
#define SAME_H

#include "yesno.h"

#define EQUAL_ITERATE(OPT_STATIC_PREFIX, PREFIX_KEYWORDS, DTYPE)	\
    OPT_STATIC_PREFIX enum yes_no							\
    DTYPE ## _same_iterate(const PREFIX_KEYWORDS DTYPE *arr_1,		\
			   const PREFIX_KEYWORDS DTYPE *arr_2,		\
			   size_t size)					\
    {									\
	size_t i;							\
	if(arr_1 == NULL || arr_2 == NULL) {				\
	    return arr_1 == arr_2;					\
	}								\
	for(i = 0; i < size; ++i) {					\
	    if(arr_1[i] != arr_2[i]) {					\
		return NO;						\
	    }								\
	}								\
	return YES;							\
    }

#define SAME_ITERATE(OPT_STATIC_PREFIX, PREFIX_KEYWORDS, DTYPE)		\
    OPT_STATIC_PREFIX enum yes_no					\
    DTYPE ## _same_iterate(const PREFIX_KEYWORDS DTYPE *arr_1,		\
			   const PREFIX_KEYWORDS DTYPE *arr_2,		\
			   size_t size)					\
    {									\
	size_t i;							\
	if(arr_1 == NULL || arr_2 == NULL) {				\
	    return arr_1 == arr_2;					\
	}								\
	for(i = 0; i < size; ++i) {					\
	    if(! DTYPE ## _same(arr_1 + i, arr_2 + i)) {		\
		return NO;						\
	    }								\
	}								\
	return YES;							\
    }

#endif
