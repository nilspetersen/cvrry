/*
 * 13 June 2017
 * rcsid = $Id$
 */

#ifndef RNA_SCORE_H
#define RNA_SCORE_H

#include "rna_frags.h"
#include "rna_alphabet.h"

void
fill_matrix_cv(struct score_mat *score_mat,
	       struct score_mat *coverage_mat,
	       const struct rna_frags *frags1,
	       const struct rna_frags *frags2);

void
scale_marginal_scores(struct score_mat *score_mat,
		      const struct score_mat *coverage_mat,
		      size_t xmer,
		      float scale_factor);

void
fill_matrix_alphabet(struct score_mat *score_mat,
		     struct score_mat *coverage_mat,
		     const struct rna_coord_seq *seq1,
		     const struct rna_coord_seq *seq2,
		     const struct float_matrix *substitution_matrix);

void
fill_matrix_alphabet_unit_score(struct score_mat *score_mat,
				const struct rna_coord_seq *seq1,
				const struct rna_coord_seq *seq2);

void
fill_matrix_alphabet_significance_weighted(struct score_mat *score_mat,
					   const struct rna_coord_seq *seq1,
					   const struct rna_coord_seq *seq2);

void
add_basepair_bonus(struct score_mat *score_mat,
		   const struct rna_coord *rna1,
		   const struct rna_coord *rna2,
		   float bonus);

int
add_base_pair_bonus_alphabet(struct score_mat *score_mat,
			     const struct rna_coord_seq *seq1,
			     const struct rna_coord_seq *seq2,
			     const struct rna_coord *rna1,
			     const struct rna_coord *rna2,
			     float bonus);

void
fill_matrix_single_letter_unit_score(struct score_mat *score_mat,
				     const struct rna_coord_seq *seq1,
				     const struct rna_coord_seq *seq2);

void
fill_matrix_single_letter_significance_weighted(struct score_mat *score_mat,
						const struct rna_coord_seq *seq1,
						const struct rna_coord_seq *seq2);

#endif /* RNA_SCORE_H */

