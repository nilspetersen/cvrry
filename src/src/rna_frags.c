#include <stdarg.h>
#include <string.h>
#include "rna_frags.h"
#include "e_malloc.h"
#include "fio.h"
#include "mprintf.h"
#include "yesno.h"
#include "cvrry_utils.h"
#include "matrix.h"
#include "bin_utils.h"

#define FRAGLEN 7

/* allocations */
static struct rna_frags*
new_rna_frags(size_t max_n_frags, size_t n_par, size_t n_nt_fragspan)
{
    struct rna_frags *frags;

    frags = E_MALLOC(sizeof(*frags));
    frags->seq_i = E_MALLOC(max_n_frags * sizeof(*(frags->seq_i)));
    /* 1 is to avoid memory errors when calling kill f_matrix: */
    frags->frags = f_matrix(max_n_frags ? max_n_frags : 1, n_par); 
    frags->n_par = n_par;
    frags->n_frags = 0;
    frags->xmer = n_nt_fragspan;

    return frags;
}

void
rna_frags_destroy(struct rna_frags *frags)
{
    if(frags) {
	kill_f_matrix(frags->frags);
	free_if_not_null(frags->seq_i);
	free_if_not_null(frags);
    }
}

/* print for testing */
void
rna_frags_dump(const struct rna_frags *frags)
{
    size_t i, j;
    for(i = 0; i < frags->n_frags; ++i) {
	mprintf("%zu ", frags->seq_i[i]);
	for(j = 0; j < frags->n_par; ++j) {
	    mprintf("%5.3e ", frags->frags[i][j]);
	}
	mprintf("\n");
    }
}

/* compute the fragments */
static int
find_first_frag(size_t *i, size_t fraglen, const struct rna_coord *rna)
{
    size_t count_nobreak=0;
    size_t j=*i;
    while(j + 1 < rna->size) {
	if(rna->chain_breaks[j]) {
	    *i = j + 1;
	    count_nobreak = 0;
	} else {
	    count_nobreak++;
	    if(count_nobreak == fraglen - 1) {
		return 1;
	    }
	}
	j++;
    }
    return 0;
}

static int
find_next_frag(size_t *i, size_t fraglen, const struct rna_coord *rna)
{
    size_t last_fragpos = (*i) + fraglen;
    (*i)++;
    if(last_fragpos < rna->size) {
	return !rna->chain_breaks[last_fragpos - 1];
    }
    return 0;
}

size_t
find_frags(size_t*fragpos, size_t fraglen, const struct rna_coord *rna)
{
    size_t i=0, nfrags=0;
    while(find_first_frag(&i, fraglen, rna)) {
	do {
	    fragpos[nfrags] = i;
	    nfrags++;
	} while(find_next_frag(&i, fraglen, rna));
    }
    return nfrags;
}

struct size_t_array*
get_fragpositions(size_t fraglen, const struct rna_coord *rna)
{
    struct size_t_array*fragpos_arr=NULL;
    fragpos_arr = new_size_t_array(rna->size);
    fragpos_arr->size = find_frags(fragpos_arr->vals, fraglen, rna);
    return fragpos_arr;
}

static void
compute_fragment(struct rna_frags *frags, size_t i, const struct rna_coord *rna)
{
    size_t j = frags->n_frags;

    frags->seq_i[j] = i;

    frags->frags[j][0] = rdist(&rna->bb[i].c4.r, &rna->bb[i+1].c4.r);
    frags->frags[j][1] = rdist(&rna->bb[i].c4.r, &rna->bb[i+3].c4.r);
    frags->frags[j][2] = rdist(&rna->bb[i].c4.r, &rna->bb[i+6].c4.r);
    frags->frags[j][3] = rdist(&rna->bb[i+1].c4.r, &rna->bb[i+3].c4.r);
    frags->frags[j][4] = rdist(&rna->bb[i+1].c4.r, &rna->bb[i+6].c4.r);

    frags->frags[j][5] = chiral_volume(&rna->bb[i].c4.r,
				       &rna->bb[i+1].c4.r,
				       &rna->bb[i+3].c4.r,
				       &rna->bb[i+6].c4.r);
    frags->n_frags++;
}

static void
compute_fragments(struct rna_frags *frags, const struct rna_coord *rna)
{
    size_t i=0;
    while(find_first_frag(&i, FRAGLEN, rna)) {
	do {
	    compute_fragment(frags, i, rna);
	} while(find_next_frag(&i, FRAGLEN, rna));
    }
}

struct rna_frags*
get_fragments(const struct rna_coord *rna)
{
    const char*this_sub = "get_fragments";
    struct rna_frags *frags;
    size_t n_frags;
    if(!rna->chain_breaks) {
	err_printf(this_sub, "Need chain break information here!\n");
	return NULL;
    }
    n_frags = posdiff(rna->size + 1, FRAGLEN);
    frags = new_rna_frags(n_frags, 6, FRAGLEN);
    compute_fragments(frags, rna);
    return frags;
}

/* NEW DISTANCE DESCRIPTION */

static void
calc_dist(struct dist_list *dist_list,
	  size_t i,
	  const struct rna_coord *rna)
{
    size_t j = dist_list->n_dists;
    dist_list->seq_i[j] = i;
    dist_list->distances[j] = rdist(&get_atom_ptr(dist_list->atom_type,
						  rna->bb + i)->r,
				    &get_atom_ptr(dist_list->atom_type,
						  rna->bb + i + dist_list->seq_dist)->r);
    dist_list->n_dists++;
}

static void
compute_distances(struct dist_list *dist_list,
		  const struct rna_coord *rna)
{
    size_t i=0;
    while(find_first_frag(&i, dist_list->seq_dist + 1, rna)) {
	do {
	    calc_dist(dist_list, i, rna);
	} while(find_next_frag(&i, dist_list->seq_dist + 1, rna));
    }
}

/* 1) create a distlist set */
static struct dist_list_set*
dist_list_set_new(size_t max_seq_dist,
		  size_t seq_len,
		  enum rna_atom_types atom_type)
{
    struct dist_list_set*dist_list_set=NULL;
    size_t i;
    dist_list_set = E_MALLOC(sizeof(*dist_list_set));
    dist_list_set->size = max_seq_dist;

    /* create and init the dist_lists */
    dist_list_set->dist_lists = E_MALLOC(max_seq_dist * sizeof(*dist_list_set->dist_lists));

    for(i = 0; i < max_seq_dist; ++i) {

	size_t n_dist = posdiff(seq_len + 1, i + 1);
	struct dist_list *distlist = dist_list_set->dist_lists + i;
	
	distlist->n_dists = 0;
	distlist->seq_dist = i + 1;

	distlist->seq_i = E_MALLOC(n_dist * sizeof(*distlist->seq_i));
	distlist->distances = E_MALLOC(n_dist * sizeof(*distlist->distances));

	distlist->atom_type = atom_type;
    }
    return dist_list_set;
}

/* 2) destroy a distlist set */
void
dist_list_set_destroy(struct dist_list_set *dist_list_set)
{
    size_t i;
    if(dist_list_set) {
	if(dist_list_set->dist_lists) {
	    for(i = 0; i < dist_list_set->size; ++i) {
		free_if_not_null(dist_list_set->dist_lists[i].distances);
		free_if_not_null(dist_list_set->dist_lists[i].seq_i);
	    }
	    free(dist_list_set->dist_lists);
	}
	free(dist_list_set);
    }
}

/* 3) compute the distances */
struct dist_list_set*
compute_distlist_set(const struct rna_coord *rna,
		     size_t max_seq_dist,
		     enum rna_atom_types atom_type)
{
    const char *this_sub = "compute_distlist_set";
    struct dist_list_set *dist_list_set=NULL;
    size_t i;
    if(!rna->chain_breaks) {
	err_printf(this_sub, "Need chain break information here!\n");
	return NULL;
    }
    dist_list_set = dist_list_set_new(max_seq_dist,
				      rna->size,
				      atom_type);
    for(i = 0; i < dist_list_set->size; ++i) {
	compute_distances(dist_list_set->dist_lists + i, rna);
    }
    return dist_list_set;
}

void
dist_list_dump(const struct dist_list *dist_list)
{
    char atom_name[4];
    size_t i;
    get_atom_name(dist_list->atom_type, atom_name);
    for(i = 0; i < dist_list->n_dists; ++i) {
	mprintf("%zu %zu %s %f\n",
		dist_list->seq_i[i],
		dist_list->seq_dist,
		atom_name,
		dist_list->distances[i]);
    }
}

void
dist_list_set_dump(const struct dist_list_set *dist_list_set)
{
    size_t i;
    for(i = 0; i < dist_list_set->size; ++i) {
	dist_list_dump(dist_list_set->dist_lists + i);
    }
}

/* END OF THE NEW DISTANCE STUFF */

static int
scan_n_floats(float *target, size_t n, FILE *fp)
{
    size_t i;
    for(i = 0; i < n; ++i) {
	if(1 != fscanf(fp, "%f ", target + i)) {
	    return 1;
	}
    }
    return 0;
}

static void
rna_frags_mult(struct rna_frags *frags, const float *weights)
{
    size_t i, j;
    for (i = 0; i < frags->n_frags; ++i) {
        for (j = 0; j < frags->n_par; ++j) {
            frags->frags[i][j] *= weights[j];
        }
    }
}

int
rna_frags_multiply(struct rna_frags *frags,
		   const struct feat_weights *weights) {
    const char *this_sub="rna_frags_multiply";
    if(frags->n_par != weights->n) {
	err_printf(this_sub,
		   "length of fragments (%zu) and weights (%zu) vectors don't match\n",
		   frags->n_par,
		   weights->n);
	return 1;
    }
    rna_frags_mult(frags, weights->w);
    return 0;
}

size_t
rna_frags_write_bin(FILE *outfile, const struct rna_frags *frags)
{
    const char *this_sub="rna_frags_write_bin";
    size_t total_memsize=0;
    if(m_bin_write(&total_memsize,
		   &frags->n_par,
		   sizeof(frags->n_par),
		   1, outfile, this_sub)) {
    	goto err_exit;
    }
    if(m_bin_write(&total_memsize,
		   &frags->n_frags,
		   sizeof(frags->n_frags),
		   1, outfile, this_sub)) {
    	goto err_exit;
    }
    if(m_bin_write(&total_memsize,
		   &frags->xmer,
		   sizeof(frags->xmer),
		   1, outfile, this_sub)) {
    	goto err_exit;
    }
    if(m_bin_write(&total_memsize,
		   frags->frags[0],
		   sizeof(**frags->frags),
		   frags->n_par * frags->n_frags,
		   outfile,
		   this_sub)) {
    	goto err_exit;
    }
    if(m_bin_write(&total_memsize,
		   frags->seq_i,
		   sizeof(*frags->seq_i),
		   frags->n_frags,
		   outfile,
		   this_sub)) {
    	goto err_exit;
    }
    return total_memsize;
err_exit:
    err_printf(this_sub, "Failed to write fragments in binary format\n");
    return 0;
}

struct rna_frags*
rna_frags_read_binary(FILE *infile)
{
    const char *this_sub="rna_coord_read_binary";
    struct rna_frags *frags=NULL;
    size_t n_frags, n_par, n_nt_fragspan;

    if(fread(&n_par, sizeof(n_par), 1, infile) != 1){
	goto err_exit;
    }
    if(fread(&n_frags, sizeof(n_frags), 1, infile) != 1){
	goto err_exit;
    }
    if(fread(&n_nt_fragspan, sizeof(n_nt_fragspan), 1, infile) != 1){
	goto err_exit;
    }

    frags = new_rna_frags(n_frags, n_par, n_nt_fragspan);

    if(fread(frags->frags[0], sizeof(**frags->frags), n_frags * n_par, infile) != n_frags * n_par){
	goto err_exit;
    }
    if(fread(frags->seq_i, sizeof(*frags->seq_i), n_frags, infile) != n_frags){
	goto err_exit;
    }
    frags->n_frags = n_frags;
    
    return frags;
err_exit:
    err_printf(this_sub, "Failed to read rna fragments in binary format\n");
    rna_frags_destroy(frags);
    return NULL;
}

/* void */
/* write_frags_to_tablefile(FILE *outfile, const struct rna_frags *frags) */
/* { */
/*     size_t i, j; */
/*     for(i = 0; i < frags->n_frags; ++i) { */
/* 	for(j = 0; j < frags->n_frags; ++j) { */
/* 	    mfprintf(outfile, "%f ", frags->frags[i][j]); */
/* 	} */
/* 	mfprintf(outfile, "\n"); */
/*     } */
/* } */

/* weights */

void
feat_weights_destroy(struct feat_weights *weights)
{
    if(weights) {
	free_if_not_null(weights->w);
	free(weights);
    }
}

struct feat_weights*
feat_weights_new(size_t n)
{
    struct feat_weights*weights=NULL;
    weights = E_MALLOC(sizeof(*weights));
    weights->n = n;
    weights->w = E_MALLOC(n * sizeof(*weights->w));
    return weights;
}

struct feat_weights*
feat_weights_read(const char*fname)
{
    const char *this_sub="feat_weights_read";
    size_t n=0;
    float dummy_float;
    struct feat_weights *weights=NULL;
    FILE *fp=NULL;
    if(!(fp = mfopen(fname, "r", this_sub))) {
	goto err_exit;
    }
    while(1 == fscanf(fp, "%f ", &dummy_float)) {
	n++;
    }
    fseek(fp, 0, SEEK_SET);
    weights = feat_weights_new(n);
    if(scan_n_floats(weights->w, n, fp)) {
	goto err_exit;
    }
exit:
    if(fp) {
	fclose(fp);
    }
    return weights;
err_exit:
    err_printf(this_sub, "failed to read parameters from %s\n", fname);
    feat_weights_destroy(weights);
    weights=NULL;
    goto exit;
}

/* normalization parameters */

struct norm_params*
norm_params_new(size_t n)
{
    struct norm_params*np = E_MALLOC(sizeof(*np));
    np->mu = E_MALLOC(n * sizeof(*np->mu));
    np->sigma = E_MALLOC(n * sizeof(*np->sigma));
    return np;
}

void
norm_params_destroy(struct norm_params *frag_norm)
{
    if(frag_norm) {
	free_if_not_null(frag_norm->mu);
	free_if_not_null(frag_norm->sigma);
	free(frag_norm);
    }
}

void
rna_frags_normalize(struct rna_frags *frags,
		    const struct norm_params *frag_norm)
{
    size_t i, j;
    /* Normalize all features of all fragments */
    for (i = 0; i < frags->n_frags; ++i) {
        for (j = 0; j < frags->n_par; ++j) {
            frags->frags[i][j] = (frags->frags[i][j] - frag_norm->mu[j]) / frag_norm->sigma[j];
        }
    }
}

struct norm_params*
norm_params_read(const char*fname, size_t n_par)
{
    const char *this_sub = "feat_weights_read";
    FILE *fp=NULL;
    int err=1;
    struct norm_params *np=NULL;
    np = norm_params_new(n_par);
    fp = mfopen(fname, "r", this_sub);
    if(fp) {
	err = scan_n_floats(np->mu, n_par, fp);
    }
    if(!err) {
	err = scan_n_floats(np->sigma, n_par, fp);
    }
    close_if_open_file(fp);
    if(err) {
	err_printf(this_sub, "Failed to read parameter from %s\n", fname);
	norm_params_destroy(np);
	return NULL;
    }
    return np;
}
