#include "rna_comparison.h"
#include "lsqf.h"
#include "mprintf.h"
#include "e_malloc.h"
#include "psi.h"
#include "cmp_dmat.h"

int
rna_structure_comparison(struct rna_comparison_result *result,
			 const struct rna_frag_coord *chain_a,
			 const struct rna_frag_coord *chain_b,
			 const struct rna_align_params *params,
			 const struct rna_cmp_opts *opts)
{
    const char *this_sub = "rna_structure_comparison";
    struct transformation *transform_rmsd = opts->superpos_opt == RMSD ? result->transform : NULL;
    struct transformation *transform_psi = opts->superpos_opt == PSI ? result->transform : NULL;
    int err=0;
    const float rmsd_thresh_psi=4.0;

    if(!(chain_a->frags->n_frags && chain_b->frags->n_frags)) {
	if(opts->verbose_err) {
	    err_printf(this_sub, "No fragments = no alignment\n");
	}
	err = 1;
	goto exit;
    }

    pair_set_destroy(result->alignment);
    result->alignment = NULL;

    result->alignment = align(chain_a, chain_b, params);
    if(!result->alignment) {
	err_printf(this_sub, "could not calculate an alignment\n");
	err = 1;
	goto exit;
    }

    if(opts->get_alignment_score) {
	result->align_score = result->alignment->score;
	result->align_score_smpl = result->alignment->smpl_score;
    }

    if(opts->calc_rmsd || opts->superpos_opt == RMSD) {
	err = rna_coord_rmsd(transform_rmsd,
			     &result->rmsd,
			     result->alignment,
			     chain_a->rna_backbone,
			     chain_b->rna_backbone);
	if(err) {
	    err_printf(this_sub, "could not calculate rmsd\n");
	    goto exit;
	}
    }

    if(opts->calc_psi || opts->superpos_opt == PSI) {
    	err = percentage_of_structure_identity(&result->psi,
					       NULL,
    					       transform_psi,
    					       result->alignment,
    					       chain_a->rna_backbone,
    					       chain_b->rna_backbone,
    					       rmsd_thresh_psi,
					       PROSUP_REFINE);
    	if(err) {
    	    err_printf(this_sub, "could not calculate psi\n");
    	    goto exit;
    	}
    }

    if(opts->calc_psd) {
	result->psd = percentage_of_similar_distances_rna(result->alignment,
							  chain_a->rna_backbone,
							  chain_b->rna_backbone,
							  opts->psd_thresh);
    }
    
exit:
    if(!opts->save_alignment) {
	pair_set_destroy(result->alignment);
	result->alignment=NULL;
    }
    return err;
}

struct rna_comparison_result*
new_rna_comparison_result(const struct rna_cmp_opts *opts)
{
    struct rna_comparison_result*res;
    res = E_MALLOC(sizeof(*res));
    res->transform = NULL;
    res->alignment = NULL;
    res->psd = res->rmsd = res->psi = -1.0;
    if(opts->superpos_opt != NONE) {
	res->transform = E_MALLOC(sizeof(*res->transform));
    }
    return res;
}

void
rna_comparison_result_destroy(struct rna_comparison_result*res)
{
    if(res) {
	pair_set_destroy(res->alignment);
	free_if_not_null(res->transform);
	free_if_not_null(res);
    }
}
