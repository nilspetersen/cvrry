#include "cvrry_indexed_chainset.h"
#include "mprintf.h"
#include "lsqf.h"
#include "wrapped_matrix.h"

size_t
indexed_chainset_get_fraglen(const struct indexed_chainset*chainset)
{
    return size_t_array_last(chainset->fragment_pattern) + 1;
}

static size_t
count_fragments_with_minlen(size_t minlen,
			    const struct rna_coord_set*rna_chains)
{
    size_t counter=0, i;
    for(i = 0; i < rna_chains->n_chains; ++i) {
	if(rna_chains->chains[i]->size >= minlen) {
	    counter += rna_chains->chains[i]->size - minlen + 1;
	}
    }
    return counter;
}

struct double_index*
get_chainpiece_indices(const struct rna_coord_set*rna_chains, size_t fraglen)
{
    const char*this_sub="get_chainpiece_indices";
    struct double_index*chainpiece_indexing=NULL;
    size_t nfrags, pos, i, j;
    nfrags = count_fragments_with_minlen(fraglen, rna_chains);
    if(!nfrags) {
	err_printf(this_sub, "No fragments with minimum length %zu found\n", fraglen);
	return NULL;
    }
    chainpiece_indexing = new_double_index(nfrags);
    for(i = 0, pos=0; i < rna_chains->n_chains; ++i) {
	for(j = 0; j + fraglen < rna_chains->chains[i]->size + 1; ++j, ++pos) {
	    chainpiece_indexing->index_a->vals[pos] = i;
	    chainpiece_indexing->index_b->vals[pos] = j;
	}
    }
    return chainpiece_indexing;
}

struct size_t_array*
create_cvrry_fragment_pattern()
{
    struct size_t_array*arr=NULL;
    arr = new_size_t_array(4);
    arr->vals[0] = 0;
    arr->vals[1] = 1;
    arr->vals[2] = 3;
    arr->vals[3] = 6;
    return arr;
}

struct size_t_array*
make_chainpiece_pattern(int fraglen_or_pattern)
{
    const char*this_sub="make_chainpiece_pattern";
    if(fraglen_or_pattern > 0) {
	return size_t_range(0, (size_t) fraglen_or_pattern, 1);
    }
    if(fraglen_or_pattern == -1) {
	return create_cvrry_fragment_pattern();
    }
    err_printf(this_sub,
	       "%i is not a valid parameter the fragment style\n",
	       fraglen_or_pattern);
    return NULL;
}

static struct indexed_chainset*
new_indexed_chainset()
{
    struct indexed_chainset*chainset=NULL;
    chainset = E_MALLOC(sizeof(*chainset));
    chainset->chainpiecelist = NULL;
    chainset->rna_chains = NULL;
    chainset->indices = NULL;
    chainset->fragment_pattern = NULL;
    return chainset;
}

static int
indexed_chainset_add_indices(struct indexed_chainset*chainset)
{
    const char *this_sub="indexed_chainset_add_indices";
    size_t fraglen = indexed_chainset_get_fraglen(chainset);
    chainset->indices = get_chainpiece_indices(chainset->rna_chains, fraglen);    
    if(!fraglen) {
	err_printf(this_sub, "fraglen is %zu\n", fraglen);
    }
    return !chainset->indices;
}

struct indexed_chainset*
indexed_chainset_load_with_pattern(const char*fn_chainpiecelist,
				   const char*chaindir,
				   const struct size_t_array*fragment_pattern,
				   const struct rna_atom_types_array*atom_types)
{
    struct indexed_chainset*chainset=NULL;
    chainset = new_indexed_chainset();
    if(chainset) {
	chainset->chainpiecelist = read_chainpiece_list(fn_chainpiecelist);
    }
    if(chainset->chainpiecelist) {
	chainset->rna_chains = load_rna_coord_set(chaindir, chainset->chainpiecelist);
    }
    if(chainset->rna_chains) {
	chainset->fragment_pattern = size_t_array_copy(fragment_pattern);
    }
    if(chainset->fragment_pattern) {
	chainset->atom_types = rna_atom_types_array_copy(atom_types);
    }
    if(chainset->atom_types) {
	indexed_chainset_add_indices(chainset);	
    }
    if(chainset) {
	if(!chainset->indices) {
	    indexed_chainset_destroy(chainset);
	    return NULL;
	}
    }
    return chainset;
}

struct indexed_chainset*
indexed_chainset_load(const char*fn_chainpiecelist,
		      const char*chaindir,
		      int fraglen_or_pattern)
{
    struct indexed_chainset*chainset=NULL;
    struct size_t_array*fragment_pattern=NULL;
    struct rna_atom_types_array*atom_types=NULL;
    fragment_pattern = make_chainpiece_pattern(fraglen_or_pattern);
    if(fragment_pattern) {
	atom_types = rna_atom_types_array_create(ALL);
    }
    if(atom_types) {
	chainset = indexed_chainset_load_with_pattern(fn_chainpiecelist,
						      chaindir,
						      fragment_pattern,
						      atom_types);
    }
    size_t_array_destroy(fragment_pattern);
    rna_atom_types_array_destroy(atom_types);
    return chainset;
}

void
indexed_chainset_destroy(struct indexed_chainset*chainset)
{
    if(chainset) {
	chainpiece_list_destroy(chainset->chainpiecelist);
	rna_coord_set_destroy(chainset->rna_chains);
	double_index_destroy(chainset->indices);
	size_t_array_destroy(chainset->fragment_pattern);
	rna_atom_types_array_destroy(chainset->atom_types);
	free(chainset);
    }
}

size_t
indexed_chainset_size(const struct indexed_chainset*chainset)
{
    return chainset->indices->index_a->size;
}

const struct rna_coord*
indexed_chainset_get_chain(const struct indexed_chainset*chainset, size_t i)
{
    return chainset->rna_chains->chains[chainset->indices->index_a->vals[i]];
}

size_t
indexed_chainset_get_pos(const struct indexed_chainset*chainset, size_t i)
{
    return chainset->indices->index_b->vals[i];
}

float
indexed_chainset_pair_rmsd(const struct indexed_chainset*chainset, size_t i, size_t j)
{
    float rmsd;
    int err;
    err = rmsd_fixed_chainlen(&rmsd,
			      indexed_chainset_get_chain(chainset, i),
			      indexed_chainset_get_chain(chainset, j),
			      indexed_chainset_get_pos(chainset, i),
			      indexed_chainset_get_pos(chainset, j),
			      chainset->fragment_pattern,
			      chainset->atom_types,
			      NO);
    if(err) {
	return -1;
    }
    return rmsd;
}

struct rna_coord_set*
indexed_chainset_extract_chainpiece_set(int nfrags, const int*indices,
					const struct indexed_chainset*chainset)
{
    struct rna_coord_set*rna_coordset=NULL;
    int i;
    size_t fraglen = size_t_array_last(chainset->fragment_pattern) + 1;
    if(fraglen) {
	rna_coordset = new_rna_coord_set(nfrags);
    }
    if(rna_coordset) {
	for(i = 0; i < nfrags; ++i) {
	    size_t ii = (size_t) indices[i];
	    struct rna_coord*rna = 
		rna_coord_copy_piece(indexed_chainset_get_chain(chainset, ii),
				     indexed_chainset_get_pos(chainset, ii),
				     fraglen);
	    if(!rna) {
		rna_coord_set_destroy(rna_coordset);
		return NULL;
	    }
	    rna_coordset->chains[rna_coordset->n_chains++] = rna;
	}
    }
    return rna_coordset;
}

struct indexed_chainset*
indexed_chainset_cut_pieces(const struct indexed_chainset*chainset_in,
			    size_t minlen, size_t maxlen)
{
    const char*this_sub="indexed_chainset_cut_pieces";
    int err=0;
    struct indexed_chainset*chainset_out=NULL;

    chainset_out=new_indexed_chainset();

    err = continuous_chains_to_pieces(&chainset_out->rna_chains,
				      &chainset_out->chainpiecelist,
				      chainset_in->rna_chains,
				      chainset_in->chainpiecelist,
				      minlen,
				      maxlen);
    if(!err) {
	chainset_out->fragment_pattern = size_t_array_copy(chainset_in->fragment_pattern);    
	chainset_out->atom_types = rna_atom_types_array_copy(chainset_in->atom_types);
    }
    if(!err) {
	err = indexed_chainset_add_indices(chainset_out);    
    }
    if(err) {
	err_printf(this_sub,
		   ("Failed to extract chains with a size"
		    " between %zu and %zu nucleotides\n"),
		   minlen, maxlen);
	indexed_chainset_destroy(chainset_out);
	return NULL;
    }
    return chainset_out;
}

struct indexed_chainset*
indexed_chainset_get_subset(const struct indexed_chainset*chainset_in,
			    int nchains, int*indices)
{
    struct indexed_chainset*chainset_out=NULL;
    
    chainset_out=new_indexed_chainset();

    chainset_out->rna_chains = rna_coord_set_get_subset(chainset_in->rna_chains,
							nchains, indices);

    chainset_out->chainpiecelist = chainpiece_list_get_subset(chainset_in->chainpiecelist,
							      nchains, indices);

    chainset_out->fragment_pattern = size_t_array_copy(chainset_in->fragment_pattern);    

    chainset_out->atom_types = rna_atom_types_array_copy(chainset_in->atom_types);

    return chainset_out;
}

struct indexed_chainset*
indexed_chainset_copy(const struct indexed_chainset*chainset)
{
    struct indexed_chainset*copy=NULL;
    copy = new_indexed_chainset();
    if(copy) {
	copy->chainpiecelist = chainpiece_list_copy(chainset->chainpiecelist);
    }
    if(copy->chainpiecelist) {
	copy->rna_chains = rna_coord_set_copy(chainset->rna_chains);
    }
    if(copy->rna_chains) {
	copy->fragment_pattern = size_t_array_copy(chainset->fragment_pattern);
    }
    if(copy->fragment_pattern) {
	copy->atom_types = rna_atom_types_array_copy(chainset->atom_types);
    }
    if(copy->atom_types) {
	copy->indices = double_index_copy(chainset->indices);
    }
    if(copy) {
	if(!copy->indices) {
	    indexed_chainset_destroy(copy);
	    return NULL;
	}
    }
    return copy;
}
