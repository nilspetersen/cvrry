#include "cvrry_chimera_script.h"
#include "cvrry_utils.h"
#include "fio.h"
#include "mprintf.h"
#include "lsqf.h"

#define CHAINID_STR_BUF 64

static void
rna_res_id_stream_(FILE*stream, const struct rna_res_id*res)
{
    mfprintf(stream, "%i%c", res->resnum, res->icode);
}

static enum yes_no
pair_set_col_has_gaps(const struct pair_set*pset, int i)
{
    size_t j;
    for(j = 0; j < pset->m; ++j) {
	if(pset->indices[i][j] == GAP_INDEX) {
	    return YES;
	}
    }
    return NO;
}

static void
write_chimera_color_aligned_region(FILE*outfile,
				   const char*color_unaligned,
				   const char*color_aligned,
				   const struct pair_set*pset,
				   const struct rna_coord*rna,
				   int align_i,
				   int struct_i)
{
    enum yes_no first_found=NO;
    size_t i;
    mfprintf(outfile, "color %s #%i\n", color_unaligned, struct_i);
    mfprintf(outfile, "color %s #%i:", color_aligned, struct_i);
    for(i = 0; i < pset->n; ++i) {
	int index = pset->indices[i][align_i];
	if(!pair_set_col_has_gaps(pset, i)) {
	    if(first_found) {
		mfprintf(outfile, ",");
	    } else {
		first_found=YES;
	    }
	    rna_res_id_stream_(outfile, rna->res + index);
	}
    }
    mfprintf(outfile, "\n");
}

static int
write_chimera_superpos_script(const char*fn_chimera_script,
			      const char*fname_structure_1,
			      const char*fname_structure_2,
			      const struct rna_coord*rna_1,
			      const struct rna_coord*rna_2,
			      const struct pair_set*pset)
{
    const char*this_sub="write_chimera_superpos_script";
    FILE*outfile = mfopen(fn_chimera_script, "w", this_sub);
    if(outfile) {
	mfprintf(outfile, "open %s\nopen %s\n", fname_structure_1, fname_structure_2);
	mfprintf(outfile, "~show\nribbon\n");
	write_chimera_color_aligned_region(outfile,
					   "white",
					   "purple",
					   pset,
					   rna_1,
					   0,
					   0);
	write_chimera_color_aligned_region(outfile,
					   "yellow",
					   "red",
					   pset,
					   rna_2,
					   1,
					   1);
	fclose(outfile);
	return 0;
    }
    return 1;
}

static void
rna_coord_move_(struct rna_coord*rna,
	       const struct RPoint *r_move,
	       void (*rfunc) (struct RPoint*, const struct RPoint*))
{
    size_t i;
    enum rna_atom_types atom_type;
    for(i = 0; i < rna->size; ++i) {
	for(atom_type = P; atom_type <= C1; ++atom_type) {
	    struct rna_atom*atom = get_atom_ptr(atom_type, rna->bb + i);
	    if(atom->is_set) {
		rfunc(&atom->r, r_move);
	    }
	}
    }
}

static void
rna_coord_rotate_(struct rna_coord*rna,
		  const struct rotation_matrix *rot_mat)
{
    size_t i;
    enum rna_atom_types atom_type;
    for(i = 0; i < rna->size; ++i) {
	for(atom_type = P; atom_type <= C1; ++atom_type) {
	    struct rna_atom*atom = get_atom_ptr(atom_type, rna->bb + i);
	    if(atom->is_set) {
		rotate_rpoint(&atom->r, rot_mat);
	    }
	}
    }
}

static struct rna_coord*
rna_coord_transform_superposed_(const struct rna_coord*rna_in,
				const struct transformation *transform)
{
    struct rna_coord *rna_transformed=NULL;
    rna_transformed = rna_coord_copy(rna_in);
    if(rna_transformed) {
	rna_coord_move_(rna_transformed, &transform->com_a, &rsubtract);
	rna_coord_rotate_(rna_transformed, &transform->rotate_a);
	rna_coord_move_(rna_transformed, &transform->com_b, &radd);
    }
    return rna_transformed;
}


int
write_chimera_superimposed_alignment(const char*dir_out,
				     const struct rna_coord*rna_1,
				     const struct rna_coord*rna_2,
				     const struct pair_set*pset)
{
    const char*this_sub="write_chimera_superimposed_alignment";
    int err;
    char *fn_chimera_script=NULL;
    char *fn_structure_1=NULL;
    char *fn_structure_2=NULL;
    struct transformation transform;
    float rmsd;
    struct rna_coord*rna_1_transformed=NULL;
    fn_chimera_script = merge_paths(dir_out, "superposition.cmd");
    fn_structure_1 = merge_paths(dir_out, "chain1.pdb");
    fn_structure_2 = merge_paths(dir_out, "chain2.pdb");
    err = write_chimera_superpos_script(fn_chimera_script,
					fn_structure_1,
					fn_structure_2,
					rna_1,
					rna_2,
					pset);
    if(!err) {
	err = rna_coord_rmsd(&transform,
			     &rmsd,
			     pset,
			     rna_1, 
			     rna_2);
    }
    if(!err) {
	rna_1_transformed = rna_coord_transform_superposed_(rna_1, &transform);
	err = !rna_1_transformed;
    }
    if(!err) {
	rna_coord_2_pdb(rna_1_transformed, fn_structure_1);
	rna_coord_2_pdb(rna_2, fn_structure_2);
    }
    if(err) {
	err_printf(this_sub, "failed");
    }
    free_if_not_null(fn_chimera_script);
    free_if_not_null(fn_structure_1);
    free_if_not_null(fn_structure_2);
    rna_coord_destroy(rna_1_transformed);
    return err;
}
