#ifndef PROSUP_H
#define PROSUP_H

#include "pair_set.h"
#include "rna_coord.h"

struct pair_set*
rna_promax(const struct rna_coord *rna1,
	   const struct rna_coord *rna2,
	   const struct pair_set *alignment,
	   size_t maxsub_seedlen,
	   float dist_thresh);


#endif
