#ifndef CVRRY_DEFAULTS_H
#define CVRRY_DEFAULTS_H

#include "rna_frags.h"

struct norm_params*
default_normalization();

struct feat_weights*
default_feat_weights();

#endif
