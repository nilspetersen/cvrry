#include "chainlist.h"
#include "e_malloc.h"
#include <stdio.h>
#include "mprintf.h"
#include "fio.h"
#include "cvrry_utils.h"
#include <string.h>

static void*
read_list_core(const char *fname,
	       void* (*constructor) (size_t),
	       void (*destructor) (void*),
	       size_t (*count_lines)(FILE*),
	       int (*read_lines)(void*, FILE*))
{
    const char *this_sub="read_list_core";
    FILE *infile;
    void *list=NULL;
    size_t n_lines;
    infile = mfopen(fname, "r", this_sub);
    if(!infile) {
	goto err_exit;
    }
    n_lines = count_lines(infile);
    fseek(infile, 0, SEEK_SET);
    list = constructor(n_lines);
    if(read_lines(list, infile)) {
	goto err_exit;
    }
    fclose(infile);
    return list;
err_exit:
    destructor(list);
    if(infile) {
	fclose(infile);
    }
    err_printf(this_sub, "failed to read list from file %s\n", fname);
    return NULL;
}

static struct chain_list*
new_chain_list(size_t size)
{
    struct chain_list *chainlist=NULL;
    chainlist = E_MALLOC(sizeof(*chainlist));
    chainlist->chains = E_MALLOC(size *  sizeof(*chainlist->chains));
    chainlist->size = size;
    return chainlist;
}

static void*
chain_list_constructor(size_t size)
{
    return (void*) new_chain_list(size);
}

void
chain_list_destroy(struct chain_list*list)
{
    if(list) {
	free_if_not_null(list->chains);
	free(list);
    }
}

static void
chain_list_destructor(void*list)
{
    chain_list_destroy((struct chain_list*) list);
}

static size_t
count_lines_chainfile(FILE *file)
{
    char dummy1[8], dummy2[8];
    int dummy_model_i;
    size_t lines=0;
    while(fscanf(file, "%7s %7s %i", dummy1, dummy2, &dummy_model_i) == 3) {
	lines++;
    }
    return lines;
}

static int
read_lines_chainfile(void*chainlist, FILE*infile)
{
    size_t i;
    int n_matches;
    struct chain_list *list = (struct chain_list*) chainlist;
    for(i = 0; i < list->size; ++i) {
	n_matches = fscanf(infile,
			   "%7s %7s %i",
			   list->chains[i].pdbid,
			   list->chains[i].chain_id,
			   &list->chains[i].model_i);
	if(n_matches != 3) {
	    return 1;
	}
    }
    return 0;
}

struct chain_list*
read_chain_list(const char *fname)
{
    return (struct chain_list*) read_list_core(fname,
					       &chain_list_constructor,
					       &chain_list_destructor,
					       &count_lines_chainfile,
					       &read_lines_chainfile);
}

void
print_chain_description(const struct chain_description*ch)
{
    mprintf("%5s %7s %7i ", ch->pdbid, ch->chain_id, ch->model_i);
}

void
stream_chain_description(FILE *stream_out,
			 const struct chain_description *ch)
{
    mfprintf(stream_out,
	     "%s %s %i",
	     ch->pdbid,
	     ch->chain_id,
	     ch->model_i);
}

/* TODO: read align list using the 'core function' */

static struct align_list*
new_align_list(size_t size)
{
    struct align_list *list=NULL;
    list = E_MALLOC(sizeof(*list));
    list->chainpairs = E_MALLOC(size * sizeof(*list->chainpairs));
    list->size = size;
    return list;
}

void
align_list_destroy(struct align_list*list)
{
    if(list) {
	free_if_not_null(list->chainpairs);
	free(list);
    }
}

static size_t
count_lines_alignfile(FILE *file)
{
    char dummy1[8], dummy2[8], dummy3[8], dummy4[8];
    int dummy_model_i, dummy_model_j;
    size_t lines=0;
    while(fscanf(file, "%7s %7s %i %7s %7s %i",
		 dummy1, dummy2, &dummy_model_i,
		 dummy3, dummy4, &dummy_model_j) == 6) {
	lines++;
    }
    return lines;
}

static int
read_lines_alignfile(struct align_list*list, FILE*infile)
{
    size_t i;
    int n_matches;
    for(i = 0; i < list->size; ++i) {
	n_matches = fscanf(infile,
			   "%7s %7s %i %7s %7s %i",
			   list->chainpairs[i].chain_a.pdbid,
			   list->chainpairs[i].chain_a.chain_id,
			   &list->chainpairs[i].chain_a.model_i,
			   list->chainpairs[i].chain_b.pdbid,
			   list->chainpairs[i].chain_b.chain_id,
			   &list->chainpairs[i].chain_b.model_i);
	if(n_matches != 6) {
	    return 1;
	}
    }
    return 0;
}

struct align_list*
read_align_list(const char*fname)
{
    const char *this_sub="read_align_list";
    struct align_list *list=NULL;
    int err=0;
    size_t n_lines;
    FILE *infile;
    infile = mfopen(fname, "r", this_sub);
    err = !infile;
    if(!err) {
	n_lines = count_lines_alignfile(infile);	
	fseek(infile, 0, SEEK_SET);
	list = new_align_list(n_lines);
	err = read_lines_alignfile(list, infile);
    }
    if(infile) {
	fclose(infile);
    }
    if(err) {
	align_list_destroy(list);
	err_printf(this_sub, "failed to read chainlist from file %s\n", fname);
	return NULL;
    }    
    return list;
}

int
is_align_list(const char*fname)
{
    const char *this_sub="is_align_list";
    char dummy1[8], dummy2[8], dummy3[8], dummy4[8];
    int dummy_model_i, dummy_model_j;
    char line_buf[256];
    int err=0;
    FILE *infile = mfopen(fname, "r", this_sub);
    if(!infile) {
	return 0;
    }
    err = !fgets(line_buf, 256, infile);
    if(!err){
	if(sscanf(line_buf,
		  "%7s %7s %i %7s %7s %i",
		  dummy1, dummy2, &dummy_model_i,
		  dummy3, dummy4, &dummy_model_j) != 6) {
	    err = 1;
	}
    }
    fclose(infile);
    return !err;
}

/* NEW STUFF */

struct chainpiece_list*
new_chainpiece_list(size_t size)
{
    struct chainpiece_list*list=NULL;
    list = E_MALLOC(sizeof(*list));
    list->chainpieces = E_MALLOC(size *  sizeof(*list->chainpieces));
    list->size = size;
    return list;
}

static void*
chainpiece_list_constructor(size_t size)
{
    return (void*) new_chainpiece_list(size);
}

static void
chainpiece_list_cpy(struct chainpiece_list*dest,
		    const struct chainpiece_list*src)
{
    size_t i;
    for(i = 0; i < src->size; ++i) {
	chainpiece_description_cpy(dest->chainpieces + i, src->chainpieces + i);
    }
}

struct chainpiece_list*
chainpiece_list_copy(const struct chainpiece_list*original)
{
    struct chainpiece_list*copy=NULL;
    copy = new_chainpiece_list(original->size);
    chainpiece_list_cpy(copy, original);
    return copy;
}

void
chainpiece_list_destroy(struct chainpiece_list*list)
{
    if(list) {
	free_if_not_null(list->chainpieces);
	free(list);
    }
}

static void
chainpiece_list_destructor(void*chainfrag_list)
{
    chainpiece_list_destroy((struct chainpiece_list*) chainfrag_list);
}

static size_t
count_lines_chainpiece_file(FILE *file)
{
    char dummy1[8], dummy2[8];
    int dummy_model_i, dummy_chainfrag_i, dummy_frag_cutted_i;
    size_t lines=0;
    while(fscanf(file, "%7s %7s %i %i %i",
		 dummy1, dummy2,
		 &dummy_model_i, &dummy_chainfrag_i,
		 &dummy_frag_cutted_i) == 5) {
	lines++;
    }
    return lines;
}

static int
read_lines_chainpiece_file(void*chainfrag_list,
			   FILE *infile)
{
    size_t i;
    int n_matches;
    struct chainpiece_list *list = (struct chainpiece_list*) chainfrag_list;
    for(i = 0; i < list->size; ++i) {
	n_matches = fscanf(infile,
			   "%7s %7s %i %i %i",
			   list->chainpieces[i].chain.pdbid,
			   list->chainpieces[i].chain.chain_id,
			   &list->chainpieces[i].chain.model_i,
			   &list->chainpieces[i].chain_i,
			   &list->chainpieces[i].fragment_i);
	if(n_matches != 5) {
	    return 1;
	}
    }
    return 0;
}

struct chainpiece_list*
read_chainpiece_list(const char *fname)
{
    return (struct chainpiece_list*) read_list_core(fname,
						    &chainpiece_list_constructor,
						    &chainpiece_list_destructor,
						    &count_lines_chainpiece_file,
						    &read_lines_chainpiece_file);
}

void
stream_chainpiece_description(FILE*stream,
			      const struct chainpiece_description*chainpiece)
{
    mfprintf(stream,
	     "%s %s %i %i %i",
	     chainpiece->chain.pdbid,
	     chainpiece->chain.chain_id,
	     chainpiece->chain.model_i,
	     chainpiece->chain_i,
	     chainpiece->fragment_i);
}

void
stream_chainpiece_list(FILE*stream,
		       struct chainpiece_list*chainpiece_list)
{
    size_t i;
    for(i = 0; i < chainpiece_list->size; ++i) {
	stream_chainpiece_description(stream, chainpiece_list->chainpieces + i);
	mfprintf(stream, "\n");
    }
}

void
dump_chainpiece_list(struct chainpiece_list*list)
{
    stream_chainpiece_list(stdout, list);
}

int
write_chainpiece_list(const char *fname,
		      struct chainpiece_list*list)
{
    const char*this_sub="write_chainpiece_list";
    FILE *f_out=mfopen(fname, "w", this_sub);
    int err=1;
    if(f_out) {
	stream_chainpiece_list(f_out, list);
	err = 0;
    }
    close_if_open_file(f_out);
    return err;
}


/* filenames etc. */

char*
fn_chainpiece(const struct chainpiece_description*chainpiece)
{
    char *fn=NULL;
    size_t memsize = (strlen(chainpiece->chain.pdbid) +
		      strlen(chainpiece->chain.chain_id) +
		      strlen_int(chainpiece->chain.model_i) +
		      strlen_int(chainpiece->chain_i) +
		      strlen_int(chainpiece->fragment_i) +
		      16);
    fn = E_MALLOC(memsize * sizeof(*fn));
    sprintf(fn,
	    "%s_%s_%i_C%i_F%i.rnachain",
	    chainpiece->chain.pdbid,
	    chainpiece->chain.chain_id,
	    chainpiece->chain.model_i,
	    chainpiece->chain_i,
	    chainpiece->fragment_i);
    return fn;
}

char*
fn_path_chainpiece(const char*dir,
		   const struct chainpiece_description*chainpiece)
{
    char *fn=NULL, *path=NULL;
    fn = fn_chainpiece(chainpiece);
    path = merge_paths(dir, fn);
    free_if_not_null(fn);
    return path;
}

void
chainpiece_description_cpy(struct chainpiece_description*to,
			   const struct chainpiece_description*from)
{
    to->chain.model_i = from->chain.model_i;
    strncpy(to->chain.pdbid, from->chain.pdbid, BUFSIZE_PDBID);
    strncpy(to->chain.chain_id, from->chain.chain_id, BUFSIZE_CHAINID);
    to->chain_i = from->chain_i;
    to->fragment_i = from->fragment_i;
}

struct chainpiece_list*
chainpiece_list_get_subset(const struct chainpiece_list*chainset_in,
			   int nchains, int*indices)
{
    struct chainpiece_list*chainset_out=NULL;
    int i;
    chainset_out = new_chainpiece_list(nchains);
    for(i = 0; i < nchains; ++i) {
	chainpiece_description_cpy(chainset_out->chainpieces + i,
				   chainset_in->chainpieces + indices[i]);
    }
    return chainset_out;
}
