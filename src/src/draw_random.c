#include "draw_random.h"

int
draw_random_int(int min, int max)
{
    int len = max - min;
    return (rand() % len) + min;
}

float
draw_random_float()
{
    return ((float) rand()) / RAND_MAX;
}

size_t
bin_search_cumulative_probability_c_arr(float p, size_t arr_size, const float*arr)
{
    const char*this_sub="bin_search_cumulative_probability_c_carr";
    size_t mid, lo=0, hi=arr_size - 1;
    while(lo <= hi) {
	mid = (lo + hi) / 2;
	if(p > arr[mid]) {
	    lo = mid + 1;
	} else if(mid == 0) {
	    return mid;
	} else if(p > arr[mid - 1]) {
	    return mid;
	} else {
	    hi = mid - 1;
	}
    }
    err_printf(this_sub, "ERROR: serious bug! binary search did not find target bin\n");
    exit(1);
    return 0;
}

size_t
bin_search_cumulative_probability(float p, const struct float_array *arr)
{
    return bin_search_cumulative_probability_c_arr(p, arr->size, arr->vals);
}
