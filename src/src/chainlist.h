#ifndef CHAINLIST_H
#define CHAINLIST_H

#include <stdlib.h>
#include <stdio.h>

#define BUFSIZE_CHAINID 8
#define BUFSIZE_PDBID 8

struct chain_description {
    char pdbid[BUFSIZE_PDBID];
    char chain_id[BUFSIZE_CHAINID];
    int model_i;
};

struct chain_list {
    struct chain_description *chains;
    size_t size;
};

struct chain_pair_description {
    struct chain_description chain_a;
    struct chain_description chain_b;
};

struct align_list {
    struct chain_pair_description *chainpairs;
    size_t size;
};

void
chain_list_destroy(struct chain_list*list);

struct chain_list*
read_chain_list(const char *fname);

void
print_chain_description(const struct chain_description*ch);

void
stream_chain_description(FILE *stream_out,
			 const struct chain_description *ch);

struct align_list*
read_align_list(const char*fname);

void
align_list_destroy(struct align_list*list);

int
is_align_list(const char*fname);

struct chainpiece_description {
    struct chain_description chain;
    int chain_i;
    int fragment_i;
};

struct chainpiece_list {
    struct chainpiece_description*chainpieces;
    size_t size;
};

struct chainpiece_list*
new_chainpiece_list(size_t size);

void
chainpiece_list_destroy(struct chainpiece_list*list);

struct chainpiece_list*
chainpiece_list_copy(const struct chainpiece_list*chainpiece_list);

struct chainpiece_list*
read_chainpiece_list(const char*fname);

int
write_chainpiece_list(const char *fname,
		      struct chainpiece_list*list);

void
stream_chainpiece_description(FILE*stream,
			      const struct chainpiece_description*chainpiece);

void
dump_chainpiece_list(struct chainpiece_list*list);

/* filenames etc. */

char*
fn_path_chainpiece(const char*dir,
		   const struct chainpiece_description*chainpiece);

void
chainpiece_description_cpy(struct chainpiece_description*to,
			   const struct chainpiece_description*from);

struct chainpiece_list*
chainpiece_list_get_subset(const struct chainpiece_list*chainset_in,
			   int nchains, int*indices);

#endif
