#ifndef RNA_CIF_COORD_H
#define RNA_CIF_COORD_H

#include "cvrry_parse_mmcif.h"

struct rna_cif_coord {
    struct cvrry_cif *cif;
    struct rna_coord *rna_backbone;
};

struct rna_cif_coord*
rna_cif_coord_read_cif(const char*filename,
		       const char*chain_id,
		       size_t model_number);

struct rna_cif_coord*
rna_cif_coord_read_binary_file(const char*fname);

int
rna_cif_coord_write_binary_file(const char*fname,
				const struct rna_cif_coord*cif_coord);

void
rna_cif_coord_destroy(struct rna_cif_coord*cif_coord);

const struct rna_coord*
rna_cif_coord_get_rna_backbone(const struct rna_cif_coord*cif_coord);

#endif
