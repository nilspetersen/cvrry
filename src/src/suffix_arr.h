#ifndef SUFFIX_ARR_H
#define SUFFIX_ARR_H

#include <stdlib.h>
#include "array.h"
#include "continuous_match.h"
#include "pair.h"

enum lcs_logp_score_type {
    LOGP_SUM,
    LOGP_LOGISTIC_SUM,
    LOGP_MAX
};

int
lcp_array_kasai(size_t *lcp,
		const int *T,
		const int *SA,
		size_t n);

int
longest_common_substring(struct continuous_match *match,
			 int min_letter,
			 const struct int_array *seq_a,
			 const struct int_array *seq_b);
int
lcs_sums(struct size_t_pair*sums,
	 int min_letter,
	 const struct int_array *seq_a,
	 const struct int_array *seq_b);

int
lcs_log_p_scores(struct float_pair*scores,
		 int min_letter,
		 const struct int_array*str_a,
		 const struct int_array*str_b,
		 const struct float_array*log_p_cumulative_a,
		 const struct float_array*log_p_cumulative_b,
		 const struct float_array*log_n_pieces_a,
		 const struct float_array*log_n_pieces_b,
		 enum lcs_logp_score_type score_type,
		 const struct float_pair*score_func_params);

#endif
