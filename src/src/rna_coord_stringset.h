#ifndef RNA_COORD_STRINGSET_H
#define RNA_COORD_STRINGSET_H

#include "double_index.h"
#include "rna_coord.h"
#include "suffix_arr.h"
#include "alphabet_index.h"

/* rna_coord_stringset is used in the optimizations */

struct rna_coord_stringset {
    size_t size;
    struct int_array**strings;
};

struct rna_coord_stringset*
make_rna_coord_stringset(const struct rna_coord_set*chains,
			 size_t fraglen);

void
rna_coord_stringset_destroy(struct rna_coord_stringset*stringset);

const struct int_array*
rna_coord_stringset_get_string(const struct rna_coord_stringset*stringset,
			       size_t i);

void
rna_coord_stringset_set(struct rna_coord_stringset*stringset,
			const struct float_matrix*const*rmsds_list,
			const struct alphabet_index*alpha_index);

int
compute_matches(struct continuous_match*matches, /* has to be the same length as pairs */
		const struct double_index*pairs,
		const struct rna_coord_stringset*stringset);

struct int_array*
rna_coord_stringset_strings_merged(const struct rna_coord_stringset*stringset);

#endif
