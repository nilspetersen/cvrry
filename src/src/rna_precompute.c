#include "rna_precompute.h"
#include "rna_frag_coord.h"
#include "mprintf.h"
#include <string.h>
#include "e_malloc.h"

/*
static size_t
max_size(size_t a, size_t b)
{
    return a > b ? a : b;
}

static size_t
maxlen_pdbid(const struct chain_list *chainlist)
{
    size_t i, maxlen=0;
    for(i = 0; i < chainlist->size; ++i) {
	maxlen = max_size(maxlen, strlen(chainlist->chains[i].pdbid));
    }
    return maxlen;
}
*/

void
pathjoin(char *dest, const char *first, const char *second)
{
    strcpy(dest, first);
    if(dest[strlen(first) - 1] != '/') {
	strcat(dest, "/");
    }
    strcat(dest, second);
}

void
make_fname_cif(char *fname, const char *directory, const char *pdbid)
{
    pathjoin(fname, directory, pdbid);
    strcat(fname, ".cif");
}

static void
make_fname_rna_chain(char *fname,
		     const char *directory,
		     const struct chain_description *ch,
		     const char*file_extension)
{
    pathjoin(fname, directory, ch->pdbid);
    strcat(fname, "_");
    strcat(fname, ch->chain_id);
    strcat(fname, file_extension);
}

void
make_fname_frag_coord(char *fname, const char *directory, const struct chain_description *ch)
{
    make_fname_rna_chain(fname, directory, ch, ".cvrry");
}

static void
make_fname_rna_chain_model(char *fname,
			     const char *directory,
			     const struct chain_description *ch,
			     const char*file_extension)
{
    pathjoin(fname, directory, ch->pdbid);
    sprintf(fname + strlen(fname), "_%s_%i%s", ch->chain_id, ch->model_i, file_extension);
}

void
make_fname_seq(char *fname, const char *directory, const struct chain_description *ch)
{
    make_fname_rna_chain_model(fname, directory, ch, ".coordseq");
}

size_t
get_bufsize_fname_bin_frag_coord(const char *outdir)
{
    return (BUFSIZE_PDBID + strlen(outdir) + BUFSIZE_CHAINID + 8) * sizeof(char);
}

int
precompute_cvrry_database(const struct chain_list *chainlist,
			  const char *indir,
			  const char *outdir,
			  const struct norm_params *norm_par)
{
    const char *this_sub="precompute_cvrry_database"; 
    size_t i;
    struct rna_frag_coord *frag_coord=NULL;
    char *fname_out=NULL;
    char *fname_cif=NULL;
    
    fname_cif = E_MALLOC(get_bufsize_fname_bin_frag_coord(indir));
    fname_out = E_MALLOC(get_bufsize_fname_bin_frag_coord(outdir));

    for(i = 0; i < chainlist->size; ++i) {
	const struct chain_description *chain = chainlist->chains + i;
	make_fname_cif(fname_cif, indir, chain->pdbid);
	frag_coord = read_n_compute_rna_frag_coord(fname_cif,
						   chain->chain_id,
						   chain->model_i);
	if(frag_coord) {
	    if(norm_par) {
		rna_frags_normalize(frag_coord->frags, norm_par);
	    }
	    make_fname_frag_coord(fname_out, outdir, chain);
	    rna_frag_coord_write_binary(frag_coord, fname_out);
	} else {
	    err_printf(this_sub,
		       ("Could not compute fragments and "
			"coordinates for %s chain %s model %i\n"),
		       chain->pdbid, chain->chain_id, chain->model_i);
	}
	rna_frag_coord_destroy(frag_coord);
    }
    
    free_if_not_null(fname_cif);
    free_if_not_null(fname_out);

    return 0;
}
