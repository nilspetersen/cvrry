#ifndef CVRRY_INDEXED_CHAINSET_H
#define CVRRY_INDEXED_CHAINSET_H

#include "rna_coord.h"
#include "double_index.h"
#include "array.h"

struct indexed_chainset {
    struct rna_coord_set*rna_chains;
    struct double_index*indices;
    struct chainpiece_list*chainpiecelist;
    struct size_t_array*fragment_pattern;
    struct rna_atom_types_array*atom_types;
};

struct indexed_chainset*
indexed_chainset_load_with_pattern(const char*fn_chainpiecelist,
				   const char*chaindir,
				   const struct size_t_array*fragment_pattern,
				   const struct rna_atom_types_array*atom_types);

struct indexed_chainset*
indexed_chainset_load(const char*fn_chainpiecelist,
		      const char*chaindir,
		      int fraglen_or_pattern);

void
indexed_chainset_destroy(struct indexed_chainset*chainset);

size_t
indexed_chainset_size(const struct indexed_chainset*chainset);

float
indexed_chainset_pair_rmsd(const struct indexed_chainset*chainset,
			   size_t i, size_t j);

struct rna_coord_set*
indexed_chainset_extract_chainpiece_set(int nfrags, const int*frags_indices,
					const struct indexed_chainset*chainset);

struct indexed_chainset*
indexed_chainset_cut_pieces(const struct indexed_chainset*chainset_in,
			    size_t minlen, size_t maxlen);

size_t
indexed_chainset_get_fraglen(const struct indexed_chainset*chainset);

struct indexed_chainset*
indexed_chainset_copy(const struct indexed_chainset*chainset);

#endif
