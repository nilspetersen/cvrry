#define BINARY_IO_HEAD(CLASS)						\
    									\
    int									\
    CLASS ## _write_binary_file(const char*fname,			\
				const struct CLASS *obj);		\
									\
    struct CLASS*							\
    CLASS ## _read_binary_file(const char*fname);

#define BINARY_IO_BODY(CLASS)						\
    									\
    int									\
    CLASS ## _write_binary_file(const char*fname,			\
				const struct CLASS *obj)		\
    {									\
	const char*this_sub="write_binary_file";			\
	FILE *f_out = mfopen(fname, "wb", this_sub);			\
	int err;							\
	if(!f_out) {							\
	    return 1;							\
	}								\
	err = CLASS ## _write_binary(f_out, obj);			\
	fclose(f_out);							\
	return err;							\
    }									\
									\
    struct CLASS*							\
    CLASS ## _read_binary_file(const char*fname)			\
    {									\
	const char*this_sub="read_binary_file";				\
	struct CLASS *obj=NULL;						\
	FILE *f_in=NULL;						\
	f_in = mfopen(fname, "rb", this_sub);				\
	if(!f_in) {							\
	    return NULL;						\
	}								\
	obj = CLASS ## _read_binary(f_in);				\
	fclose(f_in);							\
	return obj;							\
    }
