#ifndef PRECOMPUTE_H
#define PRECOMPUTE_H

#include "chainlist.h"
#include "rna_frags.h"

int
precompute_cvrry_database(const struct chain_list *chainlist,
			  const char *indir,
			  const char *outdir,
			  const struct norm_params *norm_par);

void
make_fname_frag_coord(char *fname,
		      const char *directory,
		      const struct chain_description *ch);

void
make_fname_seq(char *fname, const char *directory, const struct chain_description *ch);

size_t
get_bufsize_fname_bin_frag_coord(const char *outdir);

void
make_fname_cif(char *fname, const char *directory, const char *pdbid);

#endif
