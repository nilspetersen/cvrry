/*
 * 27 Aug 2001
 * rcsid = $Id$;
 */
#ifndef ALIGN2_H
#define ALIGN2_H

enum align_type {
    N_AND_W = 0,    /* Needleman and Wunsch (global alignment */
    S_AND_W = 1     /* Smith and Waterman (local alignment */
};

struct score_mat;
struct pair_set;

struct pair_set*
score_mat_sum_minimal(struct score_mat *smat,
		      float gap_open,
		      float gap_widen,
		      int algn_type);

#endif  /* ALIGN2_H */
