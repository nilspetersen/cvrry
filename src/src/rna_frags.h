#ifndef RNA_FRAGS_H
#define RNA_FRAGS_H

#include "rna_coord.h"

struct rna_frags {
    size_t n_par;	  /* parameters per fragment */
    size_t n_frags;	  /* number of fragments */
    size_t xmer;
    float **frags;	  /* fragments (matrix: n_frags x n_vals) */
    size_t *seq_i;	  /* position in sequence for each fragment */
};

struct rna_frags*
get_fragments(const struct rna_coord *rna);

void
rna_frags_destroy(struct rna_frags *frags);

void
rna_frags_dump(const struct rna_frags *frags);

size_t
rna_frags_write_bin(FILE *outfile, const struct rna_frags *frags);

struct rna_frags*
rna_frags_read_binary(FILE *infile);

size_t
find_frags(size_t*fragpos, size_t fraglen, const struct rna_coord *rna);

struct size_t_array*
get_fragpositions(size_t fraglen, const struct rna_coord *rna);

/* Distance list */

struct dist_list {
    size_t n_dists;
    size_t *seq_i;
    float *distances;
    size_t seq_dist;  /* interval size on the sequence*/
    enum rna_atom_types atom_type;
};

struct dist_list_set {
    size_t size;
    struct dist_list *dist_lists;
};

struct dist_list_set*
compute_distlist_set(const struct rna_coord *rna,
		     size_t max_seq_dist,
		     enum rna_atom_types atom_type);

void
dist_list_set_dump(const struct dist_list_set *dist_list_set);

void
dist_list_set_destroy(struct dist_list_set *dist_list_set);

/* weights */

struct feat_weights {
    size_t n;
    float *w;
};

void
feat_weights_destroy(struct feat_weights *weights);

struct feat_weights*
feat_weights_new(size_t n);

struct feat_weights*
feat_weights_read(const char*fname);

int
rna_frags_multiply(struct rna_frags *frags,
		   const struct feat_weights *weights);

/* normalization */

struct norm_params {
    size_t n;
    float *mu;
    float *sigma;
};

struct norm_params*
norm_params_new(size_t n);

void
norm_params_destroy(struct norm_params *frag_norm);

void
rna_frags_normalize(struct rna_frags *frags,
		    const struct norm_params *frag_norm);

struct norm_params*
norm_params_read(const char*fname, size_t n_par);

#endif	/* RNA_FRAGS_C */
