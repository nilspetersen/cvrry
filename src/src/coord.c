#include <math.h>
#include "coord.h"

float
rdist(const struct RPoint *u, const struct RPoint *v)
{
    float dx = u->x - v->x;
    float dy = u->y - v->y;
    float dz = u->z - v->z;
    return sqrt(dx*dx + dy*dy + dz*dz); 
}

static void
rdiff(struct RPoint *r_diff, const struct RPoint *u, const struct RPoint *v)
{
    r_diff->x = u->x - v->x;
    r_diff->y = u->y - v->y;
    r_diff->z = u->z - v->z;
}

static float
rdot(const struct RPoint *u, const struct RPoint *v)
{
    return u->x * v->x + u->y * v->y + u->z * v->z;
}

static void
rcross(struct RPoint *cross, const struct RPoint *u, const struct RPoint *v)
{
    cross->x = u->y * v->z - u->z * v->y;
    cross->y = u->z * v->x - u->x * v->z;
    cross->z = u->x * v->y - u->y * v->x;
}

float
chiral_volume(const struct RPoint *r_i,
	      const struct RPoint *r_j,
	      const struct RPoint *r_k,
	      const struct RPoint *r_l)
{
    struct RPoint u, v, r_jl, r_kl;
    rdiff(&u, r_i, r_l);
    rdiff(&r_jl, r_j, r_l);
    rdiff(&r_kl, r_k, r_l);
    rcross(&v, &r_jl, &r_kl);
    return rdot(&u, &v);
}

void
rsubtract(struct RPoint *u, const struct RPoint *v)
{
    u->x -= v->x;
    u->y -= v->y;
    u->z -= v->z;
}

void
radd(struct RPoint *u, const struct RPoint *v)
{
    u->x += v->x;
    u->y += v->y;
    u->z += v->z;
}
