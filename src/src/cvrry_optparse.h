#ifndef CVRRY_OPTPARSE_H
#define CVRRY_OPTPARSE_H

#include <getopt.h>

void
getarg(char **target, const char *src);

void
print_optslist(const struct option *long_options);

#endif
