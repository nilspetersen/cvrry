#include "e_malloc.h"
#include "cvrry_pairwise.h"
#include <stdlib.h>
#include "mprintf.h"
#include <getopt.h>
#include "cvrry_optparse.h"
#include "yesno.h"
#include "rna_precompute.h"
#include "fio.h"
#include "cvrry_defaults.h"

/* OPTIONS */

struct cvrry_pairwise_options*
cvrry_pairwise_options_new()
{
    struct cvrry_pairwise_options*opts=NULL;
    opts = E_MALLOC(sizeof(*opts));

    /* set paths */
    opts->paths.fname_dataset_file = NULL;
    opts->paths.path_cvrry_db = NULL;

    /* set streams */
    opts->streams.metrics = stdout;
    opts->streams.alignments = NULL;

    /* feature weights and normalization */
    opts->align_cmp_pars.weights = NULL;
    opts->align_cmp_pars.frag_norm = NULL;

    /* alignment parameters */
    opts->align_cmp_pars.params.gap_open = 2.07671; /* 2.53; */
    opts->align_cmp_pars.params.gap_widen = 0.548336; /* 0.16; */ 
    opts->align_cmp_pars.params.algn_type = N_AND_W;
    opts->align_cmp_pars.params.scale_factor = 1.0;
    opts->align_cmp_pars.params.base_pair_bonus = 0.0;

    /* cmp_opts */
    opts->align_cmp_pars.cmp_opts.verbose_err = NO;
    opts->align_cmp_pars.cmp_opts.save_alignment = NO;
    opts->align_cmp_pars.cmp_opts.calc_rmsd = YES;
    opts->align_cmp_pars.cmp_opts.calc_psi = YES;
    opts->align_cmp_pars.cmp_opts.calc_psd = NO;
    opts->align_cmp_pars.cmp_opts.psd_thresh = 5.0;
    opts->align_cmp_pars.cmp_opts.get_alignment_score = YES;
    opts->align_cmp_pars.cmp_opts.superpos_opt = NONE;

    /* the rest */
    opts->cmp_type = ALL_VS_ALL;
    
    return opts;
}

void
cvrry_pairwise_options_destroy(struct cvrry_pairwise_options *opt)
{
    if(opt) {
	free_if_not_null(opt->paths.fname_dataset_file);
	free_if_not_null(opt->paths.path_cvrry_db);
	close_if_open_file(opt->streams.metrics);
	close_if_open_file(opt->streams.alignments);
	feat_weights_destroy(opt->align_cmp_pars.weights);
	norm_params_destroy(opt->align_cmp_pars.frag_norm);
	free(opt);
    }
}

static void
cvrry_pairwise_print_usage(char*program_name)
{
    mfprintf(stderr, "usage: %s dataset_file path_to_cvrry_files\n", program_name);
    mfprintf(stderr, "[use -h to list options]\n");
}

static void
print_help(char *program_name, const struct option *long_options)
{
    mprintf("usage: %s dataset_file path_to_cvrry_files\n", program_name);
    mprintf("Options:\n");    
    print_optslist(long_options);
}

struct feat_weights*
feat_weights_parse_args(const char*comma_separated_str)
{
    const char *this_sub="feat_weights_parse_args";
    struct feat_weights*weights = feat_weights_new(6);
    float *w = weights->w;
    int n_scanned = sscanf(comma_separated_str,
			   "%f,%f,%f,%f,%f,%f",
			   w, w+1, w+2, w+3, w+4, w+5);
    if(n_scanned != 6) {
	err_printf(this_sub,
		   "failed to parse 6 weights form command line argument %s\n",
		   comma_separated_str);
	feat_weights_destroy(weights);
	weights = NULL;
    }
    return weights;
}

int
get_cvrry_pairwise_options(struct cvrry_pairwise_options *opt,
			   int argc,
			   char *argv[])
{
    const char *this_sub = "get_cvrry_pairwise_options";
    int c;
    int err=0;
    enum yes_no default_feature_normalization=YES;
    enum yes_no default_weights=YES;
    enum yes_no found_positional_arguments=NO;
    struct rna_cmp_opts *cmp_opts = &opt->align_cmp_pars.cmp_opts;
    struct rna_align_params *align_params = &opt->align_cmp_pars.params;
    struct option long_options[] =
        {
	    /* stuff read from files */
	    {"weights_infile", required_argument, 0, 'i'},
	    {"normalization_infile", required_argument, 0, 'm'},
	    {"alignments_outfile", required_argument, 0, 'o'},
	    /* flags to do something or to leave it */
	    {"psi", no_argument, 0, 'a'},
	    {"no_psi", no_argument, 0, 'b'},
	    {"psd", no_argument, 0, 'c'},
	    {"no_psd", no_argument, 0, 'd'},
	    {"get_scores", no_argument, 0, 'e'},
	    {"no_scores", no_argument, 0, 'f'},
	    /* tresholds */
	    {"psd_thresh", required_argument, 0, 'j'},
	    /* gap costs */
	    {"gap_open", required_argument, 0, 'k'},
	    {"gap_widen", required_argument, 0, 'l'},
	    /* weights */
	    {"weights", required_argument, 0, 'w'},
            {"no_featnorm", no_argument, 0, 'n'},
	    /* align who vs who */
	    {"one_vs_all", no_argument, 0, 'p'},
	    /* help */
	    {"help", no_argument, 0, 'h'},
	    {0, 0, 0, 0}
        };
    if(argc >= 3) {
	getarg(&opt->paths.fname_dataset_file, argv[1]);
	getarg(&opt->paths.path_cvrry_db, argv[2]);
	found_positional_arguments = YES;
    }
    while((c = getopt_long(argc,
			   argv,
			   "abcdefj:k:l:m:nw:o:ph",
			   long_options,
			   NULL)) != -1 && !err) {
	switch(c) {
	case 'a':
	    cmp_opts->calc_psi = YES;
	    break;
	case 'b':
	    cmp_opts->calc_psi = NO;
	    break;
	case 'c':
	    cmp_opts->calc_psd = YES;
	    break;
	case 'd':
	    cmp_opts->calc_psd = NO;
	    break;
	case 'e':
	    cmp_opts->get_alignment_score = YES;
	    break;
	case 'f':
	    cmp_opts->get_alignment_score = NO;
	    break;
	case 'j':
	    cmp_opts->calc_psd = YES;
	    cmp_opts->psd_thresh = atof(optarg);
	    break;
	case 'k':
	    align_params->gap_open = atof(optarg);
	    break;
	case 'l':
	    align_params->gap_widen = atof(optarg);
	    break;
	case 'm':
	    norm_params_destroy(opt->align_cmp_pars.frag_norm);
	    opt->align_cmp_pars.frag_norm = norm_params_read(optarg, 6);
	case 'n':
	    default_feature_normalization=NO;
	    break;
	case 'o':
	    opt->align_cmp_pars.cmp_opts.save_alignment = YES;
	    opt->streams.alignments = mfopen(optarg, "w", this_sub);
	    err = !opt->streams.alignments;
	    break;
	case 'i':
	    feat_weights_destroy(opt->align_cmp_pars.weights);
	    opt->align_cmp_pars.weights = feat_weights_read(optarg);
	    err = !opt->align_cmp_pars.weights;
	    default_weights = NO;
	    break;
	case 'w':
	    feat_weights_destroy(opt->align_cmp_pars.weights);
	    opt->align_cmp_pars.weights = feat_weights_parse_args(optarg);
	    err = !opt->align_cmp_pars.weights;
	    default_weights = NO;
	    break;
	case 'p':
	    opt->cmp_type = ONE_VS_ALL;
	    break;
	case 'h':
	    print_help(argv[0], long_options);
	    return 1;
	case '?':
	default:
	    err_printf(this_sub, "option parsing error");
	    return 1;
	}
    }
    if(!found_positional_arguments) {
	cvrry_pairwise_print_usage(argv[0]);
	return 1;
    }
    if(default_feature_normalization) {
	opt->align_cmp_pars.frag_norm = default_normalization();
    }
    if(default_weights) {
	opt->align_cmp_pars.weights = default_feat_weights();
    }
    return err;
}

/* STREAM OUTPUT */
static void
stream_float_conditional(FILE *stream_out, enum yes_no do_it, float val)
{
    if(do_it) {
	mfprintf(stream_out, " %f", val);
    }
}

static void
stream_comparison_results(FILE *stream_out,
			  const struct chain_description *chain_descr_a,
			  const struct chain_description *chain_descr_b,
			  const struct rna_cmp_opts *opts,
			  const struct rna_comparison_result *res)
{
    stream_chain_description(stream_out, chain_descr_a);
    mfprintf(stream_out, " ");
    stream_chain_description(stream_out, chain_descr_b);
    stream_float_conditional(stream_out, opts->calc_rmsd, res->rmsd);
    stream_float_conditional(stream_out, opts->calc_psi, res->psi);
    stream_float_conditional(stream_out, opts->get_alignment_score, res->align_score);
    stream_float_conditional(stream_out, opts->get_alignment_score, res->align_score_smpl);
    stream_float_conditional(stream_out, opts->calc_psd, res->psd);
    mfprintf(stream_out, "\n");
}

static void
stream_section_header(FILE *stream_out, const char*name)
{
    mfprintf(stream_out, ">%s\n", name);
}

static void
stream_entry_end(FILE *stream_out)
{
    mfprintf(stream_out, ";\n");
}

static void
stream_single_size_t_entry(FILE *stream_out, size_t val)
{
    mfprintf(stream_out, "%zu", val);
    stream_entry_end(stream_out);
}

static void
stream_alignment_indices(FILE *stream_out,
			 const struct pair_set *alignment)
{
    size_t i, j;
    for(i = 0; i < alignment->m; ++i) {
	for(j = 0; j < alignment->n; ++j) {
	    int ii = alignment->indices[j][i];
	    if(ii == GAP_INDEX) {
		mfprintf(stream_out, "   - ");
	    } else {
		mfprintf(stream_out, "%4i ", ii);
	    }
	    if(!((j+1) % 10)) {
		mfprintf(stream_out, "\n");
	    }
	}
	stream_entry_end(stream_out);
    }
}

static void
stream_rna_sequence(FILE *stream_out,
		    const struct rna_coord *chain)
{
    size_t i;
    for(i = 0; i < chain->size; ++i) {
	mfprintf(stream_out, "%c", chain->seq->seq[i]);
	if(!((i+1) % 50)) {
	    mfprintf(stream_out, "\n");
	}
    }
    stream_entry_end(stream_out);
}

static void
stream_nucleotide_ids(FILE *stream_out, const struct rna_coord *chain)
{
    size_t i;
    for(i = 0; i < chain->size; ++i) {
	mfprintf(stream_out, "%4i%c ", chain->res[i].resnum,
		 chain->res[i].icode == '?' ? ' ' : chain->res[i].icode);
	if(!((i+1) % 10)) {
	    mfprintf(stream_out, "\n");
	}
    }
    stream_entry_end(stream_out);
}

static void
stream_chain_descr(FILE *stream_out, const struct chain_description *ch)
{
    stream_chain_description(stream_out, ch);
    stream_entry_end(stream_out);    
}

static void
stream_alignment_and_info(FILE *stream_out,
			  const struct chain_description *chain_descr_a,
			  const struct chain_description *chain_descr_b,
			  const struct pair_set *alignment,
			  const struct rna_coord *chain_a,
			  const struct rna_coord *chain_b)
{
    stream_section_header(stream_out, "CHAINS");
    stream_chain_descr(stream_out, chain_descr_a);
    stream_chain_descr(stream_out, chain_descr_b);
    stream_section_header(stream_out, "LENGTHS");
    stream_single_size_t_entry(stream_out, chain_a->size);
    stream_single_size_t_entry(stream_out, chain_b->size);
    stream_section_header(stream_out, "ALIGNMENT");
    stream_alignment_indices(stream_out, alignment);
    stream_section_header(stream_out, "SEQUENCES");
    stream_rna_sequence(stream_out, chain_a);
    stream_rna_sequence(stream_out, chain_b);
    stream_section_header(stream_out, "STRUCTURE_FILE_NUCEOTIDE_IDS");
    stream_nucleotide_ids(stream_out, chain_a);
    stream_nucleotide_ids(stream_out, chain_b);
}

static int
weight_if_necessary(struct rna_frag_coord *rna,
		    const struct feat_weights *weights)
{
    if(weights == NULL) {
	return 0;
    }
    return rna_frags_multiply(rna->frags, weights);
}

static int
load_binary_rna(struct rna_frag_coord **rna,
		char*fname_buf,
		const struct chain_description *ch,
		const char*db_path,
		const struct feat_weights *weights,
		const struct norm_params *frag_norm,
		enum yes_no verbose_err)
{
    rna_frag_coord_destroy(*rna);
    make_fname_frag_coord(fname_buf, db_path, ch);
    *rna = rna_frag_coord_read_binary(fname_buf, verbose_err);
    if(!*rna) {
	return 1;
    }
    if(frag_norm) {
	rna_frags_normalize((*rna)->frags, frag_norm);
    }
    if(weight_if_necessary(*rna, weights)) {
	rna_frag_coord_destroy(*rna);
	*rna = NULL;
	return 1;
    }
    return 0;
}

static int
rna_comparison_w_stream(struct rna_comparison_result *cmp_res,
			FILE *stream_metrics,
			FILE *stream_alignments,
			const struct rna_align_params *params,
			const struct rna_cmp_opts *opts,
			struct rna_frag_coord *rna_a,
			struct rna_frag_coord *rna_b,
			const struct chain_description *ch_descr_a,
			const struct chain_description *ch_descr_b)
{
    const char *this_sub="rna_comparison_w_stream";
    int err=0;

    if(!err) {
	err = rna_structure_comparison(cmp_res, rna_a, rna_b, params, opts);
	if(err && opts->verbose_err) {
	    err_printf(this_sub,
		       "could not align %s|%s|%i and %s|%s|%i\n",
		       ch_descr_a->pdbid,
		       ch_descr_a->chain_id,
		       ch_descr_a->model_i,
		       ch_descr_b->pdbid,
		       ch_descr_b->chain_id,
		       ch_descr_b->model_i);
	}
    }

    if(!err) {
	if(stream_metrics) {
	    stream_comparison_results(stream_metrics,
				      ch_descr_a,
				      ch_descr_b,
				      opts,
				      cmp_res);
	}
	if(stream_alignments) {
	    stream_alignment_and_info(stream_alignments,
				      ch_descr_a,
				      ch_descr_b,
				      cmp_res->alignment,
				      rna_a->rna_backbone,
				      rna_b->rna_backbone);
	}
    }
    return err;
}

int
one_vs_all_align(struct cvrry_pairwise_options*opt,
		 const struct chain_description *ch,
		 const struct chain_list *chainlist,
		 size_t start_i)
{
    struct rna_frag_coord *rna_a=NULL;
    struct rna_frag_coord *rna_b=NULL;
    char *fname_buf=NULL;
    struct rna_comparison_result *cmp_res=new_rna_comparison_result(&opt->align_cmp_pars.cmp_opts);
    int err=0, err2=0;
    size_t j;
    char *db_path = opt->paths.path_cvrry_db;

    fname_buf = E_MALLOC(get_bufsize_fname_bin_frag_coord(db_path));
    err = load_binary_rna(&rna_a,
			  fname_buf,
			  ch, db_path,
			  opt->align_cmp_pars.weights,
			  opt->align_cmp_pars.frag_norm,
			  opt->align_cmp_pars.cmp_opts.verbose_err);

    for(j = start_i; j < chainlist->size && !err; ++j) {
	err2 = load_binary_rna(&rna_b,
			       fname_buf,
			       chainlist->chains + j,
			       db_path,
			       opt->align_cmp_pars.weights,
			       opt->align_cmp_pars.frag_norm,
			       opt->align_cmp_pars.cmp_opts.verbose_err);
	if(!err2) {
	    rna_comparison_w_stream(cmp_res,
				    opt->streams.metrics,
				    opt->streams.alignments,
				    &opt->align_cmp_pars.params,
				    &opt->align_cmp_pars.cmp_opts,
				    rna_a,
				    rna_b,
				    ch,
				    chainlist->chains + j);
	}
    }
    rna_comparison_result_destroy(cmp_res);
    free_if_not_null(fname_buf);
    rna_frag_coord_destroy(rna_a);
    rna_frag_coord_destroy(rna_b);
    return err || err2;
}

int
all_pairs_align(struct cvrry_pairwise_options*opt,
		const struct chain_list *chainlist)
{
    int err=0;
    size_t i;
    for(i = 0; i < chainlist->size - 1; ++i) {
	err += one_vs_all_align(opt, chainlist->chains + i, chainlist, i + 1);
    }
    return err;
}

static int
chainpair_align(struct cvrry_pairwise_options*opt,
		const struct chain_pair_description *chainpair)
{
    char *fname_buf=NULL;
    int err=0;
    struct rna_frag_coord *rna_a=NULL;
    struct rna_frag_coord *rna_b=NULL;
    struct rna_comparison_result *cmp_res=new_rna_comparison_result(&opt->align_cmp_pars.cmp_opts);
    char *db_path = opt->paths.path_cvrry_db;
    struct feat_weights *weights = opt->align_cmp_pars.weights;
    
    fname_buf = E_MALLOC(get_bufsize_fname_bin_frag_coord(db_path));

    err = load_binary_rna(&rna_a,
			  fname_buf,
			  &chainpair->chain_a,
			  db_path,
			  weights,
			  opt->align_cmp_pars.frag_norm,
			  opt->align_cmp_pars.cmp_opts.verbose_err);
    if(!err) {
	err = load_binary_rna(&rna_b,
			      fname_buf,
			      &chainpair->chain_b,
			      db_path,
			      weights,
			      opt->align_cmp_pars.frag_norm,
			      opt->align_cmp_pars.cmp_opts.verbose_err);
    }
    if(!err) {
	err = rna_comparison_w_stream(cmp_res,
				      opt->streams.metrics,
				      opt->streams.alignments,
				      &opt->align_cmp_pars.params,
				      &opt->align_cmp_pars.cmp_opts,
				      rna_a,
				      rna_b,
				      &chainpair->chain_a,
				      &chainpair->chain_b);
    }
    rna_comparison_result_destroy(cmp_res);
    free_if_not_null(fname_buf);
    rna_frag_coord_destroy(rna_a);
    rna_frag_coord_destroy(rna_b);
    return err;
}

int
alignlist_align(struct cvrry_pairwise_options*opt,
		const struct align_list *alignlist)
{
    int err=0;
    size_t i;
    for(i = 0; i < alignlist->size; ++i) {
	err += chainpair_align(opt, alignlist->chainpairs + i);
    }
    return err;
}
