#ifndef RNA_COORD_H
#define RNA_COORD_H

#include "coord.h"
#include "seq.h"
#include "yesno.h"
#include <stdio.h>
#include "chainlist.h"
#include "array.h"
#include "wrapped_matrix.h"
#include "alphabet_index.h"
#include "binary_file.h"

enum rna_atom_types { /* ordered like they are in pdb files, don't change ! */
    P=0,
    O5=1,
    C5=2,
    C4=3,
    O4=4,
    C3=5,
    O3=6,
    C2=7,
    C1=8,
    NOT_A_BACKBONE_ATOM
};

ARRAY_CLASS_HEAD(enum, rna_atom_types)

enum rna_atom_patterns {
    ALL=0
};

enum resnum_types {
    ORIGINAL_PDB_STYLE,
    SEQUENCE_ZERO_TO_ONE
};

struct rna_atom {
    enum yes_no is_set;
    struct RPoint r;
};

struct rna_backbone_nuc {
    struct rna_atom c1, c2, c3, c4, c5;
    struct rna_atom o3, o4, o5;
    struct rna_atom p;
};

struct rna_res_id {
    int resnum;	/* residue numbers in pdb file */
    char icode;	/* insertion code in pdb file */
};

struct rna_coord {
    int src_version;		/* checkpoint to make sure the files fit the current code */
    struct rna_backbone_nuc *bb;/* backbone atoms for each nucleotide */
    struct seq *seq; 		/* sequence - one letter per nucleotide */
    struct char_matrix *sequence; /* sequence with full nucleotide names */
    struct rna_res_id *res;	/* residue number and insertion code as in pdb-file */
    size_t size; 		/* number of residues */
    int *cif_seq_num;		/* sequence index as counted in mmcif-files */
    enum yes_no *chain_breaks;	/* chain break info for each residue */
    int *basepair_info;		/* information about basepairs encode in integers */
};

struct rna_coord_set {
    size_t n_chains;
    struct rna_coord **chains;
};

struct rna_atom_types_array*
rna_atom_types_array_create(enum rna_atom_patterns pattern);

enum rna_atom_types
get_atom_type_from_str(const char*atom_type_str);

struct rna_atom*
get_atom_ptr(enum rna_atom_types atype, struct rna_backbone_nuc *nuc);

const struct rna_atom*
get_const_atom_ptr(enum rna_atom_types atype,
		   const struct rna_backbone_nuc *nuc);

/* rna_coord */

void
reset_rna_backbone_nuc(struct rna_backbone_nuc *bb);

size_t
rna_coord_get_size(const struct rna_coord*rna);

size_t
rna_coord_seqlen_by_mmcif_indices(const struct rna_coord*rna);

struct rna_coord*
new_rna_coord(size_t n_res);

struct rna_coord*
rna_coord_copy(const struct rna_coord *rna);

struct rna_coord*
rna_coord_copy_piece(const struct rna_coord *old,
		     size_t start_i,
		     size_t n_res);

void
rna_coord_destroy(struct rna_coord *coord);

void 
rna_coord_dump(const struct rna_coord *coord);

void
rna_coord_chain_breaks_dump(const struct rna_coord *rna);

void
rna_coord_2_pdb_file(FILE *f, const struct rna_coord *coord);

int
rna_coord_2_pdb(const struct rna_coord *coord, const char *fname);

void
compute_chain_breaks(struct rna_coord *rna);

size_t
rna_coord_write_bin(FILE *outfile, const struct rna_coord *rna);

int
rna_coord_write_binary(FILE *outfile, const struct rna_coord *rna);

int
rna_coord_write_binary_file(const char*fname, const struct rna_coord *rna);

struct rna_coord*
rna_coord_read_binary(FILE *infile);

struct rna_coord*
rna_coord_read_binary_file(const char*fname);

size_t
count_chain_breaks(const struct rna_coord *rna);

void
get_atom_name(enum rna_atom_types atype, char *name);

void
rna_coord_set_destroy(struct rna_coord_set *rna_coordset);

struct rna_coord_set*
new_rna_coord_set(size_t max_n_chains);

void
rna_coord_set_add_chain(struct rna_coord_set*coord_set,
			const struct rna_coord*rna);

struct rna_coord_set*
extract_chains(const struct rna_coord *rna,
	       const enum rna_atom_types *atom_types,
	       size_t n_atomtypes);

int
continuous_chains_to_pieces(struct rna_coord_set**coord_set_out,
			    struct chainpiece_list**chainlist_out,
			    const struct rna_coord_set*coord_set_in,
			    const struct chainpiece_list*chainlist_in,
			    size_t minlen,
			    size_t maxlen);

struct rna_coord_set*
load_rna_coord_set(const char*dir,
		   const struct chainpiece_list*chainpiece_list);

const struct rna_coord*
rna_coord_set_get_rna(const struct rna_coord_set*rna_coordset, size_t i);

struct rna_coord*
rna_coord_set_get_rna_copy(const struct rna_coord_set*rna_coordset, size_t i);

struct rna_coord_set*
rna_coord_set_copy(const struct rna_coord_set*rna_coordset);

void
rna_coord_set_dump(const struct rna_coord_set *rna_coordset);

struct rna_coord*
read_binary_chainpiece(const char*dir,
		       const struct chainpiece_description*chainpiece);


size_t
rna_coord_set_write_bin(FILE*outfile,
			const struct rna_coord_set *rna_coordset);

int
rna_coord_set_write_binary(FILE*outfile,
			   const struct rna_coord_set *rna_coordset);

int
rna_coord_set_write_binary_file(const char*fname,
				const struct rna_coord_set *rna_coordset);

struct rna_coord_set*
rna_coord_set_read_binary(FILE*infile);

struct rna_coord_set*
rna_coord_set_read_binary_file(const char*fname);

const struct rna_res_id*
rna_coord_get_res_id(const struct rna_coord*rna, size_t i);

enum yes_no
rna_coord_same(const struct rna_coord*rna_a,
	       const struct rna_coord*rna_b);

struct rna_coord_set*
rna_coord_set_get_subset(const struct rna_coord_set*chainset_in,
			 int nchains, int*indices);

struct float_matrix*
calc_rna_coord_vs_alphabet_rmsds_chainbreaks(const struct rna_coord*rna,
					     const struct size_t_array*positions,
					     const struct rna_coord_set*alphabet,
					     const struct size_t_array*fragment_pattern,
					     const struct rna_atom_types_array*atom_types);

struct float_matrix*
calc_rna_coord_vs_alphabet_rmsds(const struct rna_coord*rna,
				 const struct rna_coord_set*alphabet,
				 const struct size_t_array *fragment_pattern,
				 const struct rna_atom_types_array*atom_types);

struct float_matrix**
calc_rna_coord_set_vs_alphabet_rmsds(const struct rna_coord_set*rna_set,
				     const struct rna_coord_set*alphabet,
				     const struct size_t_array *fragment_pattern,
				     const struct rna_atom_types_array*atom_types);

int
write_coordset_2pdb(const char *fname,
		    struct rna_coord_set *rna_coord_set);

struct float_matrix*
rna_coordset_get_rmsds_merged(const struct rna_coord_set*chains,
			      const struct size_t_array*fragment_pattern,
			      const struct rna_atom_types_array*atom_types);

struct rna_coord_set*
rna_coord_set_extract_subset(const struct rna_coord_set*chainset_in,
			     const struct size_t_array*indices);

struct rna_coord_set*
rna_coord_set_alphabet_selection(const struct rna_coord_set*chains,
				 const struct alphabet_index*indices);

void
rna_coord_set_reset_resnum(struct rna_coord_set*rna_coord_set);

int
rna_coord_set_write_separate_chains(const struct rna_coord_set*coord_set,
				    const struct chainpiece_list*chainlist,
				    const char*dir_out);

void
set_glob_old_read(int newval);

#endif
