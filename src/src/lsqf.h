#ifndef LSQF_H
#define LSQF_H

#include "rna_coord.h"
#include "pair_set.h"
#include "suffix_arr.h"
#include "double_index.h"

struct rotation_matrix {
    float rmat[3][3];
};

struct transformation {
    struct rotation_matrix rotate_a;
    struct RPoint com_a;
    struct RPoint com_b;
};

void
rotate_rpoint(struct RPoint *r,
	      const struct rotation_matrix *rot_mat);

int 
rna_coord_rmsd(struct transformation *transform,
	       float *rmsd,
	       const struct pair_set *pairset,
	       const struct rna_coord *source, 
	       const struct rna_coord *target);

int
rna_coord_rmsd_continuous_match(struct transformation *transform,
				float *rmsd,
				struct continuous_match match,
				const struct rna_coord *source, 
				const struct rna_coord *target,
				size_t fraglen);

struct pair_set*
rna_coord_superimpose(const struct pair_set *pairset,
		      const struct rna_coord *source, 
		      const struct rna_coord *target,
		      float rmsd_thresh);

struct pair_set*
maxsub(const struct pair_set *alignment,
       const struct rna_coord *source,
       const struct rna_coord *target,
       float dist_thresh,
       size_t seedlen);

/* used in prosup.c: */

struct pair_set*
maxsub_core(const struct pair_set *alignment,
	    size_t seedlen,
	    struct RPoint *bb_source,
	    struct RPoint *bb_target,
	    float dist_thresh);

int
rmsd_superpos_core(struct transformation *transform,
		   float *rmsd,
		   struct RPoint *bb_source,
		   struct RPoint *bb_target,
		   size_t n_atoms);

struct pair_set*
maxsub_refine(struct RPoint *r1,
	      struct RPoint *r2,
	      float dist_thresh,
	      const struct pair_set *pairset_no_gaps);

int
rmsd_fixed_chainlen(float *rmsd,
		    const struct rna_coord *rna_a,
		    const struct rna_coord *rna_b,
		    size_t start_i1,
		    size_t start_i2,
		    const struct size_t_array*positions,
		    const struct rna_atom_types_array*atom_types,
		    enum yes_no stop_if_atoms_missing);

int
compute_rmsds_matches(float *rmsds,	/* needs to be same size as matches and pairs */
		      const struct double_index*pairs,
		      const struct continuous_match*matches,
		      const struct rna_coord_set*chainset,
		      size_t fraglen);

struct rna_coord*
rna_coord_transform_superposed(const struct rna_coord*rna_in,
			       const struct transformation *transform);

#endif
