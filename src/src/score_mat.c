/*
 * For manipulations of score matrices.
 */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "e_malloc.h"
#include "fio.h"
#include "matrix.h"
#include "mprintf.h"
#include "score_mat.h"
#include "scratch.h"
#include "seq.h"

/* ---------------- score_mat_new   ---------------------------
 * Given the size of two things (sequences, structures),
 * allocate memory for a score matrix.
 * WATCH OUT !
 * There is a rule.. Score matrices have two extra columns and
 * two extra rows. This means makes it easier to implement
 * some scoring schemes with fancy end penalties. The extra
 * rows and columns are at the start and end of each scored
 * thing.
 */
struct score_mat*
score_mat_new (const size_t n_rows, const size_t n_cols)
{
    float **mat;
    struct score_mat *ret_mat;
    mat = f_matrix (n_rows + 2, n_cols + 2);
    memset (mat[0], (int)0.0, sizeof (mat[0][0]) * (n_rows+2) * (n_cols+2));
    ret_mat = E_MALLOC (sizeof (*ret_mat));
    ret_mat->mat = mat;
    ret_mat->n_rows = n_rows + 2;
    ret_mat->n_cols = n_cols + 2;
    return (ret_mat);
}

struct score_mat*
score_mat_copy(const struct score_mat*old_mat)
{
    struct score_mat*new_mat=NULL;
    new_mat = score_mat_new(old_mat->n_rows - 2, old_mat->n_cols - 2);
    memcpy(new_mat->mat[0], old_mat->mat[0],
	   old_mat->n_rows * old_mat->n_cols * sizeof(old_mat->mat[0][0]));
    return new_mat;
}


/* ---------------- score_mat_destroy -------------------------
 */
void
score_mat_destroy (struct score_mat *smat)
{
    const char *this_sub = "score_mat_destroy";
    if (smat == NULL) {
        err_printf (this_sub, "called to delete null score matrix\n");
        return ;
    }
    if ( smat->mat)
        kill_f_matrix (smat->mat);
    else
        err_printf (this_sub, "Called to delete score mat with no matrix\n");
    smat->mat = NULL; /* only to provoke an error if we try again */
    free (smat);
}

/* ---------------- score_mat_shift ---------------------------
 * Add a constant value to a score matrix
 * This one returns a new score matrix.
 * We shift all elements, except first and last row and column.
 */
struct score_mat *
score_mat_shift (struct score_mat *mat1, const float shift)
{
    struct score_mat *res;
    size_t i, j;
    const size_t n_rows = mat1->n_rows;
    const size_t n_cols = mat1->n_cols;
    const size_t last_row = n_rows - 1;
    const size_t last_col = n_cols - 1;
    res = score_mat_new (n_rows - 2, n_cols - 2);
    for (i = 1; i < last_row; i++)
        for (j = 1; j < last_col ; j++)
            res->mat[i][j] = mat1->mat[i][j] + shift;
    return res;
}


/* ---------------- score_mat_info   --------------------------
 * To give an idea of the size of elements in a score matrix.
 * Do not look at first or last rows or columns.
 */
void
score_mat_info (const struct score_mat *score_mat,
                float *min,
		float *max,
		float *av,
                float *std_dev)
{
    float **mat = score_mat->mat;
    double sum = 0;
    double sumsq = 0;
    unsigned n = 0;
    size_t i, j;
    const size_t last_row = score_mat->n_rows - 1;
    const size_t last_col = score_mat->n_cols - 1;
    *min = *max = mat[1][1];
    for (i = 1; i < last_row; i++) {
        for (j = 1; j < last_col; j++) {
            float f = mat[i][j];
            if (f < *min)
                *min = f;
            if (f > *max)
                *max = f;
            sum += f;
            sumsq += (f * f);
            n++;
        }
    }
    *av   = (float) (sum / n);
    *std_dev = (float) (sqrtf (n * sumsq - (sum * sum)) / n);
}

void
dump_score_mat(const struct score_mat*smat)
{
    dump_f_matrix(smat->mat, smat->n_rows, smat->n_cols);
}
