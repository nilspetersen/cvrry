#include "prosup.h"
#include "score_mat.h"
#include "align.h"
#include "e_malloc.h"
#include "lsqf.h"

static float
dist_score(const struct RPoint *r1,
	   const struct RPoint *r2)
{
    float dx, dy, dz;
    dx = r1->x - r2->x;
    dy = r1->y - r2->y;
    dz = r1->z - r2->z;
    return 1.0 / (1 + (dx*dx + dy*dy + dz*dz));
}

static void
fill_matrix_prosup(struct score_mat *score_matrix,
		   const struct RPoint *r1,
		   const struct RPoint *r2,
		   size_t len1,
		   size_t len2)
{
    size_t i, j;
    for(i = 0; i < len1; ++i) {
	for(j = 0; j < len2; ++j) {
	    score_matrix->mat[i + 1][j + 1] += dist_score(r1 + i, r2 + j);
	}
    }
}

static void
pairset_shift(struct pair_set *pset, int i, int j)
{
    size_t ii;
    for(ii = 0; ii < pset->n; ++ii) {
	if(pset->indices[ii][0] != GAP_INDEX) {
	    pset->indices[ii][0] += i;
	}
	if(pset->indices[ii][1] != GAP_INDEX) {
	    pset->indices[ii][1] += j;
	}
    }
}

/* dp_align_superimposed
 * an alignment is calculated based on the superimposed coordinates
 * r1 and r2. i1, i2, j1 and j2 give the start and end position
 * in chains r1 and r2 respectively
 */

struct pair_set*
dp_align_superimposed(const struct RPoint *r1,
		      const struct RPoint *r2,
		      int i1,
		      int i2,
		      int j1,
		      int j2)
{
    struct score_mat *score_matrix=NULL;
    struct pair_set *alignment=NULL;
    int len1, len2;
    
    if(i1 > i2 || j1 > j2 ) {
	/* err printf or swap ?? */
	return NULL;
    }

    len1 = i2 - i1 + 1;
    len2 = j2 - j1 + 1;

    score_matrix = score_mat_new(len1, len2);

    fill_matrix_prosup(score_matrix, 
		       r1 + i1,
		       r2 + j1,
		       len1,
		       len2);

    alignment = score_mat_sum_minimal(score_matrix, 0, 0, N_AND_W);

    pairset_shift(alignment, i1, j1);

    score_mat_destroy(score_matrix);
    return alignment;
}

static void
pair_set_add_pairset(struct pair_set *target, const struct pair_set *add_this)
{
    size_t i;
    for(i = 0; i < add_this->n; ++i) {
	pair_set_add_pair(target, add_this->indices[i][0], add_this->indices[i][1]);
    }
}

static void 
find_and_add_pairs_superimposed(struct pair_set *alignment,
		      const struct RPoint*r1,
		      const struct RPoint*r2,
		      int i1,
		      int i2,
		      int j1,
		      int j2)
{
    struct pair_set *new_pairs = dp_align_superimposed(r1, r2, i1, i2, j1, j2);
    pair_set_add_pairset(alignment, new_pairs);
    pair_set_destroy(new_pairs);
}

struct pair_set*
dp_fill_gaps(const struct RPoint*r1,
	     const struct RPoint*r2,
	     size_t len1,
	     size_t len2,
	     const struct pair_set *matches)  /* matching pairs WITHOUT gaps */
{
    size_t k;
    struct pair_set *alignment=NULL;
    alignment = pair_set_new(len1 + len2);
    if(matches->indices[0][0] > 0 && matches->indices[0][1] > 0) {	
	find_and_add_pairs_superimposed(alignment, r1, r2, 
					0, matches->indices[0][0] - 1,
					0, matches->indices[0][1] - 1);
    }
    for(k = 0; k < matches->n - 1; ++k) {
	int i = matches->indices[k][0];
	int j = matches->indices[k][1];
	int ii = matches->indices[k+1][0];
	int jj = matches->indices[k+1][1];
	pair_set_add_pair(alignment, i, j);
	if(i < ii - 1 && j < jj -1) {
	    find_and_add_pairs_superimposed(alignment, r1, r2, i+1, ii-1, j+1, jj-1);
	}
    }
    pair_set_add_pair(alignment,
		      matches->indices[matches->n-1][0],
		      matches->indices[matches->n-1][1]);

    if(matches->indices[matches->n-1][0] + 1 < (int) len1 &&
       matches->indices[matches->n-1][1] + 1 < (int) len2) {
	find_and_add_pairs_superimposed(alignment, r1, r2,
					matches->indices[matches->n-1][0] + 1, len1 - 1,
					matches->indices[matches->n-1][1] + 1, len2 - 1);
    }
    return alignment;
}

static struct RPoint*
get_backbone_c(const struct rna_coord *rna)
{
    size_t i;
    struct RPoint *bb=NULL;
    bb = E_MALLOC(rna->size * sizeof(*bb));
    for(i = 0; i < rna->size; ++i) {
	bb[i] = rna->bb[i].c4.r;
    }
    return bb;
}

struct pair_set*
rna_dp_fill_gaps(const struct rna_coord *rna1,
		 const struct rna_coord *rna2,
		 const struct pair_set *matches) /* matching pairs WITHOUT gaps */
{
    /* remove gaps from input matches ? */
    struct pair_set *alignment=NULL;
    struct RPoint *bb1=NULL, *bb2=NULL;
    bb1 = get_backbone_c(rna1);
    bb2 = get_backbone_c(rna2);
    
    alignment = dp_fill_gaps(bb1, bb2, rna1->size, rna2->size, matches);
    
    free_if_not_null(bb1);
    free_if_not_null(bb2);

    return alignment;
}

static void
get_matching_backbone_c(struct RPoint **r_matches_1,
			struct RPoint **r_matches_2,
			const struct RPoint *r1,
			const struct RPoint *r2,
			const struct pair_set *alignment)
{
    size_t k, l=0;
    size_t n = pair_set_get_netto_alignmentlength(alignment);
    *r_matches_1 = E_MALLOC(n * sizeof(**r_matches_1));
    *r_matches_2 = E_MALLOC(n * sizeof(**r_matches_2));
    for(k = 0; k < alignment->n; ++k) {
	int i = alignment->indices[k][0];
	int j = alignment->indices[k][1];
	if(i == GAP_INDEX || j == GAP_INDEX) {
	    continue;
	}
	(*r_matches_1)[l] = r1[i];
	(*r_matches_2)[l] = r2[j];
	l++;
    }
}

static void
shift_atoms(struct RPoint *r,
	    size_t n_atoms,
	    const struct RPoint *shift)
{
    size_t i;
    for(i = 0; i < n_atoms; ++i) {
	rsubtract(r + i, shift);
    }
}

static void
rotate_atoms(struct RPoint *r,
	     size_t n_atoms,
	     const struct rotation_matrix *rot_mat)
{
    size_t i;
    for(i = 0; i < n_atoms; ++i) {
	rotate_rpoint(r + i, rot_mat);
    }
}

struct pair_set*
rna_promax(const struct rna_coord *rna1,
	   const struct rna_coord *rna2,
	   const struct pair_set *alignment,
	   size_t maxsub_seedlen,
	   float dist_thresh)
{
    struct pair_set *maxsub_matches=NULL;
    struct pair_set *prosup_matches=NULL;
    struct pair_set *prosup_matches_no_gap=NULL;
    struct pair_set *refined_matches=NULL;
    struct pair_set *swap=NULL;
    struct RPoint *bb1=NULL, *bb2=NULL;
    struct RPoint *bb1_aligned=NULL, *bb2_aligned=NULL; 
    struct RPoint *bb1_maxsub=NULL, *bb2_maxsub=NULL; 
    struct RPoint *bb1_prosup=NULL, *bb2_prosup=NULL; 
    size_t len1, len2;
    struct transformation transform;
    int err=0;

    len1 = rna1->size;
    len2 = rna2->size;

    bb1 = get_backbone_c(rna1);
    bb2 = get_backbone_c(rna2);
    
    get_matching_backbone_c(&bb1_aligned, &bb2_aligned, bb1, bb2, alignment);

    maxsub_matches = maxsub_core(alignment,
				 maxsub_seedlen,
				 bb1_aligned,
				 bb2_aligned,
				 dist_thresh);
    err = !maxsub_matches->n;

    if(!err) {
	get_matching_backbone_c(&bb1_maxsub, &bb2_maxsub, bb1, bb2, maxsub_matches);
    
	err = rmsd_superpos_core(&transform,
				 NULL,	/* don't need rmsd */
				 bb1_maxsub,
				 bb2_maxsub,
				 maxsub_matches->n); /* there are no gaps in matches */
    }

    if(!err) {
	/* superposition of bb1 and bb2 */
	shift_atoms(bb1, len1, &transform.com_a);
	shift_atoms(bb2, len2, &transform.com_b);
	rotate_atoms(bb1, len1, &transform.rotate_a);

	/* run prosup dp algorithm */
	prosup_matches = dp_fill_gaps(bb1, bb2, len1, len2, maxsub_matches);
	
	prosup_matches_no_gap = pairset_without_gaps(prosup_matches);

	/* get the backbones for the aligned residues */
	get_matching_backbone_c(&bb1_prosup, &bb2_prosup, bb1, bb2, prosup_matches_no_gap);

	/* refine the matches maxsub style */
	refined_matches = maxsub_refine(bb1_prosup,
					bb2_prosup,
					dist_thresh,
					prosup_matches_no_gap);

	if(refined_matches != NULL) {
	    if(maxsub_matches->n > refined_matches->n) {
		swap = refined_matches;
		refined_matches = maxsub_matches;
		maxsub_matches = swap;
	    }
	} else {
	    swap = refined_matches;
	    refined_matches = maxsub_matches;
	    maxsub_matches = swap;
	}
    }
	
    free_if_not_null(bb1);
    free_if_not_null(bb2);
    free_if_not_null(bb1_aligned);
    free_if_not_null(bb2_aligned);
    free_if_not_null(bb1_maxsub);
    free_if_not_null(bb2_maxsub);
    free_if_not_null(bb1_prosup);
    free_if_not_null(bb2_prosup);

    pair_set_destroy(maxsub_matches);
    pair_set_destroy(prosup_matches);
    pair_set_destroy(prosup_matches_no_gap);
    
    return refined_matches;
}
