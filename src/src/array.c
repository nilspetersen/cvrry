#include "array.h"
#include "e_malloc.h"

ARRAY_CLASS_BODY(, size_t)
ARRAY_CLASS_BODY(, int)
ARRAY_CLASS_BODY(, float)
ARRAY_CLASS_BODY(, double)

float
float_array_avg(const struct float_array*arr)
{
    size_t i;
    double avg=0;
    for(i = 0; i < arr->size; ++i) {
	avg += (double) arr->vals[i];
    }
    avg /= (double) arr->size;
    return (float) avg;
}
