#ifndef PAIR_SET_P_H
#define PAIR_SET_P_H

#include "pair_set.h"
#include "seq.h"

char*
pair_set_pretty_string(const struct pair_set *pair_set,
		       const struct seq *s1,
		       const struct seq *s2);

char*
pair_set_fasta_string(const struct pair_set *pair_set,
		      const struct seq *s1,
		      const struct seq *s2);

float
get_seq_identity(const struct pair_set *pair_set,
		 const struct seq *s1,
		 const struct seq *s2);

#endif
