#ifndef PSI_H
#define PSI_H

#include "lsqf.h"
#include "rna_coord.h"
#include "pair_set.h"

enum superpos_methods {
    ITERATIVE_REMOVAL,
    MAXSUB,
    PROSUP_REFINE
};

int
percentage_of_structure_identity(float *psi,
				 struct pair_set **pairset_superimposed,
				 struct transformation *transform,
				 const struct pair_set *pairset,
				 const struct rna_coord *source, 
				 const struct rna_coord *target,
				 float rmsd_thresh,
				 enum superpos_methods sup_method);

#endif
