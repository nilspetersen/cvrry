#include "rna_seq_coord.h"
#include "fio.h"

void
rna_seq_coord_destroy(struct rna_seq_coord*seq_coord)
{
    if(seq_coord) {
	cvrry_cif_destroy(seq_coord->cif);
	rna_coord_destroy(seq_coord->rna_backbone);
	rna_coord_seq_destroy(seq_coord->seq);
	free(seq_coord);
    }
}

static struct rna_seq_coord*
new_rna_seq_coord()
{
    struct rna_seq_coord*chain=E_MALLOC(sizeof(*chain));
    chain->cif = NULL;
    chain->rna_backbone = NULL;
    chain->seq = NULL;
    return chain;
}

struct rna_seq_coord*
read_n_compute_rna_seq_coord(const char*mmcif_filename,
			     const char*chain_id,
			     size_t model_i,
			     const struct rna_coord_alphabet*alphabet)
{
    const char *this_sub = "read_n_compute_rna_seq_coord";
    struct rna_seq_coord *seq_coord=new_rna_seq_coord();
    int err=0;    
    err = read_mmcif(&seq_coord->cif,
		     &seq_coord->rna_backbone,
		     mmcif_filename,
		     chain_id,
		     model_i);
    if(!err) {
	if(!seq_coord->rna_backbone->size) {
	    err_printf(this_sub,
		       "Did not find any backbone nucleotides for chain %s model %zu in file %s\n",
		       chain_id,
		       model_i,
		       mmcif_filename);
	    err = 1;
	}
    }
    if(!err) {
	compute_chain_breaks(seq_coord->rna_backbone);
    }
    if(!err) {
	seq_coord->seq = rna_coord_seq_simple(seq_coord->rna_backbone, alphabet);
	err = !seq_coord->seq;
    }
    if(err) {
	err_printf(this_sub,
		   "failed to read %s and compute a sequence for chain %s model %zu\n",
		   mmcif_filename,
		   chain_id, model_i);
	rna_seq_coord_destroy(seq_coord);
	return NULL;
    }
    return seq_coord;
}

int
rna_seq_coord_write_binary(const struct rna_seq_coord*seq_coord,
			   const char*fname_out)
{
    const char*this_sub="rna_seq_coord_write_binary";
    FILE *outfile=NULL;
    size_t memsize_cif, memsize_coord;
    size_t dummy=0, zero=0, i;
    int err=0;
    outfile = mfopen(fname_out, "wb", this_sub);
    err = !outfile;
    /* the memory size of coord and cif is stored at the beginning, 
       but we don't know it yet, so write 0 now and replace later */
    for(i = 0; !err && i < 2; ++i) {
	err = m_bin_write(&dummy, &zero, sizeof(zero), 1, outfile, this_sub);
    }
    if(!err) {
	if(seq_coord->cif) {
	    memsize_cif = cvrry_cif_write_bin(outfile, seq_coord->cif);
	    err = !memsize_cif;
	} else {
	    memsize_cif = 0;
	}
    }
    if(!err) {
	memsize_coord = rna_coord_write_bin(outfile, seq_coord->rna_backbone);
	err = !memsize_coord;
    }
    if(!err) {
	err = rna_coord_seq_write_binary(outfile, seq_coord->seq);
    }
    if(!err) {
        fseek(outfile, 0, SEEK_SET);
    }
    if(!err) {
	err = m_bin_write(&dummy, &memsize_cif, sizeof(memsize_cif), 1, outfile, this_sub);
    }
    if(!err) {
	err = m_bin_write(&dummy, &memsize_coord, sizeof(memsize_coord), 1, outfile, this_sub);
    }
    close_if_open_file(outfile);
    if(err) {
	err_printf(this_sub,
		   "could not write binary rna_seq_coord structure to %s\n",
		   fname_out);
    }
    return err;
}

struct rna_seq_coord*
rna_seq_coord_read_binary(const char*fname)
{
    const char *this_sub = "rna_frag_coord_read_binary";
    FILE *infile=NULL;
    size_t memsize_cif=0, memsize_coord=0;
    int err=1;
    struct rna_seq_coord*seq_coord=NULL;
    seq_coord = new_rna_seq_coord();
    infile = mfopen(fname, "rb", this_sub);
    if(infile) {
	err = (fread(&memsize_cif, sizeof(memsize_cif), 1, infile) != 1);
    }
    if(!err) {
	err = (fread(&memsize_coord, sizeof(memsize_coord), 1, infile) != 1);
    }
    if(!err && memsize_cif) {
	seq_coord->cif = cvrry_cif_read_binary(infile);
	err = !seq_coord->cif;
    }
    if(!err && memsize_coord) {
	seq_coord->rna_backbone = rna_coord_read_binary(infile);
	err = !seq_coord->rna_backbone;
    }
    if(!err){
	seq_coord->seq = rna_coord_seq_read_binary(infile);
	err = !seq_coord->seq;
    }
    close_if_open_file(infile);
    if(err) {
	rna_seq_coord_destroy(seq_coord);
	return NULL;
    }
    return seq_coord;
}

void
rna_seq_coord_dump(const struct rna_seq_coord*seq_coord)
{
    if(seq_coord) {
	mprintf("CIF:\n");
	cvrry_cif_dump(seq_coord->cif);
	mprintf("\nCOORD:\n");
	rna_coord_dump(seq_coord->rna_backbone);
	mprintf("\nALFONS-SEQUENCE:\n");
	rna_coord_seq_dump(seq_coord->seq);
    }
}

int
rna_seq_coord_fill_missing_characters(struct rna_seq_coord*seq_coord,
				      enum yes_no add_tail)
{
    const char*this_sub="rna_seq_coord_fill_missing_characters";
    struct rna_coord_seq *seq_full=NULL;
    if(seq_coord->seq->type == RNA_COORD_SEQ_FULLSEQ) {
	return 0;
    }
    seq_full = rna_coord_seq_fullseq_fill_up(seq_coord->seq, seq_coord->rna_backbone, add_tail);
    if(seq_full) {
	rna_coord_seq_destroy(seq_coord->seq);
	seq_coord->seq = seq_full;
	return 0;
    }
    err_printf(this_sub, "failed");
    return 1;
}

size_t
rna_seq_coord_scoremat_dim(const struct rna_seq_coord*seq_coord)
{
    switch(seq_coord->seq->type) {
    case RNA_COORD_SEQ_FULLSEQ:
	return rna_coord_seq_size(seq_coord->seq);
    default:
	return seq_coord->rna_backbone->size;
    }
}

size_t
rna_seq_coord_seqlen(const struct rna_seq_coord*seq_coord)
{
    return rna_coord_seq_size(seq_coord->seq);
}

int
rna_seq_coord_add_basepair_info(struct rna_seq_coord*seq_coord)
{
    return rna_coord_seq_add_basepair_info(seq_coord->seq,
					   seq_coord->rna_backbone);
}
