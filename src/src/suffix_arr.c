#include <math.h>
#include <string.h>
#include "suffix_arr.h"
#include "e_malloc.h"
#include "mprintf.h"
#include "array.h"
#include "sais.h"
#include "pair.h"
#include "rna_alphabet.h"

/* Kasai et al. algorithm to compute LCP-Array for
 * enhanced suffix array *SA of sequence *T of lenght n
 * - suffixes have to end with unique sentinel!
 *
 * pseudocode from slides by Clemens Groepl and Knut Reinert
 * was used for this implementation (found at
 * http://www.mi.fu-berlin.de/wiki/pub/ABI/RnaSeqP4/suffix-array.pdf)
 */
int
lcp_array_kasai(size_t *lcp,
		const int *T,
		const int *SA,
		size_t n)
{
    size_t i, k, h=0;
    size_t *rank=NULL;
    /* make the 'rank' array
       - the inverse of the suffix array */
    rank = E_MALLOC(n * sizeof(*rank));
    for(i = 0; i < n; ++i) {
	rank[SA[i]] = i;
    }
    for(i = 0; i < n; ++i) {
	if(!rank[i]) {
	    continue;
	}
	k = SA[rank[i] - 1];
	/* suffixes have to end with unique sentinel,
	   otherwise this while-loop can leave the allocated memory
	   and case a segmentation fault*/
	while(T[i + h] == T[k + h]) { 
	    ++h;
	}
	lcp[rank[i]] = h;
	if(h) {
	    --h;
	}
    }
    lcp[0] = 0;
    free_if_not_null(rank);
    return 0;
}

/* make sure joined_sequence and generalized_suffix_array have 
 * enough memory allocated (sum of the lenghts of the sequences PLUS
 * the number of sequences for the sentinel characters)
 * 
 * min letter is the index of the smallest letter, the sentinels
 * are chosen to be smaller
 */
int
generalized_suffix_array(int *generalized_suffix_array,
			 int *joined_sequence,
			 size_t nseq,
			 const size_t *seq_lengths,
			 const int*const*sequences,
			 int min_letter)
{
    int err=0, max_letter;
    size_t i;
    size_t len_total=0;
    /* merge the sequences */
    for(i = 0; i < nseq; ++i) {
	/* copy a sequence */
	memcpy(joined_sequence + len_total,
	       sequences[i],
	       seq_lengths[i] * sizeof(*joined_sequence));
	len_total += seq_lengths[i];
	/* add a sentinel */
	joined_sequence[len_total] = min_letter - (int) nseq + i;
	len_total++;
    }
    /* shift the letters so the smallest is 0 and find the largest */
    joined_sequence[0] += ((int) nseq) - min_letter;
    max_letter = joined_sequence[0];
    for(i = 1; i < len_total; ++i) {
	joined_sequence[i] += ((int) nseq) - min_letter;
	max_letter = max_letter < joined_sequence[i] ? joined_sequence[i] : max_letter;
    }
    /* create the suffix array */
    err = sais_int(joined_sequence, generalized_suffix_array, (int) len_total, max_letter + 1);    
    return err;
}

/* find the longest common substring of 2 strings
 * using a generalized suffix array
 */
static void
find_lcs(struct continuous_match *match,
	 const int *gsa,
	 const size_t *lcp,
	 size_t len_a,		/* length includes the sentinel ! */
	 size_t len_total)
{
    size_t i;
    match->len = 0;
    match->matchpos_a = match->matchpos_b = 0;
    for(i = 1; i < len_total; ++i) {
	/* check if the two suffixes are from different strings */
	if((gsa[i] < len_a) == (gsa[i-1] < len_a)) {
	    continue;
	}
	/* check if the match is longer */
	if(lcp[i] > match->len) {
	    match->len = lcp[i];
	    if(gsa[i-1] < gsa[i]) {
		match->matchpos_a = gsa[i - 1];
		match->matchpos_b = gsa[i] - len_a;
	    } else {
		match->matchpos_a = gsa[i];
		match->matchpos_b = gsa[i - 1] - len_a;
	    }
	}
    }
}

void
suff_arr_dump(int*gsa,
	      int*joined_seq,
	      size_t*lcp,
	      int len_gsa,
	      int len_a)
{
    int i, j;
    for(i = 0; i < len_gsa; ++i) {
	mprintf("%i %3i %3i ", gsa[i] / len_a, (int) lcp[i], gsa[i]);
	for(j = gsa[i]; j < len_gsa; ++j) {
	    mprintf("%i ", joined_seq[j]);
	}
	mprintf("\n");
    }
}

static size_t
minlen(size_t a, size_t b)
{
    return a < b ? a : b;
}

static size_t
maxlen(size_t a, size_t b)
{
    return a > b ? a : b;
}

/* lcs_each
 *
 * get the longest match for each position
 * in the suffix array
 * output:
 *   lcs: lenghts of the matches in order
 *        of the generalized suffix array 
 */
static void
lcs_each_esa_order(size_t *lcs,
		   const int *gsa,
		   const size_t *lcp,
		   size_t len_a,
		   size_t len_total)
{
    int i;
    size_t *lcs_forward=NULL;
    size_t *lcs_backward=NULL;
    lcs_forward = E_MALLOC(len_total * sizeof(*lcs_forward));
    lcs_backward = E_MALLOC(len_total * sizeof(*lcs_backward));
    /* forward loop */
    lcs_forward[0] = 0;
    for(i = 1; i < len_total; ++i) {
	/* check if the two suffixes are from different strings */
	if((gsa[i] < len_a) == (gsa[i-1] < len_a)) {
	    lcs_forward[i] = minlen(lcp[i], lcs_forward[i-1]);
	} else {
	    lcs_forward[i] = lcp[i];
	}
    }
    /* backwards loop */
    lcs_backward[len_total-1] = 0;
    for(i = ((int) len_total) - 2; i >= 0; --i) {
	/* check if the two suffixes are from different strings */
	if((gsa[i] < len_a) == (gsa[i+1] < len_a)) {
	    lcs_backward[i] = minlen(lcp[i+1], lcs_backward[i+1]);
	} else {
	    lcs_backward[i] = lcp[i+1];
	}
    }
    /* merge forwards and backwards_loop */
    for(i = 0; i < len_total; ++i) {
	lcs[i] = maxlen(lcs_forward[i], lcs_backward[i]);
    }
    free_if_not_null(lcs_forward);
    free_if_not_null(lcs_backward);
}

static void
lcs_gsa_to_seq_order(size_t*lcs_seq_order,
		     const size_t*lcs_gsa_order,
		     const int*gsa,
		     size_t len_gsa)
{
    size_t i;
    for(i = 0; i < len_gsa; ++i) {
	lcs_seq_order[gsa[i]] = lcs_gsa_order[i];
    }
}

static void
lcs_each_seq_order(size_t*lcs_seq_order,
		   const int *gsa,
		   const size_t *lcp,
		   size_t len_a,
		   size_t len_total)
{
    size_t *lcs_gsa_order=E_MALLOC(len_total * sizeof(*lcs_gsa_order));
    lcs_each_esa_order(lcs_gsa_order, gsa, lcp, len_a, len_total);
    lcs_gsa_to_seq_order(lcs_seq_order, lcs_gsa_order, gsa, len_total);
    free_if_not_null(lcs_gsa_order);
}

struct enhanced_suffix_array {
    size_t len;
    size_t len_seq_a;
    int *joined_seq;
    int *gsa;
    size_t *lcp;    
};

static struct enhanced_suffix_array*
new_enhanced_suffix_array(size_t len_a, size_t len_b)
{
    struct enhanced_suffix_array*esa=NULL;
    size_t len_gsa = len_a + len_b + 2;
    esa = E_MALLOC(sizeof(*esa));
    esa->len = len_gsa;
    esa->len_seq_a = len_a + 1;
    esa->joined_seq = E_MALLOC(esa->len * sizeof(*esa->joined_seq));
    esa->gsa = E_MALLOC(esa->len * sizeof(*esa->gsa));
    esa->lcp = E_MALLOC(esa->len * sizeof(*esa->lcp));
    return esa;
}

static void
enhanced_suffix_array_destroy(struct enhanced_suffix_array*esa)
{
    if(esa) {
	free_if_not_null(esa->joined_seq);
	free_if_not_null(esa->gsa);
	free_if_not_null(esa->lcp);
	free(esa);
    }
}

static struct enhanced_suffix_array*
make_enhanced_suffix_array(int min_letter,
			   const struct int_array *seq_a,
			   const struct int_array *seq_b)
{
    int err=0;
    const size_t seq_lengths[] = {seq_a->size, seq_b->size};
    const int *sequences[] = {seq_a->vals, seq_b->vals};
    struct enhanced_suffix_array*esa=NULL;
    esa = new_enhanced_suffix_array(seq_lengths[0], seq_lengths[1]);
    if(esa) {
	err = generalized_suffix_array(esa->gsa,
				       esa->joined_seq,
				       2,
				       seq_lengths,
				       sequences,
				       min_letter);
    }
    if(!err) {
	err = lcp_array_kasai(esa->lcp, esa->joined_seq, esa->gsa, esa->len);
    }
    if(err) {
	enhanced_suffix_array_destroy(esa);
	return NULL;
    }
    return esa;
}

int
longest_common_substring(struct continuous_match *match,
			 int min_letter,
			 const struct int_array *seq_a,
			 const struct int_array *seq_b)
{
    struct enhanced_suffix_array*esa=NULL;
    esa = make_enhanced_suffix_array(min_letter, seq_a, seq_b);
    if(!esa) {
	return 1;
    }
    find_lcs(match, esa->gsa, esa->lcp, esa->len_seq_a, esa->len);
    enhanced_suffix_array_destroy(esa);
    return 0;
}

static struct size_t_array*
lcs_each(const struct enhanced_suffix_array*esa)
{
    struct size_t_array*lcs_each_pos=new_size_t_array(esa->len);
    lcs_each_seq_order(lcs_each_pos->vals,
		       esa->gsa,
		       esa->lcp,
		       esa->len_seq_a,
		       esa->len);
    return lcs_each_pos;
}

int
lcs_sums(struct size_t_pair*sums,
	 int min_letter,
	 const struct int_array *seq_a,
	 const struct int_array *seq_b)
{
    int err=1;
    struct size_t_array *lcs_each_pos=NULL;
    struct enhanced_suffix_array*esa=NULL;
    esa = make_enhanced_suffix_array(min_letter, seq_a, seq_b);
    if(esa) {
	lcs_each_pos = lcs_each(esa);
    }
    if(lcs_each_pos) {
	sums->first = size_t_array_sum_partial(lcs_each_pos, 0, esa->len_seq_a);
	sums->second = size_t_array_sum_partial(lcs_each_pos, esa->len_seq_a, esa->len);
	err = 0;
    }
    size_t_array_destroy(lcs_each_pos);
    enhanced_suffix_array_destroy(esa);
    return err;
}

/* lcs_significance_one_seq
 *
 * log_n_of_matchlen: array with the number of framgents of length n found in sequence b
 */
static void
lcs_significance_one_seq(float*log_p_each_match,
			 const size_t*lcs_each_pos,
			 const float*cumulative_log_p,
			 size_t len, /* no sentinel ! */
			 const float*log_n_of_matchlen) 
{
    size_t i;
    for(i = 0; i < len; ++i) {
	size_t matchlen = lcs_each_pos[i];
	log_p_each_match[i] = (cumulative_log_p[i + matchlen] - cumulative_log_p[i]
			       - log_n_of_matchlen[matchlen]);
    }
}

static void
lcs_log_p_each_pos_inner(float*log_p_each_match,
			 const size_t*lcs_each_pos,
			 const float*cumulative_log_p_1,
			 size_t seq_len_1, /* no sentinel! */
			 const float*log_n_of_matchlen_1,
			 const float*cumulative_log_p_2,
			 size_t seq_len_2,
			 const float*log_n_of_matchlen_2)
{
    /* first sequence */
    lcs_significance_one_seq(log_p_each_match,
			     lcs_each_pos,
			     cumulative_log_p_1,
			     seq_len_1,
			     log_n_of_matchlen_2);
    /* the sentinel can not match! */
    log_p_each_match[seq_len_1] = 0.0;
    /* second sequence */
    lcs_significance_one_seq(log_p_each_match + seq_len_1 + 1,
			     lcs_each_pos + seq_len_1 + 1,
			     cumulative_log_p_2,
			     seq_len_2,
			     log_n_of_matchlen_1);
}

static struct float_array*
lcs_log_p_each_pos(const struct size_t_array*lcs_each_pos,
		   const struct float_array*log_p_cumulative_a,
		   const struct float_array*log_p_cumulative_b,
		   const struct float_array*log_n_pieces_a,
		   const struct float_array*log_n_pieces_b)
{
    struct float_array*log_p_each_pos=new_float_array(lcs_each_pos->size);
    lcs_log_p_each_pos_inner(log_p_each_pos->vals,
			     lcs_each_pos->vals,
			     log_p_cumulative_a->vals,
			     log_p_cumulative_a->size - 1,
			     log_n_pieces_a->vals,
			     log_p_cumulative_b->vals,
			     log_p_cumulative_b->size - 1,
			     log_n_pieces_b->vals);
    return log_p_each_pos;
}


static float
logistic_function(float x, float k, float x_0)
{
    return 1 / (1 + expf(-k * (x - x_0)));
}

static void
lcs_log_p_sum_logistic(struct float_pair*sums,
		       const struct float_array*lcs_log_p_arr,
		       const struct enhanced_suffix_array*esa,
		       const void*logistic_function_params)
{
    size_t i;
    const struct float_pair*par = (const struct float_pair*) logistic_function_params;
    float x_0 = par->first;
    float k = par->second;
    sums->first = sums->second = 0.0;
    for(i = 0; i < esa->len_seq_a; ++i) {
	sums->first += logistic_function(lcs_log_p_arr->vals[i], k, x_0);
    }
    for(i = esa->len_seq_a; i < esa->len; ++i) {
	sums->second += logistic_function(lcs_log_p_arr->vals[i], k, x_0);
    }
}

static void
lcs_log_p_sum_simple(struct float_pair*sums,
		     const struct float_array*lcs_log_p_arr,
		     const struct enhanced_suffix_array*esa)
{
    sums->first = float_array_sum_partial(lcs_log_p_arr, 0, esa->len_seq_a);
    sums->second = float_array_sum_partial(lcs_log_p_arr, esa->len_seq_a, esa->len - 1);
    /* esa->len -1 because the last sentinel added for the gsa does not have 
       an entry in the logP sums
     */
}

static void
lcs_log_p_sum_simple_wrap(struct float_pair*sums,
			  const struct float_array*lcs_log_p_arr,
			  const struct enhanced_suffix_array*esa,
			  const void*dummy_params)
{
    lcs_log_p_sum_simple(sums, lcs_log_p_arr, esa);
}

static void
lcs_log_p_max(struct float_pair*maxima,
	      const struct float_array*lcs_log_p_arr,
	      const struct enhanced_suffix_array*esa)
{
    maxima->first = float_array_max_partial(lcs_log_p_arr, 0, esa->len_seq_a);
    maxima->second = float_array_max_partial(lcs_log_p_arr, esa->len_seq_a, esa->len - 1);
    /* esa->len -1 because the last sentinel added for the gsa does not have 
       an entry in the logP sums
     */
}

static void
lcs_log_p_max_wrap(struct float_pair*maxima,
		   const struct float_array*lcs_log_p_arr,
		   const struct enhanced_suffix_array*esa,
		   const void*dummy_params)
{
    lcs_log_p_max(maxima, lcs_log_p_arr, esa);    
}

int
lcs_log_p_scores(struct float_pair*scores, /* sums before */
		 int min_letter,
		 const struct int_array*str_a,
		 const struct int_array*str_b,
		 const struct float_array*log_p_cumulative_a,
		 const struct float_array*log_p_cumulative_b,
		 const struct float_array*log_n_pieces_a,
		 const struct float_array*log_n_pieces_b,
		 enum lcs_logp_score_type score_type,
		 const struct float_pair*score_func_params)
{
    const char*this_sub="lcs_log_p_scores";
    int err=1;
    struct enhanced_suffix_array*esa=NULL;
    struct size_t_array*lcs_each_arr=NULL;
    struct float_array*lcs_log_p_arr=NULL;

    void (*score_func) (struct float_pair*, const struct float_array*,
			const struct enhanced_suffix_array*, const void*);

    switch (score_type) {
    case LOGP_SUM:
	score_func = &lcs_log_p_sum_simple_wrap;
	break;
    case LOGP_LOGISTIC_SUM:
	score_func = &lcs_log_p_sum_logistic;
	break;
    case LOGP_MAX:
	score_func = &lcs_log_p_max_wrap;
	break;
    default:
	err_printf(this_sub, "Not a valid score function type\n");
	return 1;
    }
    
    esa = make_enhanced_suffix_array(min_letter, str_a, str_b);
    if(esa) {
	lcs_each_arr = lcs_each(esa);
    }
    if(lcs_each_arr) {
	lcs_log_p_arr = lcs_log_p_each_pos(lcs_each_arr,
					   log_p_cumulative_a,
					   log_p_cumulative_b,
					   log_n_pieces_a,
					   log_n_pieces_b);
    }
    if(lcs_log_p_arr) {
	score_func(scores, lcs_log_p_arr, esa, score_func_params);
	err = 0;
    }
    enhanced_suffix_array_destroy(esa);
    size_t_array_destroy(lcs_each_arr);
    float_array_destroy(lcs_log_p_arr);
    return err;
}
