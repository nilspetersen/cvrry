#include "rna_multialign.h"


struct rna_multialign*
new_rna_multialign(size_t max_n_chains)
{
    struct rna_multialign*multialign = E_MALLOC(sizeof(*multialign));
    multialign->seqs = E_MALLOC(max_n_chains * sizeof(*multialign->seqs));
    multialign->coords = E_MALLOC(max_n_chains * sizeof(*multialign->coords));
    multialign->n_seqs = 0;
    multialign->n_coords = 0;
    multialign->pset_multi = NULL;
    return multialign;
}

void
rna_multialign_destroy(struct rna_multialign*multialign)
{
    size_t i;
    if(multialign) {
	for(i = 0; i < multialign->n_seqs; ++i) {
	    rna_coord_seq_destroy(multialign->seqs[i]);
	}
	for(i = 0; i < multialign->n_coords; ++i) {
	    rna_coord_destroy(multialign->coords[i]);
	}
	free_if_not_null(multialign->seqs);
	free_if_not_null(multialign->coords);
	free(multialign);
    }
}

void
rna_multialign_set_pset(struct rna_multialign*multialign,
			const struct pair_set*pset_multi)
{
    pair_set_destroy(multialign->pset_multi);
    multialign->pset_multi = pair_set_copy(pset_multi);
}

static void
rna_multialign_add_seq(struct rna_multialign*multialign,
		       const struct rna_coord_seq*seq)
{
    multialign->seqs[multialign->n_seqs++] = rna_coord_seq_copy(seq);
}

static void
rna_multialign_add_coord(struct rna_multialign*multialign,
			 const struct rna_coord*coord)
{
    multialign->coords[multialign->n_coords++] = rna_coord_copy(coord);    
}

void
rna_multialign_add_seq_and_coord(struct rna_multialign*multialign,
				 const struct rna_coord_seq*seq,
				 const struct rna_coord*coord)
{
    rna_multialign_add_seq(multialign, seq);
    rna_multialign_add_coord(multialign, coord);
}


size_t
rna_multialign_length(const struct rna_multialign*multialign)
{
    if(multialign->pset_multi) {
	return multialign->pset_multi->n;
    }
    return 0;
}

float
rna_multialign_gap_share(const struct rna_multialign*multialign)
{
    if(multialign->pset_multi) {
	return pair_set_gap_share(multialign->pset_multi);
    }
    return 0;
}

