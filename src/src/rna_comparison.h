#ifndef RNA_COMPARISON
#define RNA_COMPARISON

#include "rna_frag_coord.h"
#include "pair_set.h"
#include "yesno.h"
#include "rna_align.h"

enum superposition_type {
    NONE=0,
    PSI=1,
    RMSD=2
};

struct rna_cmp_opts {
    enum yes_no verbose_err;
    enum yes_no save_alignment;
    enum yes_no calc_rmsd;
    enum yes_no calc_psi;
    enum yes_no calc_psd;
    float psd_thresh;
    enum yes_no get_alignment_score;
    enum superposition_type superpos_opt;
};

struct rna_comparison_result {
    struct pair_set *alignment;
    float psi;
    float psd;
    float rmsd;
    float align_score;
    float align_score_smpl;
    struct transformation *transform;
};

struct rna_comparison_result*
new_rna_comparison_result(const struct rna_cmp_opts *opts);

void
rna_comparison_result_destroy(struct rna_comparison_result*res);

int
rna_structure_comparison(struct rna_comparison_result *result,
			 const struct rna_frag_coord *chain_a,
			 const struct rna_frag_coord *chain_b,
			 const struct rna_align_params *params,
			 const struct rna_cmp_opts *res_opts);

#endif
