#ifndef BIN_UTILS_H
#define BIN_UTILS_H

#include <stdio.h>
#include <stdlib.h>

int
m_bin_write(size_t*size_count,
	    const void *ptr,
	    size_t size,
	    size_t nmemb,
	    FILE *stream,
	    const char *calling_sub);

int
assert_src_version(int src_version,
		   FILE *stream);

int
write_src_version(int src_version,
		  FILE *stream);


#endif
