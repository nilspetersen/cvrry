#include "alphabet_index.h"
#include "draw_random.h"

size_t
alphabet_index_get_size(const struct alphabet_index*alpha_index)
{
    return alpha_index->index->size;
}

void
alphabet_index_dump(const struct alphabet_index*alpha_index)
{
    mprintf("border_i=%zu\n", alpha_index->border_i);
    double_index_dump(alpha_index->index);
}

struct size_t_array*
alphabet_index_extract_choice(const struct alphabet_index*alpha_index)
{
    size_t i;
    struct size_t_array*choice=NULL;
    choice = new_size_t_array(alpha_index->border_i);
    for(i = 0; i < choice->size; ++i) {
	choice->vals[i] = alphabet_index_get_select(alpha_index, i);
    }
    return choice;
}

void
alphabet_index_choose_all(struct alphabet_index*alpha_index)
{
    alpha_index->border_i = alphabet_index_get_size(alpha_index);
}

int
alphabet_index_write_binary(FILE*f_out,
			    const struct alphabet_index*alpha_index)
{
    const char*this_sub="alphabet_index_write_binary";
    int err=0;
    err = m_bin_write(NULL, &alpha_index->border_i,
		      sizeof(alpha_index->border_i),
		      1, f_out, this_sub);
    if(!err) {
	err = double_index_write_binary(f_out, alpha_index->index);
    }
    return err;
}

struct alphabet_index*
alphabet_index_read_binary(FILE*f_in)
{
    struct alphabet_index*alpha_index=NULL;

    alpha_index = E_MALLOC(sizeof(*alpha_index));
    alpha_index->border_i = 0;
    alpha_index->index = NULL;

    if(fread(&alpha_index->border_i,
	     sizeof(alpha_index->border_i),
	     1, f_in) == 1) {
	alpha_index->index = double_index_read_binary(f_in);
    }
    if(!alpha_index->index) {
	alphabet_index_destroy(alpha_index);
	return NULL;
    }
    return alpha_index;
}

size_t
alphabet_index_get_pos(const struct alphabet_index*alpha_index, size_t i)
{
    return alpha_index->index->index_a->vals[i];
}

static void
alphabet_index_set_pos(struct alphabet_index*alpha_index, size_t i, size_t val)
{
    alpha_index->index->index_a->vals[i] = val;
}

size_t
alphabet_index_get_select(const struct alphabet_index*alpha_index, size_t i)
{
    return alpha_index->index->index_b->vals[i];    
}

static void
alphabet_index_set_select(struct alphabet_index*alpha_index, size_t i, size_t val)
{
    alpha_index->index->index_b->vals[i] = val;    
}

static void
alphabet_index_init(struct alphabet_index*alpha_index)
{
    size_t i;
    alpha_index->border_i = 0;
    for(i = 0; i < alpha_index->index->size; ++i) {
	alphabet_index_set_pos(alpha_index, i, i);
	alphabet_index_set_select(alpha_index, i, i);
    }
}

struct alphabet_index*
new_alphabet_index(size_t size)
{
    struct alphabet_index*alpha_index=NULL;
    alpha_index = E_MALLOC(sizeof(*alpha_index));
    alpha_index->index = new_double_index(size);
    alphabet_index_init(alpha_index);
    return alpha_index;
}

void
alphabet_index_destroy(struct alphabet_index*alpha_index)
{
    if(alpha_index) {
	double_index_destroy(alpha_index->index);
	free(alpha_index);
    }
}

void  /* use this for moves to a neighbor, add static as soon as it's used? */
alphabet_index_swap_from_pos(struct alphabet_index*alpha_index, size_t i, size_t j)
{
    size_t_array_swap(alpha_index->index->index_b,
		      alphabet_index_get_pos(alpha_index, i),
		      alphabet_index_get_pos(alpha_index, j));
    
    size_t_array_swap(alpha_index->index->index_a, i, j);
}

void
alphabet_index_swap_from_select(struct alphabet_index*alpha_index, size_t ii, size_t jj)
{
    size_t_array_swap(alpha_index->index->index_a,
		      alphabet_index_get_select(alpha_index, ii),
		      alphabet_index_get_select(alpha_index, jj));
    
    size_t_array_swap(alpha_index->index->index_b, ii, jj);
}

static void
alphabet_index_addremove(struct alphabet_index*alpha_index, size_t ii)
{
    alphabet_index_swap_from_select(alpha_index, ii, alpha_index->border_i);
}

void
alphabet_index_move(struct alphabet_index*alpha_index, size_t ii)
{
    if(ii < alpha_index->border_i) {
	--alpha_index->border_i;
	alphabet_index_addremove(alpha_index, ii);
    } else {
	alphabet_index_addremove(alpha_index, ii);
	alpha_index->border_i++;
    }
}

void
alphabet_index_cpy(struct alphabet_index*dest, const struct alphabet_index*src)
{
    double_index_cpy(dest->index, src->index);
    dest->border_i = src->border_i;
}

/* MUTATIONS */

enum move_type {
    ADD_OR_REMOVE,
    ADD,
    REMOVE,
    SWAP
};

ARRAY_CLASS_HEAD(enum, move_type)
ARRAY_CLASS_BODY(enum, move_type)

static void
alpha_index_mutate_addremove(struct alphabet_index*alpha_index)
{
    int rand_i = draw_random_int(0, (int) alphabet_index_get_size(alpha_index));
    alphabet_index_move(alpha_index, (size_t) rand_i);    
}

static void
alpha_index_mutate_add(struct alphabet_index*alpha_index)
{
    if(alpha_index->border_i < alphabet_index_get_size(alpha_index)) {
	int rand_i = draw_random_int((int) alpha_index->border_i,
				     (int) alphabet_index_get_size(alpha_index));
	alphabet_index_move(alpha_index, (size_t) rand_i);
    }
}

static void
alpha_index_mutate_remove(struct alphabet_index*alpha_index)
{
    if(alpha_index->border_i) {
	int rand_i = draw_random_int(0, (int) alpha_index->border_i);
	alphabet_index_move(alpha_index, (size_t) rand_i);
    }
}

static void
alpha_index_mutate_swap(struct alphabet_index*alpha_index)
{
    if(alpha_index->border_i && (alpha_index->border_i < alphabet_index_get_size(alpha_index))) {
	int rand_i = draw_random_int(0, (int) alpha_index->border_i);
	int rand_j = draw_random_int((int) alpha_index->border_i,
				     (int) alphabet_index_get_size(alpha_index));
	alphabet_index_swap_from_select(alpha_index, rand_i, rand_j);
    }
}

static void
alpha_index_mutate(struct alphabet_index*alpha_index, enum move_type move_type)
{
    switch(move_type) {
    case ADD_OR_REMOVE:
	alpha_index_mutate_addremove(alpha_index);
	break;
    case ADD:
	alpha_index_mutate_add(alpha_index);
	break;
    case REMOVE:
	alpha_index_mutate_remove(alpha_index);
	break;
    case SWAP:
	alpha_index_mutate_swap(alpha_index);
	break;
    }
}

static void
alpha_index_random_initialization(struct alphabet_index*alpha_index, size_t size)
{
    alpha_index->border_i = 0;
    while(alpha_index->border_i < size) {
	alpha_index_mutate_add(alpha_index);
    }
}

/* MOVE STRATEGIES */

struct move_strategy {
    void*params;
    void (*init)(struct alphabet_index*, const void*);
    void (*move)(struct alphabet_index*, const void*);
    void (*destroy_params)(void*);
    void* (*copy_params)(const void*);
};

void 
move_strategy_destroy(struct move_strategy*strategy)
{
    if(strategy) {
	if(strategy->params && strategy->destroy_params) {
	    strategy->destroy_params(strategy->params);
	}
	free(strategy);
    }
}

struct move_strategy*
move_strategy_copy(const struct move_strategy*in)
{
    struct move_strategy*out=NULL;
    out = E_MALLOC(sizeof(*out));
    out->init = in->init;
    out->move = in->move;
    out->destroy_params = in->destroy_params;
    out->copy_params = in->copy_params;
    if(in->copy_params) {
	out->params = in->copy_params(in->params);
    } else {
	out->params = NULL;
    }
    return out;
}

void
alpha_index_init_from_strategy(struct alphabet_index*alpha_index,
			       const struct move_strategy*strategy)
{
    if(strategy->init) {
	strategy->init(alpha_index, strategy->params);
    }
}

void
alpha_index_move_with_strategy(struct alphabet_index*alpha_index,
			       const struct move_strategy*strategy)
{
    strategy->move(alpha_index, strategy->params);
}

/* VERY SIMPLE MOVESET, PICK A LETTER RANDOMLY, REMOVE IF IT'S IN THE ALPHABET, ELSE ADD IT */

static void
alpha_index_mutate_addremove_wrapper(struct alphabet_index*alpha_index,
				     const void*empty_params)
{
    alpha_index_mutate_addremove(alpha_index);
}

struct move_strategy*
make_strategy_simple_add_remove()
{
    struct move_strategy*strategy=E_MALLOC(sizeof(*strategy));
    strategy->init = NULL;
    strategy->move = &alpha_index_mutate_addremove_wrapper;
    strategy->params = NULL;
    strategy->destroy_params = NULL;
    strategy->copy_params = NULL;
    return strategy;
}

/* MOVESET WITH EQUAL PROBABILITY OF ADDING, REMOVING AND SWAPPING */

static void
alpha_index_mutate_uniform_pick(struct alphabet_index*alpha_index,
				const struct move_type_array*moveset)
{
    int i = draw_random_int(0, (int) moveset->size);
    alpha_index_mutate(alpha_index, moveset->vals[i]);
}

static void
alpha_index_mutate_uniform_pick_wrapper(struct alphabet_index*alpha_index,
					const void*moveset)
{
    alpha_index_mutate_uniform_pick(alpha_index,
				    (const struct move_type_array*) moveset);
}

static void*
move_type_array_copy_wrapper(const void*moveset)
{
    return (void*) move_type_array_copy((const struct move_type_array*) moveset);
}

static void
move_type_array_destroy_wrapper(void*moveset)
{
    move_type_array_destroy((struct move_type_array*) moveset);
}

struct move_strategy*
make_strategy_add_remove_swap_eq_prob()
{
    struct move_strategy*strategy=NULL;
    struct move_type_array*params=NULL;
    strategy = E_MALLOC(sizeof(*strategy));
    strategy->init = NULL;
    strategy->move = &alpha_index_mutate_uniform_pick_wrapper;
    params = new_move_type_array(3);
    params->vals[0] = ADD;
    params->vals[1] = REMOVE;
    params->vals[2] = SWAP;
    strategy->params = (void*) params;
    strategy->destroy_params = &move_type_array_destroy_wrapper;
    strategy->copy_params = &move_type_array_copy_wrapper;
    return strategy;
}

/* SWAP ONLY MOVESET FOR FIXED ALPHABET SIZE OPTIMIZATION */

static void*
copy_size_t_wrapper(const void*params)
{
    size_t *val = E_MALLOC(sizeof(*val));
    *val = *((size_t*) params);
    return (void*) val;
}

static void
alpha_index_mutate_swap_wrapper(struct alphabet_index*alpha_index,
				const void*empty_params)
{
    alpha_index_mutate_swap(alpha_index);
}

static void
alpha_index_random_initialization_wrapper(struct alphabet_index*alpha_index, const void*size)
{
    alpha_index_random_initialization(alpha_index, *((size_t*) size));
}

struct move_strategy*
make_strategy_swap_only(size_t alphabet_size)
{
    struct move_strategy*strategy=NULL;
    strategy = E_MALLOC(sizeof(*strategy));
    strategy->params = (void*) copy_size_t_wrapper((void*) &alphabet_size);
    strategy->copy_params = &copy_size_t_wrapper;
    strategy->destroy_params = &free;
    strategy->init = &alpha_index_random_initialization_wrapper;
    strategy->move = &alpha_index_mutate_swap_wrapper;
    return strategy;
}
