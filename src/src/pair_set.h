/*
 * 12 Sep 2001
 * This defines the structures for passing alignments, or pair_sets
 * around.
 * It defines the internals, so it is only to be included by
 * the very few routines which manipulate pair_sets.
 * The functions are declared in pair_set_i.h
 * rcsid = "$Id$"
 */

#ifndef PAIR_SET_H
#define PAIR_SET_H

#include "stdlib.h"

/* ---------------- Structures  -------------------------------
 */

struct triplet{
    unsigned length;
    unsigned aligned;
    unsigned ident;
};

struct pair_set {
    int **indices;  /* alignments are now stored here. Dimensions are n*m */
    size_t n;         /* length of the alignment */
    size_t m;         /* number of sequences in alignment */
    float score;      /* Full score, including gaps */
    float smpl_score; /* Score, but without gaps */
};

/* ---------------- Enumerations ------------------------------
 */
enum long_or_short {
    EXT_LONG = 0,
    EXT_SHORT = 1
};
enum { GAP_INDEX = -1 };

void 
pair_set_dump(const struct pair_set *pair_set);

struct pair_set *
pair_set_new(size_t max_aln_length);

struct pair_set *
pair_set_copy(const struct pair_set *ps);

void
pair_set_destroy(struct pair_set *p_s);

size_t
pair_set_get_netto_alignmentlength(const struct pair_set *p_s);

void
pair_set_add_pair(struct pair_set *pset, int index1, int index2);

struct pair_set*
pairset_without_gaps(const struct pair_set *pset);

struct pair_set*
pairset_without_gaps_from_multi(const struct pair_set *pset,
				size_t seq_i, size_t seq_j);

struct pair_set*
pair_set_multi_new(size_t max_aln_length, size_t max_n_structures);

void
pair_set_multi_add_line(struct pair_set*pset, const char*line);

float
pair_set_gap_share(const struct pair_set *pset);

#endif /* PAIR_SET_H */
