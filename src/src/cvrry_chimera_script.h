#ifndef CVRRY_CHIMERA_SCRIPT
#define CVRRY_CHIMERA_SCRIPT

#include "rna_coord.h"
#include "pair_set.h"

int
write_chimera_superimposed_alignment(const char*dir_out,
				     const struct rna_coord*rna_1,
				     const struct rna_coord*rna_2,
				     const struct pair_set*pset);

#endif
