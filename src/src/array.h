#ifndef ARRAY_H
#define ARRAY_H

#include <stdlib.h>
#include <string.h>
#include "e_malloc.h"
#include "mprintf.h"
#include "bin_utils.h"


#define ARRAY_CLASS_HEAD(PREFIX, DTYPE)					\
    									\
    struct DTYPE ## _array {size_t size; PREFIX DTYPE *vals;};		\
    									\
    struct DTYPE ## _array*						\
    new_ ## DTYPE ## _array(size_t size);				\
    									\
    struct DTYPE ## _array*						\
    new_ ## DTYPE ## _array_initialized(size_t size, PREFIX DTYPE val);	\
    									\
    struct DTYPE ## _array*						\
    DTYPE ## _array_copy(const struct DTYPE ## _array*arr);		\
    									\
    void								\
    DTYPE ## _array_cpy(struct DTYPE ## _array*dest,			\
			const struct DTYPE ## _array*src);		\
    									\
    void								\
    DTYPE ## _array_cpy_from_to(struct DTYPE ## _array*dest,		\
				const struct DTYPE ## _array*src,	\
				size_t dest_i,				\
				size_t src_i);				\
    									\
    void								\
    DTYPE ## _array_destroy(struct DTYPE ## _array*arr);		\
    									\
    PREFIX DTYPE							\
    DTYPE ## _array_last(const struct DTYPE ## _array*arr);		\
    									\
    PREFIX DTYPE							\
    DTYPE ## _array_get(const struct DTYPE ## _array*arr,		\
				     size_t i);				\
    									\
    void								\
    DTYPE ## _array_set(struct DTYPE ## _array*arr,			\
			size_t i,					\
			PREFIX DTYPE val);				\
    									\
    void								\
    DTYPE ## _array_swap(struct DTYPE ## _array*arr,			\
			 size_t i, size_t j);				\
									\
    size_t								\
    DTYPE ## _array_len(const struct DTYPE ## _array*arr);		\
    									\
    struct DTYPE ## _array*						\
    DTYPE ## _range(PREFIX DTYPE from,					\
		    PREFIX DTYPE to,					\
		    PREFIX DTYPE step);					\
									\
    PREFIX DTYPE							\
    DTYPE ## _array_sum(const struct DTYPE ## _array*arr);		\
									\
    PREFIX DTYPE							\
    DTYPE ## _array_sum_partial(const struct DTYPE ## _array*arr,	\
				size_t from, size_t to);		\
									\
    void								\
    DTYPE ## _array_dump(const char*fmt,				\
			 const struct DTYPE ## _array*arr);		\
									\
    size_t								\
    DTYPE ## _array_argmin(const struct DTYPE ## _array*arr);		\
									\
    size_t								\
    DTYPE ## _array_argmax(const struct DTYPE ## _array*arr);		\
    									\
    int									\
    DTYPE ## _array_write_binary(FILE*f_out,				\
				 const struct DTYPE ## _array*arr);	\
									\
    struct DTYPE ## _array*						\
    DTYPE ## _array_read_binary(FILE*f_in);				\
									\
    PREFIX DTYPE							\
    DTYPE ## _array_max_partial(const struct DTYPE ## _array*arr,	\
				size_t from, size_t to);		\
									\
    PREFIX DTYPE							\
    DTYPE ## _array_max(const struct DTYPE ## _array*arr);		\
									\
    int									\
    DTYPE ## _array_get_i(const struct DTYPE ## _array*arr,		\
			  PREFIX DTYPE element);			\
									\
    void								\
    DTYPE ## _array_normalize(struct DTYPE ## _array*arr);		\
									\
    void								\
    DTYPE ## _array_set_all(struct DTYPE ## _array*arr,			\
			    PREFIX DTYPE val);				\
									\
    void								\
    DTYPE ## _array_apply_inplace(const struct DTYPE ## _array*arr,	\
				  PREFIX DTYPE (*func) (PREFIX DTYPE));	\
									\
    struct DTYPE ## _array*						\
    DTYPE ## _array_apply(const struct DTYPE ## _array*arr,		\
			  PREFIX DTYPE (*func) (PREFIX DTYPE));		\
									\
    void								\
    DTYPE ## _array_push(struct DTYPE ## _array*arr,			\
			 PREFIX DTYPE val);


#define ARRAY_CLASS_BODY(PREFIX, DTYPE)					\
    									\
    struct DTYPE ## _array*						\
    new_ ## DTYPE ## _array(size_t size)				\
    {									\
	struct DTYPE ## _array *arr = E_MALLOC(sizeof(*arr));		\
	arr->vals = E_MALLOC(size*sizeof(*arr->vals));			\
	arr->size = size;						\
	return arr;							\
    }									\
									\
    struct DTYPE ## _array*						\
    DTYPE ## _array_copy(const struct DTYPE ## _array*arr)		\
    {									\
	if(arr) {							\
	    struct DTYPE ## _array *new = new_ ## DTYPE ## _array(arr->size); \
	    memcpy(new->vals, arr->vals, arr->size * sizeof(*arr->vals)); \
	    return new;							\
	}								\
	return NULL;							\
    }									\
									\
    void								\
    DTYPE ## _array_cpy_from_to(struct DTYPE ## _array*dest,		\
				const struct DTYPE ## _array*src,	\
				size_t dest_i,				\
				size_t src_i)				\
    {									\
	memcpy(dest->vals + dest_i,					\
	       src->vals + src_i,					\
	       src->size * sizeof(*src->vals));				\
    }									\
									\
    void								\
    DTYPE ## _array_cpy(struct DTYPE ## _array*dest,			\
			const struct DTYPE ## _array*src)		\
    {									\
	DTYPE ## _array_cpy_from_to(dest, src, 0, 0);			\
    }									\
									\
    void								\
    DTYPE ## _array_destroy(struct DTYPE ## _array*arr)			\
    {									\
	if(arr) {							\
	    free_if_not_null(arr->vals);				\
	    free(arr);							\
	}								\
    }									\
    									\
    PREFIX DTYPE							\
    DTYPE ## _array_last(const struct DTYPE ## _array*arr)		\
    {									\
	return arr->vals[arr->size ? arr->size - 1 : 0];		\
    }									\
    									\
    PREFIX DTYPE							\
    DTYPE ## _array_get(const struct DTYPE ## _array*arr, size_t i)	\
    {									\
	return arr->vals[i];						\
    }									\
    									\
    void								\
    DTYPE ## _array_set(struct DTYPE ## _array*arr,			\
			size_t i, PREFIX DTYPE val)			\
    {									\
	arr->vals[i] = val;						\
    }									\
									\
    void								\
    DTYPE ## _array_swap(struct DTYPE ## _array*arr,			\
			 size_t i, size_t j)				\
    {									\
	PREFIX DTYPE swap = arr->vals[i];				\
	arr->vals[i] = arr->vals[j];					\
	arr->vals[j] = swap;						\
    }									\
									\
    size_t								\
    DTYPE ## _array_len(const struct DTYPE ## _array*arr)		\
    {									\
	return arr->size;						\
    }									\
									\
    struct DTYPE ## _array*						\
    DTYPE ## _range(PREFIX DTYPE from,					\
		    PREFIX DTYPE to,					\
		    PREFIX DTYPE step)					\
    {									\
	struct DTYPE ## _array*arr=NULL;				\
	PREFIX DTYPE *pos=NULL;						\
	PREFIX DTYPE i;							\
	size_t size = (size_t) ((to - from) / step);			\
	arr = new_ ## DTYPE ## _array(size);				\
	for(i = from, pos = arr->vals; i < to; i += step, pos++) {	\
	    *pos = i;							\
	}								\
	return arr;							\
    }									\
									\
    PREFIX DTYPE							\
    DTYPE ## _array_sum(const struct DTYPE ## _array*arr)		\
    {									\
	size_t i;							\
	PREFIX DTYPE sum = 0;						\
	for(i = 0; i < arr->size; ++i) {				\
	    sum += arr->vals[i];					\
	}								\
	return sum;							\
    }									\
									\
    PREFIX DTYPE							\
    DTYPE ## _array_sum_partial(const struct DTYPE ## _array*arr,	\
				size_t from, size_t to)			\
    {									\
	PREFIX DTYPE sum = (PREFIX DTYPE) 0;				\
	size_t i;							\
	for(i = from; i < to; ++i) {					\
	    sum += arr->vals[i];					\
	}								\
	return sum;							\
    }									\
    									\
    void								\
    DTYPE ## _array_dump(const char*fmt,				\
			 const struct DTYPE ## _array *arr)		\
    {									\
	size_t i;							\
	if(arr) {							\
	    for(i = 0; i < arr->size; ++i) {				\
		mprintf(fmt, arr->vals[i]);				\
	    }								\
	}								\
	mprintf("\n");							\
    }									\
    									\
    size_t								\
    DTYPE ## _array_argmin(const struct DTYPE ## _array*arr)		\
    {									\
	size_t i, min_i=0;						\
	for(i = 1; i < arr->size; ++i) {				\
	    min_i = arr->vals[i] < arr->vals[min_i] ? i : min_i;	\
	}								\
	return min_i;							\
    }									\
    									\
    size_t								\
    DTYPE ## _array_argmax(const struct DTYPE ## _array*arr)		\
    {									\
	size_t i, max_i=0;						\
	for(i = 1; i < arr->size; ++i) {				\
	    max_i = arr->vals[i] > arr->vals[max_i] ? i : max_i;	\
	}								\
	return max_i;							\
    }									\
									\
    int									\
    DTYPE ## _array_write_binary(FILE*f_out,				\
				 const struct DTYPE ## _array*arr)	\
    {									\
	const char*this_sub="array_write_binary";			\
	int err=0;							\
	size_t size = arr ? arr->size : 0;				\
	err = m_bin_write(NULL, &size, sizeof(size),			\
			  1, f_out, this_sub);				\
	if(!err && arr) {						\
	    err = m_bin_write(NULL, arr->vals, sizeof(*arr->vals),	\
			      arr->size, f_out, this_sub);		\
	}								\
	if(err) {							\
	    err_printf(this_sub, "Failed to write binary array\n");	\
	}								\
	return err;							\
    }									\
									\
    struct DTYPE ## _array*						\
    DTYPE ## _array_read_binary(FILE*f_in)				\
    {									\
	int err=0;							\
	size_t size;							\
	struct DTYPE ## _array*arr=NULL;				\
	err = (fread(&size, sizeof(size), 1, f_in) != 1);		\
	if(!err) {							\
	    err = !size;						\
	}								\
	if(!err) {							\
	    arr = new_ ## DTYPE ## _array(size);			\
	    err = !arr;							\
	}								\
	if(!err) {							\
	    err = (fread(arr->vals, sizeof(*arr->vals),			\
			 size, f_in) != size);				\
	}								\
	if(err) {							\
	    DTYPE ## _array_destroy(arr);				\
	    return NULL;						\
	}								\
	return arr;							\
    }									\
    									\
    PREFIX DTYPE							\
    DTYPE ## _array_max_partial(const struct DTYPE ## _array*arr,	\
				size_t from, size_t to)			\
    {									\
	size_t i;							\
	PREFIX DTYPE max=(arr->size ? arr->vals[from] : (PREFIX DTYPE) 0); \
	for(i = from + 1; i < to; ++i) {				\
	    max = arr->vals[i] > max ? arr->vals[i] : max;		\
	}								\
	return max;							\
    }									\
									\
    PREFIX DTYPE							\
    DTYPE ## _array_max(const struct DTYPE ## _array*arr)		\
    {									\
	return DTYPE ## _array_max_partial(arr, 0, arr->size);		\
    }									\
    									\
    int									\
    DTYPE ## _array_get_i(const struct DTYPE ## _array*arr,		\
			  PREFIX DTYPE element)				\
    {									\
	int i;								\
	for(i = 0; i < (int) arr->size; ++i) {				\
	    if(element == arr->vals[i]) { 				\
		return i;						\
	    }								\
	}								\
	return -1;							\
    }									\
									\
    void								\
    DTYPE ## _array_normalize(struct DTYPE ## _array*arr)		\
    {									\
	size_t i;							\
	PREFIX DTYPE sum;						\
	sum = DTYPE ## _array_sum(arr);					\
	for(i = 0; i < arr->size; ++i) {				\
	    arr->vals[i] /= sum;					\
	}								\
    }									\
									\
    void								\
    DTYPE ## _array_set_all(struct DTYPE ## _array*arr,			\
			    PREFIX DTYPE val)				\
    {									\
	size_t i;							\
	for(i = 0; i < arr->size; ++i) {				\
	    arr->vals[i] = val;						\
	}								\
    }									\
									\
    struct DTYPE ## _array*						\
    new_ ## DTYPE ## _array_initialized(size_t size,			\
					PREFIX DTYPE val)		\
    {									\
	struct DTYPE ## _array*arr=new_ ## DTYPE ## _array(size);	\
	DTYPE ## _array_set_all(arr, val);				\
    	return arr;							\
    }									\
    									\
    void								\
    DTYPE ## _array_apply_inplace(const struct DTYPE ## _array*arr,	\
				  PREFIX DTYPE (*func) (PREFIX DTYPE))	\
    {									\
	size_t i;							\
	for(i = 0; i < arr->size; ++i) {				\
	    arr->vals[i] = func(arr->vals[i]);				\
	}								\
    }									\
									\
    struct DTYPE ## _array*						\
    DTYPE ## _array_apply(const struct DTYPE ## _array*arr,		\
			  PREFIX DTYPE (*func) (PREFIX DTYPE))		\
    {									\
	struct DTYPE ## _array*arr_out = DTYPE ## _array_copy(arr);	\
	DTYPE ## _array_apply_inplace(arr_out, func);			\
	return arr_out;							\
    }									\
									\
    void								\
    DTYPE ## _array_push(struct DTYPE ## _array*arr,			\
			 PREFIX DTYPE val)				\
    {									\
	arr->vals[arr->size++] = val;					\
    }



ARRAY_CLASS_HEAD(, size_t)
ARRAY_CLASS_HEAD(, int)
ARRAY_CLASS_HEAD(, float)
ARRAY_CLASS_HEAD(, double)

float
float_array_avg(const struct float_array*arr);

#endif
