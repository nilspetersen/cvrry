#include "rna_get_atoms.h"

static void
rcpy(struct RPoint *dest, const struct RPoint *src)
{
    dest->x = src->x;
    dest->y = src->y;
    dest->z = src->z;
}

static size_t
copy_if_set(const struct rna_atom *a1,
	    const struct rna_atom *a2,
	    struct RPoint *atoms1,
	    struct RPoint *atoms2)
{
    if(!a1->is_set || !a2->is_set)
	return 0;
    rcpy(atoms1, &a1->r);
    rcpy(atoms2, &a2->r);
    return 1;
}

/*
 * get_backbone_atoms
 */

static const struct rna_atom*
get_bb_p(const struct rna_backbone_nuc *bb_nuc)
{
    return &bb_nuc->p;
}

static const struct rna_atom*
get_bb_c1(const struct rna_backbone_nuc *bb_nuc)
{
    return &bb_nuc->c1;
}

static const struct rna_atom*
get_bb_c2(const struct rna_backbone_nuc *bb_nuc)
{
    return &bb_nuc->c2;
}

static const struct rna_atom*
get_bb_c3(const struct rna_backbone_nuc *bb_nuc)
{
    return &bb_nuc->c3;
}

static const struct rna_atom*
get_bb_c4(const struct rna_backbone_nuc *bb_nuc)
{
    return &bb_nuc->c4;
}

static const struct rna_atom*
get_bb_c5(const struct rna_backbone_nuc *bb_nuc) {
    return &bb_nuc->c5;
}

static const struct rna_atom*
get_bb_o3(const struct rna_backbone_nuc *bb_nuc)
{
    return &bb_nuc->o3;
}

static const struct rna_atom*
get_bb_o4(const struct rna_backbone_nuc *bb_nuc)
{
    return &bb_nuc->o4;
}

static const struct rna_atom*
get_bb_o5(const struct rna_backbone_nuc *bb_nuc)
{
    return &bb_nuc->o5;
}

static const struct rna_atom*
get_atom(const struct rna_backbone_nuc *bb_nuc,
	 enum rna_atom_types atom_type)
{
     static const struct rna_atom* (* const getfuncs[9]) (const struct rna_backbone_nuc *bb_nuc) = {
	get_bb_p,
	get_bb_o5,
	get_bb_c5,
	get_bb_c4,
	get_bb_o4,
	get_bb_c3,
	get_bb_o3,
	get_bb_c2,
	get_bb_c1
    };
    return getfuncs[atom_type](bb_nuc);
}


static size_t
get_bb_atoms_one_nt_match(struct RPoint *list_1,
			  struct RPoint *list_2,
			  const struct rna_backbone_nuc *nuc_1,
			  const struct rna_backbone_nuc *nuc_2,
			  const enum rna_atom_types *atom_types,
			  size_t n_atom_types)
{
    size_t i, len=0;
    for(i = 0; i < n_atom_types; ++i) {
	enum rna_atom_types atom_type = atom_types[i];
	len += copy_if_set(get_atom(nuc_1, atom_type),
			   get_atom(nuc_2, atom_type),
			   list_1+len,
			   list_2+len);
    }
    return len;
}


size_t
get_backbone_atoms(const struct pair_set *pairset,
		   const struct rna_coord *rna1,
		   const struct rna_coord *rna2, 
		   struct RPoint *list_1,
		   struct RPoint *list_2,
		   size_t *n_atoms_per_nt,
		   const enum rna_atom_types *atom_types,
		   size_t n_atom_types)
{
    size_t k, l;
    size_t len = 0;
    size_t lastlen = 0;

    for(k = 0, l = 0; k < pairset->n; k++) {
	int i, j;
	i = pairset->indices[k][0];
	j = pairset->indices[k][1];
	if(i == GAP_INDEX || j == GAP_INDEX)
	    continue;

	len += get_bb_atoms_one_nt_match(list_1 + len,
					 list_2 + len,
					 rna1->bb + i,
					 rna2->bb + j,
					 atom_types,
					 n_atom_types);
	
	if(n_atoms_per_nt != NULL) {
	    n_atoms_per_nt[l] = len - lastlen;
	    l++;
	    lastlen = len;
	}
    }
    return len;
}

size_t
get_backbone_atoms_one_type(const struct pair_set *pairset,
			    const struct rna_coord *rna1,
			    const struct rna_coord *rna2, 
			    struct RPoint *list_1,
			    struct RPoint *list_2,
			    const enum rna_atom_types atom_type)
{
    return get_backbone_atoms(pairset,
			      rna1,
			      rna2, 
			      list_1,
			      list_2,
			      NULL,
			      &atom_type,
			      1);
}

int
get_simple_backbone(struct RPoint *bb,
		    const struct rna_coord *rna,
		    const enum rna_atom_types atom_type)
{
    size_t i;
    for(i = 0; i < rna->size; ++i) {
	const struct rna_atom *atom = get_atom(rna->bb + i,
					       atom_type);
	if(!atom->is_set) {
	    return 1;
	}
	bb[i] = atom->r;
    }
    return 0;
}

size_t
get_backbone_atoms_continuous_match(struct RPoint *list_1,
				    struct RPoint *list_2,
				    const struct continuous_match *match,
				    const struct rna_coord *rna1,
				    const struct rna_coord *rna2,
				    const enum rna_atom_types *atom_types,
				    size_t n_atom_types)
{
    size_t k, len = 0;
    for(k = 0; k < match->len; k++) {
	len += get_bb_atoms_one_nt_match(list_1 + len,
					 list_2 + len,
					 rna1->bb + match->matchpos_a + k,
					 rna2->bb + match->matchpos_b + k,
					 atom_types,
					 n_atom_types);
    }
    return len;
}
