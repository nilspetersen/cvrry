#include "pair.h"
#include "e_malloc.h"


#define PAIR_CLASS_BODY(PREFIX, DTYPE)					\
									\
    struct DTYPE ## _pair*						\
    new_ ## DTYPE ## _pair()						\
    {									\
	return E_MALLOC(sizeof(struct DTYPE ## _pair));			\
    }									\
									\
    void								\
    DTYPE ## _pair_destroy(struct DTYPE ## _pair *pair)			\
    {									\
	free_if_not_null(pair);						\
    }


PAIR_CLASS_BODY(, float)
PAIR_CLASS_BODY(, size_t)
