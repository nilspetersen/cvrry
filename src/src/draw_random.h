#ifndef DRAW_RANDOM
#define DRAW_RANDOM

#include <stdlib.h>
#include "array.h"

int
draw_random_int(int min, int max);

float
draw_random_float();

size_t
bin_search_cumulative_probability_c_arr(float p,
					size_t arr_size,
					const float*arr);

size_t
bin_search_cumulative_probability(float p,
				  const struct float_array *arr);

#endif
