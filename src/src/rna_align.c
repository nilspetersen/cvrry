#include "rna_align.h"
#include "score_mat.h"
#include "rna_score.h"
#include "align.h"
#include "pair_set_p.h"

/* TODO: pass alignment parameter and function using void* ??? */

struct pair_set*
align(const struct rna_frag_coord *rna_chain_a,
      const struct rna_frag_coord *rna_chain_b,
      const struct rna_align_params *params)
{
    size_t seq_len_a, seq_len_b;
    struct score_mat *score_matrix=NULL, *coverage_matrix=NULL;
    struct pair_set *pset=NULL;
    
    seq_len_a = rna_frag_coord_get_seq_len(rna_chain_a);
    seq_len_b = rna_frag_coord_get_seq_len(rna_chain_b);

    score_matrix = score_mat_new(seq_len_a, seq_len_b);
    coverage_matrix = score_mat_new(seq_len_a, seq_len_b);

    fill_matrix_cv(score_matrix,
		   coverage_matrix,
		   rna_chain_a->frags,
		   rna_chain_b->frags);

    if(params->scale_factor > 0) {
	scale_marginal_scores(score_matrix,
			      coverage_matrix,
			      rna_chain_b->frags->xmer,
			      params->scale_factor);
    }
    if(params->base_pair_bonus > 0.0 &&
       rna_chain_a->rna_backbone->basepair_info &&
       rna_chain_b->rna_backbone->basepair_info) {
	add_basepair_bonus(score_matrix,
			   rna_chain_a->rna_backbone,
			   rna_chain_b->rna_backbone,
			   params->base_pair_bonus);
    }
    pset = score_mat_sum_minimal(score_matrix,
				 params->gap_open,
				 params->gap_widen,
				 params->algn_type);
    
    score_mat_destroy(score_matrix);
    score_mat_destroy(coverage_matrix);
    return pset;
}



struct pair_set*
align_alfons(const struct rna_seq_coord *seq_coord_a,
	     const struct rna_seq_coord *seq_coord_b,
	     const struct rna_align_params *params,
	     enum score_type scr_type)
{
    const char*this_sub="align_alfons";
    size_t seq_len_a, seq_len_b;
    struct score_mat *score_matrix=NULL;
    struct pair_set *pset=NULL;
    int err=1;
    
    seq_len_a = rna_seq_coord_scoremat_dim(seq_coord_a);
    seq_len_b = rna_seq_coord_scoremat_dim(seq_coord_b);
    
    score_matrix = score_mat_new(seq_len_a, seq_len_b);

    switch(scr_type) {
    case UNIT_SCORE:
	fill_matrix_alphabet_unit_score(score_matrix,
					seq_coord_a->seq,
					seq_coord_b->seq);
	break;
    case SIGNIFICANCE_WEIGHTED:
	fill_matrix_alphabet_significance_weighted(score_matrix,
						   seq_coord_a->seq,
						   seq_coord_b->seq);
	break;
    default:
	err_printf(this_sub, "Score type not implemented!\n");
	return NULL;
    }

    /* { */
    /* 	float min, max, av, std_dev; */
    /* 	score_mat_info(score_matrix, */
    /* 		       &min, &max, */
    /* 		       &av, &std_dev); */
    /* 	mprintf("min=%.2f, max=%.2f, av=%.2f, std_dev=%.2f\n", */
    /* 		min, max, av, std_dev); */
    /* } */

    if(params->base_pair_bonus > 0.0 &&
       seq_coord_a->rna_backbone->basepair_info &&
       seq_coord_b->rna_backbone->basepair_info) {
	err = add_base_pair_bonus_alphabet(score_matrix,
					   seq_coord_a->seq,
					   seq_coord_b->seq,
					   seq_coord_a->rna_backbone,
					   seq_coord_b->rna_backbone,
					   params->base_pair_bonus);
    } else if(params->base_pair_bonus <= 0.0) {
	err = 0;
    }
    if(!err) {
	pset = score_mat_sum_minimal(score_matrix,
				     params->gap_open,
				     params->gap_widen,
				     params->algn_type);
    }
    score_mat_destroy(score_matrix);
    return pset;
}

float
rna_alignment_seq_id(const struct pair_set *pair_set,
		     const struct rna_coord*rna_1,
		     const struct rna_coord*rna_2)
{
    return get_seq_identity(pair_set, rna_1->seq, rna_2->seq);
}
