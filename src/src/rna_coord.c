#include <stdio.h>
#include <stdlib.h>
#include "rna_coord.h"
#include "e_malloc.h"
#include "string.h"
#include "mprintf.h"
#include "seq.h"
#include "bin_utils.h"
#include "fio.h"
#include "same.h"
#include "wrapped_matrix.h"
#include "lsqf.h"

#define CHAINBREAK_DIST_THRESH 3.0
#define RNA_COORD_SRC_VERSION 100001

/* Globals */
int glob_old_read=0;

void
set_glob_old_read(int newval) {
    glob_old_read = newval;
}

ARRAY_CLASS_BODY(enum, rna_atom_types)

size_t
rna_coord_get_size(const struct rna_coord*rna)
{
    return rna->size;
}

size_t
rna_coord_seqlen_by_mmcif_indices(const struct rna_coord*rna)
{
    int idiff;
    if(!rna->size) {
	return 0;
    }
    idiff = rna->cif_seq_num[rna->size - 1] - rna->cif_seq_num[0] + 1;
    if(idiff < 0) {
	return 0;
    }
    return (size_t) idiff;
}

struct rna_atom_types_array*
rna_atom_types_array_create(enum rna_atom_patterns pattern)
{
    const char*this_sub="rna_atom_types_array_create";
    switch(pattern) {
    case ALL:
	return rna_atom_types_range(P, C1 + 1, 1);
    }
    err_printf(this_sub, "not a valid pattern type!\n");
    return NULL;
}

enum rna_atom_types
get_atom_type_from_str(const char*atom_type_str)
{
    if(!strcmp("P", atom_type_str)) {
	return P;
    } else if(!strcmp("C1'", atom_type_str)) {
	return C1;
    } else if(!strcmp("C2'", atom_type_str)) {
	return C2;
    } else if(!strcmp("C3'", atom_type_str)) {
	return C3;
    } else if(!strcmp("C4'", atom_type_str)) {
	return C4;
    } else if(!strcmp("C5'", atom_type_str)) {
	return C5;
    } else if(!strcmp("O3'", atom_type_str)) {
	return O3;
    } else if(!strcmp("O4'", atom_type_str)) {
	return O4;
    } else if(!strcmp("O5'", atom_type_str)) {
	return O5;
    }
    return NOT_A_BACKBONE_ATOM;
}

static enum yes_no
is_chain_broken(const struct rna_backbone_nuc *first,
		const struct rna_backbone_nuc *second)
{
    const struct rna_atom *o3 = &first->o3;
    const struct rna_atom *p = &second->p;
    if(!(o3->is_set && p->is_set)) {
	return YES;
    }
    if(rdist(&o3->r, &p->r) > CHAINBREAK_DIST_THRESH) {
	return YES;
    }
    return NO;
}

void
compute_chain_breaks(struct rna_coord *rna)
{
    size_t i;
    if(rna->size) {
	free_if_not_null(rna->chain_breaks);
	/* allocate memory */
	rna->chain_breaks = E_MALLOC(rna->size * sizeof(*rna->chain_breaks));
	/* find the chain breaks */
	for(i = 0; i + 1 < rna->size; i++) {
	    rna->chain_breaks[i] = is_chain_broken(rna->bb + i, rna->bb + i + 1);
	}
	rna->chain_breaks[rna->size-1] = YES;
    } else {
	rna->chain_breaks = NULL;
    }
}

void
rna_coord_chain_breaks_dump(const struct rna_coord *rna)
{
    size_t i;
    if(!rna->chain_breaks) {
	return;
    }
    mprintf("Chain-breaks:\n");
    for(i = 0; i < rna->size; ++i) {
	mprintf("%i ", rna->chain_breaks[i]);
    }
    mprintf("\n");
}

static void
rna_coord_basepair_info_dump(const struct rna_coord *rna)
{
    size_t i;
    if(!rna->basepair_info) {
	return;
    }
    mprintf("Basepair Info:\n");
    for(i = 0; i < rna->size; ++i) {
	mprintf("%i ", rna->basepair_info[i]);
    }
    mprintf("\n");
}

static void
rna_coord_cif_seq_num_dump(const struct rna_coord *rna)
{
    size_t i;
    if(!rna->cif_seq_num) {
	return;
    }
    mprintf("Sequence numbers (mmcif):\n");
    for(i = 0; i < rna->size; ++i) {
	mprintf("%i ", rna->cif_seq_num[i]);
    }
    mprintf("\n");
}


static struct seq*
new_seq(size_t len)
{
    struct seq *ret;
    ret = E_MALLOC(sizeof(*ret));
    ret->length = len;
    ret->comment = NULL;
    ret->seq = E_MALLOC(len * sizeof(*(ret->seq)));
    return ret;
}

static size_t
seq_write_bin(FILE *outfile, const struct seq*sequence)
{
    const char *this_sub="seq_write_bin";
    size_t total_memsize=0;
    size_t comment_len=0;
    if(sequence->comment) {
	comment_len = strlen(sequence->comment) + 1;
    }
    if(m_bin_write(&total_memsize,
		   &sequence->length,
		   sizeof(sequence->length),
		   1, outfile, this_sub)) {
	goto err_exit;
    }
    if(m_bin_write(&total_memsize,
		   &comment_len,
		   sizeof(comment_len),
		   1, outfile, this_sub)) {
	goto err_exit;
    }
    if(m_bin_write(&total_memsize,
		   sequence->seq,
		   sizeof(*sequence->seq),
		   sequence->length,
		   outfile, this_sub)) {
	goto err_exit;
    }
    if(sequence->comment) {
	if(m_bin_write(&total_memsize,
		       sequence->comment,
		       sizeof(*sequence->comment),
		       comment_len,
		       outfile,
		       this_sub)) {
	    goto err_exit;
	}
    }
    return total_memsize;
err_exit:
    return 0;
}

static int
seq_read_binary(struct seq*sequence, FILE *infile)
{
    size_t seq_len;
    size_t comment_len;
    if(fread(&seq_len, sizeof(seq_len), 1, infile) != 1) {
	goto err_exit;
    }
    if(fread(&comment_len, sizeof(comment_len), 1, infile) != 1) {
	goto err_exit;
    }
    if(fread(sequence->seq, sizeof(*sequence->seq), seq_len, infile) != seq_len) {
	goto err_exit;
    }
    if(comment_len) {
	free_if_not_null(sequence->comment);
	sequence->comment = E_MALLOC(comment_len * sizeof(*sequence->comment));
	if(fread(sequence->comment, sizeof(*sequence->comment),
		 comment_len, infile) != comment_len) {
	    goto err_exit;
	}
    }
    return 0;
err_exit:
    return 1;
}

void
reset_rna_backbone_nuc(struct rna_backbone_nuc *bb)
{
    bb->c1.is_set = bb->c2.is_set = bb->c3.is_set = bb->c4.is_set = bb->c5.is_set = NO;
    bb->o3.is_set = bb->o4.is_set = bb->o5.is_set = bb->p.is_set = NO;
}

static void
set_to_origin(struct RPoint *r)
{
    r->x = r->y = r->z = 0;
}

static void
init_rna_backbone_nuc(struct rna_backbone_nuc *bb)
{
    reset_rna_backbone_nuc(bb);

    set_to_origin(&bb->c1.r);
    set_to_origin(&bb->c2.r);
    set_to_origin(&bb->c3.r);
    set_to_origin(&bb->c4.r);
    set_to_origin(&bb->c5.r);

    set_to_origin(&bb->o3.r);
    set_to_origin(&bb->o4.r);
    set_to_origin(&bb->o5.r);
    
    set_to_origin(&bb->p.r);
}

struct rna_coord*
new_rna_coord(size_t n_res)
{
    struct rna_coord *coord;
    size_t i;
    coord = E_MALLOC(sizeof(*coord));
    coord->src_version = RNA_COORD_SRC_VERSION;
    coord->res = E_CALLOC(n_res, sizeof(*coord->res));
    coord->bb  = E_MALLOC(n_res * sizeof(*coord->bb));
    coord->cif_seq_num = E_CALLOC(n_res, sizeof(*coord->cif_seq_num));
    coord->chain_breaks = NULL;
    coord->basepair_info = NULL;
    /* allocate space for the sequence */
    coord->seq = new_seq(n_res);
    coord->sequence = new_char_matrix(n_res, 4, '\0');
    coord->size = n_res;
    /* set all atoms to not set */
    for(i = 0; i < n_res; ++i) {
	init_rna_backbone_nuc(coord->bb + i);
    }
    return coord;
}

struct rna_coord*
rna_coord_copy_piece(const struct rna_coord *old,
		     size_t start_i,
		     size_t n_res)
{
    struct rna_coord *new;
    new = new_rna_coord(n_res);
    memcpy(new->bb, old->bb + start_i, n_res * sizeof(*new->bb));
    memcpy(new->res, old->res + start_i, n_res * sizeof(*new->res));
    new->seq->length = n_res;
    memcpy(new->seq->seq, old->seq->seq + start_i, n_res * sizeof(*new->seq->seq));
    memcpy(new->cif_seq_num, old->cif_seq_num + start_i, n_res * sizeof(*new->cif_seq_num));

    char_matrix_copy_lines(new->sequence, old->sequence, start_i, n_res);

    if(old->chain_breaks) {
	new->chain_breaks = E_MALLOC(n_res * sizeof(*new->chain_breaks));
	memcpy(new->chain_breaks,
	       old->chain_breaks + start_i,
	       n_res * sizeof(*new->chain_breaks));	
    }
    if(old->basepair_info) {
	new->basepair_info = E_MALLOC(n_res * sizeof(*new->basepair_info));
	memcpy(new->basepair_info,
	       old->basepair_info + start_i,
	       n_res * sizeof(*new->basepair_info));
    }

    return new;
}

struct rna_coord*
rna_coord_copy(const struct rna_coord *old)
{
    return rna_coord_copy_piece(old, 0, old->size);
}

const struct rna_res_id*
rna_coord_get_res_id(const struct rna_coord*rna, size_t i)
{
    return rna->res + i;
}

void
rna_coord_destroy(struct rna_coord *coord)
{
    if(coord) {
	free_if_not_null(coord->res);
	free_if_not_null(coord->bb);
	free_if_not_null(coord->chain_breaks);
	free_if_not_null(coord->basepair_info);
	free_if_not_null(coord->cif_seq_num);
	seq_destroy(coord->seq);
	char_matrix_destroy(coord->sequence);
	free_if_not_null(coord);
    }
}

/* printing stuff for testing purposes */

static void
print_rna_atom(const struct rna_atom *atom, const char *name)
{
    const struct RPoint *p = &atom->r;
    if(atom->is_set)
	mprintf("%s %5.3e %5.3e %5.3e\n", name, p->x, p->y, p->z);
}

void
rna_coord_dump(const struct rna_coord *coord)
{
    size_t i;
    struct rna_backbone_nuc *bb = coord->bb;
    mprintf("Coord version: %i\n", coord->src_version);
    for(i = 0; i < coord->size; ++i) {
	/* sequence info */
	mprintf("%zu %d%c %c\n", i, coord->res[i].resnum,
		coord->res[i].icode, coord->seq->seq[i]);
	/* coordinates */
	print_rna_atom(&bb->p, "P");
	print_rna_atom(&bb->c1, "C1'");
	print_rna_atom(&bb->c2, "C2'");
	print_rna_atom(&bb->c3, "C3'");
	print_rna_atom(&bb->c4, "C4'");
	print_rna_atom(&bb->c5, "C5'");
	print_rna_atom(&bb->o3, "O3'");
	print_rna_atom(&bb->o4, "O4'");
	print_rna_atom(&bb->o5, "O5'");
	bb++;
    }
    rna_coord_cif_seq_num_dump(coord);
    rna_coord_chain_breaks_dump(coord);
    rna_coord_basepair_info_dump(coord);
    /* mprintf("Original sequence:\n"); */
    /* char_matrix_dump("%c", coord->sequence); */
}

void
get_atom_name(enum rna_atom_types atype, char *name)
{
    switch(atype) {
    case C1:
	strcpy(name, "C1'");
	break;
    case C2:
	strcpy(name, "C2'");
	break;
    case C3:
	strcpy(name, "C3'");
	break;
    case C4:
	strcpy(name, "C4'");
	break;
    case C5:
	strcpy(name, "C5'");
	break;
    case O3:
	strcpy(name, "O3'");
	break;
    case O4:
	strcpy(name, "O4'");
	break;
    case O5:
	strcpy(name, "O5'");
	break;
    case P:
	strcpy(name, "P");
	break;
    default:
	strcpy(name, "?");
    }
}

static char
get_element(enum rna_atom_types atype)
{
    switch(atype) {
    case C1:
    case C2:
    case C3:
    case C4:
    case C5:
	return 'C';
    case O3:
    case O4:
    case O5:
	return 'O';
    case P:
	return 'P';
    default:
	return '?';
    }
}

struct rna_atom*
get_atom_ptr(enum rna_atom_types atype, struct rna_backbone_nuc *nuc)
{
    switch(atype) {
    case C1:
	return(&nuc->c1);
    case C2:
	return(&nuc->c2);
    case C3:
	return(&nuc->c3);
    case C4:
	return(&nuc->c4);
    case C5:
	return(&nuc->c5);
    case O3:
	return(&nuc->o3);
    case O4:
	return(&nuc->o4);
    case O5:
	return(&nuc->o5);
    case P:
	return(&nuc->p);
    default:
	return(NULL);
    }
}

const struct rna_atom*
get_const_atom_ptr(enum rna_atom_types atype,
		   const struct rna_backbone_nuc *nuc)
{
    switch(atype) {
    case C1:
	return(&nuc->c1);
    case C2:
	return(&nuc->c2);
    case C3:
	return(&nuc->c3);
    case C4:
	return(&nuc->c4);
    case C5:
	return(&nuc->c5);
    case O3:
	return(&nuc->o3);
    case O4:
	return(&nuc->o4);
    case O5:
	return(&nuc->o5);
    case P:
	return(&nuc->p);
    default:
	return(NULL);
    }
}

/*
 * print_pdb_line
 * INPUT:
 * int res_i: the number of the residue in the chain, if negative the residue
 *            names from the original pdbfile will be used
 */
static void
print_pdb_line(FILE *f, size_t atom_i, enum rna_atom_types atype, char base_type,
	       const struct rna_res_id *res, const struct RPoint *r, int res_i)
{ 				/* chain id is omitted */
    char element, icode=' ';
    char atom_name[5];
    get_atom_name(atype, atom_name);
    element = get_element(atype);
    if(res_i < 0) {
	res_i = res->resnum;
	icode = res->icode;
    } else {
	res_i++; /* start with 1, not with 0 */
    }
    mfprintf(f, "ATOM  %5zu  %-4s  %c  %4i%c   %8.3f%8.3f%8.3f                       %c\n",
	     atom_i, atom_name, base_type, res_i, icode, r->x, r->y, r->z, element);
}

void
rna_coord_2_pdb_file(FILE *f,
		     const struct rna_coord *coord)
{
    const enum rna_atom_types atoms[] = {P, O5, C5, C4, O4, C3, O3, C2, C1};
    const size_t n_atomtypes = 9;
    size_t i, j, k=0;
    /* iterate residues */
    for(i = 0; i < coord->size; ++i) {
	struct rna_backbone_nuc *p_nuc = coord->bb + i;
        /* iterate atoms */
	for(j = 0; j < n_atomtypes; ++j) {
	    enum rna_atom_types atype = atoms[j];
	    struct rna_atom *p_atom = get_atom_ptr(atype, p_nuc);
	    if(p_atom->is_set) {
		print_pdb_line(f, ++k, atype, coord->seq->seq[i], coord->res + i, &p_atom->r, -1);
	    }
	}
    }    
}

int
rna_coord_2_pdb(const struct rna_coord *coord, const char *fname) /* TODO: information about modified residues is lost here! */
{
    const char *this_sub="rna_coord_2_pdb";
    FILE *f=NULL;
    f = mfopen(fname, "w", this_sub);
    if(!f) {
	return 1;
    }
    rna_coord_2_pdb_file(f, coord);
    fclose(f);
    return 0;
}

size_t
rna_coord_get_len(const struct rna_coord *coord)
{
    return coord->size;
}

size_t
rna_coord_write_bin(FILE *outfile, const struct rna_coord *rna)
{
    const char *this_sub="rna_coord_write_bin";
    size_t total_memsize=0;
    size_t memsize_seq=0, memsize_seq_full=0;
    int has_chain_breaks_computed=0, has_basepair_information=0;
    if(m_bin_write(&total_memsize, &rna->src_version,
		   sizeof(rna->src_version), 1, outfile, this_sub)) {
    	goto err_exit;
    }
    if(m_bin_write(&total_memsize, &rna->size, sizeof(rna->size), 1, outfile, this_sub)) {
    	goto err_exit;
    }
    if(rna->chain_breaks) {
    	has_chain_breaks_computed = 1;
    }
    if(rna->basepair_info) {
	has_basepair_information = 1;
    }
    if(m_bin_write(&total_memsize,
    		   &has_chain_breaks_computed,
    		   sizeof(has_chain_breaks_computed),
    		   1,
    		   outfile,
    		   this_sub)) {
    	goto err_exit;
    }
    if(m_bin_write(&total_memsize,
    		   &has_basepair_information,
    		   sizeof(has_basepair_information),
    		   1,
    		   outfile,
    		   this_sub)) {
    	goto err_exit;
    }
    if(m_bin_write(&total_memsize, rna->bb, sizeof(*rna->bb), rna->size, outfile, this_sub)) {
    	goto err_exit;
    }
    if(m_bin_write(&total_memsize, rna->res, sizeof(*rna->res), rna->size, outfile, this_sub)) {
    	goto err_exit;
    }
    if(m_bin_write(&total_memsize, rna->cif_seq_num, sizeof(*rna->cif_seq_num),
		   rna->size, outfile, this_sub)) {
    	goto err_exit;
    }
    memsize_seq = seq_write_bin(outfile, rna->seq);
    if(!memsize_seq) {
    	goto err_exit;
    }
    total_memsize += memsize_seq;
    memsize_seq_full = char_matrix_write_bin(outfile, rna->sequence);
    if(!memsize_seq_full) {
	goto err_exit;
    }
    total_memsize += memsize_seq_full;
    if(rna->chain_breaks) {
    	if(m_bin_write(&total_memsize,
    		       rna->chain_breaks,
    		       sizeof(*rna->chain_breaks),
    		       rna->size,
    		       outfile,
    		       this_sub)) {
    	    goto err_exit;
    	}
    }
    if(rna->basepair_info) {
	if(m_bin_write(&total_memsize,
    		       rna->basepair_info,
    		       sizeof(*rna->basepair_info),
    		       rna->size,
    		       outfile,
    		       this_sub)) {
    	    goto err_exit;
    	}
    }
    return total_memsize;
err_exit:
    err_printf(this_sub, "Failed to write rna coords in binary format\n");
    return 0;
}

int
rna_coord_write_binary(FILE *outfile, const struct rna_coord *rna)
{
    return !rna_coord_write_bin(outfile, rna);
}

struct rna_coord*
rna_coord_read_binary(FILE *infile)
{
    const char *this_sub="rna_coord_read_binary";
    struct rna_coord*rna=NULL;
    size_t n_res;
    int has_chain_breaks_computed=0, has_basepair_information=0;
    int src_version;
    if(!glob_old_read) {
	if(fread(&src_version, sizeof(src_version), 1, infile) != 1){
	    goto err_exit;
	}
	if(src_version != RNA_COORD_SRC_VERSION) {
	    err_printf(this_sub,
		       "Version (%i) of rna_coord binary file does not fit current version %i\n",
		       src_version,
		       RNA_COORD_SRC_VERSION);
	    goto err_exit;
	}
    }
    if(fread(&n_res, sizeof(n_res), 1, infile) != 1){
	goto err_exit;
    }
    if(fread(&has_chain_breaks_computed, sizeof(has_chain_breaks_computed), 1, infile) != 1){
	goto err_exit;
    }
    if(fread(&has_basepair_information, sizeof(has_basepair_information), 1, infile) != 1){
	goto err_exit;
    }
    rna = new_rna_coord(n_res);
    rna->src_version = src_version;
    if(fread(rna->bb, sizeof(*rna->bb), rna->size, infile) != rna->size) {
	goto err_exit;
    }
    if(fread(rna->res, sizeof(*rna->res), rna->size, infile) != rna->size) {
	goto err_exit;
    }
    if(!glob_old_read) {
	if(fread(rna->cif_seq_num, sizeof(*rna->cif_seq_num), rna->size, infile) != rna->size) {
	    goto err_exit;
	}
    }
    if(seq_read_binary(rna->seq, infile)) {
	goto err_exit;
    }
    char_matrix_destroy(rna->sequence);
    rna->sequence = char_matrix_read_binary(infile);
    if(!rna->sequence) {
	goto err_exit;
    }
    if(has_chain_breaks_computed) {
	rna->chain_breaks = E_MALLOC(n_res * sizeof(*rna->chain_breaks));
	if(fread(rna->chain_breaks, sizeof(*rna->chain_breaks), rna->size, infile) != rna->size){
	    goto err_exit;
	}
    }
    if(has_basepair_information) {
	rna->basepair_info = E_MALLOC(n_res * sizeof(*rna->basepair_info));
	if(fread(rna->basepair_info, sizeof(*rna->basepair_info), rna->size, infile) != rna->size){
	    goto err_exit;
	}
    }
    return rna;
err_exit:
    err_printf(this_sub, "Failed to read rna coords in binary format\n");
    rna_coord_destroy(rna);
    return NULL;
}

BINARY_IO_BODY(rna_coord)

size_t
count_chain_breaks(const struct rna_coord *rna)
{
    size_t i, n_breaks = 0;
    for(i = 0; i < rna->size-1; ++i) {
	n_breaks += rna->chain_breaks[i];
    }
    return n_breaks;
}

static enum yes_no
has_atom(const struct rna_backbone_nuc *nuc,
	 enum rna_atom_types atom_type)
{
    return get_const_atom_ptr(atom_type, nuc)->is_set;
}

static int
has_all_the_atoms(const struct rna_backbone_nuc *nuc,
		  const enum rna_atom_types *atom_types,
		  size_t n_atoms)
{
    size_t i;
    for(i = 0; i < n_atoms; ++i) {
	if(!has_atom(nuc, atom_types[i])) {
	    return NO;
	}
    }
    return YES;
}

static size_t
count_continuous_chains(const struct rna_coord *rna,
			const enum rna_atom_types *atom_types,
			size_t n_atomtypes)
{
    size_t i;
    size_t n_chains = 0;
    enum yes_no in_chain = NO;
    
    for(i = 0; i < rna->size; ++i) {
	if(has_all_the_atoms(rna->bb + i, atom_types, n_atomtypes)) {
	    if(!in_chain) {
		n_chains++;
		in_chain = YES;
	    }
	    if(rna->chain_breaks[i]) {
		in_chain = NO;
	    }
	} else {
	    in_chain = NO;
	}
    }
    return n_chains;
}

struct rna_coord_set*
new_rna_coord_set(size_t max_n_chains)
{
    struct rna_coord_set *ret=NULL;
    size_t i;
    ret = E_MALLOC(sizeof(*ret));
    ret->n_chains = 0;
    ret->chains = E_MALLOC(max_n_chains * sizeof(*ret->chains));
    for(i = 0; i < max_n_chains; ++i) {
	ret->chains[i] = NULL;
    }
    return ret;
}

void
rna_coord_set_destroy(struct rna_coord_set *rna_coordset)
{
    size_t i;
    if(rna_coordset) { 
	for(i = 0; i < rna_coordset->n_chains; ++i) {
	    rna_coord_destroy(rna_coordset->chains[i]);
	}
	free_if_not_null(rna_coordset->chains);
	free(rna_coordset);
    }
}

size_t
rna_coord_set_write_bin(FILE*outfile,
			   const struct rna_coord_set *rna_coordset)
{
    const char*this_sub="rna_coord_set_write_bin";
    size_t total_size=0, rna_memsize, i;
    int err = m_bin_write(&total_size,
			  &rna_coordset->n_chains,
			  sizeof(rna_coordset->n_chains),
			  1, outfile, this_sub);
    if(!err) {
	for(i = 0; !err && i < rna_coordset->n_chains; ++i) {
	    rna_memsize = rna_coord_write_bin(outfile, rna_coordset->chains[i]);
	    err = !rna_memsize;
	}
    }
    if(err) {
	err_printf(this_sub, "Failed to write rna_coord_set in binary format\n");
	return 0;
    } 
    return total_size;
}

int
rna_coord_set_write_binary(FILE*outfile,
			   const struct rna_coord_set *rna_coordset)
{
    return !rna_coord_set_write_bin(outfile, rna_coordset);
}

BINARY_IO_BODY(rna_coord_set)

struct rna_coord_set*
rna_coord_set_read_binary(FILE*infile)
{
    int err=0;
    size_t n_chains, i;
    struct rna_coord_set*rna_coordset=NULL;
    err = (fread(&n_chains, sizeof(n_chains), 1, infile) != 1);
    if(!err) {
	rna_coordset = new_rna_coord_set(n_chains);
	err = !rna_coordset;
    }
    for(i = 0; !err && i < n_chains; ++i) {
	struct rna_coord*rna = rna_coord_read_binary(infile);
	if(!err) {
	    rna_coordset->chains[rna_coordset->n_chains++] = rna;
	}
    }
    if(err) {
	return NULL;
    }
    return rna_coordset;
}

void
rna_coord_set_add_chain(struct rna_coord_set*coord_set,
			const struct rna_coord*rna)
{
    coord_set->chains[coord_set->n_chains++] = rna_coord_copy(rna);
}

static void
extract_chains_innerloop(struct rna_coord_set *rna_coordset,
			 const struct rna_coord *rna,
			 const enum rna_atom_types *atom_types,
			 size_t n_atomtypes)
{
    size_t chainlen=0, start_i=0, i;
    for(i = 0; i < rna->size; ++i) {
	enum yes_no all_atoms_found = has_all_the_atoms(rna->bb + i,
							atom_types,
							n_atomtypes);
	if(all_atoms_found) {
	    if(chainlen == 0) {
		start_i = i;
	    }
	    chainlen++;
	}
	if((rna->chain_breaks[i] || !all_atoms_found) && chainlen) {
	    rna_coordset->chains[rna_coordset->n_chains++] = rna_coord_copy_piece(rna,
										  start_i,
										  chainlen);
	    chainlen = 0;
	}
    }
}

struct rna_coord_set*
extract_chains(const struct rna_coord *rna,
	       const enum rna_atom_types *atom_types,
	       size_t n_atomtypes)
{
    struct rna_coord_set *ret = NULL;
    size_t n_chains;
    n_chains = count_continuous_chains(rna, atom_types, n_atomtypes);
    ret = new_rna_coord_set(n_chains);
    extract_chains_innerloop(ret, rna, atom_types, n_atomtypes);
    return ret;
}

static size_t
simple_ceil(size_t x, size_t y)
{
    return (x + y - 1) / y;
}

static size_t
count_chainpieces(const struct rna_coord_set*coord_set_in,
		  size_t minlen,
		  size_t maxlen)
{
    size_t i;
    size_t n_chainpieces=0;
    for(i = 0; i < coord_set_in->n_chains; ++i) {
	size_t size = coord_set_in->chains[i]->size;
	if(size >= minlen) {
	    n_chainpieces += simple_ceil(size, maxlen);
	}
    }
    return n_chainpieces;
}

static size_t
cut_chainpieces(struct rna_coord_set*coord_set_out,
		struct chainpiece_list*chainlist_out,
		const struct rna_coord_set*coord_set_in,
		const struct chainpiece_list*chainlist_in,
		size_t minlen,
		size_t maxlen)
{
    size_t count=0, i, j, npieces, chainlen, size, start_i;
    for(i = 0; i < coord_set_in->n_chains; ++i) {
	size = coord_set_in->chains[i]->size;
	if(size < minlen) {
	    continue;
	}
	npieces = simple_ceil(size, maxlen);
	chainlen = simple_ceil(size, npieces);
	start_i = 0;
	for(j = 0; j < npieces; j++) {

	    coord_set_out->chains[count] = rna_coord_copy_piece(
		coord_set_in->chains[i],
		start_i,
		chainlen > size - start_i ? size - start_i : chainlen);

	    chainpiece_description_cpy(chainlist_out->chainpieces + count,
				       chainlist_in->chainpieces + i);

	    chainlist_out->chainpieces[count].fragment_i = j + 1;

	    start_i += chainlen;
	    count++;
	}
    }
    coord_set_out->n_chains=count;
    return count;
}

int
continuous_chains_to_pieces(struct rna_coord_set**coord_set_out,
			    struct chainpiece_list**chainlist_out,
			    const struct rna_coord_set*coord_set_in,
			    const struct chainpiece_list*chainlist_in,
			    size_t minlen,
			    size_t maxlen)
{
    const char*this_sub="continuous_chains_to_pieces";
    size_t n_chainpieces;

    minlen = minlen ? minlen : 1;
    n_chainpieces = count_chainpieces(coord_set_in, minlen, maxlen);
    if(!n_chainpieces) {
	return 0;
    }

    *coord_set_out = new_rna_coord_set(n_chainpieces);
    *chainlist_out = new_chainpiece_list(n_chainpieces);

    if(cut_chainpieces(*coord_set_out, *chainlist_out,
		       coord_set_in, chainlist_in,
		       minlen, maxlen) != n_chainpieces) {
	err_printf(this_sub,
		   "Failed to extract chainpieces with length between %zu and %zu\n",
		   minlen,
		   maxlen);
	return 1;
    }
    return 0;
}

struct rna_coord_set*
rna_coord_set_get_subset(const struct rna_coord_set*chainset_in,
			 int nchains, int*indices)
{
    struct rna_coord_set*chainset_out=NULL;
    int i;
    chainset_out = new_rna_coord_set(nchains);
    for(i = 0; i < nchains; ++i) {
	chainset_out->chains[chainset_out->n_chains++] = rna_coord_copy(
	    chainset_in->chains[indices[i]]);
    }
    return chainset_out;
}

struct rna_coord_set*
rna_coord_set_copy(const struct rna_coord_set*coordset_in)
{
    struct rna_coord_set*coordset_out=NULL;
    size_t i;
    coordset_out = new_rna_coord_set(coordset_in->n_chains);
    for(i = 0; i < coordset_in->n_chains; ++i) {
	coordset_out->chains[i] = rna_coord_copy(coordset_in->chains[i]);
    }
    coordset_out->n_chains = coordset_in->n_chains;
    return coordset_out;
}

struct rna_coord_set*
load_rna_coord_set(const char*dir,
		   const struct chainpiece_list*chainpiece_list)
{
    struct rna_coord_set *ret=NULL;
    size_t i;
    int err=0;
    ret = new_rna_coord_set(chainpiece_list->size);
    err = !ret;
    for(i = 0; !err && i < chainpiece_list->size; ++i) {
	ret->chains[i] = read_binary_chainpiece(dir, chainpiece_list->chainpieces + i);
	err = !ret->chains[i];
	ret->n_chains++;
    }
    if(err) {
	rna_coord_set_destroy(ret);
	return NULL;
    }
    return ret;
}

const struct rna_coord*
rna_coord_set_get_rna(const struct rna_coord_set*coord_set, size_t i)
{
    return coord_set->chains[i];
}

struct rna_coord*
rna_coord_set_get_rna_copy(const struct rna_coord_set*coord_set, size_t i)
{
    return rna_coord_copy(coord_set->chains[i]);
}

void
rna_coord_set_dump(const struct rna_coord_set *rna_coordset)
{
    size_t i;
    for(i = 0; i < rna_coordset->n_chains; ++i) {
	mprintf("\nCHAIN %zu\n\n", i + 1);
	rna_coord_dump(rna_coordset->chains[i]);
    }
}

struct rna_coord*
read_binary_chainpiece(const char*dir,
		       const struct chainpiece_description*chainpiece)
{
    char *fn = fn_path_chainpiece(dir, chainpiece);
    struct rna_coord *rna = rna_coord_read_binary_file(fn); 
    free_if_not_null(fn);
    return rna;
}

int
write_binary_chainpiece(const char*dir,
			const struct chainpiece_description*chainpiece,
			const struct rna_coord*rna)
{
    char *fn = fn_path_chainpiece(dir, chainpiece);
    int err = rna_coord_write_binary_file(fn, rna);
    free_if_not_null(fn);
    return err;
}

/* COMPARE STUFF */

static enum yes_no
rna_atom_same(const struct rna_atom*atom_a,
	      const struct rna_atom*atom_b)
{
    return((atom_a->is_set == atom_b->is_set) &&
	   (!atom_a->is_set || (atom_a->r.x == atom_b->r.x &&
				atom_a->r.y == atom_b->r.y &&
				atom_a->r.z == atom_b->r.z)));
}

static enum yes_no
rna_backbone_nuc_same(const struct rna_backbone_nuc*nt_a,
		      const struct rna_backbone_nuc*nt_b)
{
    enum rna_atom_types atype;
    for(atype=P; atype <= C1; atype++) {
	if(!rna_atom_same(get_const_atom_ptr(atype, nt_a),
			  get_const_atom_ptr(atype, nt_b))) {
	    return NO;
	}
    }
    return YES;
}

static enum yes_no
rna_res_id_same(const struct rna_res_id*resid_a, const struct rna_res_id*resid_b)
{
    return((resid_a->resnum == resid_b->resnum) && (resid_a->icode == resid_b->icode));
}

EQUAL_ITERATE(static, enum, yes_no)
EQUAL_ITERATE(static, , int)
SAME_ITERATE(static, struct, rna_backbone_nuc)
SAME_ITERATE(static, struct, rna_res_id)

enum yes_no
rna_coord_same(const struct rna_coord*rna_a,
	       const struct rna_coord*rna_b)
{
    if(rna_a->size != rna_b->size) {
	return NO;
    }
    return(rna_backbone_nuc_same_iterate(rna_a->bb, rna_b->bb, rna_a->size) &&
    	   rna_res_id_same_iterate(rna_a->res, rna_b->res, rna_a->size) &&
    	   int_same_iterate(rna_a->cif_seq_num, rna_b->cif_seq_num, rna_a->size) &&
	   yes_no_same_iterate(rna_a->chain_breaks, rna_b->chain_breaks, rna_a->size) &&
    	   int_same_iterate(rna_a->basepair_info, rna_b->basepair_info, rna_a->size) &&
    	   seq_same(rna_a->seq, rna_b->seq) &&
	   char_matrix_same(rna_a->sequence, rna_b->sequence)
    	);
}

struct float_matrix*
calc_rna_coord_vs_alphabet_rmsds_chainbreaks(const struct rna_coord*rna,
					     const struct size_t_array*positions,
					     const struct rna_coord_set*alphabet,
					     const struct size_t_array*fragment_pattern,
					     const struct rna_atom_types_array*atom_types)
{
    const char *this_sub="calc_rna_coord_vs_alphabet_rmsds";
    struct float_matrix*rmsds=NULL;
    size_t i, j;
    int err=0;
    if(!positions->size) {
	err_printf(this_sub, "No positions to compute rmsds for\n");
	return NULL;
    }
    rmsds = new_float_matrix(positions->size, alphabet->n_chains, -1.0);
    for(i = 0; i < positions->size; ++i) {
	for(j = 0; !err && j < alphabet->n_chains; ++j) {
	    err = rmsd_fixed_chainlen(&rmsds->vals[i][j],
				      rna,
				      alphabet->chains[j],
				      positions->vals[i],
				      0,
				      fragment_pattern,
				      atom_types,
				      NO);
	}
    }
    if(err) {
	float_matrix_destroy(rmsds);
	return NULL;
    }
    return rmsds;
}

struct float_matrix*
calc_rna_coord_vs_alphabet_rmsds(const struct rna_coord*rna,
				 const struct rna_coord_set*alphabet,
				 const struct size_t_array*fragment_pattern,
				 const struct rna_atom_types_array*atom_types)
{
    const char *this_sub="calc_rna_coord_vs_alphabet_rmsds";
    struct float_matrix*rmsds=NULL;
    size_t fraglen = size_t_array_last(fragment_pattern) + 1;
    size_t i, j;
    int err=0;
    if(rna->size < fraglen) {
	err_printf(this_sub,
		   "length of rna (%zu) smaller than fraglen(%zu)\n",
		   rna->size, fraglen);
	return NULL;
    }
    rmsds = new_float_matrix(rna->size + 1 - fraglen, alphabet->n_chains, -1.0);
    for(i = 0; i + fraglen <= rna->size; ++i) {
	for(j = 0; !err && j < alphabet->n_chains; ++j) {
	    err = rmsd_fixed_chainlen(&rmsds->vals[i][j],
				      rna,
				      alphabet->chains[j],
				      i,
				      0,
				      fragment_pattern,
				      atom_types,
				      NO);
	}
    }
    if(err) {
	float_matrix_destroy(rmsds);
	return NULL;
    }
    return rmsds;
}

struct float_matrix**
calc_rna_coord_set_vs_alphabet_rmsds(const struct rna_coord_set*rna_set,
				     const struct rna_coord_set*alphabet,
				     const struct size_t_array *fragment_pattern,
				     const struct rna_atom_types_array*atom_types)
{
    const char*this_sub="calc_rna_coord_set_vs_alphabet_rmsds";
    size_t i;
    struct float_matrix**rmsd_matrix_list=NULL;
    struct int_array*err_list=NULL;
    err_list = new_int_array(rna_set->n_chains);
    rmsd_matrix_list = E_MALLOC(rna_set->n_chains * sizeof(*rmsd_matrix_list));
    #pragma omp parallel for
    for(i = 0; i < rna_set->n_chains; ++i) {
	rmsd_matrix_list[i] = calc_rna_coord_vs_alphabet_rmsds(rna_set->chains[i],
							       alphabet,
							       fragment_pattern,
							       atom_types);
	err_list->vals[i] = !rmsd_matrix_list[i];
    }
    if(int_array_sum(err_list)) {
	if(rmsd_matrix_list) {
	    for(i = 0; i < rna_set->n_chains; ++i) {
		float_matrix_destroy(rmsd_matrix_list[i]);
	    }
	    free(rmsd_matrix_list);
	}
	err_printf(this_sub, "failed to compute the rmsd matrix\n");
	rmsd_matrix_list = NULL;
    }
    int_array_destroy(err_list);
    return rmsd_matrix_list;
}


int
write_coordset_2pdb(const char *fname,
		    struct rna_coord_set *rna_coord_set)
{
    const char *this_sub = "write_coordset_2pdb";
    size_t i;
    FILE *f=NULL;
    f = mfopen(fname, "w", this_sub);
    if(!f) {
	return 1;
    }
    for(i = 0; i < rna_coord_set->n_chains; ++i) {
	rna_coord_2_pdb_file(f, rna_coord_set->chains[i]);
    }
    fclose(f);
    return 0;
}

struct float_matrix*
rna_coordset_get_rmsds_merged(const struct rna_coord_set*chains,
			      const struct size_t_array*fragment_pattern,
			      const struct rna_atom_types_array*atom_types)
{
    struct float_matrix**rmsds=NULL;
    struct float_matrix*rmsds_merged=NULL;
    size_t i;

    rmsds = calc_rna_coord_set_vs_alphabet_rmsds(chains,
						 chains,
						 fragment_pattern,
						 atom_types);

    rmsds_merged = float_matrix_merge(chains->n_chains, rmsds);

    if(rmsds) {
	for(i = 0; i < chains->n_chains; ++i) {
	    float_matrix_destroy(rmsds[i]);
	}
	free(rmsds);
    }
    return rmsds_merged;
}

struct rna_coord_set*
rna_coord_set_extract_subset(const struct rna_coord_set*chainset_in,
			     const struct size_t_array*indices)
{
    struct rna_coord_set*chainset_out=NULL;
    size_t i;
    chainset_out = new_rna_coord_set(indices->size);
    for(i = 0; i < indices->size; ++i) {
	chainset_out->chains[chainset_out->n_chains++] = rna_coord_copy(
	    chainset_in->chains[indices->vals[i]]);
    }
    return chainset_out;    
}


struct rna_coord_set*
rna_coord_set_alphabet_selection(const struct rna_coord_set*chains,
				 const struct alphabet_index*alpha_index)
{
    struct rna_coord_set*chainset_out=NULL;
    struct size_t_array*choice=NULL;
    choice = alphabet_index_extract_choice(alpha_index);
    if(choice) {
	chainset_out = rna_coord_set_extract_subset(chains, choice);
    }
    size_t_array_destroy(choice);
    return chainset_out;
}

void
rna_coord_reset_resnum(struct rna_coord*rna)
{
    size_t i;
    for(i = 0; i < rna->size; ++i) {
	rna->res[i].resnum = (int) i + 1;
	rna->res[i].icode = ' ';
    }
}

void
rna_coord_set_reset_resnum(struct rna_coord_set*rna_coord_set)
{
    size_t i;
    for(i = 0; i < rna_coord_set->n_chains; ++i) {
	rna_coord_reset_resnum(rna_coord_set->chains[i]);
    }
}

int
rna_coord_set_write_separate_chains(const struct rna_coord_set*coord_set,
				    const struct chainpiece_list*list,
				    const char*dir_out)
{
    const char*this_sub="rna_coord_set_write_separate_chains";
    int err=0;
    size_t i;
    if(coord_set->n_chains != list->size) {
	err_printf(this_sub,
		   "Number of chains (%zu) and size of the list (%zu) don't match\n",
		   coord_set->n_chains, list->size);
	return 1;
    }
    for(i = 0; !err && i < coord_set->n_chains; ++i) {
	err = write_binary_chainpiece(dir_out,
				      list->chainpieces + i,
				      coord_set->chains[i]);
    }
    return err;
}
