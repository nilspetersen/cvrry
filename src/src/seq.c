#include <stdlib.h>
#include "seq.h"
#include "e_malloc.h"
#include "same.h"

void
seq_destroy (struct seq *s)
{
    if(s) {
	free_if_not_null(s->seq);
	free_if_not_null(s->comment);
	free(s);
    }
}

EQUAL_ITERATE(static, , char)

enum yes_no
seq_same(const struct seq *s1, const struct seq *s2)
{
    if(s1->length != s2->length) {
	return NO;
    }
    return(char_same_iterate(s1->seq, s2->seq, s1->length));
}
