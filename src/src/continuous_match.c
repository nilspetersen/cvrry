#include "continuous_match.h"

void
dump_match(struct continuous_match *match,
	   const struct int_array *seq_a,
	   const struct int_array *seq_b)
{
    size_t i;
    for(i = 0; i < match->len; ++i) {
	mprintf("%i %i\n",
		seq_a->vals[match->matchpos_a + i],
		seq_b->vals[match->matchpos_b + i]);
    }
}

void
continuous_match_destroy(struct continuous_match*match)
{
    free_if_not_null(match);
}

struct continuous_match*
new_continuous_match_initialized(size_t len, size_t matchpos_a, size_t matchpos_b)
{
    struct continuous_match*match=E_MALLOC(sizeof(*match));
    match->len = len;
    match->matchpos_a = matchpos_a;
    match->matchpos_b = matchpos_b;
    return match;
}

struct continuous_match*
new_continuous_match()
{
    return new_continuous_match_initialized(0, 0, 0);
}

void
continuous_match_cpy(struct continuous_match *dest,
		     const struct continuous_match *src)
{
    dest->len = src->len;
    dest->matchpos_a = src->matchpos_a;
    dest->matchpos_b = src->matchpos_b;
}
