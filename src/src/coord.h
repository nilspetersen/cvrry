/* 
 * computations with cartesian coordinates
 */
#ifndef COORD_H
#define COORD_H

struct RPoint {
    float x;
    float y;
    float z;
};

float
rdist(const struct RPoint *r_i,
      const struct RPoint *r_j);

float
chiral_volume(const struct RPoint *r_i,
	      const struct RPoint *r_j,
	      const struct RPoint *r_k,
	      const struct RPoint *r_l);

void
rsubtract(struct RPoint *u,
	  const struct RPoint *v);

void
radd(struct RPoint *u,
     const struct RPoint *v);

#endif  /* COORD_H */
