#include "rna_frag_coord.h"
#include "e_malloc.h"
#include "mprintf.h"
#include "bin_utils.h"
#include "fio.h"

void
rna_frag_coord_destroy(struct rna_frag_coord *frag_coord)
{
    if(frag_coord) {
	cvrry_cif_destroy(frag_coord->cif);
	rna_coord_destroy(frag_coord->rna_backbone);
	rna_frags_destroy(frag_coord->frags);
	free(frag_coord);
    }
}

static struct rna_frag_coord*
new_rna_frag_coord()
{
    struct rna_frag_coord*chain=E_MALLOC(sizeof(*chain));
    chain->cif = NULL;
    chain->rna_backbone = NULL;
    chain->frags = NULL;
    return chain;
}

struct rna_frag_coord*
read_n_compute_rna_frag_coord(const char *mmcif_filename,
			      const char *chain_id,
			      size_t model_i)
{
    const char *this_sub = "read_n_compute_rna_frag_coord";
    struct rna_frag_coord *frag_coord=new_rna_frag_coord();

    if(read_mmcif(&frag_coord->cif,
		  &frag_coord->rna_backbone,
		  mmcif_filename,
		  chain_id,
		  model_i)) {
	goto err_exit;
    }

    if(!frag_coord->rna_backbone->size) {
	err_printf(this_sub,
		   "Did not find any backbone nucleotides for chain %s model %zu in file %s\n",
		   chain_id,
		   model_i,
		   mmcif_filename);
	goto err_exit;
    }

    compute_chain_breaks(frag_coord->rna_backbone);

    frag_coord->frags = get_fragments(frag_coord->rna_backbone);
    if(!frag_coord->frags) {
	goto err_exit;	
    }
    
    return frag_coord;

err_exit:
    rna_frag_coord_destroy(frag_coord);
    return NULL;
}

void
rna_frag_coord_dump(const struct rna_frag_coord*frag_coord)
{
    mprintf("#### CIF\n");
    cvrry_cif_dump(frag_coord->cif);
    mprintf("\n#### BACKBONE\n");
    rna_coord_dump(frag_coord->rna_backbone);
    mprintf("\n#### CHAIN BREAKS\n");
    rna_coord_chain_breaks_dump(frag_coord->rna_backbone);
    mprintf("\n#### FRAGMENTS\n");
    rna_frags_dump(frag_coord->frags);
}

size_t
rna_frag_coord_get_seq_len(const struct rna_frag_coord*frag_coord)
{
    return frag_coord->rna_backbone->size;
}

int
rna_frag_coord_write_binary(const struct rna_frag_coord*frag_coord,
			    const char*fname_out)
{
    const char*this_sub="rna_frag_coord_write_binary";
    FILE *outfile;
    size_t memsize_cif, memsize_coord, memsize_frags;
    size_t dummy=0, zero=0, i;
    
    outfile = mfopen(fname_out, "wb", this_sub);
    if(!outfile) {
	goto err_exit;
    }

    /* the memory size of each part is stored in the beginning, 
       but we don't know it yet, so write 0 now and replace later */
    for(i = 0; i < 3; ++i) {
	if(m_bin_write(&dummy, &zero, sizeof(zero), 1, outfile, this_sub)){
	    goto err_exit;
	}
    }

    if(frag_coord->cif) {
	memsize_cif = cvrry_cif_write_bin(outfile, frag_coord->cif);
	if(!memsize_cif) {
	    goto err_exit;
	}
    } else {
	memsize_cif = 0;
    }
    
    memsize_coord = rna_coord_write_bin(outfile, frag_coord->rna_backbone);
    if(!memsize_coord) {
	goto err_exit;
    }
    memsize_frags = rna_frags_write_bin(outfile, frag_coord->frags);
    if(!memsize_frags) {
	goto err_exit;
    }

    fseek(outfile, 0, SEEK_SET);

    if(m_bin_write(&dummy, &memsize_cif, sizeof(memsize_cif), 1, outfile, this_sub)){
	goto err_exit;
    }
    if(m_bin_write(&dummy, &memsize_coord, sizeof(memsize_coord), 1, outfile, this_sub)){
	goto err_exit;
    }
    if(m_bin_write(&dummy, &memsize_frags, sizeof(memsize_frags), 1, outfile, this_sub)){
	goto err_exit;
    }
    fclose(outfile);
    return 0;

err_exit:
    err_printf(this_sub, "could not write binary rna_frag_coord structure\n");
    if(outfile) {
	fclose(outfile);
    }
    return 1;
}

struct rna_frag_coord*
rna_frag_coord_read_binary(const char*fname_in, enum yes_no verbose_err)
{
    const char *this_sub = "rna_frag_coord_read_binary";
    struct rna_frag_coord *frag_coord=NULL;
    FILE *infile;
    size_t memsize_cif, memsize_coord, memsize_frags;

    infile = mfopen(fname_in, "rb", this_sub);
    if(!infile) {
	goto err_exit;
    }

    if(fread(&memsize_cif, sizeof(memsize_cif), 1, infile) != 1) {
	goto err_exit;
    }
    if(fread(&memsize_coord, sizeof(memsize_coord), 1, infile) != 1) {
	goto err_exit;
    }
    if(fread(&memsize_frags, sizeof(memsize_frags), 1, infile) != 1) {
	goto err_exit;
    }

    frag_coord = new_rna_frag_coord();

    if(memsize_cif) {
	frag_coord->cif = cvrry_cif_read_binary(infile);
	if(!frag_coord->cif) {
	    goto err_exit;
	}
    }
    if(memsize_coord) {
	frag_coord->rna_backbone = rna_coord_read_binary(infile);
	if(!frag_coord->rna_backbone) {
	    goto err_exit;
	}
    }
    if(memsize_frags) {
	frag_coord->frags = rna_frags_read_binary(infile);
	if(!frag_coord->frags) {
	    goto err_exit;
	}
    }
    fclose(infile);
    return frag_coord;

err_exit:
    if(verbose_err) {
	err_printf(this_sub,
		   "could not read binary rna_frag_coord structure in file %s\n",
		   fname_in);
    }
    if(infile) {
	fclose(infile);
    }
    rna_frag_coord_destroy(frag_coord);
    return NULL;
}

void
print_cvrry_chain_information(struct rna_frag_coord *rna)
{
    mprintf("%11zu %9zu %11zu\n",
	    rna->rna_backbone->size,
	    rna->frags->n_frags,
	    count_chain_breaks(rna->rna_backbone));
}

struct rna_frag_coord*
rna_frag_coord_from_rna_coord(const struct rna_coord*rna)
{
    const char *this_sub = "rna_frag_coord_from_rna_coord";
    struct rna_frag_coord*frag_coord=new_rna_frag_coord();
    frag_coord->rna_backbone = rna_coord_copy(rna);
    if(frag_coord->rna_backbone) {
	frag_coord->frags = get_fragments(frag_coord->rna_backbone);
    }
    if(frag_coord->frags) {
	return frag_coord;
    }
    err_printf(this_sub, "Could not compute fragments\n");
    rna_frag_coord_destroy(frag_coord);
    return NULL;
}
