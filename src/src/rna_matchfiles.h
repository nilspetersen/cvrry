#ifndef RNA_MATCHFILES_H
#define RNA_MATCHFILES_H

#include "continuous_match.h"
#include "rna_coord.h"
#include "rna_alphabet.h"

int
write_match_chimera_script(const char*fn_chim_script,
			   const char*fn_pdb_1,
			   const char*fn_pdb_2,
			   const struct rna_coord*rna_1,
			   const struct rna_coord*rna_2,
			   const struct continuous_match *match);

int
write_match_sequences_file(const char*fname,
			   const struct continuous_match*match,
			   const struct int_array*str_1,
			   const struct int_array*str_2);

int
write_rna_coord_seq_match_sequences_file(const char*fname,
					 const struct continuous_match*match,
					 const struct rna_coord_seq*seq_1,
					 const struct rna_coord_seq*seq_2);

int
write_match_pdb_files(const char*fname_1,
		      const char*fname_2,
		      const struct continuous_match*match,
		      const struct rna_coord*rna_1,
		      const struct rna_coord*rna_2,
		      size_t fraglen);

#endif
