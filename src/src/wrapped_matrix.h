#ifndef WRAPPED_MATRIX_H
#define WRAPPED_MATRIX_H

#include <stdio.h>
#include <stdlib.h>
#include "yesno.h"

#define WRAPPED_MATRIX_CLASS_HEAD(PREFIX, DTYPE)			\
									\
    struct DTYPE ## _matrix {						\
	size_t n_rows, n_cols;						\
	PREFIX DTYPE **vals;						\
    };									\
									\
    struct DTYPE ## _matrix*						\
    new_ ## DTYPE ## _matrix(size_t n_rows,				\
			     size_t n_cols,				\
			     PREFIX DTYPE init_val);			\
									\
    struct DTYPE ## _matrix*						\
    DTYPE ## _matrix_from_array(size_t n_rows,				\
				size_t n_cols,				\
				const PREFIX DTYPE*arr);		\
    									\
    void								\
    DTYPE ## _matrix_destroy(struct DTYPE ## _matrix *mat);		\
									\
    PREFIX DTYPE							\
    DTYPE ## _matrix_get(const struct DTYPE ## _matrix *mat,		\
			 size_t i,					\
			 size_t j);					\
									\
    const PREFIX DTYPE*							\
    DTYPE ## _matrix_access_line(const struct DTYPE ## _matrix *mat,	\
				 size_t i);				\
    									\
    void								\
    DTYPE ## _matrix_set(struct DTYPE ## _matrix *mat,			\
			 size_t i,					\
			 size_t j,					\
			 PREFIX DTYPE val);				\
									\
    int									\
    DTYPE ## _matrix_copy_content(struct DTYPE ## _matrix *dest,	\
				  const struct DTYPE ## _matrix *src);	\
									\
    struct DTYPE ## _matrix*						\
    DTYPE ## _matrix_copy(const struct DTYPE ## _matrix *mat);		\
									\
    int									\
    DTYPE ## _matrix_copy_lines(struct DTYPE ## _matrix *dest,		\
				const struct DTYPE ## _matrix *src,	\
				size_t from, size_t n_lines);		\
									\
    size_t								\
    DTYPE ## _matrix_write_bin(FILE*outfile,				\
			       const struct DTYPE ## _matrix *src);	\
									\
    int									\
    DTYPE ## _matrix_write_binary_file(const char*fname,		\
				       const struct DTYPE ## _matrix*mat); \
									\
    struct DTYPE ## _matrix*						\
    DTYPE ## _matrix_read_binary(FILE*infile);				\
									\
    struct DTYPE ## _matrix*						\
    DTYPE ## _matrix_read_binary_file(const char*fname);		\
									\
    enum yes_no								\
    DTYPE ## _matrix_same(const struct DTYPE ## _matrix *mat1,		\
			  const struct DTYPE ## _matrix *mat2);		\
									\
    void								\
    DTYPE ## _matrix_dump(const char *fmt,				\
			  const struct DTYPE ## _matrix *mat);		\
									\
    struct DTYPE ## _matrix*						\
    DTYPE ## _matrix_merge(size_t n_matrices,				\
			   struct DTYPE ## _matrix*const*matrices);	\
									\
    struct DTYPE ## _matrix*						\
    DTYPE ## _matrix_identity_matrix(size_t size);


WRAPPED_MATRIX_CLASS_HEAD(, float)
WRAPPED_MATRIX_CLASS_HEAD(, char)

#endif
