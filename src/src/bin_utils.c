
#include "bin_utils.h"
#include "mprintf.h"

int
m_bin_write(size_t*size_count,
	    const void *ptr,
	    size_t size,
	    size_t nmemb,
	    FILE *stream,
	    const char *calling_sub)
{
    size_t written = fwrite(ptr, size, nmemb, stream);
    if(size_count) {
	*size_count += size * nmemb;
    }
    if(written != nmemb) {
	err_printf(calling_sub, "Error writing binary data\n");
	return 1;
    }
    return 0;
}

int
assert_src_version(int src_version,
		   FILE *infile)
{
    const char*this_sub="assert_src_version";
    int version_from_file;
    if(fread(&version_from_file, sizeof(version_from_file), 1, infile) != 1) {
	err_printf(this_sub, "Failed to read src-version from binary file\n");
	return 1;
    }
    if(version_from_file != src_version) {
	err_printf(this_sub, "File has version %i, source is version %i\n",
		   version_from_file, src_version);
	return 2;
    }
    return 0;
}

int
write_src_version(int src_version,
		  FILE*outfile)
{
    const char*this_sub="write_src_version";
    return m_bin_write(NULL, &src_version,
		       sizeof(src_version),
		       1, outfile, this_sub);
}
