#ifndef ALPHABET_INDEX_H
#define ALPHABET_INDEX_H

#include <stdlib.h>
#include "wrapped_matrix.h"
#include "array.h"
#include "double_index.h"

struct alphabet_index {
    size_t border_i;
    struct double_index*index;
};

struct alphabet_index*
new_alphabet_index(size_t size);

void
alphabet_index_destroy(struct alphabet_index*alpha_index);

size_t
alphabet_index_get_select(const struct alphabet_index*alpha_index, size_t i);

void
alphabet_index_move(struct alphabet_index*alpha_index, size_t i);

void
alphabet_index_swap_from_select(struct alphabet_index*alpha_index, size_t ii, size_t jj);

void
alphabet_index_cpy(struct alphabet_index*dest, const struct alphabet_index*src);

size_t
alphabet_index_get_size(const struct alphabet_index*alpha_index);

void
alphabet_index_dump(const struct alphabet_index*alpha_index);

struct size_t_array*
alphabet_index_extract_choice(const struct alphabet_index*alpha_index);

void
alphabet_index_choose_all(struct alphabet_index*alpha_index);

int
alphabet_index_write_binary(FILE*f_out,
			    const struct alphabet_index*alpha_index);

struct alphabet_index*
alphabet_index_read_binary(FILE*f_in);

/* MOVE STRATEGIES */

struct move_strategy;

void 
move_strategy_destroy(struct move_strategy*strategy);

struct move_strategy*
move_strategy_copy(const struct move_strategy*strategy);

void
alpha_index_init_from_strategy(struct alphabet_index*alpha_index,
			       const struct move_strategy*strategy);

void
alpha_index_move_with_strategy(struct alphabet_index*alpha_index,
			       const struct move_strategy*strategy);

/* SPECIFIC STRATEGIES */
struct move_strategy*
make_strategy_simple_add_remove();

struct move_strategy*
make_strategy_add_remove_swap_eq_prob();

struct move_strategy*
make_strategy_swap_only(size_t alphabet_size);

#endif
