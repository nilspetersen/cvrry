#ifndef RNA_GET_ATOMS_H
#define RNA_GET_ATOMS_H

#include <stdlib.h>
#include "rna_coord.h"
#include "coord.h"
#include "pair_set.h"
#include "suffix_arr.h"

int
get_simple_backbone(struct RPoint *bb,
		    const struct rna_coord *rna,
		    const enum rna_atom_types atom_type);

size_t
get_backbone_atoms(const struct pair_set *pairset,
		   const struct rna_coord *rna1,
		   const struct rna_coord *rna2, 
		   struct RPoint *list_1,
		   struct RPoint *list_2,
		   size_t *n_atoms_per_nt,
		   const enum rna_atom_types *atom_types,
		   size_t n_atom_types);

size_t
get_backbone_atoms_one_type(const struct pair_set *pairset,
			    const struct rna_coord *rna1,
			    const struct rna_coord *rna2, 
			    struct RPoint *list_1,
			    struct RPoint *list_2,
			    const enum rna_atom_types atom_type);

size_t
get_backbone_atoms_continuous_match(struct RPoint *list_1,
				    struct RPoint *list_2,
				    const struct continuous_match *match,
				    const struct rna_coord *rna1,
				    const struct rna_coord *rna2,
				    const enum rna_atom_types *atom_types,
				    size_t n_atom_types);

#endif
