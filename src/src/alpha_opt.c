#include <math.h>
#include "alpha_opt.h"
#include "alpha_opt_energy.h"
#include "e_malloc.h"
#include "alphabet_index.h"
#include "rna_coord_stringset.h"
#include "array.h"
#include "lsqf.h"
#include "draw_random.h"
#include "cvrry_utils.h"
#include "fio.h"
#include "rna_matchfiles.h"

/* INITIALIZATION */

static struct individual*
create_individual(const struct population*pop)
{
    struct individual*ind=NULL;

    ind = E_MALLOC(sizeof(*ind));

    ind->size = pop->pairs->size;
    ind->alpha_index = new_alphabet_index(pop->alphabet_rmsds[0]->n_cols);
    alpha_index_init_from_strategy(ind->alpha_index, pop->moveset);

    /* allocate local private stuff */
    ind->energies = new_float_array(ind->size);
    ind->match_rmsds = new_float_array(ind->size);
    ind->matches = E_MALLOC(ind->size * sizeof(*ind->matches));
    ind->strings = make_rna_coord_stringset(pop->coord_set, pop->fraglen);

    /* set reference to shared data */
    ind->pop = pop;

    return ind;
}

static void
individual_destroy(struct individual*ind)
{
    if(ind) {
	alphabet_index_destroy(ind->alpha_index);
	float_array_destroy(ind->energies);
	float_array_destroy(ind->match_rmsds);
	free_if_not_null(ind->matches);
	rna_coord_stringset_destroy(ind->strings);
	free(ind);
    }
}

static struct population*
create_population(const struct match_energy_func*energy_func,
		  const struct move_strategy*moveset,
		  const struct rna_coord_set*coord_set,
		  const struct rna_coord_set*alphabet_candidates,
		  const struct double_index*pairs,
		  size_t n_parents,
		  size_t offspring_rate,
		  const struct size_t_array *fragment_pattern,
		  const struct rna_atom_types_array*atom_types)
{
    struct population*pop=NULL;
    size_t i;
    
    pop = E_MALLOC(sizeof(*pop));
    pop->n_parents = n_parents;
    pop->offspring_rate = offspring_rate;
    pop->n_total = n_parents * offspring_rate;
    pop->temperature = 100;	/* arbitrary value */
    pop->fraglen = size_t_array_last(fragment_pattern) + 1;
    
    /* set up shared stuff */
    pop->energy_func = match_energy_func_copy(energy_func);
    pop->moveset = move_strategy_copy(moveset);
    pop->n_chains = coord_set->n_chains;
    pop->coord_set = rna_coord_set_copy(coord_set);
    pop->pairs = double_index_copy(pairs);

    pop->alphabet_rmsds = calc_rna_coord_set_vs_alphabet_rmsds(coord_set,
							       alphabet_candidates,
							       fragment_pattern,
							       atom_types);
    
    pop->survivors = E_MALLOC(pop->n_parents * sizeof(*pop->survivors));
    for(i = 0; i < pop->n_parents; ++i) {
	pop->survivors[i] = create_individual(pop);
    }
    
    /* create the individuals */
    pop->individuals = E_MALLOC(pop->n_total * sizeof(*pop->individuals));
    for(i = 0; i < pop->n_total; ++i) {
	pop->individuals[i] = create_individual(pop);
    }
    pop->energies = new_float_array(pop->n_total);
    pop->boltzman_weights = new_float_array(pop->n_total);
    pop->probabilities = new_float_array(pop->n_total);
    pop->cumulative_probabilities = new_float_array(pop->n_total);
    pop->err_energies = new_int_array(pop->n_total);

    pop->energy_up_to_date = NO;

    return pop;
}

struct population*
population_from_indexed_chainset(const struct indexed_chainset*indexed_chainset,
				 const struct match_energy_func*energy_func,
				 const struct move_strategy*moveset,
				 const struct rna_coord_set*alphabet_candidates,
				 const struct double_index*pairs,
				 size_t n_parents,
				 size_t offspring_rate)
{
    return create_population(energy_func,
			     moveset,
			     indexed_chainset->rna_chains,
			     alphabet_candidates,
			     pairs,
			     n_parents,
			     offspring_rate,
			     indexed_chainset->fragment_pattern,
			     indexed_chainset->atom_types);
}

void
population_destroy(struct population*pop)
{
    size_t i;
    if(pop) {
	if(pop->individuals) {
	    for(i = 0; i < pop->n_total; ++i) {
		individual_destroy(pop->individuals[i]);	
	    }
	    free(pop->individuals);
	}
	
	double_index_destroy(pop->pairs);

	rna_coord_set_destroy(pop->coord_set);

	float_array_destroy(pop->energies);
	float_array_destroy(pop->boltzman_weights);
	float_array_destroy(pop->probabilities);
	float_array_destroy(pop->cumulative_probabilities);
	int_array_destroy(pop->err_energies);

	if(pop->survivors) {
	    for(i = 0; i < pop->n_parents; ++i) {
		individual_destroy(pop->survivors[i]);	
	    }
	    free(pop->survivors);
	}
	
	for(i = 0; i < pop->n_chains; ++i) {
	    float_matrix_destroy(pop->alphabet_rmsds[i]);
	}
	free_if_not_null(pop->alphabet_rmsds);

	match_energy_func_destroy(pop->energy_func);
	move_strategy_destroy(pop->moveset);

	free_if_not_null(pop);
    }
}

/* INDIVIDUALS */

static int
individual_compute_energies(struct individual*ind)
{
    int err = 0;
    size_t i;
    for(i = 0; i < ind->size; ++i) {
	err = match_energy_get_energy(ind->energies->vals + i,
				      ind->pop->energy_func,
				      ind->matches + i,
				      ind->match_rmsds->vals[i]);
    }
    return err;
}

static int
individual_compute_energy(float *energy, struct individual*ind)
{
    const char*this_sub="individual_compute_energy";
    int err=0;
    if(!err) {
	rna_coord_stringset_set(ind->strings,
				(const struct float_matrix * const*) ind->pop->alphabet_rmsds,
				ind->alpha_index);
    }
    if(!err) {
	err = compute_matches(ind->matches,
			      ind->pop->pairs,
			      ind->strings);
    }
    if(!err) {
	err = compute_rmsds_matches(ind->match_rmsds->vals,
				    ind->pop->pairs,
				    ind->matches,
				    ind->pop->coord_set,
				    ind->pop->fraglen);
    }
    if(!err) {
	err = individual_compute_energies(ind);
    }
    if(!err) {
	*energy = float_array_avg(ind->energies);
    }
    if(err) {
	err_printf(this_sub, "Failed to compute energies\n");
    }
    return err;
}

static void
individual_transfer_state(struct individual*dest, const struct individual*src)
{
    alphabet_index_cpy(dest->alpha_index, src->alpha_index);
}

static size_t
individual_get_alphasize(const struct individual*ind)
{
    return ind->alpha_index->border_i;
}

static float
individual_get_avg_rmsd(const struct individual*ind)
{
    return float_array_avg(ind->match_rmsds);
}

static float
individual_get_avg_matchlen(const struct individual*ind)
{
    size_t i;
    float avg=0;
    for(i = 0; i < ind->size; ++i) {
	avg += (float) ind->matches[i].len;
    }
    return avg / ind->size;
}

/* OPTIMIZATION PROCEDURE */

static void
individual_mutate(struct individual*ind)
{
    alpha_index_move_with_strategy(ind->alpha_index, ind->pop->moveset);
}

static void
population_create_offspring(struct population*pop)
{
    size_t i, j;
    for(i = 0; i < pop->n_parents; ++i) {
	for(j = 1; j < pop->offspring_rate; ++j) {
	    size_t k = j * pop->n_parents + i;
	    individual_transfer_state(pop->individuals[k], pop->individuals[i]);
	    individual_mutate(pop->individuals[k]);    
	}
    }
}

static void
population_compute_energies_inner(struct population*pop)
{
    size_t i;
    #pragma omp parallel for
    for(i = 0; i < pop->n_total; ++i) {
	pop->err_energies->vals[i] = individual_compute_energy(
	    pop->energies->vals + i, pop->individuals[i]);
    }
}

static int
merge_err(struct population*pop)
{
    size_t i;
    int err=0;
    for(i = 0; i < pop->n_total; ++i) {
	err = err || pop->err_energies->vals[i];
    }
    return err;
}

static int
population_compute_energies(struct population*pop)
{
    population_compute_energies_inner(pop);
    return merge_err(pop);
}

static void
population_compute_boltzman_weights(struct population*pop)
{
    size_t i;
    for(i = 0; i < pop->n_total; ++i) {
	pop->boltzman_weights->vals[i] = exp(-pop->energies->vals[i] / pop->temperature);
    }
}

static void
population_compute_probabilities(struct population*pop)
{
    size_t i;
    float sum_weights = float_array_sum(pop->boltzman_weights);
    for(i = 0; i < pop->n_total; ++i) {
	pop->probabilities->vals[i] = pop->boltzman_weights->vals[i] / sum_weights;
    }
    /* cumulative probabilities */
    float_array_cpy(pop->cumulative_probabilities, pop->probabilities);
    for(i = 1; i < pop->n_total; ++i) {
	pop->cumulative_probabilities->vals[i] += pop->cumulative_probabilities->vals[i-1];
    }
}

static int
population_update_probabilities(struct population*pop)
{
    int err=0;
    if(!err) {
	err = population_compute_energies(pop);
    }
    if(!err) {
	population_compute_boltzman_weights(pop);
    }
    if(!err) {
	population_compute_probabilities(pop);
    }
    if(!err) {    
	pop->energy_up_to_date = YES;
    }
    return err;
}

int
population_assert_up2date_probabilities(struct population*pop)
{
    int err=0;
    if(!pop->energy_up_to_date) {
	err = population_update_probabilities(pop);
    }
    if(!err) {
	pop->energy_up_to_date = YES;
    }
    return err;
}


static void
population_sample_survivors(struct population*pop)
{
    float p;
    size_t i, j;    
    for(i = 0; i < pop->n_parents; ++i) {
	p = draw_random_float();
	j = bin_search_cumulative_probability(p, pop->cumulative_probabilities);
	individual_transfer_state(pop->survivors[i], pop->individuals[j]);
    }
    for(i = 0; i < pop->n_parents; ++i) {
	individual_transfer_state(pop->individuals[i], pop->survivors[i]);
    }
}

void
population_set_temperature(struct population*pop, float temperature)
{
    pop->temperature = temperature;
}

int
population_step(struct population*pop)
{
    int err=0;
    if(!err) {
	population_create_offspring(pop);
    }
    if(!err) {
	err = population_update_probabilities(pop);
    }
    if(!err) {
	population_sample_survivors(pop);
    }
    pop->energy_up_to_date = NO;
    return err;
}

int
population_assert_energies(struct population*pop)
{
    int err=0;
    if(!pop->energy_up_to_date) {
	err = population_update_probabilities(pop);
    }
    if(!err) {
	pop->energy_up_to_date = YES;
    }
    return err;
}

float
population_get_entropy(struct population*pop)
{
    float z=0.0;
    float entropy=0.0;
    int err = 0;
    size_t i;
    err = population_assert_energies(pop);
    if(err) {
	return -1;
    }
    for(i = 0; i < pop->n_parents; ++i) {
	z += pop->boltzman_weights->vals[i];
    }
    for(i = 0; i < pop->n_parents; ++i) {
	float p = pop->boltzman_weights->vals[i] / z;
	if(p > 0.0) {
	    entropy -= p * log(p);
	}
    }
    return entropy;
}

float
population_get_avg_energy(struct population*pop)
{
    size_t i;
    float avg=0;
    population_assert_energies(pop);
    for(i = 0; i < pop->n_parents; ++i) {
	avg += pop->energies->vals[i];
    }
    return avg / pop->n_parents;
}

float
population_get_avg_matchlen(struct population*pop)
{
    size_t i;
    float avg=0;
    population_assert_energies(pop);
    for(i = 0; i < pop->n_parents; ++i) {
	avg += individual_get_avg_matchlen(pop->individuals[i]);
    }
    return avg / pop->n_parents;
}

float
population_get_avg_alphabet_size(struct population*pop)
{
    size_t i;
    float avg=0;
    population_assert_energies(pop);
    for(i = 0; i < pop->n_parents; ++i) {
	avg += (float) individual_get_alphasize(pop->individuals[i]);
    }
    return avg / pop->n_parents;
}

float
population_get_avg_rmsds(struct population*pop)
{
    size_t i;
    float avg=0;
    population_assert_energies(pop);
    for(i = 0; i < pop->n_parents; ++i) {
	avg += individual_get_avg_rmsd(pop->individuals[i]);
    }
    return avg / pop->n_parents;
}

void
population_dump_probs(struct population*pop)
{
    size_t i;
    population_assert_energies(pop);
    mprintf("Energy Weight Probability\n");
    for(i = 0; i < pop->n_total; ++i) {
	mprintf("%.3e %.3e %.3e\n",
		pop->energies->vals[i],
		pop->boltzman_weights->vals[i],
		pop->probabilities->vals[i]);
    }
    fflush(stdout);
}

const struct individual*
population_get_individual(const struct population*pop, size_t i)
{
    return pop->individuals[i];
}

size_t
population_get_alphabet_size(const struct population*pop)
{
    if(!pop->n_parents) {
	return 0;
    }
    return pop->individuals[0]->alpha_index->index->size;
}

void
population_transfer_state(struct population*dest, const struct population*src)
{
    size_t i;
    for(i = 0; i < src->n_parents; ++i) {
	individual_transfer_state(dest->individuals[i], src->individuals[i]);
    }
    dest->energy_up_to_date = NO;
}

size_t
population_best_individual_index(const struct population*pop)
{
    size_t i, best_i=0;
    for(i = 1; i < pop->n_parents; ++i) {
	best_i = pop->energies->vals[i] < pop->energies->vals[best_i] ? i : best_i;
    }
    return best_i;
}

static int
individual_get_rna_coord_pair(const struct individual*ind,
			      const struct rna_coord **rna_1,
			      const struct rna_coord **rna_2,
			      size_t match_i)
{
    *rna_1 = ind->pop->coord_set->chains[ind->pop->pairs->index_a->vals[match_i]];
    *rna_2 = ind->pop->coord_set->chains[ind->pop->pairs->index_b->vals[match_i]];
    return !(*rna_1 && *rna_2);
}

static int
individual_write_match_pdbs(const struct individual*ind,
			    const char*fname_1,
			    const char*fname_2,
			    size_t match_i)
{
    const struct rna_coord *rna_1=NULL, *rna_2=NULL;
    int err=0;
    err = individual_get_rna_coord_pair(ind, &rna_1, &rna_2, match_i);
    if(!err) {
	err = write_match_pdb_files(fname_1, fname_2,
				    ind->matches + match_i,
				    rna_1, rna_2,
				    ind->pop->fraglen);
    }
    return err;
}

static char*
make_fn_match_related_stuff(const char*outdir, size_t match_i, const char*fn_suffix)
{
    char fn[128];
    char*fn_path=NULL;
    sprintf(fn, "match%zu%s", match_i, fn_suffix);
    fn_path = merge_paths(outdir, fn);
    return fn_path;    
}

static char*
make_fn_match_superposed_pdb(const char*outdir, size_t match_i, size_t chain_i)
{
    char fn_suffix[64];
    sprintf(fn_suffix, "_chain%zu.pdb", chain_i);
    return make_fn_match_related_stuff(outdir, match_i, fn_suffix);
}

static char*
make_fn_match_chimera_script(const char*outdir, size_t match_i)
{
    return make_fn_match_related_stuff(outdir, match_i, ".cmd");
}

static char*
make_fn_match_sequences(const char*outdir, size_t match_i)
{
    return make_fn_match_related_stuff(outdir, match_i, ".seq");    
}

static int
individual_write_sequences_match(const struct individual*ind,
				 const char*fn_sequence_file,
				 size_t match_i)
{
    int err=0;
    const struct int_array *str_1=NULL, *str_2=NULL;
    str_1 = ind->strings->strings[ind->pop->pairs->index_a->vals[match_i]];
    str_2 = ind->strings->strings[ind->pop->pairs->index_b->vals[match_i]];   
    err = write_match_sequences_file(fn_sequence_file,
				     ind->matches + match_i,
				     str_1, str_2);
    return err;
}

static int
individual_write_match_chimera_script(const char*fn_chim_script,
				      const struct individual*ind,
				      const char*fn_pdb_1,
				      const char*fn_pdb_2,
				      size_t match_i)
{
    const struct rna_coord*rna_1=NULL, *rna_2=NULL;
    int err=0;
    struct continuous_match match;
    err = individual_get_rna_coord_pair(ind, &rna_1, &rna_2, match_i);
    if(!err) {
	continuous_match_cpy(&match, ind->matches + match_i);
	if(match.len) {
	    match.len += ind->pop->fraglen - 1;
	}
	err = write_match_chimera_script(fn_chim_script,
					 fn_pdb_1,
					 fn_pdb_2,
					 rna_1,
					 rna_2,
					 &match);
    }
    return err;
}

static const struct individual*
population_get_best_individual(const struct population*pop)
{
    return pop->individuals[population_best_individual_index(pop)];
}

int
population_best_individual_write_match_pdbs(const struct population*pop,
					    size_t match_i,
					    const char*outdir)
{
    int err=0;
    char*fn_1=NULL, *fn_2=NULL, *fn_chimera_script=NULL, *fn_sequence_file=NULL;
    const struct individual*ind=NULL;

    fn_1 = make_fn_match_superposed_pdb(outdir, match_i, 1);
    fn_2 = make_fn_match_superposed_pdb(outdir, match_i, 2);
    fn_chimera_script = make_fn_match_chimera_script(outdir, match_i);
    fn_sequence_file = make_fn_match_sequences(outdir, match_i);

    ind = population_get_best_individual(pop);
    err = individual_write_match_pdbs(ind, fn_1, fn_2, match_i);
    if(!err) {
	err = individual_write_match_chimera_script(fn_chimera_script, ind, fn_1, fn_2, match_i);
    }
    if(!err) {
	err = individual_write_sequences_match(ind, fn_sequence_file, match_i);
    }
    
    free_if_not_null(fn_1);
    free_if_not_null(fn_2);
    free_if_not_null(fn_chimera_script);
    free_if_not_null(fn_sequence_file);
    
    return err;
}

struct float_matrix*
population_rmsds_to_alphabet(const struct population*pop)
{
    return float_matrix_merge(pop->n_chains, pop->alphabet_rmsds);
}

struct int_array*
population_best_individual_strings_merged(const struct population*pop)
{
    return rna_coord_stringset_strings_merged(population_get_best_individual(pop)->strings);
}

struct size_t_array*
populations_best_individual_alphabet_choice(const struct population*pop)
{
    const struct individual*ind=NULL;
    struct size_t_array*choice=NULL;
    size_t i;
    ind = population_get_best_individual(pop);
    choice = new_size_t_array(ind->alpha_index->border_i);
    for(i = 0; i < ind->alpha_index->border_i; ++i) {
	choice->vals[i] = alphabet_index_get_select(ind->alpha_index, i);
    }
    return choice;
}

const struct alphabet_index*
populations_best_individual_alphabet_index(const struct population*pop)
{
    return population_get_best_individual(pop)->alpha_index;
}

static void
count_letters_int_array(struct float_array*counts,
			const struct int_array*arr)
{
    size_t i;
    for(i = 0; i < arr->size; ++i) {
	counts->vals[arr->vals[i]] += 1.0;
    }
}

static void
count_letters_rna_coord_stringset(struct float_array*counts,
				  const struct rna_coord_stringset*stringset)
{
    size_t i;
    for(i = 0; i < stringset->size; ++i) {
	count_letters_int_array(counts, stringset->strings[i]);
    }
}

struct float_array*
individual_get_letter_frequencies(const struct individual*ind)
{
    struct float_array*freqs=NULL;
    size_t alphabet_size=individual_get_alphasize(ind);
    if(!alphabet_size) {
	return NULL;
    }
    freqs = new_float_array_initialized(alphabet_size, 0.0);
    count_letters_rna_coord_stringset(freqs, ind->strings);
    float_array_normalize(freqs);
    return freqs;
}

struct float_array*
population_best_individual_letter_frequencies(const struct population*pop)
{
    return individual_get_letter_frequencies(population_get_best_individual(pop));
}
