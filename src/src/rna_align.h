#ifndef RNA_ALIGN
#define RNA_ALIGN

#include "rna_frag_coord.h"
#include "pair_set.h"
#include "align.h"
#include "rna_seq_coord.h"

struct rna_align_params {
    float gap_open;
    float gap_widen;
    enum align_type algn_type;
    float scale_factor;
    float base_pair_bonus;
};

enum score_type {
    UNIT_SCORE,
    SIGNIFICANCE_WEIGHTED
};

struct pair_set*
align(const struct rna_frag_coord *rna_chain_a,
      const struct rna_frag_coord *rna_chain_b,
      const struct rna_align_params *params);

struct pair_set*
align_alfons(const struct rna_seq_coord *seq_coord_a,
	     const struct rna_seq_coord *seq_coord_b,
	     const struct rna_align_params *params,
	     enum score_type scr_type);

float
rna_alignment_seq_id(const struct pair_set *pair_set,
		     const struct rna_coord*rna_1,
		     const struct rna_coord*rna_2);
#endif
