#ifndef MATRIX_H
#define MATRIX_H
#define want_print_i_matrix 1

char**
c_matrix(size_t n_rows,
	 size_t n_cols);

void
kill_c_matrix(char **matrix);

float**
f_matrix(size_t n_rows,
	 size_t n_cols);

int**
i_matrix(size_t n_rows,
	 size_t n_cols);

unsigned char**
uc_matrix(size_t n_rows,
	  size_t n_cols);

void
kill_f_matrix(float**matrix);

void
kill_i_matrix(int**matrix);

void
kill_uc_matrix(unsigned char **matrix);

float**
copy_f_matrix(float **matrix,
	      size_t n_rows,
	      size_t n_cols);

int**
copy_i_matrix(int **matrix,
	      size_t n_rows,
	      size_t n_cols);

int**
crop_i_matrix(int **pairs,
	      size_t n_rows,
	      size_t n_cols);

/* #ifdef  want_print_f_matrix  */
void
dump_f_matrix(float *const*mat,
	      size_t n_rows,
	      size_t n_cols);
/* #endif */ /* want_print_f_matrix */

#ifdef  want_print_i_matrix
void
dump_i_matrix(const int **mat,
	      size_t n_rows,
	      size_t n_cols);
#endif /* want_print_i_matrix */

#ifdef want_print_uc_matrix
void
dump_uc_matrix(const unsigned char **mat,
	       size_t n_rows,
	       size_t n_cols);
#endif /* want_print_uc_matrix */

void*
d3_array(size_t n1,
	 size_t n2,
	 size_t n3,
	 size_t size);

void
kill_3d_array(void ***p);

#endif /* MATRIX_H */
