#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cvrry_parse_mmcif.h"
#include "mprintf.h"
#include "fio.h"
#include "coord.h"
#include "rna_coord.h"
#include "e_malloc.h"
#include "yesno.h"
#include "lsqf.h"
#include "matrix.h"
#include "bin_utils.h"

#define LINE_BUF 256
#define TOKEN_CHAINID_I 18  /* 6 new cif style, 18 old school pdb */
#define MMCIF_ATOMLINE_COLS 21
#define TOKEN_AUTH_SEQ_ID 16 	/* PDB style seq-ID */
#define TOKEN_LABEL_SEQ_ID 8	/* position in sequence as in mmcif file */

static struct cvrry_cif*
new_cvrry_cif(size_t n_lines, size_t n_char, size_t seqlen) /* n_char = number of characters in main memory */
{
    struct cvrry_cif *cif;
    cif = E_MALLOC(sizeof(*cif));
    cif->n_lines = n_lines;
    cif->mem = E_MALLOC(n_char * sizeof(*cif->mem));
    cif->lines = E_MALLOC(n_lines * sizeof(*cif->lines));
    cif->seqlen = seqlen;
    cif->sequence = c_matrix(seqlen, 4);
    cif->chain_numbers = E_MALLOC(seqlen * sizeof(*cif->chain_numbers));
    cif->res_numbers = E_MALLOC(seqlen * sizeof(*cif->res_numbers));
    return cif;
}

static void
write_mmcif_header(FILE *f_out, const char *pdbid)
{
    mfprintf(f_out, "data_%s\n", pdbid);
}

static void
write_mmcif_sequence_header(FILE *f_out)
{
    mfprintf(f_out, "loop_\n_entity_poly_seq.entity_id\n_entity_poly_seq.num\n");
    mfprintf(f_out, "_entity_poly_seq.mon_id\n_entity_poly_seq.hetero\n");
}

static void
write_mmcif_coordinates_header(FILE *f_out)
{
    mfprintf(f_out, "loop_\n");
    mfprintf(f_out, "_atom_site.group_PDB\n"); 
    mfprintf(f_out, "_atom_site.id\n");
    mfprintf(f_out, "_atom_site.type_symbol\n"); 
    mfprintf(f_out, "_atom_site.label_atom_id\n"); 
    mfprintf(f_out, "_atom_site.label_alt_id\n"); 
    mfprintf(f_out, "_atom_site.label_comp_id\n"); 
    mfprintf(f_out, "_atom_site.label_asym_id\n"); 
    mfprintf(f_out, "_atom_site.label_entity_id\n"); 
    mfprintf(f_out, "_atom_site.label_seq_id\n"); 
    mfprintf(f_out, "_atom_site.pdbx_PDB_ins_code\n"); 
    mfprintf(f_out, "_atom_site.Cartn_x\n"); 
    mfprintf(f_out, "_atom_site.Cartn_y\n"); 
    mfprintf(f_out, "_atom_site.Cartn_z\n"); 
    mfprintf(f_out, "_atom_site.occupancy\n"); 
    mfprintf(f_out, "_atom_site.B_iso_or_equiv\n"); 
    mfprintf(f_out, "_atom_site.pdbx_formal_charge\n"); 
    mfprintf(f_out, "_atom_site.auth_seq_id\n"); 
    mfprintf(f_out, "_atom_site.auth_comp_id\n"); 
    mfprintf(f_out, "_atom_site.auth_asym_id\n"); 
    mfprintf(f_out, "_atom_site.auth_atom_id\n"); 
    mfprintf(f_out, "_atom_site.pdbx_PDB_model_num\n"); 
}

static void
write_mmcif_sequence(FILE *f_out, const struct cvrry_cif *cif)
{
    size_t i;
    for(i = 0; i < cif->seqlen; i++) {
	mfprintf(f_out, "%i %i %s n\n", cif->chain_numbers[i], cif->res_numbers[i], cif->sequence[i]);
    }
}

static void
write_mmcif_coordinates(FILE *f_out, const struct cvrry_cif *cif)
{
    size_t i;
    for(i = 0; i < cif->n_lines; i++) {
	mfprintf(f_out,
		 "%s %.3f %.3f %.3f %s\n",
		 cif->lines[i].prefix,
		 cif->lines[i].r.x,
		 cif->lines[i].r.y,
		 cif->lines[i].r.z,
		 cif->lines[i].suffix);
    }
}

void
write_2mmcif(FILE *f_out, const struct cvrry_cif *cif, const char *pdbid)
{
    write_mmcif_header(f_out, pdbid);
    write_mmcif_sequence_header(f_out);
    write_mmcif_sequence(f_out, cif);
    write_mmcif_coordinates_header(f_out);
    write_mmcif_coordinates(f_out, cif);
}

int
write_mmcif(const struct cvrry_cif *cif, const char *pdbid, const char *filename)
{
    const char *this_sub = "write_mmcif";
    FILE *f_out = mfopen(filename, "w", this_sub);
    if(!f_out) {
	return 1;
    }
    write_2mmcif(f_out, cif, pdbid);
    fclose(f_out);
    return 0;
}

int
write_double_mmcif(const struct cvrry_cif *cif_a,
		   const char *pdbid_a,
		   const struct cvrry_cif *cif_b,
		   const char *pdbid_b,
		   const char *filename)
{
    const char *this_sub = "write_double_mmcif";
    FILE *f_out = mfopen(filename, "w", this_sub);
    if(!f_out) {
	return 1;
    }
    write_2mmcif(f_out, cif_a, pdbid_a);
    write_2mmcif(f_out, cif_b, pdbid_b);
    fclose(f_out);    
    return 0;
}

static size_t
cif_get_memsize(const struct cvrry_cif*cif)
{
    char *suffix = cif->lines[cif->n_lines-1].suffix;
    return (suffix + strlen(suffix) + 1) - cif->lines[0].prefix;
}

size_t 				/* error if it returns 0 */
cvrry_cif_write_bin(FILE *outfile, const struct cvrry_cif*cif)
{
    const char *this_sub = "cvrry_cif_write_bin";
    size_t total_memsize = 0;
    size_t n_char_cifmem = cif_get_memsize(cif);
    size_t i;

    if(!cif) {
	return 0;
    }

    /* save seqlen and n_lines and 'total memsize of cif->mem' */
    if(m_bin_write(&total_memsize, &cif->seqlen, sizeof(cif->seqlen), 1, outfile, this_sub)) {
	goto err_exit;
    }
    if(m_bin_write(&total_memsize, &cif->n_lines, sizeof(cif->n_lines), 1, outfile, this_sub)) {
	goto err_exit;
    }
    if(m_bin_write(&total_memsize, &n_char_cifmem, sizeof(n_char_cifmem), 1, outfile, this_sub)) {
	goto err_exit;
    }
    
    /* save mem, chain_numbers, res_numbers */
    if(m_bin_write(&total_memsize, cif->mem, sizeof(*cif->mem), n_char_cifmem, outfile, this_sub)) {
	goto err_exit;
    }
    if(m_bin_write(&total_memsize, cif->chain_numbers, sizeof(*cif->chain_numbers),
		   cif->seqlen, outfile, this_sub)) {
	goto err_exit;
    }
    if(m_bin_write(&total_memsize, cif->res_numbers, sizeof(*cif->res_numbers),
		   cif->seqlen, outfile, this_sub)) {
	goto err_exit;
    }
    
    /* save sequence */
    if(m_bin_write(&total_memsize, *cif->sequence, sizeof(**cif->sequence),
		   cif->seqlen * 4, outfile, this_sub)) {
	goto err_exit;
    }
    
    /* save the lines - prefix and suffixes as int or size_t, r points */
    for(i = 0; i < cif->n_lines; ++i) {
	/* offset prefix */
	const struct cvrry_cif_line *l = cif->lines + i;
	size_t offset = l->prefix - cif->mem;
	if(m_bin_write(&total_memsize, &offset, sizeof(offset), 1, outfile, this_sub)) {
	    goto err_exit;
	}
	/* offset suffix */
	offset = l->suffix - cif->mem;
	if(m_bin_write(&total_memsize, &offset, sizeof(offset), 1, outfile, this_sub)) {
	    goto err_exit;
	}
	/* r */
	if(m_bin_write(&total_memsize, &l->r, sizeof(l->r), 1, outfile, this_sub)) {
	    goto err_exit;
	}
    }
    return total_memsize;
err_exit:
    return 0;
}

int
cvrry_cif_write_binary(FILE *outfile, const struct cvrry_cif*cif)
{
    return !cvrry_cif_write_bin(outfile, cif);
}

struct cvrry_cif*
cvrry_cif_read_binary(FILE *infile)
{
    const char *this_sub = "cvrry_cif_read_binary";
    struct cvrry_cif *cif=NULL;
    size_t seqlen, n_lines, n_char_cifmem, i;

    /* read seqlen n_lines, n_char_cifmem */
    if(fread(&seqlen, sizeof(seqlen), 1, infile) != 1) {
	goto err_exit;
    }
    if(fread(&n_lines, sizeof(n_lines), 1, infile) != 1) {
	goto err_exit;
    }
    if(fread(&n_char_cifmem, sizeof(n_char_cifmem), 1, infile) != 1) {
	goto err_exit;
    }

    cif = new_cvrry_cif(n_lines, n_char_cifmem, seqlen);
    
    /* read cif->mem, chain_numbers, res_numbers */
    if(fread(cif->mem, sizeof(*cif->mem), n_char_cifmem, infile) != n_char_cifmem) {
	goto err_exit;
    }
    if(fread(cif->chain_numbers, sizeof(*cif->chain_numbers), seqlen, infile) != seqlen) {
	goto err_exit;
    }
    if(fread(cif->res_numbers, sizeof(*cif->res_numbers), seqlen, infile) != seqlen) {
	goto err_exit;
    }
    /* read sequence */
    if(fread(*cif->sequence, sizeof(**cif->sequence), seqlen * 4, infile) != 4 * seqlen) {
	goto err_exit;
    }
    /* read lines */
    for(i = 0; i < n_lines; ++i) {
	size_t offset_prefix, offset_suffix;
	if(fread(&offset_prefix, sizeof(offset_prefix), 1, infile) != 1) {
	    goto err_exit;
	}
	cif->lines[i].prefix = cif->mem + offset_prefix;
	if(fread(&offset_suffix, sizeof(offset_suffix), 1, infile) != 1) {
	    goto err_exit;
	}
	cif->lines[i].suffix = cif->mem + offset_suffix;
	if(fread(&cif->lines[i].r, sizeof(cif->lines[i].r), 1, infile) != 1) {
	    goto err_exit;
	}
    }
    return cif;
err_exit:
    err_printf(this_sub, "failed to read binary cvrry cif\n");
    cvrry_cif_destroy(cif);
    return NULL;
}

BINARY_IO_BODY(cvrry_cif)

void
cvrry_cif_dump(const struct cvrry_cif *cif)
{
    size_t i;
    for(i = 0; i < cif->seqlen; i++) {
	mprintf("%i %i %s n\n", cif->chain_numbers[i], cif->res_numbers[i], cif->sequence[i]);
    }
    for(i = 0; i < cif->n_lines; i++) {
	mprintf("%s %.3f %.3f %.3f %s\n",
		cif->lines[i].prefix,
		cif->lines[i].r.x,
		cif->lines[i].r.y,
		cif->lines[i].r.z,
		cif->lines[i].suffix);
    }
}

void
cvrry_cif_destroy(struct cvrry_cif*cif)
{
    if(cif) {
	free_if_not_null(cif->mem);
	free_if_not_null(cif->lines);
	free_if_not_null(cif->chain_numbers);
	free_if_not_null(cif->res_numbers);
	kill_c_matrix(cif->sequence);
	free(cif);
    }
}

static void
replace_star(char*atom_name)
{
    if(atom_name[strlen(atom_name) - 1] == '*') {
	atom_name[strlen(atom_name) - 1] = '\'';
    }
}

static int
equal_res_id(const struct rna_res_id*a, const struct rna_res_id*b)
{
    return (a->resnum == b->resnum) && (a->icode == b->icode);
}

static char
get_one_letter_residue(const char *nucleobase_str)
{
    if(!(strcmp("U", nucleobase_str) && strcmp("DU", nucleobase_str))) {
	return 'u';
    } else if(!(strcmp("A", nucleobase_str) && strcmp("DA", nucleobase_str))) {
	return 'a';
    } else if(!(strcmp("G", nucleobase_str) && strcmp("DG", nucleobase_str))) {
	return 'g';
    } else if(!(strcmp("C", nucleobase_str) && strcmp("DC", nucleobase_str))) {
    	return 'c';
    }
    return 'n';
}

static int
is_atom_line(const char*line)
{
    return (!strncmp(line, "ATOM", 4) || !strncmp(line, "HETATM", 6));
}

struct word_index {
    int start;
    int len;
};

static int
parse_word_index(size_t*pos,
		 struct word_index*index,
		 const char*line)
{
    while(line[*pos] == ' ' || line[*pos] == '\"') {
	(*pos)++;
    }
    if(line[*pos] == '\0' || line[*pos] == '\n') {
	return 1;
    }
    index->start = *pos;
    index->len = 0;
    while(line[*pos] != ' ' && line[*pos] != '\0' && line[*pos] != '\n' && line[*pos] != '\"') {
	index->len++;
	(*pos)++;	
    }
    return 0;
}

struct indexed_atom_line{
    struct word_index indices[MMCIF_ATOMLINE_COLS];
    char line[LINE_BUF];
};

static void
set_wordends(struct indexed_atom_line *line_index)
{
    size_t i;
    for(i = 0; i < MMCIF_ATOMLINE_COLS; ++i) {
	line_index->line[line_index->indices[i].start + line_index->indices[i].len] = '\0';
    }
}

static int
index_atom_line(struct indexed_atom_line *line_index,
		const char*line)
{
    size_t i, pos=0;
    for(i = 0; i < MMCIF_ATOMLINE_COLS; ++i) {
	if(parse_word_index(&pos, line_index->indices + i, line)) {
	    return 1;
	}
    }
    strncpy(line_index->line, line, LINE_BUF);
    set_wordends(line_index);
    return 0;
}

static const char*
get_str(const struct indexed_atom_line *line_index, size_t i)
{
    return line_index->line + line_index->indices[i].start;
}

static int
has_cif_seq_num(const struct indexed_atom_line *line_index)
{
    return get_str(line_index, TOKEN_LABEL_SEQ_ID)[0] != '.';
}

static size_t
get_strlen(const struct indexed_atom_line *line_index, size_t i)
{
    return line_index->indices[i].len;
}

static int
match_str(const char *str,
	  const struct indexed_atom_line *line_index,
	  size_t i)
{
    size_t l = strlen(str);
    if(l == get_strlen(line_index, i)) {
	return !strncmp(str, get_str(line_index, i), l);
    }
    return 0;
}

static int
fits_chain_id(const struct indexed_atom_line *line_index,
	      const char*chain_id,
	      const char*model_number_str)
{
    return match_str(chain_id, line_index, TOKEN_CHAINID_I) && 
	match_str(model_number_str, line_index, 20);
}

static void
get_res_id(struct rna_res_id*res_id,
	   const struct indexed_atom_line *line_index)
{
    res_id->resnum = atoi(get_str(line_index, TOKEN_AUTH_SEQ_ID));
    res_id->icode = get_str(line_index, 9)[0];
}

static enum rna_atom_types
get_atom_type_from_indexed_line(const struct indexed_atom_line *line_index)
{
    char atom_name[6];
    strncpy(atom_name, get_str(line_index, 3), 5);
    replace_star(atom_name);
    return get_atom_type_from_str(atom_name);
}

static char*
fgets_prevpos(long*f_prevpos, char*line, int size, FILE*infile, enum yes_no save_pos)
{
    if(save_pos) {
	*f_prevpos = ftell(infile);
    }
    return fgets(line, size, infile);
}

static int
count_atoms_and_res_mmcif(long*f_firstpos, /* used to return to later */
			  long*f_endpos,
			  size_t*n_atoms,
			  size_t*n_res,
			  size_t*max_linelen,
			  size_t*seqlen,
			  FILE*infile,
			  const char*chain_id,
			  const char*model_number_str)
{
    /* const char *this_sub = "count_atoms_and_res_mmcif"; */
    char line[LINE_BUF];
    struct indexed_atom_line line_index;
    enum rna_atom_types atom_type;
    size_t linelen;
    struct rna_res_id res_id, res_id_old={-999, 'z'};
    enum yes_no save_pos=YES;
    enum yes_no c4_found=NO;

    *n_atoms = *n_res = *max_linelen = *seqlen = 0;
    
    while(fgets_prevpos(f_firstpos, line, LINE_BUF, infile, save_pos)  != NULL) {
	if(!is_atom_line(line)) {
	    continue;
	}
	if(index_atom_line(&line_index, line)) {
	    /* err_printf(this_sub, "WARING: 'ATOM' line '%s' not long enough!\n", line); */
	    continue;
	}
	linelen = strlen(line); /* TODO: move this  after checking if it's a useful line??? */
	*max_linelen = *max_linelen < linelen ? linelen : *max_linelen;
	if(!fits_chain_id(&line_index, chain_id, model_number_str)) {
	    continue;
	}
	if(!has_cif_seq_num(&line_index)) {
	    continue;
	}
	(*n_atoms)++;

	/* save endposition until where to read, stop updating the start position */
	save_pos = NO;
	*f_endpos = ftell(infile);

	atom_type = get_atom_type_from_indexed_line(&line_index);
	if(atom_type == NOT_A_BACKBONE_ATOM) {
	    continue;
	}
	if(atom_type == C4) {
	    if(!c4_found) {
		(*n_res)++;
		c4_found = YES;
	    }
	}
	get_res_id(&res_id, &line_index);
	if(!equal_res_id(&res_id, &res_id_old)) {
	    (*seqlen)++;
	    c4_found = NO;
	    res_id_old = res_id;
	}
    }
    return 0;
}

static void
parse_coords(struct RPoint*r,
	     const struct indexed_atom_line *line_index)
{
    r->x = (float) atof(get_str(line_index, 10));
    r->y = (float) atof(get_str(line_index, 11));
    r->z = (float) atof(get_str(line_index, 12));
}

static char*
next_str_pos(char*predecessor)
{
    return predecessor + strlen(predecessor) + 1;
}

static void
merge_str_tokens(char*buf,
		 const struct indexed_atom_line *line_index,
		 size_t from,
		 size_t to)
{
    size_t i;
    buf[0] = '\0';
    for(i = from; i < to; i++) {
	strcat(buf, get_str(line_index, i));
	strcat(buf, " ");
    }
    /* remove the last whitespace */
    buf[strlen(buf) - 1] = '\0';
}

static void
parse_atomline(struct cvrry_cif*cif,
	       const struct indexed_atom_line *line_index,
	       size_t i)
{
    struct cvrry_cif_line *atom_line = cif->lines + i;

    /* get the part before the coordinates */
    if(i > 0) {
	atom_line->prefix = next_str_pos(cif->lines[i-1].suffix);
    } else {
	atom_line->prefix = cif->mem;
    }
    merge_str_tokens(atom_line->prefix, line_index, 0, 10);

    /* get the part after the coordinates */
    atom_line->suffix = next_str_pos(atom_line->prefix);
    merge_str_tokens(atom_line->suffix, line_index, 13, 21);
}

static int
read_lines_mmcif(struct cvrry_cif*cif,
		 struct rna_coord*rna_backbone,
		 long f_firstpos, /* used to return to later */
		 long f_endpos,
		 FILE *infile,
		 const char*chain_id,
		 const char*model_number_str)
{
    const char *this_sub="read_lines_mmcif";
    struct indexed_atom_line line_index;
    char line[LINE_BUF];
    struct RPoint r;
    enum rna_atom_types atom_type;
    struct rna_res_id res_id, res_id_old={-999, 'z'};
    size_t atom_i=0, res_i=0, seq_i=0;
    struct rna_atom *atom_ptr=NULL;
    
    fseek(infile, f_firstpos, SEEK_SET);

    while(fgets(line, LINE_BUF, infile) != NULL) {
	if(ftell(infile) > f_endpos) {
	    break;
	}
	if(!is_atom_line(line)) {
	    continue;
	}
	if(index_atom_line(&line_index, line)) {
	    err_printf(this_sub, "'ATOM' line not long enough!\n");
	    return 2;
	}
	if(!fits_chain_id(&line_index, chain_id, model_number_str)) {
	    continue;
	}
	if(!has_cif_seq_num(&line_index)) {
	    continue;
	}
	parse_coords(&r, &line_index);
	parse_atomline(cif, &line_index, atom_i);
	cif->lines[atom_i].r = r;
	atom_i++;
	atom_type = get_atom_type_from_indexed_line(&line_index);
	if(atom_type == NOT_A_BACKBONE_ATOM) {
	    continue;
	}
	get_res_id(&res_id, &line_index);
	if(!equal_res_id(&res_id, &res_id_old)) {
	    strncpy(cif->sequence[seq_i], get_str(&line_index, 5), 3);
	    cif->chain_numbers[seq_i] = atoi(get_str(&line_index, 7));
	    cif->res_numbers[seq_i] = atoi(get_str(&line_index, TOKEN_LABEL_SEQ_ID));
	    seq_i++;
	    if(res_i < rna_backbone->size) {
		if(rna_backbone->bb[res_i].c4.is_set) {
		    res_i++;
		} else {
		    reset_rna_backbone_nuc(rna_backbone->bb + res_i);
		}
	    }
	    res_id_old = res_id;
	}
	if(res_i == rna_backbone->size) {
	    continue;
	}
	if(atom_type == C4) {
	    strncpy(rna_backbone->sequence->vals[res_i], get_str(&line_index, 5), 3);
	    rna_backbone->seq->seq[res_i] = get_one_letter_residue(get_str(&line_index, 5));
	    rna_backbone->res[res_i].resnum = res_id.resnum;
	    rna_backbone->res[res_i].icode = res_id.icode;
	    rna_backbone->cif_seq_num[res_i] = atoi(get_str(&line_index, TOKEN_LABEL_SEQ_ID));
	}
	/* set backbone coordinates */
	atom_ptr = get_atom_ptr(atom_type, rna_backbone->bb + res_i);
	if(!atom_ptr->is_set) {
	    atom_ptr->is_set = YES;
	    atom_ptr->r = r;
	}
    }
    return 0;
}

int
read_mmcif(struct cvrry_cif**cif,
	   struct rna_coord**rna_backbone,
	   const char*filename,
	   const char*chain_id,
	   size_t model_number)
{
    const char *this_sub = "read_mmcif";
    char model_number_str[5];
    size_t n_atoms;
    size_t n_res;
    size_t max_linelen;
    size_t seqlen;
    long f_firstpos, f_endpos;
    FILE*infile=NULL;
    int err=0;
    
    if(model_number > 9999) {
	err_printf(this_sub,
		   "model_number is %zu, but max allowed is 9999\n",
		   model_number);
	err = 1;
    }
    snprintf(model_number_str, 5, "%zu", model_number);

    /* open the file */
    if(!err) {
	infile = mfopen(filename, "r", this_sub);
	err = !infile;
    }

    if(!err) {
	/* get the numbers */
	err = count_atoms_and_res_mmcif(&f_firstpos,
					&f_endpos,
					&n_atoms,
					&n_res,
					&max_linelen,
					&seqlen,
					infile,
					chain_id,
					model_number_str);
    }

    if(!err) {
	/* allocate memory */
	cvrry_cif_destroy(*cif);
	*cif = new_cvrry_cif(n_atoms, n_atoms * max_linelen, seqlen);
	err = !*cif;
    }

    if(!err) {
	rna_coord_destroy(*rna_backbone);
	*rna_backbone = new_rna_coord(n_res);
	err = !*rna_backbone;
    }

    if(!err) {
	/* get the data */
	err = read_lines_mmcif(*cif,
			       *rna_backbone,
			       f_firstpos,
			       f_endpos,
			       infile,
			       chain_id,
			       model_number_str);
    }

    if(infile) {
	fclose(infile);
    }
    if(err) {
	err_printf(this_sub,
		   "failed to read mmcif file %s chain %s\n",
		   filename,
		   chain_id);
    }
    return err;
}

static void
rotate_mmcif(struct cvrry_cif*cif,
	     const struct rotation_matrix*rot_mat)
{
    size_t i;
    for(i = 0; i < cif->n_lines; i++) {
	rotate_rpoint(&cif->lines[i].r, rot_mat);
    }
}

static void
move_mmcif(struct cvrry_cif*cif,
	   const struct RPoint*r,
	   void (*rfunc) (struct RPoint*, const struct RPoint*))
{
    size_t i;
    for(i = 0; i < cif->n_lines; i++) {
	rfunc(&cif->lines[i].r, r);
    }
}

void
transform_mmcif(struct cvrry_cif*cif,
		const struct transformation *transform)
{
    move_mmcif(cif, &transform->com_a, &rsubtract);
    rotate_mmcif(cif, &transform->rotate_a);
    move_mmcif(cif, &transform->com_b, &radd);
}
