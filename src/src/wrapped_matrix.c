#include "wrapped_matrix.h"
#include "e_malloc.h"
#include "mprintf.h"
#include "bin_utils.h"
#include "string.h"
#include "fio.h"

#define ALLOC_FREE_MATRIX(PREFIX, DTYPE)				\
    PREFIX DTYPE **							\
    alloc_ ## DTYPE ## _matrix(size_t n_rows,				\
			       size_t n_cols)				\
    {									\
	PREFIX DTYPE **mat=NULL, *mem=NULL;				\
	size_t i;							\
	mat = E_MALLOC(n_rows * sizeof(*mat));				\
	mem = E_MALLOC(n_rows * n_cols * sizeof(*mem));			\
	for(i = 0; i < n_rows; ++i) {					\
	    mat[i] = mem + i * n_cols;					\
	}								\
	return mat;							\
    }									\
    void								\
    free_ ## DTYPE ## _matrix(PREFIX DTYPE **mat)			\
    {									\
	free_if_not_null(*mat);						\
	free_if_not_null(mat);						\
    }									\
    void								\
    set_ ## DTYPE ## _matrix(PREFIX DTYPE **mat,			\
			     size_t n_rows,				\
			     size_t n_cols,				\
			     PREFIX DTYPE val)				\
    {									\
	size_t i, j;							\
	for(i = 0; i < n_rows; ++i) {					\
	    for(j = 0; j < n_cols; ++j) {				\
		mat[i][j] = val;					\
	    }								\
	}								\
    }


#define WRAPPED_MATRIX_CLASS_BODY(PREFIX, DTYPE)			\
									\
    struct DTYPE ## _matrix*						\
    new_ ## DTYPE ## _matrix(size_t n_rows,				\
			     size_t n_cols,				\
			     PREFIX DTYPE init_val)			\
    {									\
	struct DTYPE ## _matrix *mat=NULL;				\
	mat = E_MALLOC(sizeof(*mat));					\
	mat->n_rows = n_rows;						\
	mat->n_cols = n_cols;						\
	mat->vals = alloc_ ## DTYPE ## _matrix(n_rows, n_cols);		\
	set_ ## DTYPE ## _matrix(mat->vals,				\
				 mat->n_rows,				\
				 mat->n_cols,				\
				 init_val);				\
	return mat;							\
    }									\
									\
    struct DTYPE ## _matrix*						\
    DTYPE ## _matrix_from_array(size_t n_rows,				\
	size_t n_cols,							\
	const PREFIX DTYPE*arr)						\
    {									\
	struct DTYPE ## _matrix*mat=NULL;				\
	mat = new_ ## DTYPE ## _matrix(n_rows, n_cols, 0.0);		\
	memcpy(*mat->vals, arr,						\
	       n_rows * n_cols * sizeof(*arr));				\
	return mat;							\
    }									\
									\
    void								\
    DTYPE ## _matrix_destroy(struct DTYPE ## _matrix *mat)		\
    {									\
	if(mat) {							\
	    free_ ## DTYPE ## _matrix(mat->vals);			\
	    free(mat);							\
	}								\
    }									\
									\
    PREFIX DTYPE							\
    DTYPE ## _matrix_get(const struct DTYPE ## _matrix *mat,		\
			 size_t i,					\
			 size_t j)					\
    {									\
	return mat->vals[i][j];						\
    }									\
									\
    void								\
    DTYPE ## _matrix_set(struct DTYPE ## _matrix *mat,			\
			 size_t i,					\
			 size_t j,					\
			 PREFIX DTYPE val)				\
    {									\
	mat->vals[i][j] = val;						\
    }									\
									\
    const PREFIX DTYPE*							\
    DTYPE ## _matrix_access_line(const struct DTYPE ## _matrix *mat,	\
				 size_t i)				\
    {									\
	return mat->vals[i];						\
    }									\
									\
    int									\
    DTYPE ## _matrix_copy_content(struct DTYPE ## _matrix *dest,	\
				  const struct DTYPE ## _matrix *src)	\
    {									\
	const char*this_sub="matrix_copy_content";			\
	if((dest->n_rows != src->n_rows) ||				\
	   (dest->n_cols != src->n_cols)) {				\
	    err_printf(this_sub,					\
		       ("dimensions (%zu, %zu) and"			\
			" (%zu, %zu) do not match\n"),			\
		       src->n_rows, src->n_cols,			\
		       dest->n_rows, dest->n_cols);			\
	    return 1;							\
	}								\
	memcpy(*dest->vals, *src->vals,					\
	       src->n_rows * src->n_cols * sizeof(**dest->vals));	\
	return 0;							\
    }									\
									\
    struct DTYPE ## _matrix*						\
    DTYPE ## _matrix_copy(const struct DTYPE ## _matrix *src)		\
    {									\
	struct DTYPE ## _matrix*dest=NULL;				\
	dest = new_ ## DTYPE ## _matrix(src->n_rows, src->n_cols, 0.0);	\
	DTYPE ## _matrix_copy_content(dest, src);			\
	return dest;							\
    }									\
									\
    int									\
    DTYPE ## _matrix_copy_lines(struct DTYPE ## _matrix *dest,		\
				const struct DTYPE ## _matrix *src,	\
				size_t from, size_t n_lines)		\
    {									\
	const char*this_sub="matrix_copy_lines";			\
	if((dest->n_rows != n_lines) ||					\
	   (dest->n_cols != src->n_cols)) {				\
	    err_printf(this_sub,					\
		       ("dimensions (%zu, %zu) and"			\
			" (%zu, %zu) do not match\n"),			\
		       n_lines, src->n_cols,				\
		       dest->n_rows, dest->n_cols);			\
	    return 1;							\
	}								\
	memcpy(*dest->vals, *src->vals + (from * src->n_cols),		\
	       n_lines * src->n_cols * sizeof(**dest->vals));		\
									\
	return 0;							\
    }									\
									\
    size_t								\
    DTYPE ## _matrix_write_bin(FILE*outfile,				\
			       const struct DTYPE ## _matrix *mat)	\
    {									\
	const char*this_sub="matrix_write_bin";				\
	int err=0;							\
	size_t total_mem=0;						\
	if(!err) {							\
	    err = m_bin_write(&total_mem,				\
			      &mat->n_rows,				\
			      sizeof(mat->n_rows),			\
			      1,					\
			      outfile,					\
			      this_sub);				\
	}								\
	if(!err) {							\
	    err = m_bin_write(&total_mem,				\
			      &mat->n_cols,				\
			      sizeof(mat->n_cols),			\
			      1,					\
			      outfile,					\
			      this_sub);				\
	}								\
	if(!err) {							\
	    err = m_bin_write(&total_mem,				\
			      *mat->vals,				\
			      sizeof(**mat->vals),			\
			      mat->n_rows * mat->n_cols,		\
			      outfile,					\
			      this_sub);				\
	}								\
	if(err) {							\
	    return 0;							\
	}								\
	return total_mem;						\
    }									\
									\
    int									\
    DTYPE ## _matrix_write_binary_file(const char*fname,		\
				       const struct DTYPE ## _matrix*mat) \
    {									\
	const char*this_sub="matrix_write_binary_file";			\
	FILE *f_out = mfopen(fname, "wb", this_sub);			\
	size_t bytes_written = 0;					\
	if(!f_out) {							\
	    return 1;							\
	}								\
	bytes_written = DTYPE ## _matrix_write_bin(f_out, mat);		\
	fclose(f_out);							\
	return !bytes_written;						\
    }									\
									\
    struct DTYPE ## _matrix*						\
    DTYPE ## _matrix_read_binary(FILE*infile)				\
    {									\
	int err=0;							\
	size_t n_rows, n_cols, total_size;				\
	struct DTYPE ## _matrix *mat=NULL;				\
	if(!err) {							\
	    err = (fread(&n_rows, sizeof(n_rows), 1, infile) != 1);	\
	}								\
	if(!err) {							\
	    err = (fread(&n_cols, sizeof(n_cols), 1, infile) != 1);	\
	}								\
	mat = new_ ## DTYPE ## _matrix(n_rows, n_cols, 0);		\
	total_size = n_rows * n_cols;					\
	if(!err) {							\
	    err = (fread(*mat->vals,					\
			 sizeof(**mat->vals),				\
			 total_size,					\
			 infile) != total_size);			\
	}								\
	if(err) {							\
	    DTYPE ## _matrix_destroy(mat);				\
	    return NULL;						\
	}								\
    	return mat;							\
    }									\
									\
    struct DTYPE ## _matrix*						\
    DTYPE ## _matrix_read_binary_file(const char*fname)			\
    {									\
	const char*this_sub="matrix_read_binary_file";			\
	struct DTYPE ## _matrix *mat=NULL;				\
	FILE *f_in=NULL;						\
	f_in = mfopen(fname, "rb", this_sub);				\
        if(!f_in) {							\
	    return NULL;						\
	}								\
	mat = DTYPE ## _matrix_read_binary(f_in);			\
	fclose(f_in);						        \
	return mat;							\
    }									\
									\
    enum yes_no								\
    DTYPE ## _matrix_same(const struct DTYPE ## _matrix *mat1,		\
			  const struct DTYPE ## _matrix *mat2)		\
    {									\
	size_t i, j;							\
	if((mat1->n_rows != mat2->n_rows) ||				\
	   (mat1->n_cols != mat2->n_cols)) {				\
	    return NO;							\
	}								\
	for(i = 0; i < mat1->n_rows; ++i) {				\
	    for(j = 0; j < mat1->n_cols; ++j) {				\
		if(mat1->vals[i][j] != mat2->vals[i][j]) {		\
		    return NO;						\
		}							\
	    }								\
	}								\
	return YES;							\
    }									\
									\
    void								\
    DTYPE ## _matrix_dump(const char *fmt,				\
			  const struct DTYPE ## _matrix *mat)		\
    {									\
	size_t i, j;							\
	for(i = 0; i < mat->n_rows; ++i) {				\
	    for(j = 0; j < mat->n_cols; ++j) {				\
		mprintf(fmt, mat->vals[i][j]);				\
	    }								\
	    mprintf("\n");						\
	}								\
    }									\
									\
    struct DTYPE ## _matrix*						\
    DTYPE ## _matrix_merge(size_t n_matrices,				\
			   struct DTYPE ## _matrix*const*matrices)	\
    {									\
	struct DTYPE ## _matrix *merged=NULL;				\
	size_t rows_total=0, i, size_cumulative=0, matrix_size;		\
	for(i = 0; i < n_matrices; ++i) {				\
	    rows_total += matrices[i]->n_rows;				\
	}								\
	merged = new_ ## DTYPE ## _matrix(rows_total,			\
					  matrices[0]->n_cols,		\
					  0.0);				\
	for(i = 0; i < n_matrices; ++i) {				\
	    matrix_size = matrices[i]->n_rows * matrices[i]->n_cols;	\
	    memcpy(*merged->vals + size_cumulative,			\
		   *matrices[i]->vals,					\
		   matrix_size * sizeof(**merged->vals));		\
	    size_cumulative += matrix_size;				\
	}								\
	return merged;							\
    }									\
									\
    struct DTYPE ## _matrix*						\
    DTYPE ## _matrix_identity_matrix(size_t size)			\
    {									\
	size_t i;							\
	struct DTYPE ## _matrix *mat=NULL;				\
	mat = new_ ## DTYPE ## _matrix(size, size, 0.0);		\
	for(i = 0; i < size; ++i) {					\
	    mat->vals[i][i] = 1;					\
	}								\
	return mat;							\
    }


ALLOC_FREE_MATRIX(, float)
WRAPPED_MATRIX_CLASS_BODY(, float)

ALLOC_FREE_MATRIX(, char)
WRAPPED_MATRIX_CLASS_BODY(, char)
