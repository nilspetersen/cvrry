#include <string.h>
#include "rna_coord.h"
#include "cvrry_parse_mmcif.h"
#include "fio.h"
#include "chainlist.h"
#include "rna_precompute.h"
#include "e_malloc.h"
#include "mprintf.h"
#include "cvrry_utils.h"
#include "cvrry_continuous_chains.h"


static int
write_chains_to_directory(FILE *f_outlist,
			  const char *fn_out_template,
			  struct rna_coord_set *rna_coord_set,
			  const struct chain_description *chain)
{
    int err=0;
    size_t i;
    char *filename_buffer=NULL;
    filename_buffer = E_MALLOC(strlen(fn_out_template) + 16);
    for(i = 0; i < rna_coord_set->n_chains && !err; ++i) {
	sprintf(filename_buffer, fn_out_template, i + 1);
	err = rna_coord_write_binary_file(filename_buffer, rna_coord_set->chains[i]);
	mfprintf(f_outlist,
		 "%s %s %i %zu 0\n",
		 chain->pdbid,
		 chain->chain_id,
		 chain->model_i,
		 i + 1);
    }
    free_if_not_null(filename_buffer);
    return err;
}

static int
readwrite_unbroken_rna_backbones(FILE *f_outlist,
				 const struct chain_description *chain,
				 const char *dir_in,
				 const char *dir_out)
{
    const enum rna_atom_types atom_types[] = {P, O5, C5,
					      C4, O4, C3,
					      O3, C2, C1};
    size_t n_atomtypes=9;
    struct cvrry_cif *cif=NULL;
    struct rna_coord *rna_backbone=NULL;
    struct rna_coord_set *rna_coord_set=NULL;
    char *fn_mmcif = NULL;
    char *fn_coord_prefix = NULL;
    int err=0;
    
    fn_mmcif = E_MALLOC(strlen(dir_in) + 16);
    make_fname_cif(fn_mmcif, dir_in, chain->pdbid);

    if(!err) {
	err = read_mmcif(&cif,
			 &rna_backbone,
			 fn_mmcif,
			 chain->chain_id,
			 chain->model_i);
    }

    if(!err) {
	compute_chain_breaks(rna_backbone);
	rna_coord_set = extract_chains(rna_backbone, atom_types, n_atomtypes);
	err = !rna_coord_set;
    }

    if(!err) {
	fn_coord_prefix = E_MALLOC(strlen(dir_out) +
				   strlen(chain->pdbid) +
				   strlen(chain->chain_id) +
				   64);
	sprintf(fn_coord_prefix,
		"%s/%s_%s_%i_C%s_F0.rnachain",
		dir_out,
		chain->pdbid,
		chain->chain_id,
		chain->model_i,
		"%i");
    }
    
    if(!err) {
	err = write_chains_to_directory(f_outlist,
					fn_coord_prefix,
					rna_coord_set,
					chain);
    }

    free_if_not_null(fn_mmcif);
    free_if_not_null(fn_coord_prefix);
    
    rna_coord_set_destroy(rna_coord_set);
    rna_coord_destroy(rna_backbone);
    cvrry_cif_destroy(cif);

    return err;
}

int
read_write_unbroken_rna_backbones_set(const struct chain_list *chainlist,
				      const char *dir_in,
				      const char *dir_out,
				      const char *fn_out_new_chainlist)
{
    const char *this_sub = "read_write_unbroken_rna_backbones_set";
    int err=0;
    size_t i;
    FILE *f_new_chainlist=NULL;
    f_new_chainlist = mfopen(fn_out_new_chainlist, "w", this_sub);
    err = !f_new_chainlist;
    for(i = 0; i < chainlist->size && !err; ++i) {
	err = readwrite_unbroken_rna_backbones(f_new_chainlist,
					       chainlist->chains + i,
					       dir_in,
					       dir_out);
    }
    if(f_new_chainlist){
	fclose(f_new_chainlist);
    }    
    return err;
}
