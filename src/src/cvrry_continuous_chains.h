#ifndef CVRRY_CONTINUOUS_CHAINS_H
#define CVRRY_CONTINUOUS_CHAINS_H

int
read_write_unbroken_rna_backbones_set(const struct chain_list *chainlist,
				      const char *dir_in,
				      const char *dir_out,
				      const char *fn_out_new_chainlist);

#endif
