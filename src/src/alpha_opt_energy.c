#include <math.h>
#include "alpha_opt_energy.h"

/* Z_SCORE */

/* linear and logistic functions on z scores */

struct z_score_params {
    size_t size;
    float *means;
    float *sigmas;
    float shift;
    float min_or_slope; /* the minimum for linear, a stretching factor for logistic */
};

static struct z_score_params*
malloc_z_score_params(size_t size)
{
    struct z_score_params *params=NULL;
    params = E_MALLOC(sizeof(*params));
    params->size = size;
    params->sigmas = E_MALLOC(size * sizeof(*params->sigmas));
    params->means = E_MALLOC(size * sizeof(*params->means));
    return params;
}

void
z_score_params_destroy(struct z_score_params*params)
{
    if(params) {
	free_if_not_null(params->sigmas);
	free_if_not_null(params->means);
	free(params);
    }
}

static void
set_z_score_params(struct z_score_params*params,
		   const float*means,
		   const float*sigmas,
		   float shift,
		   float min_or_slope)
{
    if(means) {
	memcpy(params->means, means, params->size * sizeof(*means));
    }
    if(sigmas) {
	memcpy(params->sigmas, sigmas, params->size * sizeof(*sigmas));
    }
    params->min_or_slope = min_or_slope;
    params->shift = shift;
}

struct z_score_params*
new_z_score_params(size_t size,
		   const float* means,
		   const float* sigmas,
		   float shift,
		   float min_or_slope)
{
    struct z_score_params *params=NULL;
    params = malloc_z_score_params(size);
    set_z_score_params(params, means, sigmas, shift, min_or_slope);
    return params;
}

static float
linear_z_score(const struct z_score_params*params,
	       size_t len,
	       float rmsd)
{
    float sig, mean, z, score;
    if(!len) {
	return 0;
    }
    sig = params->sigmas[len - 1];
    mean = params->means[len - 1];
    z = ((mean - rmsd) / sig) - params->shift;
    score = (z > params->min_or_slope ? z : params->min_or_slope) * len;
    return -score;
}

static float
logistic_z_score(const struct z_score_params*params,
		 size_t len,
		 float rmsd)
{
    float sig, mean, z, score, k;
    if(!len) {
	return 0;
    }
    sig = params->sigmas[len - 1];
    mean = params->means[len - 1];
    z = ((mean - rmsd) / sig) - params->shift;
    k = params->min_or_slope / sig;
    score = tanh(k * z) * len;
    return -score;
}

static float
logistic_mean_score(const struct z_score_params*params,
		    size_t len,
		    float rmsd)
{
    float mean, score, k, diff;
    if(!len) {
	return 0;
    }
    mean = params->means[len - 1];
    diff = params->shift * mean - rmsd;
    k = params->min_or_slope / mean;
    score = tanh(k * diff) * len;
    return -score;
}

static int
get_z_score_energy(float*energy,
		   const struct z_score_params*params,
		   size_t len,
		   float rmsd,
		   float (*score_func)(const struct z_score_params*,
				       size_t, float))
{
    const char *this_sub="get_z_score_energy";
    if(len > params->size) {
	err_printf(this_sub,
		   "Match-length %zu does not match scoring params (size=%zu)\n",
		   len,
		   params->size);
	return 1;
    }
    *energy = score_func(params, len, rmsd);
    return 0;
}

static int
get_z_linear_energy(float*energy,
		    const void*params,
		    const struct continuous_match*match,
		    float rmsd)
{
    return get_z_score_energy(energy,
			      (const struct z_score_params*) params,
			      match->len,
			      rmsd,
			      &linear_z_score);
}

static int
get_z_logistic_energy(float*energy,
		      const void*params,
		      const struct continuous_match*match,
		      float rmsd)
{
    return get_z_score_energy(energy,
			      (const struct z_score_params*) params,
			      match->len,
			      rmsd,
			      &logistic_z_score);
}

static int
get_mean_logistic_energy(float*energy,
		      const void*params,
		      const struct continuous_match*match,
		      float rmsd)
{
    return get_z_score_energy(energy,
			      (const struct z_score_params*) params,
			      match->len,
			      rmsd,
			      &logistic_mean_score);
}

struct match_energy_func*
make_z_score_energy_func(size_t size,
			 const float*means,
			 const float*sigmas,
			 float shift,
			 float min)
{
    struct match_energy_func*efunc=NULL;
    efunc = E_MALLOC(sizeof(*efunc));
    efunc->func = &get_z_linear_energy;
    efunc->params = (void*) new_z_score_params(size, means, sigmas, shift, min);
    efunc->type = Z_LINEAR;
    return efunc;
}

struct match_energy_func*
make_z_logistic_energy_func(size_t size,
			    const float*means,
			    const float*sigmas,
			    float shift,
			    float slope)
{
    struct match_energy_func*efunc=NULL;
    efunc = E_MALLOC(sizeof(*efunc));
    efunc->func = &get_z_logistic_energy;
    efunc->params = (void*) new_z_score_params(size, means, sigmas, shift, slope);
    efunc->type = Z_LOGISTIC;
    return efunc;
}

struct match_energy_func*
make_mean_logistic_energy_func(size_t size,
			       const float*means,
			       float shift,
			       float slope)
{
    struct match_energy_func*efunc=NULL;
    efunc = E_MALLOC(sizeof(*efunc));
    efunc->func = &get_mean_logistic_energy;
    efunc->params = (void*) new_z_score_params(size, means, NULL, shift, slope);
    efunc->type = MEAN_SHIFT;
    return efunc;    
}

/* ALL */

struct match_energy_func*
match_energy_func_copy(const struct match_energy_func*efunc)
{
    const struct z_score_params *z_par;
    switch(efunc->type) {
    case Z_LINEAR:
	z_par = (const struct z_score_params*) efunc->params;
	return make_z_score_energy_func(z_par->size,
					z_par->means,
					z_par->sigmas,
					z_par->shift,
					z_par->min_or_slope);
    case Z_LOGISTIC:
	z_par = (const struct z_score_params*) efunc->params;
	return make_z_logistic_energy_func(z_par->size,
					   z_par->means,
					   z_par->sigmas,
					   z_par->shift,
					   z_par->min_or_slope);
    case MEAN_SHIFT:
	z_par = (const struct z_score_params*) efunc->params;
	return make_mean_logistic_energy_func(z_par->size,
					      z_par->means,
					      z_par->shift,
					      z_par->min_or_slope);
    }
    return NULL;
}

void
match_energy_func_destroy(struct match_energy_func*efunc)
{
    if(efunc) {
	switch(efunc->type) {
	case Z_LINEAR:
	case Z_LOGISTIC:
	case MEAN_SHIFT:
	    z_score_params_destroy((struct z_score_params*) efunc->params);
	    break;
	}
	free(efunc);
    }
}

int
match_energy_get_energy(float*energy,
			const struct match_energy_func*efunc,
			const struct continuous_match*match,
			float rmsd)
{
    return efunc->func(energy, efunc->params, match, rmsd);
}
