/*
 * Fitting code, originally written by Wilfred van Gunsteren
 */
#ifndef _XOPEN_SOURCE
#   define _XOPEN_SOURCE 500    /* Necessary to get maths constants */
#endif
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "coord.h"
#include "pair_set.h"
#include "lsqf.h"
#include "mprintf.h"
#include "seq.h"
#include "rna_coord.h"
#include "e_malloc.h"
#include "rna_get_atoms.h"


/* ---------------- constants ---------------------------------
 * We use the square root of two so often, we can help the compiler
 * by saying it is just one value we re-use.
 */
static const double m_sqrt2 = M_SQRT2;

/* ---------------- eigen -------------------------------------
 * This calculates eigenvalues and corresponding eigenvectors.
 * It came from Thomas Huber, who took it from the GROMOS
 * source code...
CCCCC W.F. VAN GUNSTEREN, CAMBRIDGE, JUNE 1979 CCCCCCCCCCCCCCCCCCCCCCCC
                                                                      C
     SUBROUTINE EIGEN (A,R,N,MV)                                      C
                                                                      C
         EIGEN COMPUTES EIGENVALUES AND EIGENVECTORS OF THE REAL      C
     SYMMETRIC N*N MATRIX A, USING THE DIAGONALIZATION METHOD         C
     DESCRIBED IN "MATHEMATICAL METHODS FOR DIGITAL COMPUTERS", EDS.  C
     A.RALSTON AND H.S.WILF, WILEY, NEW YORK, 1962, CHAPTER 7.        C
     IT HAS BEEN COPIED FROM THE IBM SCIENTIFIC SUBROUTINE PACKAGE.   C
                                                                      C
     A(0..N*(N-1)/2) = MATRIX TO BE DIAGONALIZED, STORED IN SYMMETRIC C
                       STORAGE MODE, VIZ. THE I,J-TH ELEMENT (I.GE.J) C
                       IS STORED AT THE LOCATION K=I*(I-1)/2+J IN A;  C
                       THE EIGENVALUES ARE DELIVERED IN DESCENDING    C
                       ORDER ON THE DIAGONAL, VIZ. AT THE LOCATIONS   C
                       K=I*(I-1)/2                                    C
     R(0..N-1,0..N-1) = DELIVERED WITH THE CORRESPONDING EIGENVECTORS C
                        STORED COLUMNWISE                             C
     N = ORDER OF MATRICES A AND R                                    C
     MV != 0 : EIGENVALUES AND EIGENVECTORS ARE COMPUTED              C
         = 0 : ONLY EIGENVALUES ARE COMPUTED                          C
                                                                      C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
Note: The previous description was changed to fit the c indexing
*/

static void
swap_float(float *arr, int i, int j)
{
    float swap = arr[i];
    arr[i] = arr[j];
    arr[j] = swap;
}

static void
eigen(float *a, float *r__, int n, int mv)
{
    int i, j, k, l, m;
    int il, im, ll, lm, iq, mm, lq, mq;
    int ind, ilr, imr;
    float cosx, sinx, cosx2, sinx2;
    float x, y, anorm, sincs, anrmx;
    float thr;
    static const float range = (float) 1e-12;

    if (mv) {
        for (j = 0; j < n; ++j) {
            for (i = 0; i < n; ++i) {
                r__[j * n + i] = (i == j ? 1 : 0);
            }
        }
    }

    /****COMPUTE INITIAL AND FINAL NORMS (ANORM AND ANRMX) */
    anorm = 0.0;
    for (i = 0; i < n; ++i) {
        for (j = i + 1; j < n; ++j) {
            int ia = i + ((j + 1) * j) / 2;
    	    anorm += a[ia] * a[ia];
        }
    }

    if (anorm > 0.0) {
        anorm = sqrt(anorm) * m_sqrt2;
        anrmx = anorm * range / (float) n;

        /*****INITIALIZE INDICATORS AND COMPUTE THRESHOLD, THR */
        thr = anorm;

        do { /* while (thr - anrmx > 0.0) */
            thr /= (float) n;
            do {  /* while(ind) */
                ind = 0 /*EXIT_FAILURE */ ;
                for (l = 0; l < n - 1; l++) {
                    for (m = l + 1; m < n; m++) {
                        /*****COMPUT SIN AND COS */
                        mq = (m * (m + 1)) / 2;
                        lq = (l * (l + 1)) / 2;
                        lm = l + mq;
                        if (fabs(a[lm]) >= thr) {
                            ind = 1; /* EXIT_SUCCESS */
                            ll = l + lq;
                            mm = m + mq;
                            x = (a[ll] - a[mm]) * (float) 0.5;
                            y = -a[lm] / sqrt(a[lm] * a[lm] + x * x);
                            if (x < 0.0) {
                                y = -y;
			    }
                            sinx = y / sqrt((sqrt(1.0 - y * y) + 1.0) * 2.0);
                            sinx2 = sinx * sinx;
                            cosx = sqrt(1.0 - sinx2);
                            cosx2 = cosx * cosx;
                            sincs = sinx * cosx;

                            /* *****ROTATE L AND M COLUMNS */
                            
                            for (i = 0; i < n; ++i) {
                                iq = (i * (i + 1)) / 2;
                                if ((i != l) && (i != m)) {
				    im = i < m ? i + mq : m + iq; 
				    il = i < l ? i + lq : l + iq;
				    x = a[il] * cosx - a[im] * sinx; 
				    a[im] = a[il] * sinx + a[im] * cosx;
				    a[il] = x;
				}
                                if (mv) {
                                    ilr = n * l + i;
                                    imr = n * m + i;
                                    x = r__[ilr] * cosx - r__[imr] * sinx;
                                    r__[imr] = r__[ilr] * sinx + r__[imr] * cosx;
				    r__[ilr] = x;
                                }
                            }
                            x = a[lm] * 2. * sincs;
                            y = a[ll] * cosx2 + a[mm] * sinx2 - x;
                            x = a[ll] * sinx2 + a[mm] * cosx2 + x;
                            a[lm] = (a[ll] - a[mm]) * sincs + a[lm] * (cosx2 - sinx2);
                            a[ll] = y;
                            a[mm] = x;
                        }
                    }
                }
            } while (ind);
        } while (thr > anrmx); /* *****COMPARE THRESHOLD WITH FINAL NORM */
    }
    /* ---- if (anorm > 0) ------ */

    /* *****SORT EIGENVALUES AND EIGENVECTORS */
    for (i = 0; i < n; ++i) {
        ll = i + ((i + 1) * i) / 2;
        for (j = i; j < n; ++j) {
            mm = j + ((j + 1) * j) / 2;
            if (a[ll] < a[mm]) {
		swap_float(a, ll, mm);
                if (mv) {
                    for (k = 0; k < n; ++k) {
                        swap_float(r__, i * n + k, j * n + k);
                    }
                }
            }
        }
    }
}

/* ---------------- lsq_fit  ----------------------------------
 * Least square fit routine to determine the rotation matrix to
 * superimpose coordinates r1 onto coordinates r2.
 * omega is a symmetric matrix in symmetric storage mode:
 * The element [i][j] is stored at position [i*(i+1)/2+j] with i>j
 * Thanks to Wilfred and Thomas Huber.
 */
static int
lsq_fit_core(float U[3][3], float R[3][3])
{
    int i, j, ii, jj;
    float det_U, sign_detU, sigma;
    float H[3][3], K[3][3];     /*, R[3][3]; */
    float omega[21], eve_omega[36], eva_omega[6];
    static const float SMALL = (float) 1.e-5;
    /* static const char *this_sub = "lsq_fit"; */
    int err=0;

/* calculate the determinant  */
    det_U = U[0][0] * U[1][1] * U[2][2] + U[0][2] * U[1][0] * U[2][1] +
        U[0][1] * U[1][2] * U[2][0] - U[2][0] * U[1][1] * U[0][2] -
        U[2][2] * U[1][0] * U[0][1] - U[2][1] * U[1][2] * U[0][0];

    sign_detU = det_U < 0.0 ? -1.0 : 1.0;
/* ----- CONSTRUCT OMEGA, DIAGONALIZE IT AND DETERMINE H AND K --- */

    for (i = 0; i < 6; i++)
        for (j = i; j < 6; j++)
            omega[(j * (j + 1) / 2) + i] = 0.0;
    for (j = 3; j < 6; j++) {
        jj = j * (j + 1) / 2;
        for (i = 0; i < 3; i++) {
            ii = jj + i;
            omega[ii] = U[i][j - 3];
        }
    }

    eigen(omega,
	  eve_omega,
	  6, /* dimension of omega matrix */
	  1); /* both, eigenvalues and eigenvectors are calculated */

    for (i = 0; i < 6; i++)
        eva_omega[i] = omega[i * (i + 1) / 2 + i];

    if (det_U < 0.0) {
        if (fabs(eva_omega[1] - eva_omega[2]) < SMALL) {
            /* err_printf(this_sub, */
            /*            "determinant of U < 0 && degenerated eigenvalues\n"); */
            return EXIT_FAILURE;
        }
    }

    for (i = 0; i < 3; i++){
        for (j = 0; j < 3; j++) {
            H[i][j] = m_sqrt2 * eve_omega[j * 6 + i];
            K[i][j] = m_sqrt2 * eve_omega[j * 6 + i + 3];
        }
    }
    sigma = (H[1][0] * H[2][1] - H[2][0] * H[1][1]) * H[0][2] +
        (H[2][0] * H[0][1] - H[0][0] * H[2][1]) * H[1][2] +
        (H[0][0] * H[1][1] - H[1][0] * H[0][1]) * H[2][2];

    if (sigma <= 0.0) {
        for (i = 0; i < 3; i++) {
            H[i][2] = -H[i][2];
            K[i][2] = -K[i][2];
        }
    }

/* --------- DETERMINE R AND ROTATE X ----------- */
    for (i = 0; i < 3; i++)
        for (j = 0; j < 3; j++)
            R[j][i] = K[j][0] * H[i][0] + K[j][1] * H[i][1] +
                sign_detU * K[j][2] * H[i][2];

#ifdef DEBUG
    mfprintf(stdout, "Rotation matrix:\n");
    for (j = 0; j < 3; j++) {
        for (i = 0; i < 3; i++)
            mfprintf(stdout, "%f ", R[i][j]);
        mfprintf(stdout, "\n");
    }
#endif
    return err;
    /* return EXIT_SUCCESS; */
}

static void
lsq_init_U(float U[3][3])
{
    size_t i, j;
    for (i = 0; i < 3; i++)
        for (j = 0; j < 3; j++)
            U[i][j] = 0.0;    
}

static void
lsq_add_one_to_U(float U[3][3],
		 const struct RPoint *r1,
		 const struct RPoint *r2)
{
    U[0][0] += r1->x * r2->x;
    U[0][1] += r1->x * r2->y;
    U[0][2] += r1->x * r2->z;
    U[1][0] += r1->y * r2->x;
    U[1][1] += r1->y * r2->y;
    U[1][2] += r1->y * r2->z;
    U[2][0] += r1->z * r2->x;
    U[2][1] += r1->z * r2->y;
    U[2][2] += r1->z * r2->z;
}

static void
lsq_add_to_U(float U[3][3],
	     const int nr_atoms,
	     const struct RPoint *r1,
	     const struct RPoint *r2)
{
    int i;
    for (i = 0; i < nr_atoms; i++) {
	lsq_add_one_to_U(U, r1 + i, r2 + i);
    }
}

static int
lsq_fit(const int nr_atoms,
	const struct RPoint *r1,
	const struct RPoint *r2,
	float R[3][3])
{
    float U[3][3];
    
    /* ----- CALCULATE THE MATRIX U ----- */
    lsq_init_U(U);
    lsq_add_to_U(U, nr_atoms, r1, r2);

    /* the rest */
    return lsq_fit_core(U, R);
}

/* ---------------- calc_RMSD ------------------------------------
 * Calculates the RMSD between nr_atoms Points in arrays r1 and r2
 */
static float
calc_RMSD(const int nr_atoms,
	  const struct RPoint *r1,
	  const struct RPoint *r2)
{
    float rmsd = 0.0;
    float dr_sqrlength = 0.0;
    struct RPoint dr;
    int n;

    for (n = 0; n < nr_atoms; n++) {
        dr.x = r1[n].x - r2[n].x;
        dr.y = r1[n].y - r2[n].y;
        dr.z = r1[n].z - r2[n].z;
        dr_sqrlength = dr.x * dr.x + dr.y * dr.y + dr.z * dr.z;
        rmsd += dr_sqrlength;
    }
    rmsd /= nr_atoms;
    rmsd = sqrt(rmsd);

    return (rmsd);
}

/* ---------------- CM_Translate ------------------------------
 * Translates the coordinates of two molecules to superimpose such
 * that the center of mass is in the origin.
 */

static void
rpoint_add(struct RPoint *target,
	   const struct RPoint *r)
{
    target->x += r->x;
    target->y += r->y;
    target->z += r->z;
}

static void
rpoint_subtract(struct RPoint *target,
		const struct RPoint *r)
{
    target->x -= r->x;
    target->y -= r->y;
    target->z -= r->z;
}

static void
rpoint_scalar_div(struct RPoint *target,
		   float scalar)
{
    target->x /= scalar;
    target->y /= scalar;
    target->z /= scalar;
}

static float
rpoint_dist(const struct RPoint *r1, const struct RPoint *r2) 
{
    struct RPoint dr;
    dr.x = r1->x - r2->x;
    dr.y = r1->y - r2->y;
    dr.z = r1->z - r2->z;
    return sqrt((dr.x * dr.x) + (dr.y * dr.y) + (dr.z * dr.z));
}

static struct RPoint
calc_CM(const size_t nr_atoms, const struct RPoint *r1)
{
    size_t i;
    struct RPoint cm;
    cm.x = cm.y = cm.z = 0.0;
    for (i = 0; i < nr_atoms; i++) {
	rpoint_add(&cm, r1 + i);
    }
    rpoint_scalar_div(&cm, (float) nr_atoms);
    return cm;
}

/* ---------------- apply_trans -------------------------------
 * Apply the transformation specified by the vector trans to structure
 * of size nr_atoms
 */
static void
apply_trans(const struct RPoint *trans,
	    struct RPoint *structure,
            size_t nr_atoms)
{
    size_t i;
    for (i = 0; i < nr_atoms; i++) {
	rpoint_subtract(structure + i, trans);
    }
}


/* ---------------------------------------RNA-----------------------------------*/

/* 
 * new functions for RNA by Nils, previous functions were not changed
 */

static void
rotate_point(float rmat[3][3], struct RPoint *r)
{
    struct RPoint dr;
    dr.x = rmat[0][0] * r->x + rmat[0][1] * r->y + rmat[0][2] * r->z;
    dr.y = rmat[1][0] * r->x + rmat[1][1] * r->y + rmat[1][2] * r->z;
    dr.z = rmat[2][0] * r->x + rmat[2][1] * r->y + rmat[2][2] * r->z;
    *r = dr;
}

void
rotate_rpoint(struct RPoint *r, const struct rotation_matrix *rot_mat)
{
    struct RPoint dr;
    dr.x = rot_mat->rmat[0][0] * r->x + rot_mat->rmat[0][1] * r->y + rot_mat->rmat[0][2] * r->z;
    dr.y = rot_mat->rmat[1][0] * r->x + rot_mat->rmat[1][1] * r->y + rot_mat->rmat[1][2] * r->z;
    dr.z = rot_mat->rmat[2][0] * r->x + rot_mat->rmat[2][1] * r->y + rot_mat->rmat[2][2] * r->z;
    *r = dr;
}

static void
rotate_points(size_t nr_atoms, float rmat[3][3], struct RPoint *r)
{
    size_t i;
    for(i = 0; i < nr_atoms; ++i)
	rotate_point(rmat, r+i);
}

int
rmsd_superpos_core(struct transformation *transform,
		   float *rmsd,
		   struct RPoint *bb_source,
		   struct RPoint *bb_target,
		   size_t n_atoms)
{
    const char *this_sub="rmsd_superpos_core";
    int e;
    float rmat[3][3];
    size_t i, j;
    struct RPoint cm_source, cm_target; /* centres of mass */
    
    /* translate both chains to the centre of mass */
    cm_source = calc_CM(n_atoms, bb_source);
    cm_target = calc_CM(n_atoms, bb_target);
    
    apply_trans(&cm_source, bb_source, n_atoms);
    apply_trans(&cm_target, bb_target, n_atoms);
    
    /* get rotation matrix */
    e = lsq_fit(n_atoms, bb_source, bb_target, rmat);
    if(e) {
	err_printf(this_sub, "could not find rotation matrix\n");
	return 1;
    }
    
    /* rotate source and compute rmsd */
    rotate_points(n_atoms, rmat, bb_source);
    if(rmsd) {
	*rmsd = calc_RMSD(n_atoms, bb_source, bb_target);
    }
    
    if(transform) {
	/* copy rmat to rotation matrix */
	for(i = 0; i < 3; ++i) {
	    for(j = 0; j < 3; ++j) {
		transform->rotate_a.rmat[i][j] = rmat[i][j];
	    }
	}
	transform->com_a = cm_source;
	transform->com_b = cm_target;
    }
    return 0;
}


int
rna_coord_rmsd(struct transformation *transform,
	       float *rmsd,
	       const struct pair_set *pairset,
	       const struct rna_coord *source, 
	       const struct rna_coord *target)
{
    struct RPoint *bb_source;	/* lists of backbone atoms */
    struct RPoint *bb_target;
    size_t n_atoms;  /* number of atoms used for least squares fit */
    int err=0;
    size_t msize_bb;
    const size_t BB_ATOMS_PER_NUCLEOTIDE = 9; /* the current number of backbone atoms, adapt if you change it ! */
    const enum rna_atom_types atom_types[] = {P, O5, C5,
					      C4, O4, C3,
					      O3, C2, C1};
    
    /* get atom lists of both structures */
    msize_bb = pairset->n * BB_ATOMS_PER_NUCLEOTIDE * sizeof(*bb_source); 
    bb_source = E_MALLOC(msize_bb);
    bb_target = E_MALLOC(msize_bb);
    n_atoms = get_backbone_atoms(pairset,
				 source,
				 target,
				 bb_source,
				 bb_target,
				 NULL,
				 atom_types,
				 BB_ATOMS_PER_NUCLEOTIDE);

    err = rmsd_superpos_core(transform,
			     rmsd,
			     bb_source,
			     bb_target,
			     n_atoms);

    /* free */
    free_if_not_null(bb_source);
    free_if_not_null(bb_target);
    
    return err;
}

int
rna_coord_rmsd_continuous_match(struct transformation *transform,
				float *rmsd,
				struct continuous_match match,
				const struct rna_coord *source, 
				const struct rna_coord *target,
				size_t fraglen)
{
    const char *this_sub="rna_coord_rmsd_continuous_match";
    struct RPoint *bb_source;	/* lists of backbone atoms */
    struct RPoint *bb_target;
    size_t n_atoms;  /* number of atoms used for least squares fit */
    int err=0;
    size_t msize_bb;
    const size_t BB_ATOMS_PER_NUCLEOTIDE = 9; 
    const enum rna_atom_types atom_types[] = {P, O5, C5,
					      C4, O4, C3,
					      O3, C2, C1};

    if(!match.len) {
	*rmsd = 0;
	return 0;
    }

    if(fraglen) {
	match.len += fraglen - 1;
    }
    
    /* get atom lists of both structures */
    msize_bb = match.len * BB_ATOMS_PER_NUCLEOTIDE * sizeof(*bb_source); 
    bb_source = E_MALLOC(msize_bb);
    bb_target = E_MALLOC(msize_bb);

    n_atoms = get_backbone_atoms_continuous_match(bb_source,
						  bb_target,
						  &match,
						  source,
						  target,
						  atom_types,
						  BB_ATOMS_PER_NUCLEOTIDE);
    err = rmsd_superpos_core(transform,
			     rmsd,
			     bb_source,
			     bb_target,
			     n_atoms);
    /* free */
    free_if_not_null(bb_source);
    free_if_not_null(bb_target);

    if(err) {
	err_printf(this_sub,
		   ("failed for match (%zu, %zu) with length %zu between "
		    "sequence with lengths %zu and %zu\n"),
		   match.matchpos_a,
		   match.matchpos_b,
		   match.len,
		   source->size,
		   target->size);
    }

    return err;
}

/* lsq-fit
 * with multiple grouped particles
 */

static int
lsq_fit_multi_res(const size_t n_res,
		  const struct RPoint *const*r1,
		  const struct RPoint *const*r2,
		  const size_t *n_atoms_per_nt,
		  float R[3][3])
{
    float U[3][3];
    size_t i;

    /* ----- CALCULATE THE MATRIX U ----- */
    lsq_init_U(U);
    for (i = 0; i < n_res; i++) {
	lsq_add_to_U(U, n_atoms_per_nt[i], r1[i], r2[i]);
    }
    
    /* the rest */
    return lsq_fit_core(U, R);
}

static void
get_rmsds_multi_res(const size_t n_res,
		    const struct RPoint *const*r1,
		    const struct RPoint *const*r2,
		    const size_t *n_atoms_per_nt,
		    float *rmsds)
{
    size_t i;
    for(i = 0; i < n_res; i++) {
	rmsds[i] = calc_RMSD(n_atoms_per_nt[i], r1[i], r2[i]);
    }
}

static struct RPoint
calc_CM_multi_res(const size_t n_res,
		  const struct RPoint *const*r,
		  const size_t *n_atoms_per_nt)
{
    size_t i, j, n_atoms=0;
    float total_mass;
    struct RPoint cm;

    cm.x = 0.0;
    cm.y = 0.0;
    cm.z = 0.0;

    for(i = 0; i < n_res; i++) {
	for(j = 0; j < n_atoms_per_nt[i]; j++) {
	    cm.x += r[i][j].x;
	    cm.y += r[i][j].y;
	    cm.z += r[i][j].z;
	    n_atoms++;
	}
    }

    total_mass = n_atoms;
    cm.x /= total_mass;
    cm.y /= total_mass;
    cm.z /= total_mass;
    return cm;
}

static void
apply_trans_multi_res(const struct RPoint *trans,
		      const size_t n_res,
		      const size_t *n_atoms_per_nt,
		      struct RPoint **r) 
{
    size_t i;
    for(i = 0; i < n_res; i++) {
	apply_trans(trans, r[i], n_atoms_per_nt[i]);
    }
}

static void
remove_com_multi_res(const size_t n_res, const size_t *n_atoms_per_nt, struct RPoint **r)
{
    struct RPoint cm;
    cm = calc_CM_multi_res(n_res, (const struct RPoint *const*) r,  n_atoms_per_nt);
    apply_trans_multi_res(&cm, n_res, n_atoms_per_nt, r);
}

static void
apply_rot_multi_res(float rmat[3][3],
		    const size_t n_res,
		    const size_t *n_atoms_per_nt,
		    struct RPoint **r)
{
    size_t i;
    for(i = 0; i < n_res; i++) {
	rotate_points(n_atoms_per_nt[i], rmat, r[i]);
    }
}

static void
swap_res_multi_res(struct RPoint **r, size_t i, size_t j)
{
    struct RPoint *swap = r[i];
    r[i] = r[j];
    r[j] = swap;
}

static void
swap_size_t(size_t *arr, size_t i, size_t j)
{
    size_t swap = arr[i];
    arr[i] = arr[j];
    arr[j] = swap;
}

static int
compare_size_t(const void *a, const void *b)
{
    size_t a_ = *(size_t*) a;
    size_t b_ = *(size_t*) b;
    if(a_ < b_) {
	return -1;
    } 
    if(a_ > b_) {
	return 1;
    }
    return 0;
}

struct pair_set *
rna_coord_superimpose(const struct pair_set *pairset,
		      const struct rna_coord *source, 
		      const struct rna_coord *target,
		      float rmsd_thresh) 
{
    static const char *this_sub = "rna_coord_superimpose";
    float rmat[3][3], *rmsds, max_rmsd;
    size_t n_res, *n_atoms_per_nt, i, max_rmsd_i, atom_count, res_count;
    size_t *pair_index, i_nettopairs;
    int ii, jj;
    struct RPoint *bb_source, *bb_target;
    struct RPoint **res_source, **res_target;
    struct pair_set *pairs_superimposed = NULL; /* the return value of this function */
    int e;
    size_t msize_bb, msize_res;
    size_t *pair_order;
    /* const size_t BB_ATOMS_PER_NUCLEOTIDE = 9; */
    /* const enum rna_atom_types atom_types[] = {P, O5, C5, */
    /* 					      C4, O4, C3, */
    /* 					      O3, C2, C1}; */
    const size_t BB_ATOMS_PER_NUCLEOTIDE = 1; /* the current number of backbone atoms, adapt if you change it ! */
    const enum rna_atom_types atom_types[] = {C4};

    /* allocations */
    n_res = pair_set_get_netto_alignmentlength(pairset);
    n_atoms_per_nt = E_MALLOC(n_res *  sizeof(*n_atoms_per_nt));

    msize_bb = n_res * BB_ATOMS_PER_NUCLEOTIDE * sizeof(*bb_source); 
    bb_source = E_MALLOC(msize_bb);
    bb_target = E_MALLOC(msize_bb);

    msize_res = n_res * sizeof(*res_source);
    res_source = E_MALLOC(msize_res);
    res_target = E_MALLOC(msize_res);

    pair_order = E_MALLOC(n_res * sizeof(*pair_order));
    rmsds = E_MALLOC(n_res * sizeof(*rmsds));

    /* get coordinates */
    get_backbone_atoms(pairset,
		       source,
		       target,
		       bb_source,
		       bb_target,
		       n_atoms_per_nt,
		       atom_types,
		       BB_ATOMS_PER_NUCLEOTIDE);

    /* make the pointer arrays and store the order */
    for(i = 0, atom_count = 0; i < n_res; i++) {
	pair_order[i] = i;
	res_source[i] = bb_source + atom_count;
	res_target[i] = bb_target + atom_count;
	atom_count += n_atoms_per_nt[i];
    }
    res_count = n_res;

    while(atom_count > 4 && res_count >= 1) {
	
	/* compute and remove the center of mass */
	remove_com_multi_res(res_count, n_atoms_per_nt, res_source);
	remove_com_multi_res(res_count, n_atoms_per_nt, res_target);
	
	/* lsq-fit */
	e = lsq_fit_multi_res(res_count, (const struct RPoint *const*) res_source, 
			      (const struct RPoint *const*) res_target, n_atoms_per_nt, rmat);
	if(e) {
	    err_printf(this_sub, "could not find rotation matrix\n");
	    goto exit;
	}

	/* rotate the source */
	apply_rot_multi_res(rmat, res_count, n_atoms_per_nt, res_source);

	/* get rmsds */
	get_rmsds_multi_res(res_count, (const struct RPoint *const*) res_source, 
			    (const struct RPoint *const*) res_target, n_atoms_per_nt, rmsds);
	
	/* find the largest rmsd */
	max_rmsd_i = 0;
	max_rmsd = rmsds[0];
	for(i = 1; i < res_count; i++) {
	    if(rmsds[i] > max_rmsd) {
		max_rmsd = rmsds[i];
		max_rmsd_i = i;
	    }
	}
	if(max_rmsd < rmsd_thresh)
	    break;

	/* remove the worst residue */
	/* reduce the residue- and atom-count  */
	res_count--;
	atom_count -= n_atoms_per_nt[max_rmsd_i];
	/* remove the residues by swapping them to the end of the line */
	swap_res_multi_res(res_source, res_count, max_rmsd_i);
	swap_res_multi_res(res_target, res_count, max_rmsd_i);
	swap_size_t(pair_order, res_count, max_rmsd_i);
	swap_size_t(n_atoms_per_nt, res_count, max_rmsd_i);
    }

    /* use the final indices to create a new pairset */
    /* allocate space */
    pairs_superimposed = pair_set_new(res_count);

    /* sort the indices */
    qsort(pair_order, res_count, sizeof(*pair_order), compare_size_t);
    /* copy the pairs */
    pair_index = pair_order; 
    i_nettopairs = 0;
    for(i = 0; pair_index < pair_order + res_count; i++) {
	ii = pairset->indices[i][0];
	jj = pairset->indices[i][1];
	if(ii == GAP_INDEX || jj == GAP_INDEX) 
	    continue;
	if(i_nettopairs == *pair_index) {
	    pair_set_add_pair(pairs_superimposed, ii, jj);
	    pair_index++;
	}
	i_nettopairs++;
    }
    
exit:
    /* free stuff */
    free_if_not_null(n_atoms_per_nt);
    free_if_not_null(bb_source);
    free_if_not_null(bb_target);
    free_if_not_null(res_source);
    free_if_not_null(res_target);
    free_if_not_null(pair_order);
    free_if_not_null(rmsds);

    return pairs_superimposed;
}

static struct RPoint
get_com_pairs(struct RPoint *r1,
	      const size_t *pairs,
	      size_t n_pairs)
{
    size_t i;
    struct RPoint cm;
    cm.x = cm.y = cm.z = 0.0;
    for(i = 0; i < n_pairs; ++i) {
	rpoint_add(&cm, r1 + pairs[i]);
    }
    rpoint_scalar_div(&cm, (float) n_pairs);
    return cm;
}

static void
remove_com_pairs(struct RPoint *structure,
		 size_t n_atoms_total,
		 const size_t *pairs,
		 size_t n_pairs)
{
    struct RPoint com = get_com_pairs(structure, pairs, n_pairs);
    apply_trans(&com, structure, n_atoms_total);
}

static int
lsq_fit_pairs(float R[3][3],
	      const struct RPoint *r1,
	      const struct RPoint *r2,
	      const size_t *pairs,
	      size_t n_pairs)
{
    float U[3][3];
    size_t i;
    lsq_init_U(U);
    for(i = 0; i < n_pairs; ++i) {
	size_t j = pairs[i];
	lsq_add_one_to_U(U, r1 + j, r2 + j);
    }
    return lsq_fit_core(U, R);
}

static int
superimpose_pairs(struct RPoint *r1,
		  struct RPoint *r2,
		  const size_t *pairs,
		  size_t n_pairs,
		  size_t n_atoms_total) /* should it be n_pairs total ??? */
{
    int err=0;
    float rmat[3][3];
    remove_com_pairs(r1, n_atoms_total, pairs, n_pairs);
    remove_com_pairs(r2, n_atoms_total, pairs, n_pairs);
    err = lsq_fit_pairs(rmat, r1, r2, pairs, n_pairs);
    if(!err) {
	rotate_points(n_atoms_total, rmat, r1);
    }
    return err;
}

static size_t
find_matches(size_t *pairs,
	     const struct RPoint *r1,
	     const struct RPoint *r2,
	     float dist_thresh,
	     size_t n_atoms_total)
{
    size_t i, n_matches=0;
    for(i = 0; i < n_atoms_total; ++i) {
	if(rpoint_dist(r1 + i, r2 + i) < dist_thresh) {
	    pairs[n_matches] = i;
	    n_matches++;
	}
    }
    return n_matches;
}

static void
copy_size_t_array(size_t *dest, const size_t *src, size_t n_elements)
{
    memcpy(dest, src, n_elements * sizeof(*dest));
}

static int
maxsub_extend(size_t *pairs_extended, /* returned M in the pseudocode */
	      size_t *n_pairs_extended,
	      struct RPoint *r1,
	      struct RPoint *r2,
	      size_t n_pairs_seed,
	      size_t n_atoms_total,
	      float dist_thresh)
{
    int err=0;
    size_t i;
    const size_t K=4;
    size_t n_pairs = n_pairs_seed;

    *n_pairs_extended = 0;
    
    for(i = 0; i < K && !err; ++i) {
	err = superimpose_pairs(r1, r2, pairs_extended, n_pairs, n_atoms_total);
	if(!err) {
	    float thresh = (float) K *  dist_thresh / (float) (i + 1);
            /* K and i are exchanged compared to pseudocode in the maxsub paper (Siew et al.) ! */
	    n_pairs = find_matches(pairs_extended, r1, r2, thresh, n_atoms_total);
	}
    }	
    /* last superposition, set n_pairs_total */
    err = superimpose_pairs(r1, r2, pairs_extended, n_pairs, n_atoms_total);
    if(!err) {
	*n_pairs_extended = find_matches(pairs_extended, r1, r2, dist_thresh, n_atoms_total);
    }
    return err;
}

static struct pair_set*
pair_set_get_subset(const struct pair_set *base,
		    const size_t *index_list,
		    size_t n)
{
    struct pair_set *subset=NULL;
    size_t i;
    subset = pair_set_new(n);
    for(i = 0; i < n; ++i) {
	size_t ii = index_list[i];
	pair_set_add_pair(subset,
			  base->indices[ii][0],
			  base->indices[ii][1]);
    }
    return subset;
}

struct pair_set*
maxsub_core(const struct pair_set *alignment,
	    size_t seedlen,
	    struct RPoint *bb_source,
	    struct RPoint *bb_target,
	    float dist_thresh)
{
   struct pair_set *alignment_no_gaps=NULL;
   struct pair_set *best_match_pairset=NULL;
   size_t *largest_pair_set, n_pairs_max=0;
   size_t *pairs, n_pairs=0;
   size_t n_pairs_align;
   size_t i, j;

   alignment_no_gaps = pairset_without_gaps(alignment);
   n_pairs_align = alignment_no_gaps->n;

   seedlen = seedlen < n_pairs_align ? seedlen : n_pairs_align;
   
   largest_pair_set = E_MALLOC(n_pairs_align * sizeof(*largest_pair_set));
   pairs = E_MALLOC(n_pairs_align * sizeof(*largest_pair_set));
   
   for(i = 0; i <= n_pairs_align - seedlen; ++i) {
       for(j = 0; j < seedlen; j++) {
	   pairs[j] = i + j;
       }
       maxsub_extend(pairs,
		     &n_pairs,
		     bb_source,
		     bb_target,
		     seedlen,
		     n_pairs_align,
		     dist_thresh);
       /* check if new pairset is better than old best (keep an optimum) */
       if(n_pairs > n_pairs_max) {
	   copy_size_t_array(largest_pair_set, pairs, n_pairs);
	   n_pairs_max = n_pairs;
       }
   }

   best_match_pairset = pair_set_get_subset(alignment_no_gaps,
					    largest_pair_set,
					    n_pairs_max);
   pair_set_destroy(alignment_no_gaps);
   
   free_if_not_null(pairs);
   free_if_not_null(largest_pair_set);
   return best_match_pairset;
}

struct pair_set*
maxsub(const struct pair_set *alignment,
       const struct rna_coord *source,
       const struct rna_coord *target,
       float dist_thresh,
       size_t seedlen)
{
    struct pair_set *best_match_pairset=NULL;
    struct RPoint *bb_source, *bb_target;
    size_t n_pairs_align;
    const enum rna_atom_types atomtype[] = {C4};

    n_pairs_align = pair_set_get_netto_alignmentlength(alignment);
    bb_source = E_MALLOC(n_pairs_align * sizeof(*bb_source));
    bb_target = E_MALLOC(n_pairs_align * sizeof(*bb_target));

    get_backbone_atoms(alignment, source, target, bb_source, bb_target, NULL, atomtype, 1);

    best_match_pairset = maxsub_core(alignment, seedlen, bb_source, bb_target, dist_thresh);

    free_if_not_null(bb_source);
    free_if_not_null(bb_target);
     
    return best_match_pairset;
}

/* 
 * like maxsub extend, but instead of using a seed get close 
 * residues in a superimposed alignment
 */

static int
maxsub_refine_core(size_t *pairs_extended,
		   size_t *n_pairs_extended,
		   struct RPoint *r1,
		   struct RPoint *r2,
		   size_t n_atoms_total,
		   float dist_thresh)
{
    int err = 0;
    size_t n_initial_matches;

    n_initial_matches = find_matches(pairs_extended, r1, r2, 6 * dist_thresh, n_atoms_total);

    err = maxsub_extend(pairs_extended, /* returned M in the pseudocode */
			n_pairs_extended,
			r1,
			r2,
			n_initial_matches,
			n_atoms_total,
			dist_thresh);
    return err;
}

struct pair_set*
maxsub_refine(struct RPoint *r1,
	      struct RPoint *r2,
	      float dist_thresh,
	      const struct pair_set *pairset_no_gaps)
{
    int err = 0;
    size_t n = pairset_no_gaps->n;
    size_t n_pairs_refined=0;
    size_t *pairslist_refined=NULL;
    struct pair_set *pairset_refined=NULL;

    pairslist_refined = E_MALLOC(n * sizeof(*pairslist_refined));

    err = maxsub_refine_core(pairslist_refined, &n_pairs_refined, r1, r2, n, dist_thresh);

    if(!err) {
	pairset_refined = pair_set_get_subset(pairset_no_gaps,
					      pairslist_refined,
					      n_pairs_refined);
    }
    free_if_not_null(pairslist_refined);
    return pairset_refined;
}

/*
 * get_backbone c4 atoms
 */
static size_t
get_backbone_c4(const struct pair_set *pairset,
		const struct rna_coord *rna1,
		const struct rna_coord *rna2, 
		struct RPoint *list_1,
		struct RPoint *list_2)
{
    return get_backbone_atoms_one_type(pairset,
				       rna1,
				       rna2, 
				       list_1,
				       list_2,
				       C4);
}


/*
 * gdt-ts score for c4 backbone
 */

static void
swap_coords(struct RPoint *bb_target, size_t i, size_t j)
{
    struct RPoint swap;
    swap = bb_target[i];
    bb_target[i] = bb_target[j];
    bb_target[j] = swap;
}

float
rna_coord_gdt_ts(const struct pair_set *pairset, 
		 const struct rna_coord *source,
		 const struct rna_coord *target)
{
    static const char *this_sub = "rna_coord_gdt_ts";
    struct RPoint cm_source, cm_target; /* centres of mass */
    struct RPoint *bb_source, *bb_target; /* lists of (c4-)backbone atoms */
    size_t i, distmax_i;
    float rmat[3][3]; /* rotation matrix */
    int e;
    size_t msize_bb;
    const float THRESHOLDS[] = {8.0, 4.0, 2.0, 1.0};
    const size_t N_THRESH = 4;
    size_t thresh_i = 0, n_atoms;
    float dist, dist_temp;
    int matched = 0;
    
    /* get atom lists of both structures */
    msize_bb = pairset->n * sizeof(*bb_source);
    bb_source = E_MALLOC(msize_bb);
    bb_target = E_MALLOC(msize_bb);
    n_atoms = get_backbone_c4(pairset, source, target, bb_source, bb_target);
    
    while(n_atoms > 3 && thresh_i < N_THRESH) {
	
	/* translate both chains to the centre of mass */
	cm_source = calc_CM(n_atoms, bb_source);
	cm_target = calc_CM(n_atoms, bb_target);
	
	apply_trans(&cm_source, bb_source, n_atoms);
	apply_trans(&cm_target, bb_target, n_atoms);
	
	/* least squares fit */
	e = lsq_fit(n_atoms, bb_source, bb_target, rmat); 
	if(e) {
	    err_printf(this_sub, "could not find rotation matrix\n");
	    matched = -1;
	    goto exit;
	}
	
	/* rotate source */
	rotate_points(n_atoms, rmat, bb_source);
	
	/* find the largest index */
	dist = rpoint_dist(bb_source, bb_target);
	distmax_i = 0;
	for(i = 1; i < n_atoms; ++i) {
	    dist_temp = rpoint_dist(bb_source + i, bb_target + i); 
	    if(dist_temp > dist) {
		dist = dist_temp;
		distmax_i = i;
	    }
	}
	
	/* is a new_threshold reached? (increase index until a threshold is not fulfilled) */
	while(thresh_i < N_THRESH) {
	    if(dist > THRESHOLDS[thresh_i])
		break;
	    matched += n_atoms;
	    thresh_i++;
	}
	
	/* swap in bb_source and bb_target */
	--n_atoms;
	swap_coords(bb_source, distmax_i, n_atoms);
    	swap_coords(bb_target, distmax_i, n_atoms);
    }
    
exit:
    free_if_not_null(bb_source);
    free_if_not_null(bb_target);
    
    return (float) matched / (4.0 * pair_set_get_netto_alignmentlength(pairset));
}

int
rmsd_fixed_chainlen(float *rmsd,
		    const struct rna_coord *rna_a,
		    const struct rna_coord *rna_b,
		    size_t start_i1,
		    size_t start_i2,
		    const struct size_t_array *positions,
		    const struct rna_atom_types_array*atom_types,
		    enum yes_no stop_if_atoms_missing)
{
    const char *this_sub="rmsd_fixed_chainlen";
    struct RPoint *bb1=NULL, *bb2=NULL;
    size_t i, j, k=0;
    int err=0;
    bb1 = E_MALLOC(atom_types->size * positions->size * sizeof(*bb1));
    bb2 = E_MALLOC(atom_types->size * positions->size * sizeof(*bb2));
    for(i = 0; i < positions->size; i++) {
	size_t pos = positions->vals[i];
	for(j = 0; !err && j < atom_types->size; j++) {
	    const struct rna_atom*atom1 = get_const_atom_ptr(atom_types->vals[j],
							     rna_a->bb + start_i1 + pos);
	    const struct rna_atom*atom2 = get_const_atom_ptr(atom_types->vals[j],
							     rna_b->bb + start_i2 + pos);
	    int atom_missing = !(atom1->is_set && atom2->is_set);
	    if(!atom_missing) {
		bb1[k] = atom1->r;
		bb2[k] = atom2->r;
		k++;
	    }
	    if(stop_if_atoms_missing) {
		err = atom_missing;
	    }
	}
    }
    if(!err) {
	err = (k < 4);
    }
    if(!err) {
	err = rmsd_superpos_core(NULL, rmsd, bb1, bb2, k);
    }
    free_if_not_null(bb1);
    free_if_not_null(bb2);
    if(err) {
	err_printf(this_sub, "failed to compute rmsd for two fragments\n");
    }
    return err;
}

int
compute_rmsds_matches(float *rmsds,	/* needs to be same size as matches and pairs */
		      const struct double_index*pairs,
		      const struct continuous_match*matches,
		      const struct rna_coord_set*chainset,
		      size_t fraglen)
{
    const char*this_sub="compute_rmsds";
    int err=0;
    size_t i;
    for(i = 0; !err && i < pairs->size; ++i) {
	err = rna_coord_rmsd_continuous_match(NULL,
					      rmsds + i,
					      matches[i],
					      chainset->chains[pairs->index_a->vals[i]],
					      chainset->chains[pairs->index_b->vals[i]],
					      fraglen);
    }
    if(err) {
	err_printf(this_sub, "failed at i=%zu\n", i);
    }
    return err;
}

void
rna_coord_move(struct rna_coord*rna,
	       const struct RPoint *r_move,
	       void (*rfunc) (struct RPoint*, const struct RPoint*))
{
    size_t i;
    enum rna_atom_types atom_type;
    for(i = 0; i < rna->size; ++i) {
	for(atom_type = P; atom_type <= C1; ++atom_type) {
	    struct rna_atom*atom = get_atom_ptr(atom_type, rna->bb + i);
	    if(atom->is_set) {
		rfunc(&atom->r, r_move);
	    }
	}
    }
}

void
rna_coord_rotate(struct rna_coord*rna,
		 const struct rotation_matrix *rot_mat)
{
    size_t i;
    enum rna_atom_types atom_type;
    for(i = 0; i < rna->size; ++i) {
	for(atom_type = P; atom_type <= C1; ++atom_type) {
	    struct rna_atom*atom = get_atom_ptr(atom_type, rna->bb + i);
	    if(atom->is_set) {
		rotate_rpoint(&atom->r, rot_mat);
	    }
	}
    }
}

struct rna_coord*
rna_coord_transform_superposed(const struct rna_coord*rna_in,
			       const struct transformation *transform)
{
    struct rna_coord *rna_transformed=NULL;
    rna_transformed = rna_coord_copy(rna_in);
    if(rna_transformed) {
	rna_coord_move(rna_transformed, &transform->com_a, &rsubtract);
	rna_coord_rotate(rna_transformed, &transform->rotate_a);
	rna_coord_move(rna_transformed, &transform->com_b, &radd);
    }
    return rna_transformed;
}
