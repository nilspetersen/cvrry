#ifndef ALPHAOPT_ENERGY_H
#define ALPHAOPT_ENERGY_H

#include "suffix_arr.h"

enum match_energy_type {
    Z_LINEAR,
    Z_LOGISTIC,
    MEAN_SHIFT
};

struct match_energy_func {
    int (*func)(float*, const void*, const struct continuous_match*, float);
    const void *params;
    enum match_energy_type type;
};

void
match_energy_func_destroy(struct match_energy_func*efunc);

struct match_energy_func*
match_energy_func_copy(const struct match_energy_func*efunc);

int
match_energy_get_energy(float*energy,
			const struct match_energy_func*efunc,
			const struct continuous_match*match,
			float rmsd);

/* CONSTRUCTORS FOR SPECIFIC SCORING METHODS */

struct match_energy_func*
make_z_score_energy_func(size_t size,
			 const float*means,
			 const float*sigmas,
			 float shift,
			 float min);

struct match_energy_func*
make_z_logistic_energy_func(size_t size,
			    const float*means,
			    const float*sigmas,
			    float shift,
			    float slope);

struct match_energy_func*
make_mean_logistic_energy_func(size_t size,
			       const float*means,
			       float shift,
			       float slope);

#endif
