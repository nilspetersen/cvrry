#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "mprintf.h"
#include "e_malloc.h"
#include "rna_frags.h"
#include "score_mat.h"
#include "rna_alphabet.h"


/* ----------------------- dist -----------------------------
 * Calculates the L2 norm of the difference of two vectors.
 */
static float
dist (size_t n_elements, float *u, float *v)
{
    size_t i;
    float ssd=0;  /* Sum of squares of the differences */
    for (i = 0; i < n_elements; ++i) {
        float diff = u[i] - v[i];        
        ssd += diff * diff;
    }
    return sqrtf(ssd);
}

/* ----------------------- score -----------------------------
 * Makes the distances converge by calculating 1 / (1 + distance).
 */
static float
score (float distance)
{
    return 1 / (1 + distance);
}

/* ---------------- fill_matrix_cv ---------------------------
 * Fills the score matrix with the distance of the feature vectors
 * of the corresponding fragments.
 * Arguments:
 * score_mat: Score matrix to be filled.
 * coverage_mat: Empty matrix to track the fragment coverage of each
 *               matrix cell.
 * frags1, frags2: Collection fragments to compute the scores.
 */
void
fill_matrix_cv (struct score_mat *score_mat,
		struct score_mat *coverage_mat,
                const struct rna_frags *frags1,
		const struct rna_frags *frags2)
{
    size_t i, j, k;
    /* Calculate distance between fragments and put it into the matrix */
    for (i = 0; i < frags1->n_frags; ++i) {
        for (j = 0; j < frags2->n_frags; ++j) {
            float d = dist(frags1->n_par, frags1->frags[i], frags2->frags[j]);
            float s = score(d);
            /* Add one for the matrix border */
            size_t pos_i = frags1->seq_i[i] + 1;
            size_t pos_j = frags2->seq_i[j] + 1;
            for (k = 0; k < frags1->xmer; ++k) {
                score_mat->mat[pos_i + k][pos_j + k] += s;
                /* How many fragments cover each position?
                 * Will be used for scaling the scores. */
                coverage_mat->mat[pos_i + k][pos_j + k] += 1;
            }
        }
    }
}

void
fill_matrix_alphabet(struct score_mat *score_mat,
		     struct score_mat *coverage_mat,
		     const struct rna_coord_seq *seq1,
		     const struct rna_coord_seq *seq2,
		     const struct float_matrix *substitution_matrix)
{
    size_t i, j, k, pos_i, pos_j;
    float score;
    const size_t fraglen = seq1->fraglen;
    for(i = 0; i < rna_coord_seq_size(seq1); ++i) {
	for(j = 0; j < rna_coord_seq_size(seq2); ++j) {
	    score = float_matrix_get(substitution_matrix,
				     (size_t) seq1->seq->vals[i],
				     (size_t) seq2->seq->vals[j]);
	    pos_i = seq1->positions->vals[i] + 1;
	    pos_j = seq2->positions->vals[j] + 1;
	    for(k = 0; k < fraglen; ++k) {
		score_mat->mat[pos_i + k][pos_j + k] += score;
		coverage_mat->mat[pos_i + k][pos_j + k] += 1;
	    }
	}
    }
}

void
fill_matrix_alphabet_unit_score(struct score_mat *score_mat,
				const struct rna_coord_seq *seq1,
				const struct rna_coord_seq *seq2)
{
    size_t i, j, k, pos_i, pos_j;
    const float score=1.0;
    const size_t fraglen = seq1->fraglen;
    for(i = 0; i < rna_coord_seq_size(seq1); ++i) {
	if(rna_coord_seq_is_break_char(seq1, i)) {
	    continue;
	}
	for(j = 0; j < rna_coord_seq_size(seq2); ++j) {
	    if(seq1->seq->vals[i] == seq2->seq->vals[j]) {
		pos_i = seq1->positions->vals[i] + 1;
		pos_j = seq2->positions->vals[j] + 1;
		for(k = 0; k < fraglen; ++k) {
		    score_mat->mat[pos_i + k][pos_j + k] += score;
		}
	    }
	}
    }
}

void
fill_matrix_alphabet_significance_weighted(struct score_mat *score_mat,
					   const struct rna_coord_seq *seq1,
					   const struct rna_coord_seq *seq2)
{
    size_t i, j, k, pos_i, pos_j;
    int ci, cj;
    float score;
    const size_t fraglen = seq1->fraglen;
    for(i = 0; i < rna_coord_seq_size(seq1); ++i) {
	if(rna_coord_seq_is_break_char(seq1, i)) {
	    continue;
	}
	for(j = 0; j < rna_coord_seq_size(seq2); ++j) {    
	    ci = seq1->seq->vals[i];
	    cj = seq2->seq->vals[j];
	    if(ci == cj) {
		score = -seq1->log_p->vals[ci];
		pos_i = seq1->positions->vals[i] + 1;
		pos_j = seq2->positions->vals[j] + 1;
		for(k = 0; k < fraglen; ++k) {
		    score_mat->mat[pos_i + k][pos_j + k] += score;
		}
	    }
	}
    }
}

void
add_basepair_bonus(struct score_mat *score_mat,
		   const struct rna_coord *rna1,
		   const struct rna_coord *rna2,
		   float bonus)
{
    size_t i, j;
    for(i = 0; i < rna1->size; ++i) {
	for(j = 0; j < rna2->size; ++j) {
	    if(rna1->basepair_info[i] == rna2->basepair_info[j]) {
		score_mat->mat[i + 1][j + 1] += bonus;
	    }
	}
    }
}

void
add_base_pair_bonus_coord_seq_fullseq(struct score_mat *score_mat,
				      const struct rna_coord_seq *seq1,
				      const struct rna_coord_seq *seq2,
				      const struct rna_coord *rna1,
				      const struct rna_coord *rna2,
				      float bonus)
{
    size_t i, j;
    int pi, pj;
    for(i = 0; i < rna_coord_seq_size(seq1); ++i) {
	pi = seq1->coord_positions->vals[i];
	if(pi < 0) {
	    continue;
	}
	for(j = 0; j < rna_coord_seq_size(seq2); ++j) {
	    pj = seq2->coord_positions->vals[j];
	    if(pj < 0) {
		continue;
	    }    
	    if(rna1->basepair_info[pi] == rna2->basepair_info[pj]) {
		score_mat->mat[i + 1][j + 1] += bonus;
	    }
	}
    }
}

int
add_base_pair_bonus_alphabet(struct score_mat *score_mat,
			     const struct rna_coord_seq *seq1,
			     const struct rna_coord_seq *seq2,
			     const struct rna_coord *rna1,
			     const struct rna_coord *rna2,
			     float bonus)
{
    const char*this_sub="add_base_pair_bonus_alphabet";
    switch(seq1->type) {
    case RNA_COORD_SEQ_SIMPLE:
	add_basepair_bonus(score_mat,
			   rna1,
			   rna2,
			   bonus);
	break;
    case RNA_COORD_SEQ_FULLSEQ:
	add_base_pair_bonus_coord_seq_fullseq(score_mat,
					      seq1,
					      seq2,
					      rna1,
					      rna2,
					      bonus);
	break;
    default:
	err_printf(this_sub, "base pair bonus not implemented for this sequence type\n");
	return 1;
    }
    return 0;
}

/* TODO: a function that automatically checks the sequence type and chooses the best basepair scoring - here or in align main function ??? */


/* ---------------- scale_marginal_scores ---------------------------
 * Scales the residues' scores according to fragment coverage at the
 * respective position. That means: Scores close to chain breaks and
 * to the start/end of the sequence will get multiplied by k / coverage.
 * Arguments:
 * score_mat: Score matrix to be filled/scaled.
 * coverage_mat: Coverage matrix which indicates the number of fragments
 *               that cover the matrix cell.
 * xmer: span of the fragments (e.g. 7 nucleotides)
 */
void
scale_marginal_scores (struct score_mat *score_mat,
		       const struct score_mat *coverage_mat,
		       size_t xmer,
		       float scale_factor)
{
    size_t i, j;
    float coverage, xmer_f=(float) xmer;
    /* Scale the scores according to fragment coverage */
    /* ignore first and last index because matrix border is irrelevant */
    for (i = 1; i < score_mat->n_rows - 1; ++i) {
        for (j = 1; j < score_mat->n_cols - 1; ++j) {
            if (coverage_mat->mat[i][j] < xmer && coverage_mat->mat[i][j] > 0) {
		coverage = coverage_mat->mat[i][j];
		score_mat->mat[i][j] += (scale_factor
					 * (xmer_f - coverage) 
					 * score_mat->mat[i][j] / coverage);
	    }
        }
    }
}

void
fill_matrix_single_letter_unit_score(struct score_mat *score_mat,
				     const struct rna_coord_seq *seq1,
				     const struct rna_coord_seq *seq2)
{
    size_t i, j, pos_i, pos_j;
    for(i = 0; i < rna_coord_seq_size(seq1); ++i) {
	if(rna_coord_seq_is_break_char(seq1, i)) {
	    continue;
	}
	for(j = 0; j < rna_coord_seq_size(seq2); ++j) {
	    if(seq1->seq->vals[i] == seq2->seq->vals[j]) {
		pos_i = seq1->positions->vals[i] + 1;
		pos_j = seq2->positions->vals[j] + 1;
		score_mat->mat[pos_i][pos_j] = 1.0;
	    }
	}
    }
}

void
fill_matrix_single_letter_significance_weighted(struct score_mat *score_mat,
						const struct rna_coord_seq *seq1,
						const struct rna_coord_seq *seq2)
{
    size_t i, j, pos_i, pos_j;
    int ci, cj;
    for(i = 0; i < rna_coord_seq_size(seq1); ++i) {
	if(rna_coord_seq_is_break_char(seq1, i)) {
	    continue;
	}
	for(j = 0; j < rna_coord_seq_size(seq2); ++j) {    
	    ci = seq1->seq->vals[i];
	    cj = seq2->seq->vals[j];
	    if(ci == cj) {
		pos_i = seq1->positions->vals[i] + 1;
		pos_j = seq2->positions->vals[j] + 1;
		score_mat->mat[pos_i][pos_j] = -seq1->log_p->vals[ci];
	    }
	}
    }
}
