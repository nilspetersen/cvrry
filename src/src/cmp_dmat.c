/*
 * 6 Dec 2001
 *
 * For calculating and comparing alpha carbon-based distance
 * matrices.
 * The main complication is that the model and reference
 * structure are usually different sizes, so the calculation is
 * done on the common sites, marked in the "mask" array.
 *
 * $Id$
 */


#include <math.h>
#include <stdlib.h>
#include <string.h>

#include "cmp_dmat.h"
#include "coord.h"
#include "e_malloc.h"
#include "pair_set.h"
#include "rna_coord.h"
#include "yesno.h"
#include "rna_get_atoms.h"

#define N_THRESH_GDTTS 4

/* ---------------- local structures --------------------------
 */
struct dmat {
    float *d;            /* Actual distance matrix */
    size_t n;             /* num atoms */
    size_t sz;            /* Size = (n(n-1))/2 for triangular matrices */
};

struct dme_res {
    size_t sz;            /* size of DME matrix */
    size_t n_left;        /* After iterating, how many are left */
    float dme;            /* Distance matrix error */
};

/* ---------------- alloc_dmat --------------------------------
 */
static struct dmat *
alloc_dmat (struct dmat *dm, size_t n)
{
    size_t tomall;
    dm->n = n;
    dm->sz = (n * (n - 1)) / 2;
    tomall = dm->sz * sizeof (dm->d[0]);
    dm->d = E_MALLOC (tomall);
    memset (dm->d, 0, tomall);
    return dm;
}

/* ---------------- del_dmat ----------------------------------
 */
static void
del_dmat (struct dmat *dm)
{
    dm->sz = 0;
    dm->n = 0;
    free (dm->d);
}

/* ---------------- flt_compar --------------------------------
 */
static int
flt_compar (const void *p, const void *q)
{
    const float f1 = *(float *)p;
    const float f2 = *(float *)q;
    if ( f1 > f2)
        return -1;
    if ( f1 < f2)
        return 1;
    return 0;
}

/* ---------------- get_dme    --------------------------------
 */
static struct dme_res
get_dme(const struct dmat d1, const struct dmat d2, float thresh)
{
    float *f1, *f2, *last;
    float sum = 0;
    struct dme_res result;
    memset (&result, 0, sizeof (result));
    result.sz = d1.sz;
    result.n_left = d1.sz;
    last = d1.d + d1.sz;
    if ( d1.n < 2 ) {
        result.dme = 0;
    } else if (thresh == 0.0) {
        for (f1 = d1.d, f2 = d2.d; f1 < last; f1++, f2++) {
            float diff = *f1 - *f2;
            sum += diff * diff;
        }
        result.dme = sqrtf (sum / d1.sz);
    } else {
        float *p;
        float thresh2, tmp_dme2;
        struct dmat d_tmp;
        size_t nleft;
        alloc_dmat (&d_tmp, d1.n);

        nleft = d_tmp.sz;
        p = d_tmp.d;

        for (f1 = d1.d, f2 = d2.d; f1 < last; f1++, f2++, p++) {
            float diff = *f1 - *f2;
            float diff2 = diff * diff;
            sum += diff2;
            *p = diff2;
        }
        tmp_dme2 = sum / d1.sz;
        result.dme = sqrtf (tmp_dme2);
        thresh2 = thresh * thresh;
        p = d_tmp.d;
        qsort (p, d_tmp.sz, sizeof (p[0]), flt_compar);
        while ((tmp_dme2 > thresh2) && (--nleft > 0)) {
            sum -= *p;
            tmp_dme2 = sum / nleft;
            p++;
        }
        result.n_left = nleft;
        del_dmat (&d_tmp);
    }
    return result;
}

/* #################### RNA-specific code ##############################
 */

/* ----------------------- dist_rpoints --------------------------------
 * Calculates the L2 norm of the difference of two <RPoint>s.
 */
static float
dist_rpoints (const struct RPoint *u, const struct RPoint *v)
{
    float dx = u->x - v->x;
    float dy = u->y - v->y;
    float dz = u->z - v->z;   
    return sqrtf(dx*dx + dy*dy + dz*dz);
}

static void
fill_dmat_rpoint(struct dmat *dm, const struct RPoint *r, size_t n)
{
    size_t i, j;
    float *dm_contents = dm->d;
    for(i = 0; i < n; ++i) {
	for(j = i + 1; j < n; ++j) {
	    *dm_contents = dist_rpoints(r + i, r + j);
	    dm_contents++;
	}
    }
}

static float
dmat_score_rna(const struct pair_set *pset,
	       const struct rna_coord *rna1,
	       const struct rna_coord *rna2,
	       void *parameters,
	       float val_nopairs,
	       float (*score_function) (const struct dmat dm1, const struct dmat dm2, const void*parameters))
{
    float ret=val_nopairs;
    size_t n_atoms;
    struct dmat dm1, dm2;
    struct RPoint *bb1=NULL, *bb2=NULL;
    
    bb1 = E_MALLOC(pset->n * sizeof(*bb1));
    bb2 = E_MALLOC(pset->n * sizeof(*bb2));

    n_atoms = get_backbone_atoms_one_type(pset, rna1, rna2, bb1, bb2, C4);

    if(n_atoms > 1) {
	alloc_dmat(&dm1, n_atoms);
	alloc_dmat(&dm2, n_atoms);
	
	fill_dmat_rpoint(&dm1, bb1, n_atoms);
	fill_dmat_rpoint(&dm2, bb2, n_atoms);

	ret = score_function(dm1, dm2, parameters);
	
	del_dmat(&dm1);
	del_dmat(&dm2);
    }

    free_if_not_null(bb1);
    free_if_not_null(bb2);
    
    return ret;
}

static float
frac_dme(const struct dmat dm1, const struct dmat dm2, const void *thresh_param)
{
    float frac_dme;
    const float *thresh = (const float*) thresh_param;
    struct dme_res result = get_dme(dm1, dm2, *thresh);
    frac_dme = (float) result.n_left / (float) result.sz;
    return frac_dme;
}

float
dme_thresh_rna(const struct pair_set *pset,
	       const struct rna_coord *rna1,
	       const struct rna_coord *rna2,
	       float thresh)
{
    return dmat_score_rna(pset, rna1, rna2, &thresh, 0.0, &frac_dme);
}

struct good_bad_parameters {
    float thresh;
    size_t normalizer;
};

static size_t
count_good(const struct dmat*dm1, const struct dmat*dm2, float thresh)
{
    float thresh_sq = thresh * thresh;
    size_t i, good_count=0;
    for(i = 0; i < dm1->sz; ++i) {
	float diff = dm1->d[i] - dm2->d[i];
	if(diff*diff <= thresh_sq) {
	    good_count++;
	}
    }
    return good_count;
}

static float
score_good_bad(const struct dmat dm1, const struct dmat dm2, const void *params_void)
{
    const struct good_bad_parameters *params = (const struct good_bad_parameters*) params_void;
    size_t good_count, bad_count;
    good_count = count_good(&dm1, &dm2, params->thresh);
    bad_count = dm1.sz - good_count;
    return ((float) good_count - (float) bad_count) / (float) params->normalizer;
}

static struct good_bad_parameters
good_bad_parameters_get(const struct rna_coord *rna1,
			const struct rna_coord *rna2,
			float thresh)
{
    struct good_bad_parameters params;
    params.thresh = thresh;
    params.normalizer = rna1->size < rna2->size ? rna1->size : rna2->size;
    params.normalizer = params.normalizer * (params.normalizer - 1) / 2;  
    return params;
}

float
good_bad_rna(const struct pair_set *pset,
	     const struct rna_coord *rna1,
	     const struct rna_coord *rna2,
	     float thresh)
{
    struct good_bad_parameters params = good_bad_parameters_get(rna1, rna2, thresh);
    return dmat_score_rna(pset, rna1, rna2, &params, 0.0, &score_good_bad);
}

static float
percentage_of_similar_distances(const struct dmat dm1,
				const struct dmat dm2,
				const void *params_void)
{
    const struct good_bad_parameters *params = (const struct good_bad_parameters*) params_void;
    return count_good(&dm1, &dm2, params->thresh) / (float) params->normalizer;
}

float
percentage_of_similar_distances_rna(const struct pair_set *pset,
				    const struct rna_coord *rna1,
				    const struct rna_coord *rna2,
				    float thresh)
{
    struct good_bad_parameters params = good_bad_parameters_get(rna1, rna2, thresh);
    return dmat_score_rna(pset, rna1, rna2, &params, 0.0, &percentage_of_similar_distances);    
}

struct params_gdtts {
    size_t normalizer;
    float dist_thresholds[N_THRESH_GDTTS];
};

static float
distmat_gdtts(const struct dmat dm1,
	      const struct dmat dm2,
	      const void *params_void)
{
    size_t i, j;
    size_t counter=0;
    struct params_gdtts params_sq = *((const struct params_gdtts*) params_void);
    for(j = 0; j < N_THRESH_GDTTS; ++j) {
	params_sq.dist_thresholds[j] *= params_sq.dist_thresholds[j];
    }
    for(i = 0; i < dm1.sz; ++i) {
	float diff_sq = dm1.d[i] - dm2.d[i];
	diff_sq *= diff_sq;
	for(j = 0; j < N_THRESH_GDTTS; ++j) {
	    if(diff_sq < params_sq.dist_thresholds[j]) {
		counter++;
	    }
	}
    }
    return (float) counter / ((float) N_THRESH_GDTTS * params_sq.normalizer);
}

float
dmat_score_fixed_frags(const struct RPoint *bb1,
		       const struct RPoint *bb2,
		       size_t n_atoms,
		       const void *params_void,
		       float (*score_function) (const struct dmat dm1,
						const struct dmat dm2,
						const void*parameters))
{
    float score;
    struct dmat dm1, dm2;
    alloc_dmat(&dm1, n_atoms);
    alloc_dmat(&dm2, n_atoms);
    fill_dmat_rpoint(&dm1, bb1, n_atoms);
    fill_dmat_rpoint(&dm2, bb2, n_atoms);
    score = score_function(dm1, dm2, params_void);
    del_dmat(&dm1);
    del_dmat(&dm2);
    return score;
}

float
distmat_gdtts_fixed_frags(const struct RPoint *bb1,
			  const struct RPoint *bb2,
			  size_t n_atoms)
{
    struct params_gdtts params = {1, {1.0, 2.0, 4.0, 8.0}};
    params.normalizer = n_atoms * (n_atoms - 1) / 2;
    return dmat_score_fixed_frags(bb1,
				  bb2,
				  n_atoms,
				  (const void*) &params,
				  &distmat_gdtts);
}

float
percentage_of_similar_distances_fixed_frags(const struct RPoint *bb1,
					    const struct RPoint *bb2,
					    size_t n_atoms,
					    float thresh)
{
    struct good_bad_parameters params;
    params.thresh = thresh;
    params.normalizer = n_atoms * (n_atoms - 1) / 2;
    return dmat_score_fixed_frags(bb1,
				  bb2,
				  n_atoms,
				  &params,
				  &percentage_of_similar_distances);
}

float
coverage_fracdme_rna(const struct pair_set *pset,
		     const struct rna_coord *rna1,
		     const struct rna_coord *rna2,
		     float thresh)
{
    float frac_dme, coverage;
    size_t n_aligned, minlen;
    frac_dme = dme_thresh_rna(pset, rna1, rna2, thresh);
    n_aligned = pair_set_get_netto_alignmentlength(pset);
    minlen = rna1->size < rna2->size ? rna1->size : rna2->size;
    coverage = ((float) n_aligned) / ((float) minlen);
    return coverage * frac_dme;
}

float
coverage_fracdme_rna_seq(const struct pair_set *mapped_pset,
			 const struct rna_coord_seq*seq1,
			 const struct rna_coord_seq*seq2,
			 const struct rna_coord *rna1,
			 const struct rna_coord *rna2,
			 float thresh)
{
    size_t len1, len2;
    float frac_dme, coverage;
    size_t n_aligned, minlen;
    frac_dme = dme_thresh_rna(mapped_pset, rna1, rna2, thresh);
    n_aligned = pair_set_get_netto_alignmentlength(mapped_pset);
    len1 = rna_coord_seq_nobreakchar_len(seq1);
    len2 = rna_coord_seq_nobreakchar_len(seq2);
    minlen = len1 < len2 ? len1 : len2;
    coverage = ((float) n_aligned) / ((float) minlen);
    return coverage * frac_dme;
}
