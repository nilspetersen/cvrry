#ifndef RNA_MULTALIGN_SCORES_H
#define RNA_MULTALIGN_SCORES_H

#include "rna_multialign.h"

enum dme_norm_type {
    DME_NORM_NONORM,
    DME_NORM_COVERAGE
};

float
rna_multalign_all_pairs_avg_frac_dme(struct rna_multialign*multialign,
				     float thresh,
				     enum dme_norm_type normtype);

int
rna_multialign_write_to_files(const struct rna_multialign*multialign,
			      const char*path_out);

float
rna_multalign_all_pairs_avg_rmsd(struct rna_multialign*multialign);

float
rna_multialign_frac_matches(const struct rna_multialign*multialign);

#endif
