#include "mprintf.h"
#include "chainlist.h"
#include "rna_frag_coord.h"
#include "e_malloc.h"
#include "rna_precompute.h"

static void
print_chain_properties(const struct chain_list*chainlist,
		       const char*db_path)
{
    size_t i;
    struct rna_frag_coord *rna=NULL;
    char *fname_buf = E_MALLOC(get_bufsize_fname_bin_frag_coord(db_path));
    mprintf("PDBID CHAINID Model_i Nucleotides Fragments Chainbreaks\n");
    for(i = 0; i < chainlist->size; ++i) {
	make_fname_frag_coord(fname_buf, db_path, chainlist->chains + i);
	rna = rna_frag_coord_read_binary(fname_buf, NO);
	print_chain_description(chainlist->chains + i);
	if(rna) {
	    print_cvrry_chain_information(rna);
	} else {
	    mprintf("%11i %9i %11i\n", 0, 0, 0);
	}
	rna_frag_coord_destroy(rna);
    }
    free_if_not_null(fname_buf);
}

int
main(int argc, char *argv[])
{
    const char *this_sub="main";
    struct chain_list *chainlist=NULL;
    int err=0;

    if(argc != 3) {
	err_printf(this_sub, "usage: %s dataset_file path_to_cvrry_files\n", argv[0]);
	return EXIT_FAILURE;
    }

    chainlist = read_chain_list(argv[1]);
    err = !chainlist;

    if(!err) {
	print_chain_properties(chainlist, argv[2]);
    }
    
    chain_list_destroy(chainlist);
    return err;
}
