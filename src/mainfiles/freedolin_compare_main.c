#include "rna_alphabet.h"
#include "rna_precompute.h"

int
main(int argc, char*argv[])
{
    const char*this_sub="main";
    const char*dir_seqfiles=NULL;
    const char*fn_chainlist=NULL;
    const char*fn_query_seq=NULL;
    struct chain_list*chainlist=NULL;
    struct rna_coord_seq*seq_1=NULL;
    struct rna_coord_seq*seq_2=NULL;
    char*fn_seqfile_buf=NULL;
    size_t seqfile_name_bufsize;
    int mode=2;
    size_t i;
    struct float_pair scores;
    struct float_pair (*score_func) (const struct rna_coord_seq*,
				     const struct rna_coord_seq*);

    if(argc < 4) {
	mprintf("usage: %s query.seq chainlist seq_datadir [mode]\n", argv[0]);
	return 0;
    }

    fn_query_seq = argv[1];
    fn_chainlist = argv[2];
    dir_seqfiles = argv[3];

    if(argc > 4) {
	mode = atoi(argv[4]);
    }
    
    switch(mode) {
    case 2:
	/* freedolin */
	score_func = &rna_coord_seq_weighted_lcs_sums;
	break;
    case 3:
	/* ursula */
	score_func = &rna_coord_seq_weighted_lcs_max;
	break;
    default:
	err_printf(this_sub, "Mode choice %i not in available choice (2,3)\n", mode);
	return 1;
    }

    seq_1 = rna_coord_seq_read_binary_file(fn_query_seq);
    chainlist = read_chain_list(fn_chainlist);

    seqfile_name_bufsize = get_bufsize_fname_bin_frag_coord(dir_seqfiles);
    fn_seqfile_buf = E_MALLOC(seqfile_name_bufsize * sizeof(*fn_seqfile_buf));

    for(i = 0; i < chainlist->size; ++i) {
	make_fname_seq(fn_seqfile_buf, dir_seqfiles, chainlist->chains + i);
	seq_2 = rna_coord_seq_read_binary_file(fn_seqfile_buf);
	if(!seq_2) {
	    err_printf(this_sub, "could not get sequence to file %s\n", fn_seqfile_buf);
	} else {
	    scores = score_func(seq_1, seq_2);
	    mprintf("%s %s %i %f %f\n",
		    chainlist->chains[i].pdbid,
		    chainlist->chains[i].chain_id,
		    chainlist->chains[i].model_i,
		    scores.first, scores.second);
	}
	rna_coord_seq_destroy(seq_2);
    }

    /* destroy everything */
    free_if_not_null(fn_seqfile_buf);
    rna_coord_seq_destroy(seq_1);
    chain_list_destroy(chainlist);
    return 0;
}
