#include "rna_alphabet.h"

int
main(int argc, char*argv[])
{
    const char*this_sub="main";
    const char*fn_seq_1=NULL;
    const char*fn_seq_2=NULL;
    int mode=2;
    int err=0;
    struct rna_coord_seq*seq_1=NULL;
    struct rna_coord_seq*seq_2=NULL;

    if(argc < 3) {
	mprintf("%s usage: 1.seq 2.seq [mode]\n", argv[0]);
	return 0;
    }

    fn_seq_1 = argv[1];
    fn_seq_2 = argv[2];

    if(argc > 3) {
	mode = atoi(argv[3]);
    }

    seq_1 = rna_coord_seq_read_binary_file(fn_seq_1);
    if(seq_1) {
	seq_2 = rna_coord_seq_read_binary_file(fn_seq_2);
    }

    if(!seq_2) {
	err_printf(this_sub, "failed\n");
	return 0;
    }

    switch(mode) {
    case 0:
	/* lcs */
    {
	struct continuous_match match;
	err = rna_coord_seq_lcs_match(&match, seq_1, seq_2);
	if(!err) {
	    mprintf("LCS: %zu\n", match.len);
	}
    }
	break;
    case 1:
	/* lcs-sum */
    {
	struct size_t_pair sums = rna_coord_seq_lcs_sums(seq_1, seq_2);
	mprintf("LCS_SUM:\nscore A: %zu\nscore B: %zu\n", sums.first, sums.second);	
    }
	break;
    case 2:
	/* freedolin */
    {
	struct float_pair scores = rna_coord_seq_weighted_lcs_sums(seq_1, seq_2);
	mprintf("FREEDOLIN:\nscore A: %f\nscore B: %f\n", scores.first, scores.second);	
    }
    break;
    case 3:
	/* ursula */
    {
	struct float_pair scores = rna_coord_seq_weighted_lcs_max(seq_1, seq_2);
	mprintf("URSULA\nscore A: %f\nscore B: %f\n", scores.first, scores.second);
    }
    break;
    default:
	err_printf(this_sub, "Mode choice %i not in available choice (0,1,2,3)\n", mode);
	err = 1;
    }
	
    rna_coord_seq_destroy(seq_1);
    rna_coord_seq_destroy(seq_2);
    if(err) {
	err_printf(this_sub, "failed\n");
    }
    return 0;
}
