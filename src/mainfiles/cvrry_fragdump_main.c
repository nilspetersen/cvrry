#include "mprintf.h"
#include "chainlist.h"
#include "e_malloc.h"
#include "rna_precompute.h"
#include "rna_frag_coord.h"

static int
dump_frags_chainlist(const char *db_path,
		     const struct chain_list *chainlist)
{
    size_t i;
    int err=0;
    char *fname_buf=NULL;
    struct rna_frag_coord *rna=NULL;
    fname_buf = E_MALLOC(get_bufsize_fname_bin_frag_coord(db_path));
    for(i = 0; i < chainlist->size; ++i) {
	make_fname_frag_coord(fname_buf, db_path, chainlist->chains + i);
	rna = rna_frag_coord_read_binary(fname_buf, YES);
	if(!rna) {
	    err = 1;
	    break;
	}
	rna_frags_dump(rna->frags);
	rna_frag_coord_destroy(rna);
    }
    free_if_not_null(fname_buf);
    return err;
}

int
main(int argc, char *argv[])
{
    const char *this_sub = "main";
    struct chain_list *chainlist=NULL;
    char *fn_chainlist;
    char *indir;
    int err=1;

    if(argc != 3) {
	err_printf(this_sub,
		   "Usage: %s chainlist_file dir_in\n",
		   argv[0]);
	return EXIT_FAILURE;
    }

    fn_chainlist = argv[1];
    indir = argv[2];

    chainlist = read_chain_list(fn_chainlist);
    if(!chainlist) {
	return EXIT_FAILURE;
    }
    err = dump_frags_chainlist(indir, chainlist);

    chain_list_destroy(chainlist);
    
    return err ? EXIT_FAILURE : EXIT_SUCCESS;
}
