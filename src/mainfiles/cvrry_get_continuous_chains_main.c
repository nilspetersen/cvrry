/* SHORT DESCRIPTION
   
   Extract continuous chains (rna chains without breaks) from a set of rna chains.
   E.g. if there is one break in the chain two chains are written to separate files 

   IN:
   - a list with rna chains, given by pdbid, chainid and model number
   - a directory with the corresponding cif files

   OUT:
   - all continuous chains as binary files in the output directory
   - a file that lists all those chains
 */

#include "chainlist.h"
#include "mprintf.h"
#include "cvrry_continuous_chains.h"
#include "cvrry_utils.h"
#include "e_malloc.h"


int
main(int argc, char *argv[])
{
    struct chain_list *chainlist=NULL;
    char *fn_chainlist=NULL, *dir_in=NULL, *dir_out=NULL, *fn_chainlist_out=NULL;
    int err=0;

    if(argc != 5) {
	mprintf("Usage: %s chainlistfile directory_input directory_output new_chainlist_file\n",
		argv[0]);
	return 1;
    }

    fn_chainlist = argv[1];
    dir_in = no_final_slash(argv[2]);
    dir_out = no_final_slash(argv[3]);
    fn_chainlist_out = argv[4];

    if(!err) {
	chainlist = read_chain_list(fn_chainlist);
	err = !chainlist;
    }

    if(!err) { 
	err = read_write_unbroken_rna_backbones_set(chainlist,
						    dir_in,
						    dir_out,
						    fn_chainlist_out);
    }

    chain_list_destroy(chainlist);
    free_if_not_null(dir_in);
    free_if_not_null(dir_out);
    
    return err;
}
