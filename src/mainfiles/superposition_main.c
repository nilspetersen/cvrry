#include <ctype.h>
#include "fio.h"
#include "mprintf.h"
#include "pair_set.h"
#include "rna_frag_coord.h"
#include "psi.h"
#include "yesno.h"

/* quick and dirty implementation to read an alignment to test sara alignments */

static int
read_alignment_index(int *index, FILE *f_in)
{
    char c;
    enum yes_no found_digit=NO;
    *index = 0;
    while((c = fgetc(f_in)) != EOF) {
	if(c == '-') {
	    *index = GAP_INDEX;
	    return 0;
	} else if(isdigit(c)) {
	    found_digit = YES;
	    *index = *index * 10 + (int) (c - '0');
	} else if(found_digit) {
	    return 0;
	}
    }
    return 1;
}

static struct pair_set*
parse_alignment(FILE *f_in)
{
    int err;
    size_t len, i;
    struct pair_set *alignment=NULL;
    err = fscanf(f_in, ">ALIGNLENGTH %zu", &len) != 1;
    if(!err) {
	alignment = pair_set_new(len + 1); /* +1 to not get a bug if alignment is empty */
	alignment->n = len;
    }
    for(i = 0; i < len && !err; ++i) {
	err = read_alignment_index(&alignment->indices[i][0], f_in);
    }
    for(i = 0; i < len && !err; ++i) {
	err = read_alignment_index(&alignment->indices[i][1], f_in);	
    }
    if(err) {
	pair_set_destroy(alignment);
	return NULL;
    }
    return alignment;
}

struct pair_set*
read_alignment_simple_format(const char *filename)
{
    const char *this_sub="read_alignment_simple_format";
    FILE *f_in=NULL;
    struct pair_set *alignment=NULL;
    f_in = mfopen(filename, "r", this_sub);
    if(f_in) {
	alignment = parse_alignment(f_in);
    }
    close_if_open_file(f_in);
    return alignment;
}

int
main(int argc, char *argv[])
{
    const char *this_sub="main";
    struct pair_set *alignment=NULL;
    struct rna_frag_coord *chain_a=NULL;
    struct rna_frag_coord *chain_b=NULL;
    int err=1;
    float psi;
    
    if(argc != 4) {
	mprintf("usage: %s alignfilename cvrry_chainfile_1 cvrry_chainfile_2\n", argv[0]);
	return EXIT_FAILURE;
    }
    alignment = read_alignment_simple_format(argv[1]);
    if(alignment) {
	chain_a = rna_frag_coord_read_binary(argv[2], YES);
    }
    if(chain_a) {
	chain_b = rna_frag_coord_read_binary(argv[3], YES);
    }
    if(chain_b) {
	err = percentage_of_structure_identity(&psi,
					       NULL,
					       NULL,
					       alignment,
					       chain_a->rna_backbone,
					       chain_b->rna_backbone,
					       4,
					       PROSUP_REFINE);
    }
    if(!err) {
	mprintf("psi: %f\n", psi);
    } else {
	err_printf(this_sub, "Failed to compute PSI !!\n");
    }

    pair_set_destroy(alignment);
    rna_frag_coord_destroy(chain_a);
    rna_frag_coord_destroy(chain_b);

    return err ? EXIT_FAILURE : EXIT_SUCCESS;
}
