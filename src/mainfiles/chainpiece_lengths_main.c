/* SHORT DESCRIPTION

   Calculate the length of continuous chains (rna chains without breaks!)

   IN: 
   - a list with chain identifiers
   - a directory with binary files of those chains
   OUT:
   - lengths of the chains
 */

#include "mprintf.h"
#include "rna_coord.h"
#include "chainlist.h"

static int
get_chainpiece_length(size_t *length,
		      const struct chainpiece_description*chainpiece,
		      const char*chaindir)
{
    struct rna_coord*rna=NULL;
    rna = read_binary_chainpiece(chaindir, chainpiece);
    if(!rna) {
	*length = 0;
	return 1;
    }
    *length = rna->size;
    rna_coord_destroy(rna);
    return 0;
}

static int
print_chainpiece_lengths(FILE*stream,
			 const struct chainpiece_list*chainpiece_list,
			 const char*chaindir)
{
    size_t i, length;
    int err=0;
    for(i = 0; !err && (i < chainpiece_list->size); ++i) {
	stream_chainpiece_description(stream, chainpiece_list->chainpieces + i);
	err = get_chainpiece_length(&length, chainpiece_list->chainpieces + i, chaindir);
	mfprintf(stream, " %zu\n", length);
    }
    return err;
}

int
main(int argc, char*argv[])
{
    /* const char *this_sub="main"; */
    struct chainpiece_list*chainpiece_list=NULL;
    int err=0;
    FILE *outstream=stdout;

    if(argc < 3) {
	mprintf("usage: %s chainlistfile chain_directory\n", argv[0]);
	return 1;
    }

    chainpiece_list = read_chainpiece_list(argv[1]);
    err = !chainpiece_list;

    if(!err) {
	err = print_chainpiece_lengths(outstream,
				       chainpiece_list,
				       argv[2]);
    }

    chainpiece_list_destroy(chainpiece_list);
    
    return err;
}
