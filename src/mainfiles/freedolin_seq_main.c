/* make an rna coordinate string with no basepair information but with chain-break and 
   alignment-free annotations
 */

#include "rna_alphabet.h"
#include "cvrry_parse_mmcif.h"


int
main(int argc, char*argv[])
{
    const char*this_sub="main";
    struct rna_coord_alphabet*alphabet=NULL;
    struct rna_coord*rna=NULL;
    struct rna_coord_seq*seq=NULL;
    struct cvrry_cif*dummy_cif=NULL;
    int err=1;

    const char*fn_in_mmcif=NULL;
    int model_i;
    const char*chainid=NULL;
    const char*fn_alphabet=NULL;
    const char*fn_out_seq=NULL;

    if(argc < 5) {
	mprintf("usage: %s input.mmcif chainid model_i alphabet.alphabet output.seq\n", argv[0]);
	return 0;
    }

    fn_in_mmcif = argv[1];
    chainid = argv[2];
    model_i = atoi(argv[3]);
    fn_alphabet = argv[4];
    fn_out_seq = argv[5];

    err = read_mmcif(&dummy_cif,
		     &rna,
		     fn_in_mmcif,
		     chainid,
		     model_i);
    if(!err) {
	compute_chain_breaks(rna);
	alphabet = rna_coord_alphabet_read_binary_file(fn_alphabet);
    }
    if(alphabet) {
	seq = rna_coord_seq_w_break_chars(rna, alphabet);
    }
    if(seq) {
	err = rna_coord_seq_write_binary_file(fn_out_seq, seq);
    } else {
	err = 1;
    }
    rna_coord_alphabet_destroy(alphabet);
    rna_coord_destroy(rna);
    rna_coord_seq_destroy(seq);
    cvrry_cif_destroy(dummy_cif);
    if(err) {
	err_printf(this_sub, "failed\n");
    }
    return err;
}
