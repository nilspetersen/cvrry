#include "rna_comparison.h"
#include "chainlist.h"
#include "mprintf.h"
#include "e_malloc.h"
#include "pair_set.h"
#include "rna_coord.h"
#include "fio.h"
#include "cvrry_pairwise.h"

static int
align_chainlist(struct cvrry_pairwise_options*opt)
{
    const char*this_sub="align_chainlist";
    int err=0;
    struct chain_list *chainlist=read_chain_list(opt->paths.fname_dataset_file);
    if(!chainlist) {
	return 1;
    }
    switch(opt->cmp_type) {
    case ALL_VS_ALL:
	all_pairs_align(opt, chainlist);
	break;
    case ONE_VS_ALL:
	one_vs_all_align(opt,
			 &chainlist->chains[0],
			 chainlist,
			 1);    
	break;
    default:
	err = 1;
	err_printf(this_sub, "Invalid option for chainlist alignment type\n");
    }
    chain_list_destroy(chainlist);    
    return err;
}

static int
align_from_alignlist(struct cvrry_pairwise_options*opt)
{
    int err=0;
    struct align_list *alignlist=NULL;
    alignlist = read_align_list(opt->paths.fname_dataset_file);
    if(!alignlist) {
	return 1;
    }
    alignlist_align(opt, alignlist);
    align_list_destroy(alignlist);
    return err;
}

int
main(int argc, char *argv[])
{
    struct cvrry_pairwise_options *opt=NULL;
    int err=0;

    opt = cvrry_pairwise_options_new();
    err = get_cvrry_pairwise_options(opt, argc, argv);

    if(!err) {
	if(is_align_list(opt->paths.fname_dataset_file)) {
	    /* make alignments for given list of alignments */
	    err = align_from_alignlist(opt);
	} else {
	    /* align all pairs for a list of chains or one vs the rest */
	    err = align_chainlist(opt);
	}
    }

    cvrry_pairwise_options_destroy(opt);

    return err;
}
