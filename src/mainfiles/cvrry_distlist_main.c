
#include "rna_frags.h"
#include "cvrry_parse_mmcif.h"
#include "mprintf.h"

int
main(int argc, char *argv[])
{
    struct cvrry_cif *cif=NULL;
    struct rna_coord *rna=NULL;
    struct dist_list_set*dist_list_set=NULL;
    int err=0;
    size_t max_seq_dist=20;

    if(argc != 4) {
	mprintf("usage: %s pdbidfile chainid model_number\n", argv[0]);
	return 1;
    }
    err = read_mmcif(&cif,
		     &rna,
		     argv[1],
		     argv[2],
		     atoi(argv[3]));
    if(!err) {
	compute_chain_breaks(rna);
	dist_list_set = compute_distlist_set(rna, max_seq_dist, C4);
	err = !dist_list_set;
    }

    if(!err) {
	dist_list_set_dump(dist_list_set);
    }

    dist_list_set_destroy(dist_list_set);
    rna_coord_destroy(rna);
    cvrry_cif_destroy(cif);
    return err;
}
