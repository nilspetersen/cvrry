#include "rna_align.h"
#include "cvrry_defaults.h"
#include "yesno.h"
#include "psi.h"
#include <string.h>
#include "cvrry_utils.h"
#include "rna_seq_coord.h"

int
main(int argc, char*argv[])
{
    const char*this_sub="main";
    const float rmsd_thresh=4.0;
    float psi=-1.0;
    const char*fn_seq_coord_a=NULL;
    const char*fn_seq_coord_b=NULL;
    struct transformation psi_transform;

    struct pair_set*alignment=NULL;
    struct pair_set*pairset_superimposed=NULL;
    struct rna_seq_coord *seq_coord_a=NULL;
    struct rna_seq_coord *seq_coord_b=NULL;
    struct rna_align_params align_params;
    enum score_type scr_type=UNIT_SCORE;
    /* enum score_type scr_type=SIGNIFICANCE_WEIGHTED; */
    int err=1;
    
    align_params.scale_factor = 0.0;
    align_params.base_pair_bonus = 3.265190000000000037e+00;
    align_params.gap_open = 1.458169999999999966e+01;
    align_params.gap_widen = 3.140670000000000073e+00;
    align_params.algn_type = N_AND_W;

    if(argc < 3) {
	mprintf("usage: %s fn_seq_coord_a fn_seq_coord_b [outdir]\n", argv[0]);
	return 1;
    }

    fn_seq_coord_a = argv[1];
    fn_seq_coord_b = argv[2];

    seq_coord_a = rna_seq_coord_read_binary(fn_seq_coord_a);
    if(seq_coord_a) { 
	seq_coord_b = rna_seq_coord_read_binary(fn_seq_coord_b);
    }
    if(seq_coord_b) {
	if(seq_coord_a->seq->type != seq_coord_b->seq->type) {
	    err_printf(this_sub, "sequence types don't match!\n");
	} else {
	    alignment = align_alfons(seq_coord_a, seq_coord_b, &align_params, scr_type);
	}
    }
    if(alignment) {
	if(seq_coord_a->seq->type == RNA_COORD_SEQ_FULLSEQ) {
	    struct pair_set*pset_backmapped = rna_coord_seq_full_backmap_pair_set(seq_coord_a->seq,
										  seq_coord_b->seq,
										  alignment);
	    pair_set_destroy(alignment);
	    alignment = pset_backmapped;
	}
    }
    if(alignment) {
	err = percentage_of_structure_identity(&psi,
					       &pairset_superimposed,
					       &psi_transform,
					       alignment,
					       seq_coord_a->rna_backbone, 
					       seq_coord_b->rna_backbone,
					       rmsd_thresh,
					       /* ITERATIVE_REMOVAL); */
					       PROSUP_REFINE);
    }
    if(!err) {
	mprintf("PSI=%.3f\n", psi);
    }

    if(argc > 3) {
	char *fn_out_a = cif_make_outfilename(fn_seq_coord_a, argv[3]);
	char *fn_out_b = cif_make_outfilename(fn_seq_coord_b, argv[3]);
	if(!err) {
	    transform_mmcif(seq_coord_a->cif, &psi_transform);
	    err = write_mmcif(seq_coord_a->cif, "chaA", fn_out_a);
	}
	if(!err) {
	    err = write_mmcif(seq_coord_b->cif, "chaB", fn_out_b);
	}
	if(!err) {
	    mprintf("Superimposed chains are written to %s and %s\n\n",
		    fn_out_a, fn_out_b);
	}
	free_if_not_null(fn_out_a);
	free_if_not_null(fn_out_b);
    }

    rna_seq_coord_destroy(seq_coord_a);
    rna_seq_coord_destroy(seq_coord_b);
    pair_set_destroy(alignment);
    pair_set_destroy(pairset_superimposed);

    if(err) {
	err_printf(this_sub, "failed\n");
    }
    
    return err;
}
