#include "rna_align.h"
#include "cvrry_defaults.h"
#include "yesno.h"
#include "psi.h"
#include <string.h>
#include "cvrry_utils.h"

int
main(int argc, char *argv[])
{
    const char*this_sub="main";
    const float rmsd_thresh=4.0;
    float psi=-1.0;
    const char*fn_frag_coord_a=NULL;
    const char*fn_frag_coord_b=NULL;
    struct transformation psi_transform;

    struct pair_set*alignment=NULL;
    struct pair_set*pairset_superimposed=NULL;
    struct rna_frag_coord *frag_coord_a=NULL;
    struct rna_frag_coord *frag_coord_b=NULL;
    struct rna_align_params align_params;
    struct norm_params*norm=NULL;
    int err=1;

    align_params.scale_factor = 2.097529999999999950e-01;
    align_params.base_pair_bonus = 4.525020000000000153e-01;
    align_params.gap_open = 4.776939999999999742e+00;
    align_params.gap_widen = 5.568710000000000049e-01;
    align_params.algn_type = N_AND_W;

    if(argc < 3) {
	mprintf("usage: %s fn_frag_coord_a fn_frag_coord_b [outdir]\n", argv[0]);
	return 1;
    }

    fn_frag_coord_a = argv[1];
    fn_frag_coord_b = argv[2];

    frag_coord_a = rna_frag_coord_read_binary(fn_frag_coord_a, NO);
    if(frag_coord_a) { 
	frag_coord_b = rna_frag_coord_read_binary(fn_frag_coord_b, NO);
    }
    if(frag_coord_b) {
	norm = default_normalization();
	rna_frags_normalize(frag_coord_a->frags, norm);
	rna_frags_normalize(frag_coord_b->frags, norm);
	alignment = align(frag_coord_a, frag_coord_b, &align_params);
    }
    
    if(alignment) {
	err = percentage_of_structure_identity(&psi,
					       &pairset_superimposed,
					       &psi_transform,
					       alignment,
					       frag_coord_a->rna_backbone, 
					       frag_coord_b->rna_backbone,
					       rmsd_thresh,
					       PROSUP_REFINE);
    }
    if(!err) {
	mprintf("PSI=%.3f\n", psi);
    }

    if(argc > 3) {
	char *fn_out_a = cif_make_outfilename(fn_frag_coord_a, argv[3]);
	char *fn_out_b = cif_make_outfilename(fn_frag_coord_b, argv[3]);
	if(!err) {
	    transform_mmcif(frag_coord_a->cif, &psi_transform);
	    err = write_mmcif(frag_coord_a->cif, "chaA", fn_out_a);
	}
	if(!err) {
	    err = write_mmcif(frag_coord_b->cif, "chaB", fn_out_b);
	}
	if(!err) {
	    mprintf("Superimposed chains are written to %s and %s\n\n",
		    fn_out_a, fn_out_b);
	}
	free_if_not_null(fn_out_a);
	free_if_not_null(fn_out_b);
    }

    rna_frag_coord_destroy(frag_coord_a);
    rna_frag_coord_destroy(frag_coord_b);
    pair_set_destroy(alignment);
    pair_set_destroy(pairset_superimposed);
    norm_params_destroy(norm);

    if(err) {
	err_printf(this_sub, "failed\n");
    }
    
    return err;
}
