/* SHORT DESCRIPTION

   Compute RMSDs for continuous RNA chain fragments of a fixed length
   or with a fixed pattern

   IN: 
   - a list with continuous chains (broken chains are 
     separated to multiple files)
   - a directory with binary files of those chains
   - fragment length, if -1 the cvrry style pattern (1,2,4,7)
     is chosen instead
   - number of samples, if zero all possible pairs will be enumerated
   - optional: output file were RMSDs are written to
  OUT:
   - RMSDs (optionally written to a file)
 */
#include <stdlib.h>
#include "mprintf.h"
#include "chainlist.h"
#include "rna_coord.h"
#include "e_malloc.h"
#include "lsqf.h"
#include "bin_utils.h"
#include "fio.h"
#include "array.h"
#include "cvrry_indexed_chainset.h"

static int
write_print_rmsd(FILE *outfile, float rmsd)
{
    const char*this_sub = "write_print_rmsd";
    int err=0;
    if(rmsd > 0) {
	if(outfile) {
	    err = m_bin_write(NULL,
			      (void*) &rmsd,
			      sizeof(rmsd),
			      1,
			      outfile,
			      this_sub);
	} else {
	    mprintf("%e\n", rmsd);
	}
    }
    return err;
}


static int
pairwise_rmsds(FILE *outfile,
	       const struct indexed_chainset*chainset)
{
    const char*this_sub="pairwise_rmsds";
    size_t i, j, nfrags;
    float rmsd;
    int err=0;
    nfrags = indexed_chainset_size(chainset);
    for(i = 0; i < nfrags; ++i) {
	for(j = i+1; !err && j < nfrags; ++j) {
	    rmsd = indexed_chainset_pair_rmsd(chainset, i, j);
	    err = write_print_rmsd(outfile, rmsd);
	}
    }    
    if(err) {
	err_printf(this_sub, "failed to compute rmsds\n");
    }
    return err;
}

static int
sampled_rmsds(FILE *outfile,
	      const struct indexed_chainset*chainset,	      
	      size_t n_samples)
{
    size_t i, rand_i, rand_j;
    float rmsd;
    int err=0;
    size_t nfrags = indexed_chainset_size(chainset);
    
    for(i = 0; !err && i < n_samples; ++i) {
	rand_i = rand() % nfrags;
	rand_j = rand() % nfrags;
	
	rmsd = indexed_chainset_pair_rmsd(chainset, rand_i, rand_j);
	err = write_print_rmsd(outfile, rmsd);	
    }
    return err;
}

int
main(int argc, char*argv[])
{
    const char *this_sub="main";
    int err=0;
    FILE *outfile=NULL;
    unsigned int seed=1;
    struct indexed_chainset*chainset=NULL;

    srand(seed);
    
    if(argc < 5) {
	mprintf("usage: %s chainlistfile chain_directory fraglen/pattern nsamples [outfilename]\n",
		argv[0]);
	return 1;
    }

    if(argc == 6) {
	outfile = mfopen(argv[5], "wb", this_sub);
	if(!outfile) {
	    return 1;
	}
    }

    if(!err) {
	int fraglen_or_pattern = atoi(argv[3]);
	chainset = indexed_chainset_load(argv[1], argv[2], fraglen_or_pattern);
	err = !chainset;
    }

    if(!err) {
	size_t n_samples = (size_t) atoi(argv[4]);
	if(n_samples) {
	    err = sampled_rmsds(outfile, chainset, n_samples);
	} else {
	    err = pairwise_rmsds(outfile, chainset);
	}
    }

    indexed_chainset_destroy(chainset);
    close_if_open_file(outfile);
    
    return err;
}
