#include "rna_frags.h"
#include "cmp_dmat.h"
#include "wrapped_matrix.h"
#include "matrix.h"
#include "e_malloc.h"
#include "rna_get_atoms.h"
#include "cvrry_parse_mmcif.h"
#include "mprintf.h"

static void
cmp_frags_lists(float **scores,
		struct RPoint *bb_coords_1,
		struct RPoint *bb_coords_2,
		size_t fraglen,
		size_t nfrags_1,
		size_t nfrags_2,
		const size_t *fragpos_1,
		const size_t *fragpos_2)
{
    const size_t *pi, *pj;
    for(pi = fragpos_1; pi < fragpos_1 + nfrags_1; ++pi) {
	for(pj = fragpos_2; pj < fragpos_2 + nfrags_2; ++pj) {
	    scores[*pi][*pj] = distmat_gdtts_fixed_frags(bb_coords_1 + *pi,
	    						 bb_coords_2 + *pj,
	    						 fraglen);
	}
    }
}

int
compare_all_fragments(struct float_matrix **scores,
		      const struct rna_coord *rna1,
		      const struct rna_coord *rna2,
		      size_t fraglen)
{
    size_t nfrags_1, nfrags_2;
    size_t *fragpos_1=NULL, *fragpos_2=NULL;
    struct RPoint *bb_1=NULL, *bb_2=NULL;
    int err=0;

    fragpos_1 = E_MALLOC(rna1->size * sizeof(*fragpos_1));
    fragpos_2 = E_MALLOC(rna2->size * sizeof(*fragpos_2));
    
    nfrags_1 = find_frags(fragpos_1, fraglen, rna1);
    nfrags_2 = find_frags(fragpos_2, fraglen, rna2);
    
    bb_1 = E_MALLOC(rna1->size * sizeof(*bb_1));
    bb_2 = E_MALLOC(rna2->size * sizeof(*bb_2));
    
    err = get_simple_backbone(bb_1, rna1, C4);
    if(!err) {
	err = get_simple_backbone(bb_2, rna2, C4);
    }

    if(!err) {
	/* TODO: make sure rna->size is larger than fraglen ! */
	*scores = new_float_matrix(rna1->size - fraglen + 1,
				   rna2->size - fraglen + 1,
				   -1.0);
    
	cmp_frags_lists((*scores)->vals,
			bb_1,
			bb_2,
			fraglen,
			nfrags_1,
			nfrags_2,
			fragpos_1,
			fragpos_2);
    }
    
    if(err) {
	float_matrix_destroy(*scores);
	*scores = NULL;
    }
    
    free_if_not_null(bb_1);
    free_if_not_null(bb_2);
    free_if_not_null(fragpos_1);
    free_if_not_null(fragpos_2);
    return err;
}


int
main(int argc, char *argv[])
{
    int err=0;
    struct float_matrix *scores=NULL;
    struct cvrry_cif *cif_1=NULL;
    struct cvrry_cif *cif_2=NULL;
    struct rna_coord *rna1=NULL;
    struct rna_coord *rna2=NULL;
    size_t fraglen=25;

    if(argc < 5) {
	mprintf("usage: %s rna1.cif chainID_1 rna2.cif chainID_2\n", argv[0]);
	return err;
    }
    if(argc > 5) {
	fraglen = atoi(argv[5]);
    }
    
    err = read_mmcif(&cif_1,
		     &rna1,
		     argv[1],
		     argv[2],
		     1);
    if(!err) {
	err = read_mmcif(&cif_2,
			 &rna2,
			 argv[3],
			 argv[4],
			 1);
    }

    if(!err) {
	compute_chain_breaks(rna1);
	compute_chain_breaks(rna2);
    }
    
    if(!err) {
	err = compare_all_fragments(&scores, rna1, rna2, fraglen);
    }
    if(!err) {
	float_matrix_dump("%5g ", scores);
    }
    cvrry_cif_destroy(cif_1);
    cvrry_cif_destroy(cif_2);
    rna_coord_destroy(rna1);
    rna_coord_destroy(rna2);
    float_matrix_destroy(scores);
    return err;
}
