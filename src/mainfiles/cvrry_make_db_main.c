#include "mprintf.h"
#include "chainlist.h"
#include "rna_precompute.h"


int
main(int argc, char *argv[])
{
    const char *this_sub = "main";
    struct chain_list *chainlist=NULL;
    char *fn_chainlist;
    char *indir;
    char *outdir;
    int err=1;

    if(argc < 4) {
	err_printf(this_sub,
		   "Usage: %s chainlist_file dir_in dir_out\n",
		   argv[0]);
	return EXIT_FAILURE;
    }

    fn_chainlist = argv[1];
    indir = argv[2];
    outdir = argv[3];

    chainlist = read_chain_list(fn_chainlist);
    if(!chainlist) {
	return EXIT_FAILURE;
    }

    err = precompute_cvrry_database(chainlist,
				    indir,
				    outdir,
				    NULL);

    chain_list_destroy(chainlist);
    
    return err ? EXIT_FAILURE : EXIT_SUCCESS;
}
