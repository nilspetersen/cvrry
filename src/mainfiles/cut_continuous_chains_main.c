#include "rna_coord.h"
#include "chainlist.h"
#include "cvrry_utils.h"

int
main(int argc, char*argv[])
{
    int err=0;
    char *fn_chainlist_in=NULL, *dir_in=NULL, *dir_out=NULL, *fn_chainlist_out=NULL;
    size_t minlen, maxlen;
    struct chainpiece_list*list_in=NULL, *list_out=NULL;
    struct rna_coord_set*coord_set_in=NULL, *coord_set_out=NULL;
    
    if(argc < 7) {
	mprintf(("Usage: %s chainlistfile directory_input"
		 " directory_output new_chainlist_file minlen maxlen\n"),
		 argv[0]);
	return 1;
    }

    fn_chainlist_in = argv[1];
    dir_in = no_final_slash(argv[2]);
    dir_out = no_final_slash(argv[3]);
    fn_chainlist_out = argv[4];
    minlen = (size_t) atoi(argv[5]);
    maxlen = (size_t) atoi(argv[6]);

    if(!err) {
	list_in = read_chainpiece_list(fn_chainlist_in);
	err = !list_in;
    }
    if(!err) {
	coord_set_in = load_rna_coord_set(dir_in, list_in);
	err = !coord_set_in;
    }
    if(!err) {
	err = continuous_chains_to_pieces(&coord_set_out,
					  &list_out,
					  coord_set_in,
					  list_in,
					  minlen,
					  maxlen);
    }
    if(!err) {
	err = rna_coord_set_write_separate_chains(coord_set_out,
						  list_out,
						  dir_out);
    }
    if(!err) {
	err = write_chainpiece_list(fn_chainlist_out, list_out);
    }    
    rna_coord_set_destroy(coord_set_in);
    rna_coord_set_destroy(coord_set_out);
    chainpiece_list_destroy(list_in);
    chainpiece_list_destroy(list_out);
    free_if_not_null(dir_in);
    free_if_not_null(dir_out);

    return err;
}
