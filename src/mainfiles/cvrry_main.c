#include <string.h>
#include <getopt.h>
#include "mprintf.h"
#include "rna_frag_coord.h"
#include "rna_align.h"
#include "pair_set.h"
#include "pair_set_p.h"
#include "align.h"
#include "e_malloc.h"
#include "lsqf.h"
#include "cvrry_optparse.h"
#include "psi.h"
#include "cvrry_defaults.h"

void
stream_pair_matches(FILE *stream_out,
		    const struct pair_set *pairset,
		    const struct rna_coord *rna_a,
		    const struct rna_coord *rna_b)
{
    size_t i;
    for(i = 0; i < pairset->n; ++i) {
	size_t ii = pairset->indices[i][0];
	size_t jj = pairset->indices[i][1];
	if(ii == GAP_INDEX || jj == GAP_INDEX) {
	    continue;
	}
	mfprintf(stream_out,
		 "%i%c %i%c\n",
		 rna_a->res[ii].resnum,
		 rna_a->res[ii].icode == '?' ? ' ' : rna_a->res[ii].icode,
		 rna_b->res[jj].resnum,
		 rna_b->res[jj].icode == '?' ? ' ' : rna_b->res[jj].icode);
    }
}

static int
find_last_occurrence(const char *str, size_t strlen, char c)
{
    int i;
    for(i = strlen; i > -1 && str[i] != c; --i);
    return i;
}

static void
truncate_filename(char**truncated, const char *fname)
{
    int end, start;
    free_if_not_null(*truncated);
    *truncated = E_MALLOC((strlen(fname) + 1) * sizeof(**truncated));
    end = find_last_occurrence(fname, strlen(fname), '.');
    start = find_last_occurrence(fname, strlen(fname), '/') + 1;
    if(end > start) {
	strncpy(*truncated, fname + start, end - start);
	(*truncated)[end - start] = '\0';
    } else {
	(*truncated)[0] = '\0';
    }
}

struct cvrry_opt {
    char *filename_a;
    char *filename_b;
    char *filename_out_a;
    char *filename_out_b;
    char *chain_id_a;
    char *chain_id_b;
    char *pdbid_a;
    char *pdbid_b;
    size_t model_number_a;
    size_t model_number_b;
    int write_superimposed_coords;
};

struct cvrry_opt*
cvrry_opt_new()
{
    struct cvrry_opt *opt;
    opt = E_MALLOC(sizeof(*opt));
    opt->filename_a = opt->filename_b = NULL;
    opt->filename_out_a = opt->filename_out_b = NULL;
    opt->chain_id_a = opt->chain_id_b = NULL;
    opt->pdbid_a = opt->pdbid_b = NULL;
    return opt;
}

void
cvrry_opt_destroy(struct cvrry_opt *opt)
{
    if(opt) {
	free_if_not_null(opt->filename_out_a);
	free_if_not_null(opt->filename_out_b);
	free_if_not_null(opt->filename_a);
	free_if_not_null(opt->filename_b);
	free_if_not_null(opt->chain_id_a);
	free_if_not_null(opt->chain_id_b);
	free_if_not_null(opt->pdbid_a);
	free_if_not_null(opt->pdbid_b);
	free(opt);
    }
}

void
get_cif_outfilename(char**fname, const char*path, const char*pdbid, const char*chain_id)
{
    size_t pathlen = strlen(path);
    size_t pathlen_full = pathlen + strlen(pdbid) + strlen(chain_id) + 16;
    char *fn;
    free_if_not_null(*fname);
    *fname = E_MALLOC(pathlen_full * sizeof(**fname));
    fn = *fname;
    strcpy(fn, path);
    if(fn[pathlen-1] != '/') {
	fn[pathlen] = '/';
	pathlen++;
    }
    sprintf(fn + pathlen, "%s_%s_cvrry.cif", pdbid, chain_id);
}

int
get_cvrry_opt(struct cvrry_opt *opt, int argc, char *argv[])
{
    const char *this_sub = "get_cvrry_opt";
    int c;
    struct option long_options[] =
        {
	    {"coords_outpath",  required_argument, 0, 'o'},
	    {0, 0, 0, 0}
        };

    if(argc < 5) {
	mprintf("usage: %s mmcif_file_a chainID_a mmcif_file_b chainID_b\n", argv[0]);
	return 1;
    }
    
    /* positional arguments */
    getarg(&opt->filename_a, argv[1]);
    getarg(&opt->chain_id_a, argv[2]);
    getarg(&opt->filename_b, argv[3]);
    getarg(&opt->chain_id_b, argv[4]);
    
    opt->model_number_a = opt->model_number_b = 1;
    opt->write_superimposed_coords = 0;
    
    truncate_filename(&opt->pdbid_a, opt->filename_a);
    truncate_filename(&opt->pdbid_b, opt->filename_b);
    
    /* optional stuff */
    while((c = getopt_long(argc, argv, "o:", long_options, NULL)) != -1) {
	switch(c) {
	case 'o':
	    opt->write_superimposed_coords = 1;
	    get_cif_outfilename(&opt->filename_out_a, optarg, opt->pdbid_a, opt->chain_id_a);
	    get_cif_outfilename(&opt->filename_out_b, optarg, opt->pdbid_b, opt->chain_id_b);
	    break;
	case '?':
	default:
	    err_printf(this_sub, "option parsing error");
	    return 1;
	}
    }

    return 0;
}

void
printopts(const struct cvrry_opt *opt)
{
    mprintf("\n");
    mprintf("###################\n");
    mprintf("CVRRY RNA ALIGNMENT\n");
    mprintf("###################\n");
    mprintf("\n");
    mprintf("Input:\n");
    mprintf("file %s chain %s\n", opt->filename_a, opt->chain_id_a);
    mprintf("file %s chain %s\n", opt->filename_b, opt->chain_id_b);
    mprintf("\n");
}

int
main(int argc, char *argv[])
{
    const char *this_sub = "main";
    struct cvrry_opt *opt;
    struct rna_frag_coord *chain_a=NULL, *chain_b=NULL;
    struct pair_set *alignment=NULL;
    struct pair_set *superimposed_residues=NULL;
    struct rna_align_params align_params;
    char *align_str=NULL;
    struct transformation transform;
    float rmsd;
    const float rmsd_thresh_psi=4.0;
    float psi;
    int err=0;
    struct norm_params *frag_norm = default_normalization();
    struct feat_weights *weights = default_feat_weights();

    if(!err) {
	opt = cvrry_opt_new();
	err = get_cvrry_opt(opt, argc, argv);
    }

    if(!err) {
	printopts(opt);
	
	align_params.gap_open = 2.07671;
	align_params.gap_widen = 0.548336;
	align_params.algn_type = N_AND_W;
	align_params.scale_factor = 1.0;
	align_params.base_pair_bonus = 0.0;
    }

    if(!err) {
	chain_a = read_n_compute_rna_frag_coord(opt->filename_a,
						opt->chain_id_a,
						opt->model_number_a);
	err = !chain_a;
    }

    if(!err) {
	chain_b = read_n_compute_rna_frag_coord(opt->filename_b,
						opt->chain_id_b,
						opt->model_number_b);
	err = !chain_b;
    }
    
    if(!err) {
	err = !chain_a->frags->n_frags;
	if(err) {
	    err_printf(this_sub,
		       "No fragments found in file %s for chain %s\n",
		       opt->filename_a, opt->chain_id_a);
	}
    }
    
    if(!err) {
	err = !chain_b->frags->n_frags;
	if(err) {
	    err_printf(this_sub,
		       "No fragments found in file %s for chain %s\n",
		       opt->filename_b, opt->chain_id_b);
	}
    }

    if(!err) {
	rna_frags_normalize(chain_a->frags, frag_norm);
	rna_frags_normalize(chain_b->frags, frag_norm);
    }

    if(!err) {
	err = rna_frags_multiply(chain_a->frags, weights);
    }

    if(!err) {
	err = rna_frags_multiply(chain_b->frags, weights);
    }

    if(!err) {
	alignment = align(chain_a, chain_b, &align_params);
	err = !alignment;
    }

    if(!err) {
	align_str = pair_set_pretty_string(alignment,
					   chain_a->rna_backbone->seq,
					   chain_b->rna_backbone->seq);
	err = !align_str;
    }

    if(!err) {
	mprintf("%s", align_str);
	err = rna_coord_rmsd(&transform,
			     &rmsd,
			     alignment,
			     chain_a->rna_backbone,
			     chain_b->rna_backbone);
    }

    if(!err) {
	mprintf("RMSD=%.2f\n", rmsd);
	err = percentage_of_structure_identity(&psi,
					       &superimposed_residues,
					       &transform,
					       alignment,
					       chain_a->rna_backbone,
					       chain_b->rna_backbone,
					       rmsd_thresh_psi,
					       PROSUP_REFINE);
	if(err) {
	    err_printf(this_sub, "could not get a superposition\n");
	}
    }

    if(!err) {
	mprintf("PSI=%.3f\n\n", psi);
    }

    if(opt->write_superimposed_coords) {
	if(!err) {
	    transform_mmcif(chain_a->cif, &transform);
	    err = write_mmcif(chain_a->cif, opt->pdbid_a, opt->filename_out_a);
	}
	if(!err) {
	    err = write_mmcif(chain_b->cif, opt->pdbid_b, opt->filename_out_b);
	}
	if(!err) {
	    mprintf("Superimposed chains are written to %s and %s\n\n",
		    opt->filename_out_a, opt->filename_out_b);
	}
    }
    
    norm_params_destroy(frag_norm);
    feat_weights_destroy(weights);
    free_if_not_null(align_str);
    pair_set_destroy(superimposed_residues);
    pair_set_destroy(alignment);
    rna_frag_coord_destroy(chain_a);
    rna_frag_coord_destroy(chain_b);
    cvrry_opt_destroy(opt);
    return err;
}
