import os
import cvrry

# Download and save he cif files first !


def main():

    pdbid_a = "4v88"
    chain_id_a = "A6"
    model_i_a = 1

    pdbid_b = "3j9m"
    chain_id_b = "AA"
    model_i_b = 1

    mmcif_filename_a = pdbid_a.upper() + ".cif"
    mmcif_filename_b = pdbid_b.upper() + ".cif"

    align_type = cvrry.N_AND_W
    # arbitrary gap costs
    gap_open = 5
    gap_widen = 1.0

    frag_norm = cvrry.default_normalization()
    weights = cvrry.default_feat_weights()

    outdir = (f"superpos_{pdbid_a}_{chain_id_a}_{model_i_a}"
              f"_{pdbid_b}_{chain_id_b}_{model_i_b}")
    outdir = os.path.abspath(outdir)

    rna_frag_coord_a = cvrry.read_n_compute_rna_frag_coord(
        mmcif_filename_a, chain_id_a, model_i_a
    )

    rna_frag_coord_b = cvrry.read_n_compute_rna_frag_coord(
        mmcif_filename_b, chain_id_b, model_i_b
    )

    cvrry.rna_frags_normalize(rna_frag_coord_a.frags, frag_norm)
    cvrry.rna_frags_normalize(rna_frag_coord_b.frags, frag_norm)
    cvrry.rna_frags_multiply(rna_frag_coord_a.frags, weights)
    cvrry.rna_frags_multiply(rna_frag_coord_b.frags, weights)

    try:
        os.makedirs(outdir)
    except FileExistsError:
        pass

    seq_len_a = cvrry.rna_frag_coord_get_seq_len(rna_frag_coord_a)
    seq_len_b = cvrry.rna_frag_coord_get_seq_len(rna_frag_coord_b)

    score_matrix = cvrry.score_mat_new(seq_len_a, seq_len_b)
    coverage_matrix = cvrry.score_mat_new(seq_len_a, seq_len_b)

    cvrry.fill_matrix_cv(
        score_matrix, coverage_matrix,
        rna_frag_coord_a.frags, rna_frag_coord_b.frags
    )

    cvrry.scale_marginal_scores(
        score_matrix,
        coverage_matrix,
        rna_frag_coord_b.frags.xmer,
        1.0
    )

    pset = cvrry.score_mat_sum_minimal(
        score_matrix,
        gap_open,
        gap_widen,
        align_type
    )

    cvrry.write_chimera_superimposed_alignment(
        outdir,
        rna_frag_coord_a.rna_backbone,
        rna_frag_coord_b.rna_backbone,
        pset
    )

    cvrry.rna_frag_coord_destroy(rna_frag_coord_a)
    cvrry.rna_frag_coord_destroy(rna_frag_coord_b)
    cvrry.pair_set_destroy(pset)
    cvrry.norm_params_destroy(frag_norm)
    cvrry.feat_weights_destroy(weights)
    cvrry.score_mat_destroy(score_matrix)
    cvrry.score_mat_destroy(coverage_matrix)

    print(f"wrote results to {outdir}")


if __name__ == "__main__":
    main()
