import sys
import numpy as np
import cvrry


""" example for Irina on how to use python interface to cvrry """


def main():
    pdbid = "1TRA"
    mmcif_filename = pdbid + ".cif"
    chain_id = "A"
    model_i = 1

    rna_frag_coord = cvrry.read_n_compute_rna_frag_coord(
        mmcif_filename, chain_id, model_i
    )
    nucleotide_ids = cvrry.rna_coord_get_res_ids(rna_frag_coord.rna_backbone)

    print("NUCLEOTIDE IDS:")
    print(nucleotide_ids)

    nucleotide_sequence = cvrry.rna_coord_get_full_sequence(
        rna_frag_coord.rna_backbone
    )
    print("NUCLEOTIDE SEQUENCE:")
    print(nucleotide_sequence)

    # create some arbitrary basepair info and put that into the
    # backbone structure
    basepair_info = np.zeros(
        rna_frag_coord.rna_backbone.size,
        dtype=np.int32
    )
    basepair_info[5] = 1
    basepair_info[17] = -1

    cvrry.rna_coord_set_basepair_info(
        rna_frag_coord.rna_backbone,
        basepair_info
    )

    # save the whole structure in binary format, you can load it later on
    # with cvrry.rna_frag_coord_read_binary
    filename_out = cvrry.fn_rna_frag_coord(pdbid, chain_id, model_i)
    cvrry.rna_frag_coord_write_binary(rna_frag_coord, filename_out)

    # load the structure from the binary file
    rna_frag_coord_copy = cvrry.rna_frag_coord_read_binary(
        filename_out, cvrry.YES
    )

    print("RNA_FRAG_COORD structure for {}:".format(mmcif_filename))
    sys.stdout.flush()
    cvrry.rna_frag_coord_dump(rna_frag_coord_copy)

    # free stuff
    cvrry.rna_frag_coord_destroy(rna_frag_coord)
    cvrry.rna_frag_coord_destroy(rna_frag_coord_copy)


if __name__ == "__main__":
    main()
