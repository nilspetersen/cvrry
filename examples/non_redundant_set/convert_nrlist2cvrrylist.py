#!/usr/bin/env python3

import argparse
import pandas as pd


def main():

    parser = argparse.ArgumentParser(
        description = ("Read a list of non-redundant RNA structures "
                       "as distributed on the RNA3D Hub server and "
                       "write it in a format that cvrry can read"))

    parser.add_argument("input_file", help="non-redundant from RNA 3D hub")

    args = parser.parse_args()

    fn_nr_list = args.input_file
    fn_out = fn_nr_list.replace('.csv', '.chainlist')

    # load the representatives from the table
    nr_list_table = pd.read_csv(fn_nr_list,
                                usecols=[1],
                                names=['representative'])
    # sometimes two chains are the representative together
    # concatenated with a '+', they are separated here
    repr_chains = pd.DataFrame([[pdbid, chain, model]
                                for pdbid, model, chain in (chain.split('|')
                                                            for chains in nr_list_table['representative']
                                                            for chain in chains.split('+'))],
                               columns=['pdbid', 'model_nr', 'chainid'])
    # write to a file readable for the cvrry programs
    repr_chains.to_csv(fn_out, sep=' ', index=False, header=False)


if __name__=='__main__':
    main()
