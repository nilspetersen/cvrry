#!/usr/bin/env python3

import urllib.request
import subprocess
import os


def get_cif(pdbid, target_directory):
    fname = "{}.cif".format(pdbid.upper())
    fname_gz = fname + ".gz"
    fname_full = os.path.join(target_directory, fname)
    url = "https://files.rcsb.org/download/{}".format(fname_gz)
    print("Downloading " + url)
    urllib.request.urlretrieve(url, fname_gz)      
    subprocess.call(["gunzip", fname_gz, "-f"])
    os.rename(fname, fname_full)


def create_cif_filename(pdbid, pdbdir):
    return os.path.join(pdbdir, pdbid.upper() + ".cif")


def get_if_not_have(pdbid, pdbdir):
    if not os.path.exists(create_cif_filename(pdbid, pdbdir)):
        get_cif(pdbid, pdbdir)


def get_missing_cifs(pdbids, pdbdir):
    for pdbid in pdbids:
        get_if_not_have(pdbid, pdbdir)


def read_cvrry_chainlist(fname_chains):
    with open(fname_chains, 'r') as f_in:
        return [line.split() for line in f_in]


def read_pdbids_from_chainlist(fname_chains):
    return set([case[0] for case in read_cvrry_chainlist(fname_chains)])


def main():
    import argparse
    parser = argparse.ArgumentParser(prog="GET_CIF")
    parser.add_argument("chains_file", help="file with pdbids in the first column")
    parser.add_argument("target_directory", help="cif files will be stored here")
    args = parser.parse_args()
    pdbids = read_pdbids_from_chainlist(args.chains_file)
    get_missing_cifs(pdbids, args.target_directory)


if __name__ == "__main__":
    main()
