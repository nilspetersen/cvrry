import os
from setuptools import setup


setup(
    name='alfons_tools',
    version='0.2',
    packages=['alfons', 'alfons.tools', 'alfons.utils'],
    description='python tools for CVRRY, ALFONS, URSULA and FREEDOLIN',
    author='Nils Petersen',
    author_email='petersen@zbh.uni-hamburg.de',
    include_package_data=True,
    package_data={'alfons': [
        os.path.join('data', 'alphabets', 'alpha6seed2.alphabet'),
        os.path.join('data', 'alphabets', 'alpha7seed1.alphabet')
    ]},
    entry_points={'console_scripts': [
        'precompute_alfons = alfons.tools.precompute:precompute_alfons_main',
        'precompute_cvrry = alfons.tools.precompute:precompute_cvrry_main',
        'alfons_seq_p = alfons.tools.print_stuff:print_alfons_seq',
        'alfons_dump = alfons.tools.print_stuff:dump_cvrry_alfons'
    ]}
)
