import os
import numpy as np

import cvrry

from alfons.utils.syscalls import syscall_or_raise, find_program


def call_dssr(dssr_path, fn_cif):
    tmpfile = os.path.basename(fn_cif) + "_basepairs.tmp"
    syscall = [
        dssr_path,
        f"-i={fn_cif}",
        f"-o={tmpfile}",
        "--pair-only"
    ]
    syscall_or_raise(syscall)
    try:
        with open(tmpfile, 'r') as f_bp:
            lines = f_bp.readlines()
            os.remove(tmpfile)
    except FileNotFoundError:
        print(f'WARNING: no basepairs in {fn_cif}')
        return ''
    return lines


def get_basepairs_from_cif(fn_cif,
                           chainid,
                           dssr_path=None,
                           bp_types=['WC', 'Wobble', '~Wobble']):
    # get dssr_path
    fn_dssr_default = 'x3dna-dssr'
    if dssr_path is None:
        try:
            dssr_path = os.environ['DSSR_PATH']
        except KeyError:
            dssr_path = find_program(fn_dssr_default)
            if dssr_path is None:
                raise RuntimeError(
                    "Need path for 'x3dna-dssr' program, "
                    "set environment variable DSSR_PATH or add "
                    "directory containing 'x3dna-dssr' to PATH variable"
                )
    if os.path.isdir(dssr_path):
        dssr_path = os.path.join(dssr_path, fn_dssr_default)
    if not (os.path.isfile(dssr_path) and os.access(dssr_path, os.X_OK)):
        raise RuntimeError(f"{dssr_path} is not an excecutable")
    print(f"using dssr at {dssr_path}")

    # get the basepairs
    dssr_output = call_dssr(dssr_path, fn_cif)
    lines_split = (line.split() for line in dssr_output)
    bp_lines = (
        (l[1].split('.') + l[2].split('.') + [l[4]])
        for l in lines_split
        if (len(l) == 8 and
            len(l[1].split('.')) == 2 and
            len(l[2].split('.')) == 2 and
            l[4] in bp_types)
    )
    basepairs_chain = [
        (nt_a.replace('^', ''), nt_b.replace('^', ''), bp_type)
        for chain_a, nt_a, chain_b, nt_b, bp_type in bp_lines
        if chain_a == chainid and chain_b == chainid
    ]
    return basepairs_chain


def add_basepairs(rna_coord, basepairs_list):
    basepair_arr = np.ones(rna_coord.size, dtype=np.int32)
    # check for size
    res_ids = cvrry.rna_coord_get_res_ids(rna_coord)
    seq = cvrry.rna_coord_get_full_sequence(rna_coord)
    res_ids_dict = {
        seq + res_id: i
        for i, (seq, res_id) in enumerate(zip(seq, res_ids))
    }
    for nt1, nt2, _ in basepairs_list:
        try:
            i = res_ids_dict[nt1]
            j = res_ids_dict[nt2]
            basepair_arr[min(i, j)] = 2
            basepair_arr[max(i, j)] = 0
        except KeyError:
            print(f"Invalid keys: {nt1} or {nt2}")
    cvrry.rna_coord_set_basepair_info(rna_coord, basepair_arr)


def get_and_add_basepairs(frag_or_seq_coord,
                          pdbid, chainid, model_i,
                          dssr_path):
    # compute and parse the basepairs and add the info
    fn_cif_chain = f"tmp_{pdbid}_{chainid}_{model_i}.cif"
    if cvrry.write_mmcif(frag_or_seq_coord.cif, pdbid, fn_cif_chain):
        raise IOError(f"Failed to write {fn_cif_chain}")
    basepairs_list = get_basepairs_from_cif(
        fn_cif_chain, chainid, dssr_path
    )
    add_basepairs(frag_or_seq_coord.rna_backbone, basepairs_list)
    os.remove(fn_cif_chain)
