import subprocess
import os


def output_to_file(output, filename):

    """ write output of a program to a file,
    pass stdout or stderr to output """

    with open(filename, "w") as f_out:
        f_out.write(output.decode('utf-8'))


def syscall_or_raise(syscall):

    """ system call wrapper """

    result = subprocess.run(
        syscall,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
    )
    if result.returncode:
        syscall_joined = " ".join(syscall)
        err_msg = f'Error: {syscall_joined} failed ({result.stderr})'
        raise RuntimeError(err_msg)
    return result


def find_program(program):
    def is_exe(fpath):
        return os.path.isfile(fpath) and os.access(fpath, os.X_OK)
    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file
    return None
