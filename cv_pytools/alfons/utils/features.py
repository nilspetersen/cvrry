import cvrry
from alfons.utils.basepair import (
    get_and_add_basepairs
)


def compute_alfons_seq_coord(fn_cif,
                             pdbid,
                             chainid,
                             model_i,
                             alphabet,
                             full_seq,
                             addtail=True,
                             dssr_path=None):
    seq_coord = cvrry.read_n_compute_rna_seq_coord(
        fn_cif, chainid, model_i, alphabet
    )
    if seq_coord is None:
        raise RuntimeError(
            f'failed to get seq_coord structure for {fn_cif}'
        )
    get_and_add_basepairs(
        seq_coord, pdbid, chainid,
        model_i, dssr_path
    )
    if full_seq:
        err = cvrry.rna_seq_coord_fill_missing_characters(
            seq_coord, cvrry.YES if addtail else cvrry.NO
        )
        if err:
            cvrry.rna_seq_coord_destroy(seq_coord)
            raise RuntimeError(
                f'Failed to compute full seq for {pdbid}_{chainid}_{model_i}'
            )
    return seq_coord
