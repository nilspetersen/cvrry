import os
import argparse
import cvrry
from alfons.utils.features import (
    compute_alfons_seq_coord
)
from alfons.utils.basepair import (
    get_and_add_basepairs
)


def get_cmd_args(alfons=False):
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'cif_file', help='file with rna coordinates'
    )
    parser.add_argument(
        'chainid', help='select your favourite chain'
    )
    parser.add_argument(
        '--model', default=1, type=int,
        help='model number in case there are more than one'
    )
    parser.add_argument(
        '--fn_out',
        help='model number in case there are more than one'
    )
    parser.add_argument(
        '--dssr_path', default=None,
        help='path to x3dna-dssr program for base pair detection'
    )
    if 'alfons':
        fn_default_alphabet = os.path.join(
            os.path.split(__file__)[0],
            '..', 'data', 'alphabets', 'alpha7seed1.alphabet'
        )
        parser.add_argument(
            '--alphabet_file', help='binary ALFONS alphabet file',
            default=fn_default_alphabet
        )
        parser.add_argument(
            '--old_rna_bin_read',
            help='old version used to read old alphabets',
            action='store_true'
        )
        parser.add_argument(
            '--fill_up_seq',
            help=(
                'add empty characters for sequence positions not '
                'covered by the structural alphabet letters'
            ),
            action='store_true'
        )
        parser.add_argument(
            '--no_tail',
            help=(
                'for sequences with empty character do not add a '
                'tail of emtpy characters for the last fragment'
            ),
            action='store_true'
        )
    args = parser.parse_args()
    return args


def make_fn_out(fn_out, pdbid, chainid,
                model_i, cvrry_or_alfons="cvrry"):
    return (
        fn_out if fn_out is not None
        else f'{pdbid}_{chainid}_{model_i}.{cvrry_or_alfons}'
    )


def precompute_cvrry_main():

    args = get_cmd_args()

    fn_cif = args.cif_file
    pdbid = os.path.splitext(os.path.basename(args.cif_file))[0]
    chainid = args.chainid
    model_i = args.model
    fn_out = make_fn_out(
        args.fn_out, pdbid, chainid, model_i, "cvrry"
    )
    dssr_path = args.dssr_path

    rna_frag_coord = cvrry.read_n_compute_rna_frag_coord(
        fn_cif, chainid, model_i
    )

    if rna_frag_coord is None:
        raise RuntimeError(
            f'Failed to get frag_coord structure from {fn_cif}'
        )

    get_and_add_basepairs(
        rna_frag_coord, pdbid,
        chainid, model_i, dssr_path
    )

    # save the frag_coord structure
    if cvrry.rna_frag_coord_write_binary(rna_frag_coord, fn_out):
        raise IOError(f'failed to write {fn_out}')


def precompute_alfons_main():
    args = get_cmd_args(alfons=True)
    fn_cif = args.cif_file
    pdbid = os.path.splitext(os.path.basename(args.cif_file))[0]
    chainid = args.chainid
    model_i = args.model
    fn_out = make_fn_out(
        args.fn_out, pdbid,
        chainid, model_i, "alfons"
    )
    fn_alphabet = args.alphabet_file
    dssr_path = args.dssr_path
    full_seq = args.fill_up_seq
    if args.old_rna_bin_read:
        cvrry.set_glob_old_read(1)
    alphabet = cvrry.rna_coord_alphabet_read_binary_file(
        fn_alphabet
    )
    if alphabet is None:
        raise RuntimeError(
            f'failed to read alphabet from {fn_alphabet}'
        )
    addtail = False if args.no_tail else True
    seq_coord = compute_alfons_seq_coord(
        fn_cif, pdbid, chainid, model_i,
        alphabet, full_seq, addtail, dssr_path
    )
    if cvrry.rna_seq_coord_write_binary(seq_coord, fn_out):
        raise RuntimeError(f'Failed to write {fn_out}')
    cvrry.rna_seq_coord_destroy(seq_coord)
    cvrry.rna_coord_alphabet_destroy(alphabet)
