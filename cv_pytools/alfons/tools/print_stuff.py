import os
import argparse
import cvrry


# Functions for executables to print and dump stuff


def print_alfons_seq():
    parser = argparse.ArgumentParser()
    parser.add_argument("input_file", help="alfons (rna_seq_coord) file")
    args = parser.parse_args()

    seq_coord = cvrry.rna_seq_coord_read_binary(args.input_file)
    coord_string = cvrry.int_array_2_numpy_array(seq_coord.seq.seq)

    alpha_string = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

    string_translated = ''.join([alpha_string[i] for i in coord_string])
    string_raw = ''.join([str(i) for i in coord_string])

    print(f'size={len(coord_string)}')
    print(string_translated)
    print(string_raw)

    cvrry.rna_seq_coord_destroy(seq_coord)


def dump_cvrry_alfons():
    parser = argparse.ArgumentParser()
    parser.add_argument("input_file", help="alfons (rna_seq_coord) file")
    args = parser.parse_args()

    infile = args.input_file

    _, file_ext = os.path.splitext(infile)
    if file_ext == ".alfons":
        data_container = cvrry.rna_seq_coord_read_binary(args.input_file)
        cvrry.rna_seq_coord_dump(data_container)
        cvrry.rna_seq_coord_destroy(data_container)
    elif file_ext == ".rnachain":
        rna_coord = cvrry.rna_coord_read_binary_file(args.input_file)
        cvrry.rna_coord_dump(rna_coord)
        cvrry.rna_coord_destroy(rna_coord)
    else:
        raise RuntimeError(
            f"options for file type {file_ext} not implemented"
        )
