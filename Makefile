# TODO:
# maybe the compiled c program get faster if not
# compiled with fPIC, so maybe compile the objects
# for the shared lib for perl separately

# DIRECTORIES
SRC_MAIN=src/mainfiles
SRC_TEST=src/test
SRC_LIB=src/src
HEADERS_DIR=$(SRC_LIB)
OBJ=obj
BIN=bin
TEST=test

# COMPILER FLAGS
CC=gcc
CFLAGS=-Wall -Werror -Wno-format-truncation -fpic -fopenmp -Dbool=char
DEBUG_FLAGS=-g
OPT_FLAGS=-O3
PROFILE_FLAGS=-pg
INCLUDES=-I$(HEADERS_DIR)
LD_FLAGS=-lm -lgomp

ifeq ($(mode),debug)
COMPILE_FLAGS=$(DEBUG_FLAGS)
else
ifeq ($(mode),profile)
COMPILE_FLAGS=$(PROFILE_FLAGS)
else
COMPILE_FLAGS=$(OPT_FLAGS)
endif
endif

# INTERFACE OPTIONS
# UNI PC:
# PERL_INCLUDE_CORE=-I/usr/lib/perl5/5.26.1/x86_64-linux-thread-multi/CORE
# MY COMPUTER:
# PERL_INCLUDE_CORE=-I/usr/lib/x86_64-linux-gnu/perl/5.30.0/CORE
# C_INLUDE_PATH is set in .bashrc instead !

SWIG=swig
SWIG_OPTS=-Wall -Werror
INTERFACE_DIR=interfaces
PYTHON_INTERFACE_DIR=$(INTERFACE_DIR)/python
PYTHON_INTERFACE_SRC_DIR=$(PYTHON_INTERFACE_DIR)/c_src
PERL_INTERFACE_DIR=$(INTERFACE_DIR)/perl
SWIG_OUTDIR_PY=$(PYTHON_INTERFACE_DIR)/swig
SWIG_OUTDIR_PERL=$(PERL_INTERFACE_DIR)/swig

CVRRY_I=$(INTERFACE_DIR)/cvrry_body.i $(INTERFACE_DIR)/cvrry_head.i
CVRRY_PY_I=$(INTERFACE_DIR)/cvrry_python.i
CVRRY_PERL_I=$(INTERFACE_DIR)/cvrry_perl.i

# required to keep object files (make deletes them otherwise)
.PRECIOUS: $(OBJ)/%.o

# LISTS OF FILES
# headers
HEADERS=$(wildcard $(HEADERS_DIR)/*.h)
# lib
SOURCES_LIB=$(wildcard $(SRC_LIB)/*.c)
OBJECTS_LIB=$(patsubst $(SRC_LIB)/%.c, $(OBJ)/%.o, $(SOURCES_LIB))
# mains
SOURCES_MAIN=$(wildcard $(SRC_MAIN)/*_main.c)
EXECUTABLES=$(patsubst $(SRC_MAIN)/%_main.c, $(BIN)/%.x, $(SOURCES_MAIN))
# tests
SOURCES_TESTS=$(wildcard $(SRC_TEST)/*.c)
EXECUTABLES_TESTS=$(patsubst $(SRC_TEST)/%.c, $(TEST)/%.x, $(SOURCES_TESTS))

# TARGETS TO BUILD
all: directories executables tests
executables: $(EXECUTABLES)
tests: $(EXECUTABLES_TESTS)

new: clean all

# MAKE THE DIRECTORIES
directories:
	mkdir -p bin test obj

# RULES FOR EXECUTABLES
$(BIN)/%.x: $(OBJECTS_LIB) $(OBJ)/%_main.o
	$(CC) $(CFLAGS) $(COMPILE_FLAGS) $(INCLUDES) $(OBJECTS_LIB) $(OBJ)/$*_main.o \
	-o $@ $(LD_FLAGS)

$(TEST)/%.x: $(OBJECTS_LIB) $(OBJ)/%.o
	$(CC) $(CFLAGS) $(COMPILE_FLAGS) $(INCLUDES) $(OBJECTS_LIB) $(OBJ)/$*.o \
	-o $@ $(LD_FLAGS)

# INTERFACES

interfaces: python_interface perl_interface 

perl_interface: $(SWIG_OUTDIR_PERL)/cvrry.so

$(SWIG_OUTDIR_PERL)/cvrry.so: $(OBJ)/cvrry_wrap_perl.o $(OBJECTS_LIB)
	$(CC) $(CFLAGS) $(COMPILE_FLAGS) $(INCLUDES) $(OBJECTS_LIB) $(OBJ)/cvrry_wrap_perl.o \
	-o $@ $(LD_FLAGS) -shared

$(SWIG_OUTDIR_PERL)/cvrry_wrap_perl.c: $(CVRRY_I) $(CVRRY_PERL_I) $(HEADERS)
	mkdir -p $(SWIG_OUTDIR_PERL)
	$(SWIG) $(SWIG_OPTS) -perl5 $(INCLUDES) -o $(SWIG_OUTDIR_PERL)/cvrry_wrap_perl.c \
	-outdir $(SWIG_OUTDIR_PERL) $(CVRRY_PERL_I)

python_interface: $(SWIG_OUTDIR_PY)/cvrry_wrap_python.c $(SOURCES_LIB)
	mkdir -p $(PYTHON_INTERFACE_SRC_DIR)
	cp $(SRC_LIB)/*.c $(PYTHON_INTERFACE_SRC_DIR) 
	cp $(SRC_LIB)/*.h $(PYTHON_INTERFACE_SRC_DIR) 
	pip install $(PYTHON_INTERFACE_DIR)
	rm -r $(PYTHON_INTERFACE_SRC_DIR)

$(SWIG_OUTDIR_PY)/cvrry_wrap_python.c:  $(CVRRY_I) $(CVRRY_PY_I) $(HEADERS)
	mkdir -p $(SWIG_OUTDIR_PY)
	$(SWIG) $(SWIG_OPTS) -python $(INCLUDES) -o $(SWIG_OUTDIR_PY)/cvrry_wrap_python.c \
	-outdir $(SWIG_OUTDIR_PY) $(CVRRY_PY_I)

python_tools:
	pip install pytools/

# PATTERN RULES

dummy:

# a) library objects
$(OBJ)/%.o: $(SRC_LIB)/%.c $(HEADERS) Makefile $(patsubst %,dummy,$(mode))
	$(CC) $(CFLAGS) $(COMPILE_FLAGS) $(INCLUDES) -c $< -o $@

# b) main objects
$(OBJ)/%.o: $(SRC_MAIN)/%.c $(HEADERS) Makefile $(patsubst %,dummy,$(mode))
	$(CC) $(CFLAGS) $(COMPILE_FLAGS) $(INCLUDES) -c $< -o $@

# c) tests objects 
$(OBJ)/%.o: $(SRC_TEST)/%.c $(HEADERS) Makefile $(patsubst %,dummy,$(mode))
	$(CC) $(CFLAGS) $(COMPILE_FLAGS) $(INCLUDES) -c $< -o $@

# d) perl interface objects
$(OBJ)/%.o: $(SWIG_OUTDIR_PERL)/%.c $(HEADERS) Makefile $(patsubst %,dummy,$(mode))
	$(CC) $(CFLAGS) $(COMPILE_FLAGS) $(INCLUDES) -c $< -o $@ \
	$(PERL_INCLUDE_CORE) -Doff64_t=__off64_t

# Clean
clean:
	-rm $(OBJ)/*.o $(BIN)/*.x $(TEST)/*.x \
	$(SWIG_OUTDIR_PY)/* $(SWIG_OUTDIR_PERL)/*	
	-rm -r $(PYTHON_INTERFACE_SRC_DIR)
