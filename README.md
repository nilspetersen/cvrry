# Tools for RNA 3D structure comparison

This repository contains several tools for fast RNA 3D structure comparisons.
The underlying methods are described and evaluated in my PhD thesis. It has the title "Computational methods for RNA 3D structure prediction and comparison" and is available at https://ediss.sub.uni-hamburg.de/volltexte/2020/10605/pdf/Dissertation.pdf

## Tools

The following programs are provided here.

3D structure alignment tools:

- CVRRY uses inter-nucleotide distances and a chirality measure
- ALFONS is utilizing a structural alphabet

Tools for alignment-free comparison of 3D RNA structures:

- FREEDOLIN based on multiple substring matches of structural alphabet strings
- URSULA based on the most significant common substring of two structural alphabet strings

All tools are fed with structure files in mmcif format.
You can get them from the PDB (www.rcsb.org).

## Technology

So far, these tools were only applied on my system, which is
- Linux openSUSE Leap 15.0
- x86-64 architecture
- gcc (SUSE Linux) 7.4.1
- python 3.6.5
- pip 20.0.2
- numpy 1.16.4
- SWIG 4.0.0
- GNU Make 4.2.1
- DSSR 1.9.3 (http://x3dna.org)

## Installation

Make sure you have the requirement listed above.
You need a python environment with pip!

The software given here comprises three parts.
- a C-library and some executables.
- a python interface
- a python package with additional utility functions and tools

The python packages are called cvrry and alfons.
However, both contain code for both programs.
The cvrry package is the interface to the C-library and the alfons package contains the utility functions.
You will need all three to make alignments.

To compile the C-library and the executables, change into the cvrry directory and type
```
make
```
To build the python interface, make sure you have the correct python environement loaded. Then type
```
make python_interface
```
To add the additional python package type
```
pip install cv_pytools/
```

## RNA 3D structure alignments

This section describes the computation of RNA 3D structure alignments using CVRRY or ALFONS.
Alignments are computed in two steps.
The first step is the computation of the descriptors.
The descriptors are saved in files called .cvrry and .alfons.
These files are fed to the alignment programs afterwards.

The precomputations rely on the tool DSSR for the detection of base pairs.
Make sure you have it installed. You can find it at http://x3dna.org .
To use it in this context, the program either needs to be in a directory listed in PATH or you need to create an environment variable called DSSR_PATH guiding the way to the binary of this tool.

In the following it is assumed one has a structure file [PDBID].cif file from the PDB and one wants to compute the feature of the chain with the ID [CHAINID].
An example would be the file 1TRA.cif with chainID A.

For CVRRY type
```
precompute_cvrry [PDBID].cif [CHAINID]
```
For ALFONS type
```
precompute_alfons [PDBID].cif [CHAINID]
```
In both cases replace [PDBID] and [CHAINID] with the respective values.
If the structure file is in a different directory, the path has to be provided.

There are two excecutables in the directory `bin/` that can be used to make pairwise CVRRY and ALFONS alignments.
In both cases, you need the corresponding feature files.
For a CVRRY alignment type
```
./bin/cvrry_pairwise_align.x [PDBID_A]_[CHAINID_A]_1.cvrry [PDBID_B]_[CHAINID_B]_1.cvrry [output_directory]
```
[PDBID_A], [CHAINID_A], [PDBID_B] and [CHAINID_B] are the corresponding identifiers again.
The third number _1 is the model number.
If there are several models in a file, an alternative number is possible.
[output_directory] is optional.
Pass a path in your system (it must already exist), where files with superimposed coordinates are written to.
For an ALFONS alignment type
```
./bin/alfons_pairwise_align.x [PDBID_A]_[CHAINID_A]_1.alfons [PDBID_B]_[CHAINID_B]_1.alfons
```

## Alignment-free comparison of RNA 3D structures

As previously for the alignments, you need to precompute a feature file.
There is a binary for this cause in the bin/ directory.
However, you also need to provide an alphabet file.
The file pytools/alfons/data/alphabets/alpha6seed2.alphabet comprises the alphabet used in chapter 5 of my thesis.
To precompute the features for model [MODEL] of chain [CHAINID] in file [PDBID].cif, change to the project directory and type
```
./bin/freedolin_seq.x [PDBID].cif [CHAINID] [MODEL] pytools/alfons/data/alphabets/alpha6seed2.alphabet output_name.seq
```
To get a score for two chains, type
```
./bin/freedolin_pairwise.x name1.seq name2.seq [MODE]
```
where name1.seq and name2.seq are the names given to the .seq files.
For [MODE], use 2 to make a FREEDOLIN comparison and 3 to make an URSULA comparison.

If you have a directory with .seq files prepared, you can query the dataset.
You also need a list with all chains where each row is formatted as [PDBID] [CHAINID] [MODEL].
For now, you would have to write a script which calls freedolin_seq.x to get the files for all chains.
However, once there is a directory with .seq files and a file comprising the list of chains, you can type
```
./bin/freedolin_compare.x [QUERYNAME].seq [LISTNAME].chainlist [SEQ_DATA_PATH] [MODE]
```
where QUERYNAME.seq is the file of the query, [LISTNAME].chainlist is the file containing the chains, [SEQ_DATA_PATH] is the directory with the database .seq files and [MODE] is the type of the comparison as previously.

## Authors and origins

The tools here were mostly developed and written by me (Nils Petersen).
However, pieces of code were taken from the program/library wurst, which was developed by different members of the biomolecular modeling group at the centre for bioinformatics at the university hamburg.
Jan Schawo has also contributed code for the developement of CVRRY.