import itertools
import os
import unittest
import cvrry
import shutil
import numpy as np


def make_rna_coord_set(*rna_chains):
    coord_set = cvrry.cv_new_rna_coord_set(len(rna_chains))
    for rna in rna_chains:
        cvrry.rna_coord_set_add_chain(coord_set, rna)
    return coord_set


def make_alpha_index_permutation(size):
    return [np.random.randint(i, size) for i in range(size)]


def get_measures(pop):
    measure_funcs = {
        'entropy': cvrry.population_get_entropy,
        'avg_energy': cvrry.population_get_avg_energy,
        'avg_matchlen': cvrry.population_get_avg_matchlen,
        'avg_alphasize': cvrry.population_get_avg_alphabet_size,
        'avg_rmsd': cvrry.population_get_avg_rmsds
    }
    return {
        measure: func(pop)
        for measure, func in measure_funcs.items()
    }


class TestMatch(unittest.TestCase):

    def setUp(self):
        self.datadir = "data"
        self.tmpdir = "tmp_test_match"
        os.makedirs(self.tmpdir)

    def tearDown(self):
        shutil.rmtree(self.tmpdir)

    def cvrry_string_as_list(self, cvrry_string):
        return [cvrry.int_array_get(cvrry_string, i)
                for i in range(cvrry_string.size)]

    def cvrry_seq_as_lists(self, cvrry_seq):
        size = cvrry.rna_coord_seq_size(cvrry_seq)
        seq = [
            cvrry.int_array_get(cvrry_seq.seq, i)
            for i in range(size)
        ]
        pos = [
            cvrry.size_t_array_get(cvrry_seq.positions, i)
            for i in range(size)
        ]
        return seq, pos

    def assert_list_equality(self, list1, list2):
        for a, b in zip(list1, list2):
            self.assertEqual(a, b)

    def assert_cvrry_seq_equal(self, seq1, seq2):
        s1, p1 = self.cvrry_seq_as_lists(seq1)
        s2, p2 = self.cvrry_seq_as_lists(seq2)
        self.assert_list_equality(s1, s2)
        self.assert_list_equality(p1, p2)

    def make_cvrry_seqs_and_assert(self,
                                   rna_coordset,
                                   rna_alphabet_1,
                                   rna_alphabet_2):
        for i in range(rna_coordset.n_chains):
            rna_chain = cvrry.rna_coord_set_get_rna(rna_coordset, i)
            seq1 = cvrry.rna_coord_seq_simple(
                rna_chain, rna_alphabet_1
            )
            seq2 = cvrry.rna_coord_seq_simple(
                rna_chain, rna_alphabet_2
            )
            self.assert_cvrry_seq_equal(seq1, seq2)
            cvrry.rna_coord_seq_destroy(seq1)
            cvrry.rna_coord_seq_destroy(seq2)

    def assert_string(self, cvrry_str, ref):
        self.assert_list_equality(
            self.cvrry_string_as_list(cvrry_str), ref
        )

    def assert_string_both_cvrry(self, cvrry_str, cvrry_str_ref):
        self.assert_list_equality(
            self.cvrry_string_as_list(cvrry_str),
            self.cvrry_string_as_list(cvrry_str_ref)
        )

    def validate_cvrry_str(self, cvrry_str, alpha_index):
        for i in self.cvrry_string_as_list(cvrry_str):
            self.assertGreater(i, -1)
            self.assertGreater(alpha_index.border_i, i)

    def read_chainset(self, fn_chainlist):
        chainlist = cvrry.read_chain_list(
            os.path.join(self.datadir, fn_chainlist)
        )
        fn_chains_continuous = os.path.join(
            self.tmpdir, "testchains_1.continuous"
        )
        cvrry.read_write_unbroken_rna_backbones_set(
            chainlist, self.datadir, self.tmpdir, fn_chains_continuous
        )
        pre_chains = cvrry.indexed_chainset_load(
            fn_chains_continuous, self.tmpdir, -1
        )
        chains = cvrry.indexed_chainset_cut_pieces(
            pre_chains, 1, 100
        )
        cvrry.indexed_chainset_destroy(pre_chains)
        cvrry.chain_list_destroy(chainlist)
        return chains

    def make_alphabet_index(self, permutation):
        alpha_index = cvrry.cv_new_alphabet_index(len(permutation))
        for i in permutation:
            cvrry.alphabet_index_move(alpha_index, int(i))
        return alpha_index

    def make_rna_set(self, base_rna, chain_boundaries):
        rna_coord_list = [
            cvrry.rna_coord_copy_piece(base_rna, start_i, chainlen)
            for start_i, chainlen in chain_boundaries
        ]
        rna_coord_set = make_rna_coord_set(*rna_coord_list)
        for rna_coord in rna_coord_list:
            cvrry.rna_coord_destroy(rna_coord)
        return rna_coord_set

    def validate_stringset(self, stringset, alpha_index):
        for i in range(stringset.size):
            self.validate_cvrry_str(
                cvrry.rna_coord_stringset_get_string(stringset, i),
                alpha_index
            )

    def make_stringset(self,
                       coord_set,
                       alphabet,
                       fragment_pattern,
                       fraglen,
                       alpha_index):

        atom_types = cvrry.rna_atom_types_array_create(cvrry.ALL)
        rmsd_matrices = cvrry.calc_rna_coord_set_vs_alphabet_rmsds(
            coord_set, alphabet, fragment_pattern, atom_types
        )
        stringset = cvrry.make_rna_coord_stringset(
            coord_set, fraglen
        )
        cvrry.rna_coord_stringset_set(stringset, rmsd_matrices, alpha_index)

        # TODO: destroy rmsds_matrices ?

        cvrry.rna_atom_types_array_destroy(atom_types)
        self.validate_stringset(stringset, alpha_index)

        return stringset

    def assert_equal_stringsets(self, stringset_1, stringset_2):
        self.assertEqual(stringset_1.size, stringset_2.size)
        for i in range(stringset_1.size):
            cvrry_str_1 = cvrry.rna_coord_stringset_get_string(stringset_1, i)
            cvrry_str_2 = cvrry.rna_coord_stringset_get_string(stringset_2, i)
            self.assert_string_both_cvrry(cvrry_str_1, cvrry_str_2)

    def test_alphabet_extraction(self):
        chains_1 = self.read_chainset("testchains_1.chainlist")
        chains_1_size = cvrry.indexed_chainset_size(chains_1)
        fraglen = cvrry.indexed_chainset_get_fraglen(chains_1)

        alphabet_positions = np.random.permutation(
            np.arange(
                chains_1_size - 77 + fraglen, chains_1_size, dtype=np.int32
            )
        )
        alphabet = cvrry.indexed_chainset_extract_chainpiece_set(
            alphabet_positions, chains_1
        )
        alpha_index = self.make_alphabet_index(
            make_alpha_index_permutation(alphabet_positions.size)
        )
        alpha_size = int(alpha_index.border_i / 2)
        alpha_index.border_i = alpha_size

        stringset_1 = self.make_stringset(
            chains_1.rna_chains, alphabet,
            chains_1.fragment_pattern,
            fraglen, alpha_index
        )

        alphabet_2 = cvrry.rna_coord_set_alphabet_selection(
            alphabet, alpha_index
        )
        alpha_index_2 = self.make_alphabet_index(range(alpha_size))

        stringset_2 = self.make_stringset(
            chains_1.rna_chains, alphabet_2,
            chains_1.fragment_pattern,
            fraglen, alpha_index_2
        )

        self.assert_equal_stringsets(stringset_1, stringset_2)

        # assemble the alphabet structure

        alpha2_len = alphabet_2.n_chains
        letter_frequencies = cvrry.numpy_array_to_float_array(
            np.ones(alpha2_len, dtype=np.float32) / alpha2_len
        )
        rna_alphabet = cvrry.rna_coord_alphabet_assemble(
            alphabet_2, chains_1.fragment_pattern,
            chains_1.atom_types, alpha_index_2,
            letter_frequencies
        )
        cvrry.float_array_destroy(letter_frequencies)

        fn_rna_alphabet = os.path.join(self.tmpdir, "rna_alphabet_1.bin")
        cvrry.rna_coord_alphabet_write_binary_file(
            fn_rna_alphabet, rna_alphabet
        )
        rna_alphabet_2 = cvrry.rna_coord_alphabet_read_binary_file(
            fn_rna_alphabet
        )
        self.make_cvrry_seqs_and_assert(
            chains_1.rna_chains, rna_alphabet, rna_alphabet_2
        )

        # destroy stuff
        cvrry.rna_coord_stringset_destroy(stringset_1)
        cvrry.rna_coord_stringset_destroy(stringset_2)
        cvrry.alphabet_index_destroy(alpha_index)
        cvrry.indexed_chainset_destroy(chains_1)
        cvrry.rna_coord_set_destroy(alphabet)

    def inner_test_sequence(self, alpha_permutation):

        chain_boundaries = (
            (0, 70),
            (5, 30),
            (17, 20)
        )

        chains_1 = self.read_chainset("testchains_1.chainlist")
        chains_1_size = cvrry.indexed_chainset_size(chains_1)
        fraglen = cvrry.indexed_chainset_get_fraglen(chains_1)

        rna_1 = cvrry.rna_coord_set_get_rna_copy(
            chains_1.rna_chains, chains_1.rna_chains.n_chains - 1
        )
        coord_set = self.make_rna_set(rna_1, chain_boundaries)

        # Create a permutation of the alphabet
        alphabet_positions = np.arange(
            chains_1_size - 77 + fraglen, chains_1_size, dtype=np.int32
        )
        alpha_len = alphabet_positions.size
        expected_seq = np.zeros(alpha_len, dtype=np.int32)

        if alpha_permutation == "random_fragments":
            alphabet_positions = np.random.permutation(alphabet_positions)
            expected_seq[alphabet_positions -
                         alphabet_positions.min()] = np.arange(alpha_len)

        if alpha_permutation == "random_index":
            alpha_index = self.make_alphabet_index(
                make_alpha_index_permutation(alpha_len)
            )
            alpha_index_order = [
                cvrry.alphabet_index_get_select(alpha_index, i)
                for i in range(alpha_index.border_i)
            ]
            expected_seq[alpha_index_order] = np.arange(alpha_len)
        else:
            alpha_index = self.make_alphabet_index(range(alpha_len))

        if alpha_permutation is None:
            expected_seq = np.arange(alpha_len, dtype=np.int32)

        alphabet = cvrry.indexed_chainset_extract_chainpiece_set(
            alphabet_positions, chains_1
        )

        expected_sequences = [
            expected_seq[start_i: start_i + chainlen - fraglen + 1]
            for start_i, chainlen in chain_boundaries
        ]

        stringset = self.make_stringset(
            coord_set, alphabet,
            chains_1.fragment_pattern,
            fraglen, alpha_index
        )

        for i in range(stringset.size):
            self.assert_string(
                cvrry.rna_coord_stringset_get_string(stringset, i),
                expected_sequences[i]
            )

        cvrry.indexed_chainset_destroy(chains_1)
        cvrry.rna_coord_set_destroy(alphabet)
        cvrry.rna_coord_destroy(rna_1)
        cvrry.rna_coord_stringset_destroy(stringset)
        cvrry.alphabet_index_destroy(alpha_index)

    def test_sequence_mapping(self):
        self.inner_test_sequence("random_fragments")
        self.inner_test_sequence("random_index")
        self.inner_test_sequence(None)

    def test_fullmatch(self):

        def expected_overlap(bound_1, bound_2, fraglen):
            return max(0,
                       min(sum(bound_1) - bound_2[0] - fraglen + 1,
                           sum(bound_2) - bound_1[0] - fraglen + 1,
                           bound_1[1] - fraglen + 1,
                           bound_2[1] - fraglen + 1))

        def expected_start(bound_1, bound_2):
            return(max(bound_2[0] - bound_1[0], 0),
                   max(bound_1[0] - bound_2[0], 0))

        chain_boundaries = (
            (0, 70),
            (5, 10),
            (17, 20),
            (5, 30)
        )
        combis = list(itertools.combinations(range(len(chain_boundaries)), 2))

        chains_1 = self.read_chainset("testchains_1.chainlist")
        chains_1_size = cvrry.indexed_chainset_size(chains_1)
        fraglen = cvrry.indexed_chainset_get_fraglen(chains_1)

        alphabet_positions = np.random.permutation(
            np.arange(
                chains_1_size - 77 + fraglen, chains_1_size, dtype=np.int32
            )
        )

        alphabet = cvrry.indexed_chainset_extract_chainpiece_set(
            alphabet_positions, chains_1
        )

        rna_1 = cvrry.rna_coord_set_get_rna_copy(
            chains_1.rna_chains, chains_1.rna_chains.n_chains - 1
        )

        coord_set = self.make_rna_set(rna_1, chain_boundaries)

        alpha_index = self.make_alphabet_index(
            make_alpha_index_permutation(alphabet.n_chains)
        )

        stringset = self.make_stringset(
            coord_set, alphabet,
            chains_1.fragment_pattern,
            fraglen, alpha_index
        )

        overlap = [expected_overlap(chain_boundaries[c1],
                                    chain_boundaries[c2],
                                    fraglen)
                   for c1, c2 in combis]

        start = [expected_start(chain_boundaries[c1],
                                chain_boundaries[c2])
                 for c1, c2 in combis]

        rmsds, matches = cvrry.testfunc_getmatches(
            combis, stringset, coord_set,
            cvrry.indexed_chainset_get_fraglen(chains_1)
        )

        for (matchlen, i, j), rmsd, e_len, (e_i, e_j) in zip(matches,
                                                             rmsds,
                                                             overlap,
                                                             start):
            self.assertEqual(matchlen, e_len)
            if matchlen > 0:
                self.assertEqual(i, e_i)
                self.assertEqual(j, e_j)
                self.assertAlmostEqual(0.0, rmsd, places=4)

        cvrry.indexed_chainset_destroy(chains_1)
        cvrry.rna_coord_set_destroy(alphabet)
        cvrry.rna_coord_destroy(rna_1)
        cvrry.rna_coord_stringset_destroy(stringset)
        cvrry.alphabet_index_destroy(alpha_index)

    def assert_dicts_equal(self, dict1, dict2):
        for key, val in dict1.items():
            self.assertEqual(val, dict2[key])

    def assert_dicts_not_equal(self, dict1, dict2):
        for key, val in dict1.items():
            self.assertNotEqual(val, dict2[key])

    def assert_equal_array(self, array1, array2):
        self.assertEqual(array1.size, array2.size)
        self.assertTrue((array1 == array2).all())

    def make_populations(self, n_pops):
        n_parents = 5
        offspring_rate = 3
        temperature = 10.0

        chainset = self.read_chainset("testchains_1.chainlist")
        alphabet = cvrry.indexed_chainset_extract_chainpiece_set(
            np.random.permutation(np.arange(
                np.ceil(cvrry.indexed_chainset_size(chainset) / 2),
                dtype=np.int32
            )),
            chainset
        )
        # make the energy function
        means = np.loadtxt('data/rmsd_means.txt', dtype=np.float32)
        sigmas = np.loadtxt('data/rmsd_sigmas.txt', dtype=np.float32)
        e_func = cvrry.make_z_score_energy_function(means, sigmas, 1.5, -1)
        # make the pairs
        pairs = list(
            itertools.combinations(range(chainset.rna_chains.n_chains), 2)
        )
        pairs_double_index = cvrry.pairs_list_2_double_index(pairs)
        moveset = cvrry.make_strategy_simple_add_remove()

        populations = [
            cvrry.population_from_indexed_chainset(
                chainset, e_func, moveset, alphabet,
                pairs_double_index, n_parents, offspring_rate
            ) for _ in range(n_pops)
        ]
        for pop in populations:
            cvrry.population_set_temperature(pop, temperature)

        cvrry.move_strategy_destroy(moveset)
        cvrry.rna_coord_set_destroy(alphabet)
        cvrry.indexed_chainset_destroy(chainset)
        cvrry.match_energy_func_destroy(e_func)
        cvrry.double_index_destroy(pairs_double_index)

        return populations

    def test_population_state_io(self):

        pop1, pop2, pop3 = self.make_populations(3)

        init_measures = get_measures(pop1)

        # make a few steps
        for _ in range(100):
            if(cvrry.population_step(pop1)):
                raise RuntimeError("Error during optimization")

        #  PART 1: transferring the state with numpy arrays
        measures_first_steps = get_measures(pop1)
        self.assert_dicts_not_equal(init_measures, measures_first_steps)

        init_measures_2 = get_measures(pop2)
        self.assert_dicts_equal(init_measures, init_measures_2)

        state_pop1 = cvrry.population_get_states_np(pop1)

        cvrry.population_set_states_np(pop2, state_pop1)
        state_pop2 = cvrry.population_get_states_np(pop2)

        self.assert_equal_array(state_pop1, state_pop2)

        measures_transferred_state_pop2 = get_measures(pop2)
        self.assert_dicts_equal(measures_first_steps,
                                measures_transferred_state_pop2)

        # PART 2: direct state transfer
        init_measures_3 = get_measures(pop3)
        self.assert_dicts_equal(init_measures, init_measures_3)
        cvrry.population_transfer_state(pop3, pop1)

        measures_transferred_state_pop3 = get_measures(pop3)
        self.assert_dicts_equal(measures_first_steps,
                                measures_transferred_state_pop3)

        # free stuff
        cvrry.population_destroy(pop1)
        cvrry.population_destroy(pop2)
        cvrry.population_destroy(pop3)


if __name__ == '__main__':
    unittest.main()
