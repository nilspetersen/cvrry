import cvrry
import numpy as np
from matplotlib import pyplot as plt


def main():

    _, axes = plt.subplots(2, 1, sharex=True, sharey=True)

    means = np.loadtxt('data/rmsd_means.txt', dtype=np.float32)
    sigmas = np.loadtxt('data/rmsd_sigmas.txt', dtype=np.float32)

    rmsds = np.linspace(0, 50, 500)

    # e_func_linear = cvrry.make_z_score_energy_function(means,
    #                                                    sigmas,
    #                                                    2.0, -1)
    # scores_linear = [
    #     cvrry.get_match_energy(e_func_linear, matchlen, rmsd)
    #     for rmsd in rmsds
    # ]

    colors = ["r", "g", "b", "y", "k"]
    matchlengths = [1, 5, 10, 50, 93]

    e_func_logistic = cvrry.make_z_logistic_energy_function(means,
                                                            sigmas,
                                                            2.0, 7.0)

    for color, matchlen in zip(colors, matchlengths):
        scores_logistic = [
            cvrry.get_match_energy(e_func_logistic, matchlen, rmsd)
            for rmsd in rmsds
        ]

        axes[0].plot(rmsds, scores_logistic, color=color)

    e_func_logistic = cvrry.make_mean_logistic_energy_function(means,
                                                               0.15, 15.0)

    for color, matchlen in zip(colors, matchlengths):
        scores_logistic = [
            cvrry.get_match_energy(e_func_logistic, matchlen, rmsd)
            for rmsd in rmsds
        ]

        axes[1].plot(rmsds, scores_logistic, color=color)
    plt.show()


if __name__ == "__main__":
    main()
