import cvrry
import os
import unittest


class TestMatch(unittest.TestCase):

    def setUp(self):
        self.datadir = "data"
        self.fn_cif_1 = os.path.join(self.datadir, "1TRA.cif")
        self.fn_cif_2 = os.path.join(self.datadir, "1TRA_cutout15to18.cif")
        self.fn_cif_3 = os.path.join(
            self.datadir, "1TRA_cutout15to18and22to24.cif"
        )
        self.fn_alphabet = os.path.join(self.datadir, "testalphabet.alphabet")

    def tearDown(self):
        pass

    def sequences_with_cutout(self,
                              fn_cif_1,
                              fn_cif_2,
                              coordsize_1,
                              coordsize_2,
                              all_pieces_longer_than_fraglen,
                              add_tail=True):

        cif_coord_1 = cvrry.rna_cif_coord_read_cif(
            fn_cif_1, "A", 1
        )
        cif_coord_2 = cvrry.rna_cif_coord_read_cif(
            fn_cif_2, "A", 1
        )

        self.assertEqual(
            cvrry.rna_coord_get_size(cif_coord_1.rna_backbone), coordsize_1
        )
        self.assertEqual(
            cvrry.rna_coord_get_size(cif_coord_2.rna_backbone), coordsize_2
        )

        alphabet = cvrry.rna_coord_alphabet_read_binary_file(self.fn_alphabet)

        yesno_tailoption = cvrry.YES if add_tail else cvrry.NO
        seq_fullseq_1 = cvrry.rna_coord_seq_fullseq_withoption(
            cif_coord_1.rna_backbone, alphabet, yesno_tailoption
        )
        seq_fullseq_2 = cvrry.rna_coord_seq_fullseq_withoption(
            cif_coord_2.rna_backbone, alphabet, yesno_tailoption
        )

        # fraglen = cvrry.rna_coord_alphabet_get_fraglen(alphabet)
        expected_seqlen = 76 if add_tail else 70
        
        self.assertEqual(
            cvrry.rna_coord_seq_size(seq_fullseq_1), expected_seqlen
        )
        self.assertEqual(
            cvrry.rna_coord_seq_size(seq_fullseq_2), expected_seqlen
        )

        seqlen_1 = cvrry.rna_coord_seq_size(seq_fullseq_1)
        seqlen_2 = cvrry.rna_coord_seq_size(seq_fullseq_2)
        score_mat = cvrry.score_mat_new(seqlen_1, seqlen_2)

        if add_tail:
            cvrry.fill_matrix_alphabet_unit_score(
                score_mat, seq_fullseq_1, seq_fullseq_2
            )
        else:
            cvrry.fill_matrix_single_letter_unit_score(
                score_mat, seq_fullseq_1, seq_fullseq_2
            )

        go = 1000
        gw = 1000
        pair_set_initial = cvrry.score_mat_sum_minimal(
            score_mat, go, gw, cvrry.N_AND_W
        )

        # cvrry.dump_score_mat(score_mat)

        self.assertEqual(
            cvrry.rna_coord_seq_size(seq_fullseq_2),
            cvrry.pair_set_get_netto_alignmentlength(pair_set_initial)
        )

        pair_set_backmapped = cvrry.rna_coord_seq_full_backmap_pair_set(
            seq_fullseq_1, seq_fullseq_2, pair_set_initial
        )

        if all_pieces_longer_than_fraglen and add_tail:
            self.assertEqual(
                cvrry.rna_coord_get_size(cif_coord_2.rna_backbone),
                cvrry.pair_set_get_netto_alignmentlength(pair_set_backmapped)
            )
        # cvrry.pair_set_dump(pair_set_backmapped)
        rmsd = cvrry.rna_alignment_simple_rmsd(
            pair_set_backmapped,
            cif_coord_1.rna_backbone,
            cif_coord_2.rna_backbone
        )
        self.assertAlmostEqual(0.0, rmsd, 4)

        fmde_thresh = 2.0
        fdme = cvrry.dme_thresh_rna(
            pair_set_backmapped,
            cif_coord_1.rna_backbone,
            cif_coord_2.rna_backbone,
            fmde_thresh
        )
        self.assertAlmostEqual(1.0, fdme)

        if all_pieces_longer_than_fraglen and add_tail:
            fdme_cov = cvrry.coverage_fracdme_rna(
                pair_set_backmapped,
                cif_coord_1.rna_backbone,
                cif_coord_2.rna_backbone,
                fmde_thresh
            )
            self.assertAlmostEqual(1.0, fdme_cov)
        elif not add_tail:
            fdme_cov = cvrry.coverage_fracdme_rna_seq(
                pair_set_backmapped,
                seq_fullseq_1,
                seq_fullseq_2,
                cif_coord_1.rna_backbone,
                cif_coord_2.rna_backbone,
                fmde_thresh
            )
            self.assertAlmostEqual(1.0, fdme_cov)

        cvrry.rna_cif_coord_destroy(cif_coord_1)
        cvrry.rna_cif_coord_destroy(cif_coord_2)
        cvrry.rna_coord_alphabet_destroy(alphabet)
        cvrry.rna_coord_seq_destroy(seq_fullseq_1)
        cvrry.rna_coord_seq_destroy(seq_fullseq_2)
        cvrry.pair_set_destroy(pair_set_initial)
        cvrry.pair_set_destroy(pair_set_backmapped)
        cvrry.score_mat_destroy(score_mat)

    def test_sequences_with_cutout_1(self):
        self.sequences_with_cutout(self.fn_cif_1, self.fn_cif_2, 76, 72, True)

    def test_sequences_with_cutout_2(self):
        self.sequences_with_cutout(self.fn_cif_1, self.fn_cif_3, 76, 69, False)

    def test_sequences_with_cutout_3(self):
        self.sequences_with_cutout(self.fn_cif_2, self.fn_cif_3, 72, 69, False)

    def test_sequences_with_cutout_1_notail(self):
        self.sequences_with_cutout(self.fn_cif_1, self.fn_cif_2, 76, 72, True, False)

    def test_sequences_with_cutout_2_notail(self):
        self.sequences_with_cutout(self.fn_cif_1, self.fn_cif_3, 76, 69, False, False)

    def test_sequences_with_cutout_3_notail(self):
        self.sequences_with_cutout(self.fn_cif_2, self.fn_cif_3, 72, 69, False, False)


if __name__ == '__main__':
    unittest.main()
