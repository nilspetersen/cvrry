import unittest
import shutil
import os
import numpy as np
import cvrry


# TODO: test for a group (rna_coord_set) ?


class TestRnaCoord(unittest.TestCase):

    def setUp(self):

        self.tmpdir = os.path.join('tmp_test_rnacoord')
        self.copy_count = 0
        self.datadir = 'data'
        self.cases = [
            ('1TRA', 'A', 1),
            ('1Y0Q', 'A', 1)
        ]
        os.makedirs(self.tmpdir)

    def get_tmp_fn(self, ext='.bin'):

        self.copy_count += 1
        return os.path.join(self.tmpdir, "file_" + str(self.copy_count) + ext)

    def tearDown(self):

        shutil.rmtree(self.tmpdir)

    def assert_equal_coord(self, rna1, rna2, yesno=cvrry.YES):

        self.assertEqual(cvrry.rna_coord_same(rna1, rna2), yesno)

    def copy_save_load_compare(self, rna_coord):

        rna_coord_copy = cvrry.rna_coord_copy(rna_coord)

        self.assert_equal_coord(rna_coord, rna_coord_copy)

        fn_inout = self.get_tmp_fn()
        cvrry.rna_coord_write_binary_file(fn_inout, rna_coord_copy)
        rna_coord_read = cvrry.rna_coord_read_binary_file(fn_inout)

        self.assert_equal_coord(rna_coord, rna_coord_copy)

        cvrry.rna_coord_destroy(rna_coord_copy)
        return rna_coord_read

    def one_case(self, pdbid, chain_id, model_i):

        frag_coord = cvrry.read_n_compute_rna_frag_coord(
            os.path.join(self.datadir, pdbid.upper() + '.cif'),
            chain_id, model_i
        )

        rna_coord_copy = self.copy_save_load_compare(frag_coord.rna_backbone)

        bp_info = np.random.randint(
            -1, 1, rna_coord_copy.size
        ).astype(dtype=np.int32)

        cvrry.rna_coord_set_basepair_info(rna_coord_copy, bp_info)

        self.assert_equal_coord(
            frag_coord.rna_backbone, rna_coord_copy, cvrry.NO
        )

        rna_coord_copy2 = self.copy_save_load_compare(rna_coord_copy)

        cvrry.rna_coord_destroy(rna_coord_copy)
        cvrry.rna_coord_destroy(rna_coord_copy2)

    def test_copy_save_load(self):

        for case in self.cases:
            self.one_case(*case)

    def test_full_sequence(self):

        pdbid = "1TRA"
        chain_id = "A"
        model_i = 1

        nucleotide_ids_given = [
            "G", "C", "G", "G", "A", "U", "U", "U", "A", "2MG", "C", "U",
            "C", "A", "G", "H2U", "H2U", "G", "G", "G", "A", "G", "A", "G",
            "C", "M2G", "C", "C", "A", "G", "A", "OMC", "U", "OMG", "A",
            "A", "YG", "A", "PSU", "5MC", "U", "G", "G", "A", "G", "7MG",
            "U", "C", "5MC", "U", "G", "U", "G", "5MU", "PSU", "C", "G",
            "1MA", "U", "C", "C", "A", "C", "A", "G", "A", "A", "U", "U",
            "C", "G", "C", "A", "C", "C", "A"
        ]

        rna_frag_coord = cvrry.read_n_compute_rna_frag_coord(
            os.path.join(self.datadir, pdbid.upper() + '.cif'),
            chain_id, model_i
        )

        nucleotide_ids_coord = cvrry.rna_coord_get_full_sequence(
            rna_frag_coord.rna_backbone
        )

        for nt_coord, nt_given in zip(nucleotide_ids_given,
                                      nucleotide_ids_coord):
            self.assertEqual(nt_coord, nt_given)


if __name__ == '__main__':
    unittest.main()
