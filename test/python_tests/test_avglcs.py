import unittest
import cvrry


def gauss_sum(size):
    return size * (size + 1) / 2


class TestAvgLcs(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def compare_same(self, seq):
        seqlen = cvrry.rna_coord_seq_size(seq)
        lcs_sums = cvrry.rna_coord_seq_lcs_sums(seq, seq)
        expected = gauss_sum(seqlen)
        self.assertEqual(expected, lcs_sums.first)
        self.assertEqual(expected, lcs_sums.second)

    def compare_subchains(self, seq_smaller, seq_larger):
        lcs_sums = cvrry.rna_coord_seq_lcs_sums(seq_smaller, seq_larger)
        seqlen_smaller = cvrry.rna_coord_seq_size(seq_smaller)
        expected = gauss_sum(seqlen_smaller)
        self.assertEqual(expected, lcs_sums.first)

    def test_avglcs(self):

        fn_cif = 'data/1TRA.cif'
        fn_alphabet = 'data/testalphabet.alphabet'
        chainid = 'A'
        model_nr = 1
        alphabet = cvrry.rna_coord_alphabet_read_binary_file(fn_alphabet)

        subchain_len = 50

        cif_coord = cvrry.rna_cif_coord_read_cif(fn_cif, chainid, model_nr)
        chain1 = cif_coord.rna_backbone
        subchain = cvrry.rna_coord_copy_piece(
            chain1, 10, subchain_len
        )

        chain_1_seq = cvrry.rna_coord_seq_w_break_chars(chain1, alphabet)
        subchain_seq = cvrry.rna_coord_seq_w_break_chars(subchain, alphabet)

        self.compare_same(chain_1_seq)
        self.compare_same(subchain_seq)
        self.compare_subchains(subchain_seq, chain_1_seq)

        cvrry.rna_coord_alphabet_destroy(alphabet)
        cvrry.rna_cif_coord_destroy(cif_coord)
        cvrry.rna_coord_destroy(subchain)
        cvrry.rna_coord_seq_destroy(chain_1_seq)
        cvrry.rna_coord_seq_destroy(subchain_seq)


if __name__ == '__main__':
    unittest.main()
